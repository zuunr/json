package com.zuunr.json.jsonpath;

import com.jayway.jsonpath.*;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonValue;
import com.zuunr.json.JsonValueFactory;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class JsonPathTest {

    Configuration config = Configuration.builder().jsonProvider(new ZuunrJsonProvider()).build();

    @Test
    void test0() {

        String json = "{\n" +
                "    \"book\": \n" +
                "    [\n" +
                "        {\n" +
                "            \"title\": \"Beginning JSON\",\n" +
                "            \"author\": \"Ben Smith\",\n" +
                "            \"price\": 49.99\n" +
                "        },\n" +
                "\n" +
                "        {\n" +
                "            \"title\": \"JSON at Work\",\n" +
                "            \"author\": \"Tom Marrs\",\n" +
                "            \"price\": 29.99\n" +
                "        },\n" +
                "\n" +
                "        {\n" +
                "            \"title\": \"Learn JSON in a DAY\",\n" +
                "            \"author\": \"Acodemy\",\n" +
                "            \"price\": 8.99\n" +
                "        },\n" +
                "\n" +
                "        {\n" +
                "            \"title\": \"JSON: Questions and Answers\",\n" +
                "            \"author\": \"George Duckett\",\n" +
                "            \"price\": 6.00\n" +
                "        }\n" +
                "    ],\n" +
                "\n" +
                "    \"priceRange\": \n" +
                "    {\n" +
                "        \"exactMatch\": 8.99,\n" +
                "        \"cheap\": 10.00,\n" +
                "        \"medium\": 20.00\n" +
                "}}";

        JsonValue document = JsonValueFactory.create(json);

        Filter expensiveFilter = Filter.filter(Criteria.where("price").gt(20.00));

        DocumentContext documentContext = JsonPath.parse(document, config);
        //DocumentContext documentContext = JsonPath.parse(jsonValue.asMapsAndLists());
        JsonValue result = JsonArray.of(((ArrayList) documentContext.read("$['book'][?]", expensiveFilter)).toArray()).jsonValue();

        JsonValue expected = JsonValueFactory.create("[{\"author\":\"Ben Smith\",\"title\":\"Beginning JSON\",\"price\":49.99}, {\"author\":\"Tom Marrs\",\"title\":\"JSON at Work\",\"price\":29.99}]");

        assertThat(result, is(expected));


        assertThat(readJsonPath("$['book'][?(@.price > $['priceRange']['medium'])]", document), is(expected));
        assertThat(readJsonPath("$['book'][?(@['price'] == $['priceRange']['exactMatch'])]", document).get(0).get("author").getString(), is("Acodemy"));
    }

    private JsonValue readJsonPath(String jsonPath, JsonValue document) {
        DocumentContext documentContext = JsonPath.parse(document, config);
        JsonValue result = JsonArray.of(((ArrayList) documentContext.read(jsonPath)).toArray()).jsonValue();
        return result;
    }
}
