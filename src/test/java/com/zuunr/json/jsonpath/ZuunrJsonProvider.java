package com.zuunr.json.jsonpath;

import com.jayway.jsonpath.InvalidJsonException;
import com.jayway.jsonpath.spi.json.JsonProvider;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.JsonValueFactory;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class ZuunrJsonProvider implements JsonProvider {

    @Override
    public Object parse(String s) throws InvalidJsonException {
        return JsonValueFactory.create(s);
    }

    @Override
    public Object parse(InputStream inputStream, String s) throws InvalidJsonException {
        return JsonValueFactory.create(inputStream); // TODO: Make sure to handle charset!!!
    }

    @Override
    public String toJson(Object o) {
        return JsonValue.of(o).asJson();
    }

    @Override
    public Object createArray() {
        return new ArrayList();
    }

    @Override
    public Object createMap() {
        return new HashMap<>();
    }

    @Override
    public boolean isArray(Object o) {
        return o.getClass() == ArrayList.class || o.getClass() == JsonValue.class && ((JsonValue) o).isJsonArray();
    }

    @Override
    public int length(Object o) {
        if (o.getClass() == ArrayList.class) {
            return ((ArrayList) o).size();
        }

        if (o.getClass() == JsonValue.class) {
            return ((JsonValue) o).getJsonArray().size();
        }

        throw new RuntimeException("There is no length for " + o.getClass());
    }

    @Override
    public Iterable<?> toIterable(Object o) {
        if (o.getClass() == JsonValue.class) {
            return ((JsonValue) o).getJsonArray();
        }

        if (o.getClass() == ArrayList.class) {
            return (ArrayList) o;
        }

        throw new RuntimeException("Object is not iterable!");
    }

    @Override
    public Collection<String> getPropertyKeys(Object o) {


        if (JsonValue.class == o.getClass()) {
            return ((JsonValue)o).getJsonObject().keySet();
        }

        if (o.getClass() == JsonObject.class) {
            return ((JsonObject) o).keySet();
        }

        if (o.getClass() == HashMap.class) {
            return ((HashMap) o).keySet();
        }

        throw new RuntimeException("There are no Property keys");
    }

    @Override
    public Object getArrayIndex(Object o, int i) {
        return getArrayIndex(o, i, true);
    }

    @Override
    public Object getArrayIndex(Object o, int i, boolean unwrap) {
        if (o.getClass() == JsonValue.class) {
            Object val = ((JsonValue) o).getJsonArray().get(i);
            return unwrap ? unwrap(val) : val;
        }

        if (o.getClass() == ArrayList.class) {
            Object val = ((ArrayList) o).get(i);
            return unwrap ? unwrap(val) : val;
        }

        throw new RuntimeException("Cant get array index");
    }

    @Override
    public void setArrayIndex(Object array, int i, Object item) {

        if (array.getClass() == ArrayList.class) {

            ArrayList arrayList = (ArrayList) array;
            if (arrayList.size() == i) {
                arrayList.add(item);
            } else {
                arrayList.set(i, item);
            }
        } else {
            throw new RuntimeException("Cannot set element");
        }
    }

    @Override
    public Object getMapValue(Object o, String key) {

        JsonValue result;
        if (o.getClass() == JsonValue.class) {
            result = ((JsonValue) o).get(key);
        } else if (o.getClass() == JsonObject.class) {
            result = ((JsonObject) o).get(key);
        } else if (o.getClass() == HashMap.class) {
            result = (JsonValue) ((HashMap) o).get(key);
        } else {
            throw new RuntimeException("Cannot get map value from" + o);
        }
        return result == null ? UNDEFINED : result;
    }

    @Override
    public void setProperty(Object o, Object key, Object value) {
        if (o.getClass() == HashMap.class) {
            ((HashMap) o).put(key.toString(), JsonValue.of(value));
        } else {
            throw new RuntimeException("Immutable object: " + o);
        }
    }

    @Override
    public void removeProperty(Object o, Object key) {
        if (o.getClass() == HashMap.class) {
            ((HashMap) o).remove(key.toString());
        } else {
            throw new RuntimeException("Immutable object: " + o);
        }
    }

    @Override
    public boolean isMap(Object o) {
        return JsonObject.class == o.getClass() || o.getClass() == JsonValue.class && ((JsonValue) o).isJsonObject() || o.getClass() == HashMap.class;
    }

    @Override
    public Object unwrap(Object o) {
        return o.getClass() == JsonValue.class
                ? ((JsonValue) o).getValue()
                : o;
    }

    @Override
    public Object parse(byte[] json) throws InvalidJsonException {
        throw new UnsupportedOperationException();
    }
}
