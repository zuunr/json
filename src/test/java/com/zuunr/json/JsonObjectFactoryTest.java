/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/*
 * Created by niklas on 18/12/15.
 */
class JsonObjectFactoryTest {

    String json = "{\n" +
            "\t\"glossary\": {\n" +
            "\t\t\"title\": \"example glossary\",\n" +
            "\t\t\"GlossDiv\": {\n" +
            "\t\t\t\"title\": \"S\",\n" +
            "\t\t\t\"GlossList\": {\n" +
            "\t\t\t\t\"GlossEntry\": {\n" +
            "\t\t\t\t\t\"ID\": \"SGML\",\n" +
            "\t\t\t\t\t\"SortAs\": \"SGML\",\n" +
            "\t\t\t\t\t\"GlossTerm\": \"Standard Generalized Markup Language\",\n" +
            "\t\t\t\t\t\"Acronym\": \"SGML\",\n" +
            "\t\t\t\t\t\"Abbrev\": \"ISO 8879:1986\",\n" +
            "\t\t\t\t\t\"GlossDef\": {\n" +
            "\t\t\t\t\t\t\"para\": \"A meta-markup language, used to create markup languages such as DocBook.\",\n" +
            "\t\t\t\t\t\t\"GlossSeeAlso\": [\"GML\", \"XML\"]\n" +
            "\t\t\t\t\t},\n" +
            "\t\t\t\t\t\"GlossSee\": \"markup\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t}\n" +
            "\t\t}\n" +
            "\t}\n" +
            "}";

    String objectsInObjects = "{\"a\":{\"c\":{}},\"b\":{\"d\":{}}}";

    String arrayInObject = "{\"a\":{\"c\":{}},\"b\":[\"d\",\"e\"]}";

    String arrayInObjectWithPrimitives = "{\"a\":{\"c\":{}},\"b\":[\"d\",\"e\"],\"bb\":[true,false],\"bbb\":[1.2,5]}";

    String superSimple = "{\"a\":\"b\",\"c\":false,\"d\":1,\"e\":5998.87,\"f\":[\"d\",\"e\"]}";

    Map<?, ?> nestedObjectsTestMap;


    @BeforeEach
    public void init() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS);
        nestedObjectsTestMap = mapper.readValue(objectsInObjects, Map.class);
    }

    @Test
    void objectsInObjects_createJsonObjectTest(){
        JsonObjectFactory factory = new JsonObjectFactory();
        JsonObject jsonObject = factory.createJsonObject(nestedObjectsTestMap);
        assertThat(jsonObject
                .get("a").getValue(JsonObject.class)
                .get("c").getValue(JsonObject.class), is(notNullValue()));

    }

    @Test
    void arraysInObjects_createJsonObjectTest() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Map<?, ?> testMap = mapper.readValue(arrayInObject, Map.class);
        JsonObjectFactory factory = new JsonObjectFactory();
        JsonObject jsonObject = factory.createJsonObject(testMap);
        assertThat(jsonObject
                .get("b").getValue(JsonArray.class).get(0).getValue(String.class), is("d"));

    }

    @Test
    void arraysInObjectsWithPrimitives_createJsonObjectTest() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS);
        Map<?, ?> testMap = mapper.readValue(arrayInObjectWithPrimitives, Map.class);
        JsonObjectFactory factory = new JsonObjectFactory();
        JsonObject jsonObject = factory.createJsonObject(testMap);
        assertThat(jsonObject
                .get("bb").getValue(JsonArray.class).get(1).getValue(Boolean.class), is(false));

    }

    @Test
    void superSimple_createJsonObjectTest() throws Exception {
        JsonObjectFactory factory = new JsonObjectFactory();
        JsonObject jsonObject = factory.createJsonObject(superSimple);
        assertThat(jsonObject
                .get("a").getValue(String.class), is("b"));
        assertThat(jsonObject
                .get("c").getValue(Boolean.class), is(false));
        assertThat(jsonObject
                .get("d").asInteger(), is(1));
        assertThat(jsonObject
                .get("e").getJsonNumber().getBigDecimal(), is(new BigDecimal("5998.87")));
        assertThat(jsonObject
                .get("f").getValue(JsonArray.class).get(1).getValue(String.class), is("e"));

    }

    @Test
    void test() {
        PersonPojo myPojo = new PersonPojo("Peter", PersonPojo.Gender.MALE);

        JsonObject jsonObject = new JsonObjectFactory().createJsonObject(myPojo);

        assertThat(jsonObject.get("name").as(String.class), is("Peter"));
        assertThat(jsonObject.get("gender").as(String.class), is("MALE"));
    }
    
    @Test
    void givenDecimalValuesShouldMapThemCorrectly() {
        JsonObjectFactory factory = new JsonObjectFactory();
        String json = "{\"a\":9.0,\"b\":9.05,\"c\":9.00,\"d\":150.0,\"e\":150.05,\"f\":150.00,\"g\":0.0}";
        JsonObject jsonObject = factory.createJsonObject(json);
        
        assertEquals(new BigDecimal("9.0"), jsonObject.get("a").getJsonNumber().getBigDecimal());
        assertEquals(new BigDecimal("9.05"), jsonObject.get("b").getJsonNumber().getBigDecimal());
        assertEquals(new BigDecimal("9.00"), jsonObject.get("c").getJsonNumber().getBigDecimal());
        assertEquals(new BigDecimal("150.0"), jsonObject.get("d").getJsonNumber().getBigDecimal());
        assertEquals(new BigDecimal("150.05"), jsonObject.get("e").getJsonNumber().getBigDecimal());
        assertEquals(new BigDecimal("150.00"), jsonObject.get("f").getJsonNumber().getBigDecimal());
        assertEquals(new BigDecimal("0.0"), jsonObject.get("g").getJsonNumber().getBigDecimal());
    }

    public static class PersonPojo {

        private String name;
        private Gender gender;

        public PersonPojo(String name, Gender gender) {
            this.name = name;
            this.gender = gender;
        }

        public String getName() {
            return name;
        }

        public Gender getGender() {
            return gender;
        }

        public static enum Gender {
            MALE, FEMALE
        }
    }
}
