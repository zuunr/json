/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.jupiter.api.Test;

class JsonArrayCollectorTest {

    @Test
    void test() {
        JsonArray jsonArray = JsonArray.of("Hello", "World");
        JsonArray collected = jsonArray.stream().map(jsonValue -> JsonValue.of(jsonValue.getValue(String.class).toUpperCase())).collect(new JsonArrayCollector());
        String hello = collected.get(0).getValue(String.class);
        String world = collected.get(1).getValue(String.class);
        assertThat(hello, is("HELLO"));
        assertThat(world, is("WORLD"));
    }
}
