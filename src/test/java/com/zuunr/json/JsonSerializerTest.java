/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

/*
 * Created by niklas on 2017-09-06.
 */
class JsonSerializerTest {

    private final JsonObjectFactory jsonObjectFactory = new JsonObjectFactory();

    @Test
    void serializeTest() {
        JsonSerializer jsonSerializer = new JsonSerializer();

        JsonObject toBeSerialized1 = JsonObject.EMPTY
                .put("\"key1", "value2")
                .put("key2", "\uaaaa<script type=\"text/javascript\">(function(){var ERRORS_LOGGED=false;window.onerror=function(msg,file){if(!ERRORS_LOGGED&&msg!==\"NOTAREALERROR\"&&file&&file.match(/meetup|facebook/gi)&&!file.match(/_crweb\\.js$/gi)){ERRORS_LOGGED=true;var err_file=file.split(\"/\").pop().replace(/\\W/gi,\"_\");if(err_file.length>50){return}if(file.split(\".\").pop()!==\"js\"){err_file=\"in_page\"}var readyState=(typeof(document.readyState)!==\"undefined\")?document.readyState:\"complete\";if(err_file===\"in_page\"&&readyState===\"loading\"){return}var xhr=new XMLHttpRequest();xhr.open(\"GET\",\"/api/?method=logJSError&arg_error=&arg_key=js.error.\"+err_file,true);xhr.send()}}})();</script>")
                .put("key3", JsonArray.EMPTY.add("value3").add("value4"))
                .put("key4", 4)
                .put("key5", false)
                .put("key5", JsonValue.NULL);

        String me = jsonSerializer.serialize(toBeSerialized1.asMapsAndLists());

        JsonObject toBeSerialized2 = toBeSerialized1.put("me", me);
        String json = jsonSerializer.serialize(toBeSerialized2.asMapsAndLists());

        JsonObject recreated = jsonObjectFactory.createJsonObject(json);
        assertThat(recreated, is(toBeSerialized2));

        assertThat(recreated.get("me").getValue(String.class), is(me));
    }

    @Test
    void integerValueTest() {
        JsonSerializer jsonSerializer = new JsonSerializer();

        JsonObject toBeSerialized1 = JsonObject.EMPTY.put("key4", 4);

        String me = jsonSerializer.serialize(toBeSerialized1.asMapsAndLists());

        JsonObject toBeSerialized2 = toBeSerialized1.put("me", me);
        String json = jsonSerializer.serialize(toBeSerialized2.asMapsAndLists());

        JsonObject recreated = jsonObjectFactory.createJsonObject(json);
        assertThat(recreated, is(toBeSerialized2));

        assertThat(recreated.get("me").getValue(String.class), is(me));
    }

    @Test
    void allTypesTest() {
        JsonSerializer jsonSerializer = new JsonSerializer();

        JsonObject toBeSerialized1 = JsonObject.EMPTY
                .put("integer", 4)
                .put("boolean", true)
                .put("decimal", new BigDecimal("8716286.98719823719873"))
                .put("string", "abc")
                .put("short", (short) 1)
                .put("long", 5L)
                ;

        String me = jsonSerializer.serialize(toBeSerialized1.asMapsAndLists());

        JsonObject toBeSerialized2 = toBeSerialized1.put("me", me);
        String json = jsonSerializer.serialize(toBeSerialized2.asMapsAndLists());

        JsonObject recreated = jsonObjectFactory.createJsonObject(json);
        assertThat(recreated, is(toBeSerialized2));

        assertThat(recreated.get("me").getValue(String.class), is(me));
    }
}
