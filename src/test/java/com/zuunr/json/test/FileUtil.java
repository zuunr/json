package com.zuunr.json.test;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectFactory;
import com.zuunr.json.JsonValue;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.IOException;

/**
 * @author Niklas Eldberger
 */
public class FileUtil {

    private final JsonObjectFactory jsonObjectFactory = new JsonObjectFactory();

    public JsonObject getJsonObject(String classPathPath) {
        try {
            PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();

            Resource resource = resolver.getResource("classpath:" + classPathPath);
            if (resource.exists() && resource.isFile()) {
                System.out.println("filename:" + resource.getURI());
            }
            return jsonObjectFactory.createJsonObject(resource.getInputStream());
        } catch (IOException ioException) {
            throw new RuntimeException(ioException);
        }
    }

    public JsonValue getJsonValue(String classPathPath) {
        try {
            PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();

            Resource resource = resolver.getResource("classpath:" + classPathPath);
            if (resource.exists() && resource.isFile()) {
                System.out.println("filename:" + resource.getURI());
            }
            return jsonObjectFactory.createJsonValue(resource.getInputStream());
        } catch (IOException ioException) {
            throw new RuntimeException(ioException);
        }
    }


}
