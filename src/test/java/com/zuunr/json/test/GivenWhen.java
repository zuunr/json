package com.zuunr.json.test;

import com.zuunr.json.JsonValue;

public interface GivenWhen {

         JsonValue doGivenWhen(JsonValue given, JsonValue when);
}
