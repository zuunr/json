package com.zuunr.json.test;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.JsonValueFactory;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GivenWhenThenRunner {


    private static final Logger logger = LoggerFactory.getLogger(GivenWhenThenRunner.class);
    private static FileUtil fileUtil = new FileUtil();

    @Test
    public void runTests() {
        try {
            PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();

            Resource[] resources = resolver.getResources("classpath:" + "**/*.test.json");

            Optional<JsonArray> errors = Arrays.stream(resources).map(
                    resource -> testResource(resource)).reduce(
                    (errors1, errors2) -> errors1.addAll(errors2)
            );

            JsonArray errorsJsonArray = errors.orElse(JsonArray.EMPTY);

            assertThat("Error in " + (errorsJsonArray.isEmpty() ? "" : errorsJsonArray.head().getJsonObject().get("uri").getString()), errorsJsonArray, is(JsonArray.EMPTY));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private JsonArray testResource(Resource resource) {

        JsonArray testResult;
        String uri = "";
        try {
            uri = resource.getURI().toString();
            JsonObject testCase = JsonValueFactory.create(resource.getInputStream()).getJsonObject();

            testResult = runTestCase(testCase);

            if (testResult.isEmpty()) {
                System.out.println("Test " + uri + " ok");
                return testResult;
            }
        } catch (Exception e) {
            e.printStackTrace();
            testResult = JsonArray.of(JsonObject.EMPTY.put("exception", e.toString()));
        }
        System.out.println("Test " + uri + " failed!");
        return JsonArray.of(testResult.get(0).getJsonObject().put("uri", uri));
    }

    private JsonArray runTestCase(JsonObject testCase) {

        try {
            GivenWhen givenWhen = (GivenWhen) Class.forName(testCase.get("testRunner").getString()).getDeclaredConstructor().newInstance();

            JsonValue given = testCase.get("given");
            JsonValue when = testCase.get("when");
            JsonValue then = testCase.get("then");
            JsonValue result = givenWhen.doGivenWhen(given, when);
            return assertResult(result, then);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public JsonArray
    assertResult(JsonValue result, JsonValue then) {
        return then.equals(result)
                ? JsonArray.EMPTY
                : JsonArray.of(
                JsonObject.EMPTY
                        .put("expected", then)
                        .put("actual", result));
    }

}
