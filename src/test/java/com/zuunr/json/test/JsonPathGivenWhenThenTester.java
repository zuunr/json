package com.zuunr.json.test;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.jsonpath.ZuunrJsonProvider;

import java.util.ArrayList;


public class JsonPathGivenWhenThenTester implements GivenWhen {


    public JsonValue doGivenWhen(JsonValue given, JsonValue when) {

        Configuration config = Configuration.builder().jsonProvider(new ZuunrJsonProvider()).build();

        String jsonPath = when.getString();

        DocumentContext documentContext = JsonPath.parse(given, config);
        Object object = documentContext.read(jsonPath);
        return jsonValueResult(object);
    }

    private JsonValue jsonValueResult(Object object) {

        if (JsonValue.class == object.getClass()) {
            return (JsonValue) object;
        }

        if (JsonObject.class == object.getClass()) {
            return ((JsonObject) object).jsonValue();
        }

        if (ArrayList.class == object.getClass()) {
            return JsonArray.of(((ArrayList<Object>) object).toArray()).jsonValue();
        }

        return JsonValue.of(object);
    }
}