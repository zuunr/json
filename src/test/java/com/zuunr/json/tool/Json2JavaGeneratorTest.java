/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json.tool;

import java.io.File;
import java.util.UUID;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;

class Json2JavaGeneratorTest {

    @Test
    void generateModels() {

        Json2JavaGenerator generator = new Json2JavaGenerator();

        JsonObject testModel = JsonObject.EMPTY
                .put("config", JsonObject.EMPTY
                        .put("processing", JsonObject.EMPTY
                                .put("decoration", JsonObject.EMPTY
                                        .put("decorators", JsonArray.EMPTY
                                                .add(JsonObject.EMPTY
                                                        .put("collectionName", "")
                                                        .put("decorator", "")
                                                        .put("expressions", JsonArray.EMPTY
                                                                .add(JsonObject.EMPTY
                                                                        .put("target", "")
                                                                        .put("expression", "")
                                                                        .put("resource", JsonObject.EMPTY
                                                                                .put("resourceRetriever", "")
                                                                                .put("url", "")
                                                                                .put("queryExpression", "")

                                                                        )

                                                                ))

                                                ))))
                );


        JsonObject models = generator.generateModels("com.zuunr.restbed.core.servicescheme", "Scheme", testModel);

        JsonObject renderedModels = generator.renderModels(models);

        File file = null;
        try {
            file = generator.makeZip(UUID.randomUUID().toString() + ".zip", renderedModels);
        } finally {
            file.delete();
        }
    }

    //@Test
    void generateFormModels() {

        Json2JavaGenerator generator = new Json2JavaGenerator();

        JsonObject testModel = JsonObject.EMPTY
                .put("form", JsonObject.EMPTY
                                .put("exclusive", false)
                                .put("href", "https://zuunr.com/v1/somesystem/api/somecollection/itemid")
                        //.put("value" JsonArray)
                );

        JsonObject models = generator.generateModels("com.zuunr.json.example.contact", "Contact", testModel);

        JsonObject renderedModels = generator.renderModels(models);

        File file = null;
        try {
            file = generator.makeZip(UUID.randomUUID().toString() + ".zip", renderedModels);
        } finally {
            file.delete();
        }
    }

    @Test
    void testRenderings() {
        Json2JavaGenerator generator = new Json2JavaGenerator();

        File file = null;
        try {
            JsonObject models = generator.generateModels("com.zuunr.file", "Renderings", JsonArray.of(JsonObject.EMPTY
                    .put("templates", JsonObject.EMPTY)
                    .put("fragments", JsonObject.EMPTY)));
            JsonObject rendered = generator.renderModels(models);
            file = new Json2JavaGenerator().makeZip(UUID.randomUUID().toString() + ".zip", rendered);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            file.delete();
        }
    }
}
