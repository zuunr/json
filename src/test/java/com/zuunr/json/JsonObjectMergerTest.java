/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import com.zuunr.json.merge.MergeStrategy;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/*
 * Created by niklas on 09/01/16.
 */
class JsonObjectMergerTest {

    @Test
    void mergeSimpleTest() {
        JsonObject original = JsonObject.EMPTY.put("attribute1", JsonValue.of("value1-versionA")).put("attribute2", JsonValue.of("value2-versionA"));
        JsonObject overrrider = JsonObject.EMPTY.put("attribute1", JsonValue.of("value1-versionB")).put("attribute3", JsonValue.of("value3-versionA"));
        JsonObject merged = new JsonObjectMerger().merge(original, overrrider);
        assertThat(merged.get("attribute2").getValue(String.class), is("value2-versionA"));
        assertThat(merged.get("attribute1").getValue(String.class), is("value1-versionB"));
        assertThat(merged.get("attribute3").getValue(String.class), is("value3-versionA"));
    }

    @Test
    void mergeBooleanTest() {
        JsonObject original = JsonObject.EMPTY.put("TODO", true);

        JsonObject overrider = JsonObject.EMPTY.put("TODO", JsonObject.EMPTY.put("TODO", true));

        JsonObject merged = new JsonObjectMerger().merge(original, overrider);
        assertThat(merged, is(overrider));

        merged = new JsonObjectMerger().merge(overrider, original);
        assertThat(merged, is(original));
    }

    @Test
    void mergeComplexTest() {

        JsonObject original = JsonObject.EMPTY
                .put("name", JsonValue.of("The movie"))
                .put("comments", JsonArray.EMPTY
                        .add("I like this")
                        .add("Nope - changed my mind").jsonValue())
                .put("friends", JsonObject.EMPTY
                        .put("kalle",
                                JsonObject.EMPTY
                                        .put("friendshipAge", JsonValue.of(3))
                                        .put("friendshipStatus", JsonValue.of("Very strong")).jsonValue())
                        .put("stina",
                                JsonObject.EMPTY
                                        .put("friendshipAge", JsonValue.of(10))
                                        .put("friendshipStatus", JsonValue.of("Weak"))
                                        .put("more", JsonObject.EMPTY
                                                .put("someMoreStuff", JsonValue.of("some more stuff")).jsonValue())
                                        .jsonValue()).jsonValue());


        JsonObject newJsonObject = JsonObject.EMPTY
                .put("name", JsonValue.of("The movie"))
                .put("comments", JsonArray.EMPTY
                        .add("I like this")
                        .add("Nope - changed my mind").jsonValue())
                .put("friends", JsonObject.EMPTY
                        .put("kalle",
                                JsonObject.EMPTY
                                        .put("friendshipAge", JsonValue.of(3))
                                        .put("friendshipStatus", JsonValue.of("Very strong")).jsonValue())
                        .put("stina",
                                JsonObject.EMPTY
                                        .put("friendshipAge", JsonValue.of(10))
                                        .put("friendshipStatus", JsonValue.of("good"))
                                        .put("other", JsonObject.EMPTY
                                                .put("info", JsonValue.of("some more info")).jsonValue())
                                        .jsonValue()).jsonValue());


        JsonObjectMerger merger = new JsonObjectMerger();

        JsonObject merged = merger.merge(original, newJsonObject);

        String good = merged.get("friends").get("stina").get("friendshipStatus").getValue(String.class);

        assertThat(good, is("good"));
        assertThat(merged.get("friends").get("stina").get("other").get("info").getValue(String.class), is("some more info"));
        assertThat(merged.get("friends").get("stina").get("more").get("someMoreStuff").getValue(String.class), is("some more stuff"));
    }

    @Test
    void noValueVsNullValueTest() {
        JsonObjectMerger merger = new JsonObjectMerger();

        JsonObject original = JsonObject.EMPTY.put("name", "Olle");
        JsonObject overwriter = JsonObject.EMPTY;

        assertThat(merger.merge(original, overwriter), is(original));

        overwriter = JsonObject.EMPTY.put("name", JsonValue.NULL);
        assertThat(merger.merge(original, overwriter), is(overwriter));

    }

    @Test
    void mergeJsonArrays() {

        JsonObject original = JsonObject.EMPTY.put("mergeArray", JsonArray.EMPTY.add(JsonObject.EMPTY.put("original-key", "original-value")));
        JsonObject overrider = JsonObject.EMPTY.put("mergeArray", JsonArray.EMPTY.add(JsonObject.EMPTY.put("overrider-key", "overrider-value")));

        JsonObjectMerger merger = new JsonObjectMerger();
        merger.merge(original, overrider);
        JsonObject result = merger.mergeAll(original, overrider);

        JsonObject expected = JsonObject.EMPTY.put("mergeArray", JsonArray.EMPTY.add(JsonObject.EMPTY.put("original-key", "original-value").put("overrider-key", "overrider-value")));
        assertThat(result, is(expected));
    }

    @Test
    void mergeJsonArraysWithNull() {

        JsonObject original = JsonObject.EMPTY.put("mergeArray", JsonArray.EMPTY.add(JsonValue.NULL));
        JsonObject overrider = JsonObject.EMPTY.put("mergeArray", JsonArray.EMPTY.add(JsonObject.EMPTY.put("overrider-key", "overrider-value")));

        JsonObjectMerger merger = new JsonObjectMerger();
        merger.merge(original, overrider);
        JsonObject result = merger.mergeAll(original, overrider);

        JsonObject expected = JsonObject.EMPTY.put("mergeArray", JsonArray.EMPTY.add(JsonObject.EMPTY.put("overrider-key", "overrider-value")));
        assertThat(result, is(expected));
    }

    @Test
    void mergeJsonArraysWithNull2() {

        JsonObject original = JsonObject.EMPTY
                .put("mergeArray", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("original-key", "original-value"))
                        .add(JsonValue.NULL));

        JsonObject overrider = JsonObject.EMPTY
                .put("mergeArray", JsonArray.EMPTY
                        .add(JsonValue.NULL)
                        .add(JsonObject.EMPTY
                                .put("overrider-key", "overrider-value")));


        JsonObjectMerger merger = new JsonObjectMerger();
        merger.merge(original, overrider);
        JsonObject result = merger.mergeAll(original, overrider);

        JsonObject expected = JsonObject.EMPTY.put("mergeArray", JsonArray.EMPTY.add(JsonValue.NULL).add(JsonObject.EMPTY.put("overrider-key", "overrider-value")));
        assertThat(result, is(expected));
    }

    @Test
    void mergeArrayTest() {
        JsonObjectMerger merger = new JsonObjectMerger();

        JsonObject original = JsonObject.EMPTY.put("string", "stringValue").put("array", JsonArray.EMPTY.add(JsonObject.EMPTY.put("name", "AAAAAAAAAA")).add(JsonObject.EMPTY.put("name", "BBBBBBBBBB"))).put("will", "remain");
        JsonObject patch = JsonObject.EMPTY.put("string", "new Stringvalue").put("array", JsonArray.EMPTY.add(JsonObject.EMPTY.put("name", "CCCCCCCCCCC")));

        JsonObject result = merger.merge2(original, patch, false);
        JsonObject expectedResult = JsonObject.EMPTY.put("string", "new Stringvalue").put("array", JsonArray.EMPTY.add(JsonObject.EMPTY.put("name", "CCCCCCCCCCC"))).put("will", "remain");
        assertThat(result, is(expectedResult));
    }

    @Test
    void mergeArraySameSize() {
        JsonObjectMerger merger = new JsonObjectMerger();

        JsonArray jsonArray1 = JsonArray.of(JsonObject.EMPTY.put("firstName", "Laura"), JsonObject.EMPTY.put("firstName", "Peter"), JsonObject.EMPTY.put("firstName", "Rob"));
        JsonArray jsonArray2 = JsonArray.of(JsonObject.EMPTY.put("lastName", "Larsen"), JsonObject.EMPTY.put("lastName", "Petersson"), JsonObject.EMPTY.put("lastName", "Robson"));

        JsonArray expected = JsonArray.of(JsonObject.EMPTY.put("lastName", "Larsen").put("firstName", "Laura"), JsonObject.EMPTY.put("lastName", "Petersson").put("firstName", "Peter"), JsonObject.EMPTY.put("lastName", "Robson").put("firstName", "Rob"));

        assertThat(merger.mergeJsonArray2(jsonArray1, jsonArray2, true), is(expected));
    }

    @Test
    void mergeArrayOriginalLonger() {
        JsonObjectMerger merger = new JsonObjectMerger();

        JsonArray jsonArray1 = JsonArray.of(JsonObject.EMPTY.put("firstName", "Laura"), JsonObject.EMPTY.put("firstName", "Peter"), JsonObject.EMPTY.put("firstName", "Rob"));
        JsonArray jsonArray2 = JsonArray.of(JsonObject.EMPTY.put("lastName", "Larsen"), JsonObject.EMPTY.put("lastName", "Petersson"));

        JsonArray expected = JsonArray.of(JsonObject.EMPTY.put("lastName", "Larsen").put("firstName", "Laura"), JsonObject.EMPTY.put("lastName", "Petersson").put("firstName", "Peter"), JsonObject.EMPTY.put("firstName", "Rob"));

        assertThat(merger.mergeJsonArray(jsonArray1, jsonArray2, MergeStrategy.NULL_AS_VALUE_AND_ARRAY_BY_ELEMENT), is(expected));
    }

    @Test
    void nullAsDelete() {
        JsonObject original = JsonObject.EMPTY
                .put("a", "b")
                .put("c", "d")
                .put("array", JsonArray.of("A","B"));


        JsonObjectMerger merger = new JsonObjectMerger(MergeStrategy.NULL_AS_DELETE_AND_ARRAY_AS_ATOM);
        JsonObject merged = merger.merge(
                original,
                JsonObject.EMPTY
                        .put("c", JsonValue.NULL)
                        .put("e", "f")
                        .put("array", JsonArray.EMPTY)
        );
        JsonObject expected = JsonObject.EMPTY
                .put("a", "b")
                .put("e", "f")
                .put("array", JsonArray.EMPTY);

        assertThat(merged, is(expected));
    }

    @Test
    void nullAsValue() {
        JsonObject original = JsonObject.EMPTY
                .put("a", "b")
                .put("c", "d")
                .put("array", JsonArray.of("A", "B"));

        JsonObjectMerger merger = new JsonObjectMerger(MergeStrategy.NULL_AS_VALUE_AND_ARRAY_AS_ATOM);
        JsonObject merged = merger.merge(
                original,
                JsonObject.EMPTY
                        .put("c", JsonValue.NULL)
                        .put("e", "f")
                        .put("array", JsonArray.EMPTY)
        );
        JsonObject expected = JsonObject.EMPTY
                .put("a", "b")
                .put("e", "f")
                .put("c", JsonValue.NULL)
                .put("array", JsonArray.EMPTY);
        assertThat(merged, is(expected));
    }
}
