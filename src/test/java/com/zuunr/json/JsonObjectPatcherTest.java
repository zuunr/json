/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
class JsonObjectPatcherTest {

    @Test
    void patchTest() {
        JsonObject original = JsonObject.EMPTY
                .put("will", "remain")
                .put("string", "stringValue")
                .put("array", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("name", "AAAAAAAAAA"))
                        .add(JsonObject.EMPTY
                                .put("name", "BBBBBBBBBB")));
        JsonObject patch = JsonObject.EMPTY
                .put("string", "new Stringvalue")
                .put("array", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("name", "CCCCCCCCCCC")));

        JsonObjectPatcher patcher = new JsonObjectPatcher();
        JsonObject result = patcher.patch(original, patch);
        JsonObject expectedResult = JsonObject.EMPTY
                .put("will", "remain")
                .put("string", "new Stringvalue")
                .put("array", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("name", "CCCCCCCCCCC")));
        assertThat(result, is(expectedResult));
    }
}
