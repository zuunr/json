/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

class JsonDeserializerTest {

    private JsonObjectFactory jsonObjectFactory = new JsonObjectFactory();

    @Test
    void dezerializeTest() throws Exception {
        JsonObject jsonObject = jsonObjectFactory.createJsonObject(jsonOf("{'firstName': 'Peter', 'contact': {'email':'peter@andersson.se'}}"));
        JsonObject expected = JsonObject.EMPTY.put("firstName", "Peter").put("contact", JsonObject.EMPTY.put("email", "peter@andersson.se"));
        assertThat(jsonObject, is(expected));
    }

    @Test
    void deserializeNumbersTest() throws Exception {

        JsonObject jsonObject = jsonObjectFactory.createJsonObject(jsonOf("{'integer': 20, 'decimal': 20.3}"));
        JsonValue integer = jsonObject.get("integer");
        JsonValue decimal = jsonObject.get("decimal");

        assertThat(integer.getValue(), is(JsonNumber.of(20L)));
        assertThat(decimal.getValue(), is(JsonNumber.of(new BigDecimal("20.3"))));
        assertThat(integer.asInteger(), is(20));
        assertThat(decimal.asJson(), is("20.3"));
    }

    private String jsonOf(String string) {
        return string.replace('\'','"' );
    }
}
