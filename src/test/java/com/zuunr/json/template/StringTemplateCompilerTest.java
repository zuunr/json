package com.zuunr.json.template;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class StringTemplateCompilerTest {

    StringTemplateCompiler stringTemplateCompiler = new StringTemplateCompiler();

    @Test
    void test() {
        JsonValue template = JsonValue.of("Hello {{firstname}} {{lastname}}!");
        assertThat(stringTemplateCompiler.compile(template).dynamicFragments, is(JsonArray.of("firstname", "lastname")));
        assertThat(stringTemplateCompiler.compile(template).staticFragments, is(JsonArray.of("Hello ", " ", "!")));
    }

    @Test
    void test1() {
        JsonValue template = JsonValue.of("{{$/firstname}}");
        assertThat(stringTemplateCompiler.compile(template).dynamicFragments, is(JsonArray.of("$/firstname")));
        assertThat(stringTemplateCompiler.compile(template).staticFragments, is(JsonArray.of("", "")));
        assertThat(stringTemplateCompiler.compile(template).render(JsonObject.EMPTY.put("firstname","Peter").jsonValue()).getString(), is("Peter"));
    }

    @Test
    void test2() {
        JsonValue template = JsonValue.of("{{firstname}}{{lastname}}");
        assertThat(stringTemplateCompiler.compile(template).dynamicFragments, is(JsonArray.of("firstname", "lastname")));
        assertThat(stringTemplateCompiler.compile(template).staticFragments, is(JsonArray.of("", "", "")));
    }

    @Test
    void test3() {
        JsonValue template = JsonValue.of("{{firstname}}{{lastname}");
        assertThat(stringTemplateCompiler.compile(template).dynamicFragments, is(JsonArray.of("firstname")));
        assertThat(stringTemplateCompiler.compile(template).staticFragments, is(JsonArray.of("", "{{lastname}")));
    }

    @Test
    void test4() {
        JsonValue template = JsonValue.of("{{");
        assertThat(stringTemplateCompiler.compile(template).dynamicFragments, is(JsonArray.EMPTY));
        assertThat(stringTemplateCompiler.compile(template).staticFragments, is(JsonArray.of("{{")));
    }

    @Test
    void renderSuccessfulTest() {
        JsonValue template = JsonValue.of("Hello {{$/firstName}} {{$/lastName}}!");
        JsonValue model = JsonObject.EMPTY.put("firstName", "Peter").put("lastName", "Andersson").jsonValue();
        String result = stringTemplateCompiler.compile(template).render(model).getString();
        assertThat(result, is("Hello Peter Andersson!"));
    }

    @Test
    void renderFailsTest() {
        JsonValue template = JsonValue.of("Hello {{$/firstName}} {{$/lastName}}!");
        JsonValue model = JsonObject.EMPTY.put("lastName", "Andersson").jsonValue();
        try {
            String result = stringTemplateCompiler.compile(template).render(model).getString();
            assertThat(false, is(true));
        } catch (ModelValueUndefinedException e) {
            assertThat(true, is(true));
        }
    }

    @Test
    void renderNonStringValue() {
        JsonValue template = JsonValue.of("{{$/boolean}}");
        JsonValue model = JsonObject.EMPTY.put("boolean", true).jsonValue();

        JsonValue result = stringTemplateCompiler.compile(template).render(model);
        assertThat(result.getBoolean(), is(true));
    }

    @Test
    void renderNonStringValue2() {
        JsonValue template = JsonValue.of(JsonObject.EMPTY.put("value", "{{#/someArray}}").asJson());
        JsonValue model = JsonObject.EMPTY.put("someArray", JsonArray.EMPTY).jsonValue();

        JsonValue result = stringTemplateCompiler.compile(template).render(model, '#');
        assertThat(result.getString(), is(JsonObject.EMPTY.put("value", JsonArray.EMPTY).asJson()));
    }

    @Test
    void renderNonStringValue3() {
        JsonValue template = JsonValue.of(JsonObject.EMPTY.put("value", "{{$/someArray}}").asJson());
        JsonValue model = JsonObject.EMPTY.put("someArray", JsonArray.EMPTY).jsonValue();

        JsonValue result = stringTemplateCompiler.compile(template).render(model, '#');
        assertThat(result.getString(), is(template.getString()));
    }

}
