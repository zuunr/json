/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import com.zuunr.json.tool.JsonUtil;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class JsonArrayTest {

    @Test
    void givenStringPatternAndValueIsStringShouldFilterArrayObjects() {
        JsonArray persons = JsonArray.EMPTY
                .add(JsonObject.EMPTY.put("name", "Laura Andersson").put("age", 19))
                .add(JsonObject.EMPTY.put("name", "Pete Peteson").put("age", 23))
                .add(JsonObject.EMPTY.put("name", "Phil Andersson").put("age", 22));

        JsonArray filteredPersons = persons.filter(JsonArray.of("name"), ".*Andersson");

        assertEquals(2, filteredPersons.size());
        assertEquals("Laura Andersson", filteredPersons.get(0).get("name").getString());
        assertEquals("Phil Andersson", filteredPersons.get(1).get("name").getString());
    }

    @Test
    void givenStringPatternAndValueIsIntegerShouldFilterArrayObjects() {
        JsonArray persons = JsonArray.EMPTY
                .add(JsonObject.EMPTY.put("name", "Laura Andersson").put("age", 19))
                .add(JsonObject.EMPTY.put("name", "Pete Peteson").put("age", 23))
                .add(JsonObject.EMPTY.put("name", "Phil Andersson").put("age", 22));

        JsonArray filteredPersons = persons.filter(JsonArray.of("age"), "19");

        assertEquals(1, filteredPersons.size());
        assertEquals("Laura Andersson", filteredPersons.get(0).get("name").getString());
    }

    @Test
    void givenStringPatternAndValueIsLongShouldFilterArrayObjects() {
        JsonArray cars = JsonArray.EMPTY
                .add(JsonObject.EMPTY.put("type", "Ferrari").put("miles", 50000000000L))
                .add(JsonObject.EMPTY.put("type", "Lamborghini").put("miles", 55000000000L))
                .add(JsonObject.EMPTY.put("type", "Aston Martin").put("miles", 65000000000L));

        JsonArray filteredCars = cars.filter(JsonArray.of("miles"), "65000000000");

        assertEquals(1, filteredCars.size());
        assertEquals("Aston Martin", filteredCars.get(0).get("type").getString());
    }

    @Test
    void givenNonImplementedFilterValueShouldThrowException() {
        JsonArray persons = JsonArray.EMPTY
                .add(JsonObject.EMPTY.put("name", "Laura Andersson").put("hasPhone", true))
                .add(JsonObject.EMPTY.put("name", "Pete Peteson").put("hasPhone", false))
                .add(JsonObject.EMPTY.put("name", "Phil Andersson").put("hasPhone", true));

        JsonArray path = JsonArray.of("hasPhone");
        assertThrows(RuntimeException.class, () -> persons.filter(path, "false"));
    }

    // Cache is removed for now @Test
    void perfomanceTest() {
        JsonArray jsonArray0 = JsonArray.EMPTY.add("path");//.add("to").add("some").add("value");
        JsonArray jsonArray1 = JsonArray.EMPTY.add("path");//.add("to").add("some").add("value");

        assertThat("Should be the same cached value", jsonArray0 == jsonArray1, is(true));

        for (int i = 0; i < 100000000; i++) {
            JsonArray jsonArray2 = JsonArray.EMPTY.add("path").add("to").add("some").add("value");
        }
    }

    @Test
    void compareTo() {
        assertThat(JsonArray.EMPTY.add("samples").compareTo(JsonArray.EMPTY.add("something else")), lessThan(0));
    }

    @Test
    void putTest() {
        JsonArray result = JsonArray.EMPTY.put(2, JsonObject.EMPTY.jsonValue());
        JsonArray expected = JsonArray.of(JsonValue.NULL, JsonValue.NULL, JsonObject.EMPTY);

        assertThat(result, is(expected));
    }

    @Test
    void add() {
        assertThat(JsonArray.EMPTY.size(), is(0));
        assertThat(JsonArray.EMPTY.add("one").size(), is(1));
        assertThat(JsonArray.EMPTY.add("one").get(0).getValue(String.class), is("one"));
        assertThat(JsonArray.EMPTY.add("one").add("three").add(1, JsonValue.of("two")), is(JsonArray.EMPTY.add("one").add("two").add("three")));

        assertThat(JsonArray.EMPTY.add("one").add("two").add("three").size(), is(3));
        assertThat(JsonArray.EMPTY.add("one").add("two").add("three").remove(1).size(), is(2));
        assertThat(JsonArray.EMPTY.add("one").add("two").add("three").remove(1), is(JsonArray.EMPTY.add("one").add("three")));

        assertThat(JsonArray.EMPTY.add("one").add("two").add("three").put(1, JsonValue.of("one_and_a_half")), is(JsonArray.EMPTY.add("one").add("one_and_a_half").add("three")));
        assertThat(JsonArray.EMPTY.add(JsonValue.NULL).size(), is(1));
    }

    @Test
    void asListTest() {
        assertThat(JsonArray.EMPTY.add(true).asList(Object.class).get(0), is(true));
    }

    @Test
    void asArrayTest() {
        JsonValue[] array = new JsonValue[2];
        array[0] = JsonValue.of("a");
        array[1] = JsonValue.of("b");
        assertThat(JsonArray.of("a", "b").asArray(), is(array));
    }

    @Test
    void sort() {
        JsonArray result = JsonArray.of(JsonArray.of("b", "c", "a"), JsonArray.of("a", "c"), JsonArray.of("c"), JsonArray.of("ac"), JsonArray.of(1)).sort();
        JsonArray expected = JsonUtil.create("{'array':[[1],['ac'],['c'],['a','c'],['b','c','a']]}").get("array").getValue(JsonArray.class);
        assertThat(result, is(expected));
    }

    @Test
    void addFirstToEachTest() {
        JsonArray result = JsonArray.of(JsonArray.of("a"), JsonArray.of("b")).addFirstToEach(1);
        JsonArray expected = JsonArray.of(JsonArray.of(1, "a"), JsonArray.of(1, "b"));
        assertThat(result, is(expected));
    }
}
