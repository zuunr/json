/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

/**
 * @author Niklas Eldberger
 */
class NoJsonValueMapTest {

    @Test
    void testWithStrings() {
        JsonObject jsonObject = JsonObject.EMPTY.put("firstName", "Peter").put("lastName", "Andersson");
        Map<String, Object> map = jsonObject.as(NoJsonValueMap.class);

        assertThat(map.get("firstName"), is("Peter"));
        assertThat(map.get("lastName"), is("Andersson"));

        Iterator<Map.Entry<String, Object>> iter = map.entrySet().iterator();

        assertThat(iter.hasNext(), is(true));
        Map.Entry<String, Object> entry = iter.next();
        assertThat(entry.getKey(), is("lastName"));
        assertThat(entry.getValue(), is("Andersson"));

        assertThat(iter.hasNext(), is(true));
        entry = iter.next();
        assertThat(entry.getKey(), is("firstName"));
        assertThat(entry.getValue(), is("Peter"));
    }

    @Test
    void testWithJsonObject() {
        JsonObject jsonObject = JsonObject.EMPTY.put("person", JsonObject.EMPTY.put("name", "Peter"));
        Map<String, Object> map = jsonObject.as(NoJsonValueMap.class);

        assertThat(map.get("person") instanceof Map, is(true));

        @SuppressWarnings("unchecked")
        Map<String, Object> personMap = (Map<String, Object>) map.get("person");

        assertThat(personMap.get("name"), is("Peter"));
    }

    @Test
    void testWithJsonObjectAndList() {
        JsonObject jsonObject = JsonObject.EMPTY.put("colors", JsonArray.EMPTY.add("red").add("blue"));
        Map<String, Object> map = jsonObject.as(NoJsonValueMap.class);

        assertThat(map.get("colors") instanceof List, is(true));

        @SuppressWarnings("unchecked")
        List<String> colorList = (List<String>) map.get("colors");

        assertThat(colorList.get(0), is("red"));
        assertThat(colorList.get(1), is("blue"));

        for (Object entry : map.entrySet()) {
            System.out.println(entry);
        }
    }

    @Test
    void entriesTest() {
        JsonObject jsonObject = JsonObject.EMPTY
                .put("firstName", "Peter")
                .put("lastName", "Andersson");
        Map<String, Object> map = jsonObject.as(NoJsonValueMap.class);

        for (Map.Entry<String, Object> entry : map.entrySet()) {
            System.out.println(entry);
        }
    }
}
