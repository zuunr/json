/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
class NoJsonValueListTest {

    @Test
    void test() {
        JsonArray jsonArray = JsonArray.EMPTY.add("Niklas").add("Eldberger");

        List<Object> list = jsonArray.as(NoJsonValueList.class);

        assertThat(list.get(0), is("Niklas"));
        assertThat(list.get(1), is("Eldberger"));

        JsonArrayBuilder jsonArrayBuilder = JsonArray.EMPTY.builder();
        for (Object object : list) {
            jsonArrayBuilder.add(JsonValue.of(object));
        }

        assertThat(jsonArrayBuilder.build(), is(jsonArray));

        jsonArrayBuilder = JsonArray.EMPTY.builder();
        for (int i = 0; i < list.size(); i++) {
            jsonArrayBuilder.add(JsonValue.of(list.get(i)));
        }

        assertThat(jsonArrayBuilder.build(), is(jsonArray));
    }
}
