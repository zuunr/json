/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
class JsonValueUtilTest {

    @Test
    void createJsonArrayPathFromString() {
        JsonArray path = new JsonValueUtil().createJsonArrayPathFromStringPath("aaa.bbbbbb.cc", "\\.");
        JsonArray expected = JsonArray.EMPTY.add("aaa").add("bbbbbb").add("cc");
        assertThat(path, is(expected));
    }

    @Test
    void fullRoundTest() {
        JsonArray array1 = JsonArray.EMPTY.add("Hello. I am Mr Foo.").add("Hello Mr Foo. I am Mr Noo");
        JsonValueUtil jsonValueUtil = new JsonValueUtil();
        String dotSeparatedString = jsonValueUtil.dotSeparatedString(array1);
        assertThat(dotSeparatedString, is("Hello\\u002e I am Mr Foo\\u002e.Hello Mr Foo\\u002e I am Mr Noo"));
        JsonArray array2 = jsonValueUtil.createJsonArrayFromDotSeparated(dotSeparatedString);
        assertThat(array2, is(array1));
    }

    @Test
    void appendAndEscapeDotsTest() throws Exception {
        String toBeEscaped = ".a.b\\\\u002e._.whatever";
        JsonValueUtil jsonValueUtil = new JsonValueUtil();
        String escaped = jsonValueUtil.escapeDotsWithUnicode(toBeEscaped);
        assertThat(escaped, is(".a.b\\\\u002e._.whatever".replaceAll("[.]", "\\\\u002e")));
        byte[] escapedAsBytes = escaped.getBytes("UTF8");
        String unescaped = jsonValueUtil.decodeUnicoded(escaped.toCharArray()).toString();
        assertThat(unescaped, is(toBeEscaped));
    }

    @Test
    void doubleBackslashTest() {
        assertThat("\\\\u002e".replaceAll("\\\\\\\\", "\\\\"), is("\\u002e"));
    }

    @Test
    void escapTheDotWithUnicodeTest() throws Exception {
        String toBeEscaped = ".a.b.";

        System.out.println("split: " + toBeEscaped.split(""));

        String escaped = "\\u" + Integer.toHexString('.' | 0x10000).substring(1);
        System.out.println("escaped: " + "\\u" + Integer.toHexString('.' | 0x10000).substring(1));

        byte[] escapedAsBytes = escaped.getBytes("UTF8");
        String text = new String(escapedAsBytes, "UTF8");

        System.out.println("escaped and unescaped: " + text);
        toBeEscaped.replaceAll("[.]", "\\U+002E");
    }

    @Test
    void decodeUnicodedTest() {
        JsonValueUtil jsonValueUtil = new JsonValueUtil();
        char[] chars2 = {'\\', '\\', 'a', '\\', 'u', '0', '0', '2', 'e', '_'};

        String result = jsonValueUtil.decodeUnicoded(chars2).toString();
        String expected = "\\\\a._";
        assertThat(result, is(expected));
    }

    @Test
    void minOf1() {

        JsonValueUtil jsonValueUtil = new JsonValueUtil();
        assertThat(jsonValueUtil.minOf(JsonValue.of(1), JsonValue.of(2000000000L)), is(JsonValue.of(1)));
        assertThat(jsonValueUtil.minOf(JsonValue.of(2000000000L), JsonValue.of(1)), is(JsonValue.of(1)));

        assertThat(jsonValueUtil.minOf(JsonValue.of("1"), JsonValue.of("2")), is(JsonValue.of("1")));
        assertThat(jsonValueUtil.minOf(JsonValue.of("2"), JsonValue.of("1")), is(JsonValue.of("1")));
    }

    @Test
    void maxOf1() {

        JsonValueUtil jsonValueUtil = new JsonValueUtil();
        assertThat(jsonValueUtil.maxOf(JsonValue.of(1), JsonValue.of(2000000000L)), is(JsonValue.of(2000000000L)));
        assertThat(jsonValueUtil.maxOf(JsonValue.of(2000000000L), JsonValue.of(1)), is(JsonValue.of(2000000000L)));

        assertThat(jsonValueUtil.maxOf(JsonValue.of("1"), JsonValue.of("2")), is(JsonValue.of("2")));
        assertThat(jsonValueUtil.maxOf(JsonValue.of("2"), JsonValue.of("1")), is(JsonValue.of("2")));
    }
}
