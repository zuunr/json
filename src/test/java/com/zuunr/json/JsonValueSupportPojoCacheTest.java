/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.HashMap;

import org.junit.jupiter.api.Test;

/**
 * @author Niklas Eldberger
 */
class JsonValueSupportPojoCacheTest {

    @Test
    void test() {
        JsonValueSupportedPojoCache cache = new JsonValueSupportedPojoCache();

        HashMap<?,?> hashMap = new HashMap<>();

        cache
                .putPojo(String.class, "This is a string")
                .putPojo(Integer.class, 1)
                .putPojo(HashMap.class, hashMap)
                .putPojo(Long.class, 47L);


        assertThat(cache.getPojo(String.class), is("This is a string"));
        assertThat(cache.getPojo(HashMap.class), is(hashMap));
        assertThat(cache.getPojo(Integer.class), is(1));
        assertThat(cache.getPojo(Long.class), is(47L));

    }
}
