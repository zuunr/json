/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json.example;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.jupiter.api.Test;

class ExampleTest {

    @Test
    void test() {

        // The Person, Child, Children and Contact classes can be generated from a plain JSON string - TODO Implement this!

        // Creating an initial Person object
        Person person = Person.builder()
                .name("Peter Andersson")
                .contact(Contact.builder()
                        .email("peter@gmail.com")
                        .twitter("@peterandersson")
                        .build()
                )
                .children(Children.builder()
                        .add(Child.builder()
                                .name("Laura Andersson")
                                .build())
                        .add(Child.builder()
                                .name("Phil Andersson")
                                .build())
                        .build()
                )
                .build();

        assertThat(person.getName(), is("Peter Andersson"));
        assertThat(person.getContact().getEmail(), is("peter@gmail.com"));
        assertThat(person.getContact().getTwitter(), is("@peterandersson"));
        assertThat(person.getChildren().get(0), is(Child.builder().name("Laura Andersson").build()));
        assertThat(person.getChildren().get(1), is(Child.builder().name("Phil Andersson").build()));

        // Adding the parts of person that should be updated
        Person personPatch = Person.builder()
                .contact(Contact.builder()
                        .email("peter@andersson.com") // New email address
                        .build())
                .children(Children.builder()
                        .add(Child.builder()
                                .name("Robert Andersson") // Only one child named "Robert Andersson"
                                .build())
                        .build())
                .build();

        // Update the original person with data in personPatch object
        Person patchedPerson = person.updateWith(personPatch);
        Person expectedPatchedPerson = Person.builder()
                .name("Peter Andersson")
                .contact(Contact.builder()
                        .email("peter@andersson.com") // The new email address
                        .twitter("@peterandersson")   // Same as before
                        .build()
                )
                .children(Children.builder()
                        .add(Child.builder()
                                .name("Robert Andersson") // Children replaced by one child named "Robert Andersson"
                                .build()).build()
                )
                .build();

        assertThat(patchedPerson, is(expectedPatchedPerson));
    }
}
