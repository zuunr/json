/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json.example;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectBuilder;
import com.zuunr.json.JsonValue;
import com.zuunr.json.TypedObject;

public class Contact extends TypedObject<Contact> {

    private String email;
    private String twitter;

    public static ContactBuilder builder() {
        return new ContactBuilder();
    }

    private Contact(JsonValue jsonValue) {
        this(jsonValue.getValue(JsonObject.class));
    }

    private Contact(JsonObject jsonObject) {
        super(jsonObject);
    }

    @Override
    protected Contact createNew(JsonObject jsonObject) {
        return new Contact(jsonObject);
    }

    public String getEmail() {
        return jsonObject.get("email", JsonValue.NULL).getValue(String.class);
    }

    public String getTwitter() {
        return jsonObject.get("twitter", JsonValue.NULL).getValue(String.class);
    }

    public static final class ContactBuilder {
        private JsonObjectBuilder jsonObjectBuilder;

        private ContactBuilder() {
            jsonObjectBuilder = JsonObject.EMPTY.builder();
        }


        public ContactBuilder email(String twitter) {
            jsonObjectBuilder.put("email", twitter);
            return this;
        }

        public ContactBuilder twitter(String twitter) {
            jsonObjectBuilder.put("twitter", twitter);
            return this;
        }

        public Contact build() {
            return new Contact(jsonObjectBuilder.build());
        }
    }
}
