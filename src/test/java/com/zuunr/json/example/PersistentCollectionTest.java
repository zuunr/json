/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json.example;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.List;

import org.junit.jupiter.api.Test;

import clojure.lang.APersistentVector;
import clojure.lang.IPersistentMap;
import clojure.lang.ISeq;
import clojure.lang.MapEntry;
import clojure.lang.PersistentHashMap;
import clojure.lang.PersistentVector;


class PersistentCollectionTest {

    @Test
    void test() {
        PersistentVector persistentVector = PersistentVector.EMPTY.assocN(0, "Kalle");

        persistentVector = persistentVector.assocN(persistentVector.size(), "Olle");
        persistentVector = persistentVector.assocN(persistentVector.size(), "Stina");

        assertThat(persistentVector.get(0), is("Kalle"));
        assertThat(persistentVector.get(1), is("Olle"));
        assertThat(persistentVector.get(2), is("Stina"));


        persistentVector = persistentVector.assocN(1, "Berra");
        assertThat(persistentVector, is(PersistentVector.EMPTY.assocN(0, "Kalle").assocN(1, "Berra").assocN(2, "Stina")));


        List<?> subList = persistentVector.subList(0,2);
        assertThat(subList, is(((APersistentVector)PersistentVector.EMPTY).assocN(0, "Kalle").assocN(1, "Berra")));
    }

    @Test
    void testPersistentMap(){
        IPersistentMap persistentHashMap = PersistentHashMap.EMPTY.assoc("key1", "value1").assoc("key2", "value2").assoc("key3", "value3");

        ISeq seq = persistentHashMap.seq();

        for (; seq != null; seq = seq.next()) {
            Object first = seq.first();

            MapEntry mapEntry = (MapEntry)first;
            System.out.println(mapEntry.getKey());
            System.out.println(mapEntry.getValue());
        }
    }
}
