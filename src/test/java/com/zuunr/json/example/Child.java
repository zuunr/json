/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json.example;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectBuilder;
import com.zuunr.json.JsonValue;
import com.zuunr.json.TypedObject;

public class Child extends TypedObject<Child> {

    public static Builder builder() {
        return new Builder();
    }

    private Child(JsonValue jsonValue) {
        this(jsonValue.getValue(JsonObject.class));
    }

    private Child(JsonObject jsonObject) {
        super(jsonObject);
    }

    public String getName() {
        return jsonObject.get("name", JsonValue.NULL).getValue(String.class);
    }

    @Override
    protected Child createNew(JsonObject jsonObject) {
        return new Child(jsonObject);
    }

    public static class Builder {
        private final JsonObjectBuilder jsonObjectBuilder;

        public Builder() {
            this.jsonObjectBuilder = JsonObject.EMPTY.builder();
        }

        public Builder name(String name) {
            jsonObjectBuilder.put("name", name);
            return this;
        }

        public Child build() {
            return new Child(jsonObjectBuilder.build());
        }
    }
}
