/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json.example;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArrayBuilder;
import com.zuunr.json.JsonArraySupport;

public class Children implements JsonArraySupport {
    
    private final JsonArray jsonArray;

    public static Builder builder() {
        return new Builder();
    }

    private Children(JsonArray jsonArray) {
        this.jsonArray = jsonArray;
    }

    @Override
    public JsonArray asJsonArray() {
        return jsonArray;
    }

    public static class Builder {

        JsonArrayBuilder jsonArrayBuilder;

        public Builder() {
            jsonArrayBuilder = JsonArray.EMPTY.builder();
        }

        public Builder add(Child child) {
            jsonArrayBuilder.add(child.asJsonValue());
            return this;
        }

        public Children build() {
            return new Children(jsonArrayBuilder.build());
        }
    }
}
