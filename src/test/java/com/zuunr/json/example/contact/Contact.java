/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json.example.contact;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectBuilder;
import com.zuunr.json.JsonValue;
import com.zuunr.json.TypedObject;

/** This class is generated from JSON **/
public class Contact extends TypedObject<Contact> {

    public static Builder builder() {
        return new Builder();
    }

    private Contact(JsonObject jsonObject) {
        super(jsonObject);
    }

    private Contact(JsonValue jsonValue) {
        super(jsonValue.getValue(JsonObject.class));
    }

    @Override
    protected Contact createNew(JsonObject jsonObject) {
        Contact model = new Contact(jsonObject);
        return model;
    }

    public Contact_name getName() {
        return jsonObject.get("name", JsonValue.NULL).as(Contact_name.class);
    }

    public Contact_addresses getAddresses() {
        return jsonObject.get("addresses", JsonValue.NULL).as(Contact_addresses.class);
    }

    public static final class Builder {
        private JsonObjectBuilder jsonObjectBuilder = JsonObject.EMPTY.builder();
        
        public Builder name(Contact_name name) {
            jsonObjectBuilder.put("name", name);
            return this;
        }

        public Builder addresses(Contact_addresses addresses) {
            jsonObjectBuilder.put("addresses", addresses);
            return this;
        }
        
        public Contact build() {
            return new Contact(jsonObjectBuilder.build());
        }

    }
}
