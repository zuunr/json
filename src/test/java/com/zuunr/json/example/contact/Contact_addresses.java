/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json.example.contact;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArrayBuilder;
import com.zuunr.json.JsonArraySupport;
import com.zuunr.json.JsonValue;

import java.util.List;

/* This class is generated from JSON **/
public class Contact_addresses implements JsonArraySupport {
    
    private JsonArray jsonArray;

    public static Builder builder() {
        return new Builder();
    }

    private Contact_addresses(JsonArray jsonArray) {
        this.jsonArray = jsonArray;
    }

    private Contact_addresses(JsonValue jsonValue) {
        this.jsonArray = jsonValue.getValue(JsonArray.class);
    }

    @Override
    public JsonArray asJsonArray() {
        return jsonArray;
    }

    public List<Contact_addresses_elem> asList() {
        return jsonArray.asList(Contact_addresses_elem.class);
    }

    public static class Builder {

        JsonArrayBuilder jsonArrayBuilder;

        public Builder() {
            jsonArrayBuilder = JsonArray.EMPTY.builder();
        }

        public Builder add(Contact_addresses_elem element) {
            jsonArrayBuilder.add(element);
            return this;
        }

        public Contact_addresses build() {
            return new Contact_addresses(jsonArrayBuilder.build());
        }
    }
}
