/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json.example.contact;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectBuilder;
import com.zuunr.json.JsonValue;
import com.zuunr.json.TypedObject;

/**
 * This class is generated from JSON
 **/
public class Contact_name extends TypedObject<Contact_name> {

    public static Builder builder() {
        return new Builder();
    }

    private Contact_name(JsonObject jsonObject) {
        super(jsonObject);
    }

    private Contact_name(JsonValue jsonValue) {
        super(jsonValue.getValue(JsonObject.class));
    }

    @Override
    protected Contact_name createNew(JsonObject jsonObject) {
        Contact_name model = new Contact_name(jsonObject);
        return model;
    }

    public String getLast() {
        return jsonObject.get("last", JsonValue.NULL).getValue(String.class);
    }

    public String getFirst() {
        return jsonObject.get("first", JsonValue.NULL).getValue(String.class);
    }

    public static final class Builder {
        private JsonObjectBuilder jsonObjectBuilder = JsonObject.EMPTY.builder();

        public Builder last(String last) {
            jsonObjectBuilder.put("last", last);
            return this;
        }

        public Builder first(String first) {
            jsonObjectBuilder.put("first", first);
            return this;
        }

        public Contact_name build() {
            return new Contact_name(jsonObjectBuilder.build());
        }
    }
}
