/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json.example.contact;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectBuilder;
import com.zuunr.json.JsonValue;
import com.zuunr.json.TypedObject;

/**
 * This class is generated from JSON
 **/
public class Contact_addresses_elem extends TypedObject<Contact_addresses_elem> {

    public static Builder builder() {
        return new Builder();
    }

    private Contact_addresses_elem(JsonObject jsonObject) {
        super(jsonObject);
    }

    private Contact_addresses_elem(JsonValue jsonValue) {
        super(jsonValue.getValue(JsonObject.class));
    }

    @Override
    protected Contact_addresses_elem createNew(JsonObject jsonObject) {
        Contact_addresses_elem model = new Contact_addresses_elem(jsonObject);
        return model;
    }

    public String getCountry() {
        return jsonObject.get("country", JsonValue.NULL).getValue(String.class);
    }

    public String getStreet() {
        return jsonObject.get("street", JsonValue.NULL).getValue(String.class);
    }

    public static final class Builder {
        private JsonObjectBuilder jsonObjectBuilder = JsonObject.EMPTY.builder();

        public Builder country(String country) {
            jsonObjectBuilder.put("country", country);
            return this;
        }

        public Builder street(String street) {
            jsonObjectBuilder.put("street", street);
            return this;
        }

        public Contact_addresses_elem build() {
            return new Contact_addresses_elem(jsonObjectBuilder.build());
        }

    }
}
