/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json.example;

import com.zuunr.json.*;

import java.util.List;

public class Person extends TypedObject<Person> {

    public static Builder builder() {
        return new Builder();
    }

    private Person(JsonObject jsonObject) {
        super(jsonObject);
    }

    @Override
    protected Person createNew(JsonObject jsonObject) {
        Person person = new Person(jsonObject);
        return person;
    }

    public String getName() {
        return jsonObject.get("name", JsonValue.NULL).getValue(String.class);
    }

    public Contact getContact() {
        return jsonObject.get("contact", JsonValue.NULL).as(Contact.class);
    }

    public List<Child> getChildren() {
        return jsonObject.get("children", JsonArray.EMPTY).getValue(JsonArray.class).asList(Child.class);
    }

    public static final class Builder {
        private final JsonObjectBuilder jsonObjectBuilder = JsonObject.EMPTY.builder();

        public Builder name(String name) {
            jsonObjectBuilder.put("name", name);
            return this;
        }

        public Builder contact(Contact contact) {
            jsonObjectBuilder.put("contact", contact.asJsonValue());
            return this;
        }

        public Builder children(Children children) {
            jsonObjectBuilder.put("children", children.asJsonValue());
            return this;
        }

        public Person build() {
            return new Person(jsonObjectBuilder.build());
        }
    }
}
