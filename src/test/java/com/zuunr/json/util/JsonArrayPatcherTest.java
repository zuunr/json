package com.zuunr.json.util;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
class JsonArrayPatcherTest {

    @Test
    void patchArrayTest() {
        JsonArray toBePatched = JsonArray.of(
                JsonObject.EMPTY
                        .put("a", "A1")
                        .put("b", "B1")
                        .put("c", "C1")
                        .put("d", "D1"),
                JsonObject.EMPTY
                        .put("a", "A2")
                        .put("b", "B2")
                        .put("c", "C2")
                        .put("d", "D2"),
                JsonObject.EMPTY
                        .put("a", "A3")
                        .put("b", "B3")
                        .put("c", "C3")
                        .put("d", "D3")
        );

        JsonArray overrider = JsonArray.of(
                JsonObject.EMPTY
                        .put("a", "A1")
                        .put("b", "B1")
                        .put("c", "C1")
                        .put("d", "NEW VALUE"),
                JsonObject.EMPTY
                        .put("a", "A3")
                        .put("b", "B3")
                        .put("c", "C3")
                        .put("d", "D3")
        );

        JsonArray meta = JsonArray.of(JsonArray.of("a"), JsonArray.of("b"), JsonArray.of("c"));

        JsonArray result = new JsonArrayPatcher().patch(toBePatched, overrider, meta);

        JsonArray expected = JsonArray.of(
                JsonObject.EMPTY
                        .put("a", "A1")
                        .put("b", "B1")
                        .put("c", "C1")
                        .put("d", "NEW VALUE"),
                JsonObject.EMPTY
                        .put("a", "A2")
                        .put("b", "B2")
                        .put("c", "C2")
                        .put("d", "D2"),
                JsonObject.EMPTY
                        .put("a", "A3")
                        .put("b", "B3")
                        .put("c", "C3")
                        .put("d", "D3")
        );

        assertThat(result.sort(), is(expected.sort()));
    }

    @Test
    void arrayOfObject_test() {
        JsonArrayPatcher patcher = new JsonArrayPatcher();
        JsonArray result = patcher.arrayOfObject(
                JsonObject.EMPTY
                        .put("query", JsonObject.EMPTY
                                .put("filter.firstName.eq", JsonObject.EMPTY
                                        .put("schema", JsonObject.EMPTY
                                                .put("description", "description of filter.firstName.eq")))
                                .put("filter.lastName.eq", JsonObject.EMPTY
                                        .put("schema", JsonObject.EMPTY
                                                .put("description", "description of filter.lastName.eq")))
                                .put("offset", JsonObject.EMPTY
                                        .put("schema", JsonObject.EMPTY
                                                .put("description", "description of offset"))))
                        .put("path", JsonObject.EMPTY.put("id", JsonObject.EMPTY
                                .put("schema", JsonObject.EMPTY
                                        .put("description", "description of id")))),
                JsonArray.of(JsonArray.of("in"), JsonArray.of("name")));

        JsonArray expected = JsonArray.of(
                JsonObject.EMPTY
                        .put("schema", JsonObject.EMPTY
                                .put("description", "description of filter.firstName.eq"))
                        .put("in", "query")
                        .put("name", "filter.firstName.eq"),
                JsonObject.EMPTY
                        .put("schema", JsonObject.EMPTY
                                .put("description", "description of filter.lastName.eq"))
                        .put("in", "query")
                        .put("name", "filter.lastName.eq"),
                JsonObject.EMPTY
                        .put("schema", JsonObject.EMPTY
                                .put("description", "description of offset"))
                        .put("in", "query")
                        .put("name", "offset"),
                JsonObject.EMPTY
                        .put("schema", JsonObject.EMPTY
                                .put("description", "description of id"))
                        .put("in", "path")
                        .put("name", "id")


        ).sort();
        assertThat(result.sort(), is(expected));
    }
}