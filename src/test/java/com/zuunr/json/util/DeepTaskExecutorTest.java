/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json.util;

import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Iterator;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
class DeepTaskExecutorTest {

    private static Long LIMIT = 1000L;

    @Test
    void test1() {
        TaskExecutor<Long, Long, Long> taskExecutor = new TaskExecutor<>();

        MyTask task = new MyTask(1L);

        long start = System.nanoTime();
        Long result = taskExecutor.solve(task);
        System.out.println("TaskExecutor: " + (System.nanoTime() - start));

        assertThat(result, is(LIMIT));


        start = System.nanoTime();
        result = task.count(1L);
        System.out.println("Recursive: " + (System.nanoTime() - start));
        assertThat(result, is(LIMIT));


    }



    static class MyTask extends Task<Long, Long, Long> {

        public MyTask(Long in) {
            super(in);
        }

        public Long count(Long l) {
            //System.out.println(l);
            if (getOutFromBasecase(l) == null) {
                return count(l+1);
            }
            return getOutFromBasecase(l);
        }

        @Override
        public void splitIntoSubtasks(Iterator<Subtask<Long, Long, Long>> finishedSubtasks, Collection<Subtask<Long, Long, Long>> subtasks) {
            subtasks.add(new Subtask<Long, Long, Long>(null, new MyTask(getIn() + 1)));
        }

        @Override
        public Long getOutFromFinishedSubtasks(Collection<Subtask<Long, Long, Long>> finishedSubtasks) {
            return finishedSubtasks.iterator().next().getTask().getOut();
        }

        @Override
        public Long getOutFromBasecase(Long in) {

            if (in < LIMIT) {
                return null;
            }
            return in;
        }
    }
}
