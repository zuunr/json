package com.zuunr.json.util;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectFactory;
import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.JsonSchemaValidator;
import com.zuunr.json.schema.validation.OutputStructure;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
class ApiErrorCreatorTest {

    JsonSchemaValidator validator = new JsonSchemaValidator();

    private JsonObjectFactory jsonObjectFactory = new JsonObjectFactory();

    @Test
    void apiErrorCreator_object_1() {

        JsonObject testData = jsonObjectFactory.createJsonObject(this.getClass().getResourceAsStream("ApiErrorCreatorTest.errorobject_1.json"));
        JsonValue instance = testData.get(JsonArray.of("when", "instanceToBeValidated"));
        JsonSchema schema = testData.get(JsonArray.of("given", "schema")).as(JsonSchema.class);
        JsonObject validationResult = validator.validate(instance, schema, OutputStructure.DETAILED);
        JsonValue expectedApiError = testData.get(JsonArray.of("then", "expectedApiError", "errors"));
        JsonValue apiError = ApiErrorCreator.ERROR_OBJECT_WITH_VIOLATIONS_OBJECT.createErrors(validationResult, instance, schema);
        assertThat(apiError, is(expectedApiError));
    }

    @Test
    void apiErrorCreator_object_2() {

        JsonObject testData = jsonObjectFactory.createJsonObject(this.getClass().getResourceAsStream("ApiErrorCreatorTest.errorobject_2.json"));

        JsonValue instance = testData.get(JsonArray.of("when", "instanceToBeValidated"));

        JsonSchema schema = testData.get(JsonArray.of("given", "schema")).as(JsonSchema.class);

        JsonObject validationResult = validator.validate(instance, schema, OutputStructure.DETAILED);

        JsonValue expectedApiError = testData.get(JsonArray.of("then", "expectedApiError", "errors"));
        JsonValue apiError = ApiErrorCreator.ERROR_OBJECT_WITH_VIOLATIONS_OBJECT.createErrors(validationResult, instance, schema);
        assertThat(apiError, is(expectedApiError));
    }

    @Test
    void apiErrorCreator_object_3() {

        JsonObject testData = jsonObjectFactory.createJsonObject(this.getClass().getResourceAsStream("ApiErrorCreatorTest.errorobject_3.json"));

        JsonValue instance = testData.get(JsonArray.of("when", "instanceToBeValidated"));

        JsonSchema schema = testData.get(JsonArray.of("given", "schema")).as(JsonSchema.class);

        JsonObject validationResult = validator.validate(instance, schema, OutputStructure.DETAILED);

        JsonValue expectedApiError = testData.get(JsonArray.of("then", "expectedApiError", "errors"));
        JsonValue apiError = ApiErrorCreator.ERROR_OBJECT_WITH_VIOLATIONS_OBJECT.createErrors(validationResult, instance, schema);
        assertThat(apiError, is(expectedApiError));
    }

    @Test
    void apiErrorCreator_list_1() {

        JsonObject testData = jsonObjectFactory.createJsonObject(this.getClass().getResourceAsStream("ApiErrorCreatorTest.errorlist_1.json"));

        JsonValue instance = testData.get(JsonArray.of("when", "instanceToBeValidated"));

        JsonSchema schema = testData.get(JsonArray.of("given", "schema")).as(JsonSchema.class);

        JsonObject validationResult = validator.validate(instance, schema, OutputStructure.DETAILED);

        JsonValue expectedApiError = testData.get(JsonArray.of("then", "expectedApiError", "errors"));
        JsonValue apiError = ApiErrorCreator.ERROR_ARRAY_WITH_VIOLATIONS_ARRAY.createErrors(validationResult, instance, schema);
        assertThat(apiError, is(expectedApiError));
    }

    @Test
    void apiErrorCreator_list_2() {

        JsonObject testData = jsonObjectFactory.createJsonObject(this.getClass().getResourceAsStream("ApiErrorCreatorTest.errorlist_2.json"));

        JsonValue instance = testData.get(JsonArray.of("when", "instanceToBeValidated"));

        JsonSchema schema = testData.get(JsonArray.of("given", "schema")).as(JsonSchema.class);

        JsonObject validationResult = validator.validate(instance, schema, OutputStructure.DETAILED);

        JsonValue expectedApiError = testData.get(JsonArray.of("then", "expectedApiError", "errors"));
        JsonValue apiError = ApiErrorCreator.ERROR_ARRAY_WITH_VIOLATIONS_ARRAY.createErrors(validationResult, instance, schema);
        assertThat(apiError, is(expectedApiError));
    }

}