/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json.util;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArrayBuilder;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
class GetPathsTaskExecutorTest {

    private static Long LIMIT = 100L;


    @Test
    void test1() {
        TaskExecutor<JsonValue, JsonArray, JsonValue> taskExecutor = new TaskExecutor<>();

        MyTask task = new MyTask(JsonValue.of("a"));

        long start = System.nanoTime();
        JsonArray result = taskExecutor.solve(task);
        System.out.println("TaskExecutor: " + (System.nanoTime() - start));

        assertThat(result, is(JsonArray.of(JsonArray.of("a"))));

    }

    @Test
    void test2() { //NOSONAR
        TaskExecutor<JsonValue, JsonArray, JsonValue> taskExecutor = new TaskExecutor<>();

        JsonValue jsonValue = JsonObject.EMPTY.put("a", JsonArray.of(JsonObject.EMPTY.put("b", "c"))).put("b", JsonObject.EMPTY.put("c", "d")).jsonValue();

        MyTask task;

        JsonArray result = null;
        long createTaskTime = 0L;
        taskExecutor.solve(generateTask()); // varm up
        long start = System.nanoTime();
        for (int i = 0; i < LIMIT; i++) {
            long taskStart = System.nanoTime();
            task = generateTask();
            createTaskTime = createTaskTime + System.nanoTime() - taskStart;
            result = taskExecutor.solve(task);

            /*
            printDiagnistics(taskExecutor);
            taskExecutor = new TaskExecutor<>();
            */
        }
        System.out.println("JsonValue Paths -> TaskExecutor: " + ((System.nanoTime() - start - createTaskTime) / LIMIT));

    }



    @Test
    void test3() { //NOSONAR
        TaskExecutor<JsonValue, JsonArray, JsonValue> taskExecutor = new TaskExecutor<>();

        JsonValue jsonValue = JsonObject.EMPTY.put("a", JsonArray.of(JsonObject.EMPTY.put("b", "c"))).put("b", JsonObject.EMPTY.put("c", "d")).jsonValue();

        MyTask task;

        GetPathsTaskExecutorTest.paths(generateTask().getIn(), true);//warm up

        JsonArray result = null;
        long createTaskTime = 0L;

        long start2 = System.nanoTime();
        for (int i = 0; i < LIMIT; i++) {
            long taskStart = System.nanoTime();
            task = generateTask();
            createTaskTime = createTaskTime + System.nanoTime() - taskStart;
            result = GetPathsTaskExecutorTest.paths(task.getIn(), true);
        }
        System.out.println("JsonValue Paths -> Recursive: " + ((System.nanoTime() - start2 - createTaskTime) / LIMIT));

    }

    @Test
    void test4() {
        TaskExecutor<JsonValue, JsonArray, JsonValue> taskExecutor = new TaskExecutor<>();

        JsonValue jsonValue = JsonObject.EMPTY.put("a", JsonArray.of(JsonObject.EMPTY.put("b", "c"))).put("b", JsonObject.EMPTY.put("c", "d")).jsonValue();

        MyTask task;

        generateTask().getIn().getPaths(true);

        JsonArray result = null;
        long createTaskTime = 0L;
        long start = System.nanoTime();
        for (int i = 0; i < LIMIT; i++) {
            long taskStart = System.nanoTime();
            task = generateTask();
            createTaskTime = createTaskTime + System.nanoTime() - taskStart;
            result = task.getIn().getPaths(true);
        }
        System.out.println("JsonValue Paths -> jsonValue.getPaths(true): " + ((System.nanoTime() - start - createTaskTime) / LIMIT));

    }

    public static MyTask generateTask() {
        return new MyTask(JsonObject.EMPTY.put(UUID.randomUUID().toString(), JsonArray.of(JsonObject.EMPTY.put(UUID.randomUUID().toString(), UUID.randomUUID().toString()))).put(UUID.randomUUID().toString(), JsonObject.EMPTY.put(UUID.randomUUID().toString(), UUID.randomUUID().toString())).jsonValue());

    }


    static class MyTask extends Task<JsonValue, JsonArray, JsonValue> {

        public MyTask(JsonValue in) {
            super(in);
        }

        @Override
        public void splitIntoSubtasks(Iterator<Subtask<JsonValue, JsonArray, JsonValue>> finishedSubtasks, Collection<Subtask<JsonValue, JsonArray, JsonValue>> subtasks) {

            if (getIn().isJsonObject()) {
                Iterator<JsonValue> valuesIter = getIn().getJsonObject().values().iterator();
                for (JsonValue key : getIn().getJsonObject().keys()) {
                    subtasks.add(new Subtask<>(key, new MyTask(valuesIter.next())));
                }
            } else {
                int i = 0;
                for (JsonValue value : getIn().getJsonArray()) {
                    subtasks.add(new Subtask<>(JsonValue.of(i++), new MyTask(value)));
                }
            }
        }

        @Override
        public JsonArray getOutFromFinishedSubtasks(Collection<Subtask<JsonValue, JsonArray, JsonValue>> finishedSubtasks) {

            JsonArrayBuilder builder = JsonArray.EMPTY.builder();
            for (Subtask<JsonValue, JsonArray, JsonValue> subtask : finishedSubtasks) {
                builder.addAll(subtask.getTask().getOut().addFirstToEach(subtask.getContext()));
            }
            return builder.build();
        }

        @Override
        public JsonArray getOutFromBasecase(JsonValue in) {
            if (in.isLeaf()) {
                return JsonArray.of(JsonArray.of(in));
            }
            return null;
        }
    }

    @Test
    void getOutFromFinishedSubtasks() {
        ArrayList<Subtask<JsonValue, JsonArray, JsonValue>> finishedSubtasks = new ArrayList<>();
        MyTask finishedTask1 = new MyTask(JsonValue.NULL);
        finishedTask1.setOut(JsonArray.of(JsonArray.of("b")));

        Subtask<JsonValue, JsonArray, JsonValue> subtask = new Subtask<>(JsonValue.of("a"), finishedTask1);
        finishedSubtasks.add(subtask);

        MyTask taskToTest = new MyTask(JsonObject.EMPTY.put("a", "b").jsonValue());
        JsonArray result = taskToTest.getOutFromFinishedSubtasks(finishedSubtasks);
        assertThat(result, is(JsonArray.of(JsonArray.of("a", "b"))));

    }

    @Test
    void lab() {

    }

    private static JsonArray EMPTY_IN_EMPTY = JsonArray.of(JsonArray.EMPTY);

    public static JsonArray paths(JsonValue jsonValue, boolean includeValue) {
        if (jsonValue.isLeaf()) {
            return includeValue
                    ? JsonArray.of(JsonArray.of(jsonValue))
                    : EMPTY_IN_EMPTY;
        }
        JsonArrayBuilder builder = JsonArray.EMPTY.builder();

        if (jsonValue.isJsonObject()) {
            JsonObject jsonObject = jsonValue.getJsonObject();
            Iterator<JsonValue> jsonValueIterator = jsonObject.values().iterator();
            for (JsonValue key : jsonObject.keys()) {
                builder.addAll(paths(jsonValueIterator.next(), includeValue).addFirstToEach(key));
            }
        } else {
            if (jsonValue.isJsonArray()) {
                int i = 0;
                for (JsonValue value : jsonValue.getJsonArray()) {
                    builder.addAll(paths(value, includeValue).addFirstToEach(i++));
                }
            }
        }
        return builder.build();
    }

    public static class RecursiveCaller {

    }
}
