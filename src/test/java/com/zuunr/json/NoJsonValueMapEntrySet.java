/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import clojure.lang.PersistentHashMap;

import java.util.*;
import java.util.function.Consumer;

import static com.zuunr.json.NoJsonValueUtil.noJsonValueOf;

/**
 * @author Niklas Eldberger
 */
public class NoJsonValueMapEntrySet implements JsonObjectSupport, Set<Map.Entry<String, Object>> {

    private Set<Map.Entry<String, JsonValue>> wrappedEntrySet;

    private final JsonObject jsonObject;

    public NoJsonValueMapEntrySet(JsonValue jsonValue) {
        this.jsonObject = jsonValue.getValue(JsonObject.class);
    }

    private Set<Map.Entry<String, JsonValue>> getWrappedEntrySet() {
        if (wrappedEntrySet == null) {
            wrappedEntrySet = ((PersistentHashMap) jsonObject.persistentMap).entrySet();
        }
        return wrappedEntrySet;
    }

    @Override
    public JsonObject asJsonObject() {
        return jsonObject;
    }

    @Override
    public JsonValue asJsonValue() {
        return jsonObject.jsonValue();
    }

    @Override
    public int size() {
        return jsonObject.size();
    }

    @Override
    public boolean isEmpty() {
        return jsonObject.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        throw new RuntimeException("Not implemented!");
    }

    @Override
    public Iterator<Map.Entry<String, Object>> iterator() {
        return new EntryIterator(((PersistentHashMap) jsonObject.persistentMap).iterator());
    }

    @Override
    public Object[] toArray() {
        return Arrays.stream(getWrappedEntrySet().toArray())
                .map(entry -> {
                    Map.Entry<String, JsonValue> jsonValueEntry = (Map.Entry<String, JsonValue>) entry;
                    return new Entry(jsonValueEntry.getKey(), noJsonValueOf(jsonValueEntry.getValue()));
                }).toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return (T[]) toArray();
    }

    @Override
    public boolean add(Map.Entry<String, Object> stringObjectEntry) {
        throw new RuntimeException("Not supported!");
    }

    @Override
    public boolean remove(Object o) {
        throw new RuntimeException("Not supported!");
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new RuntimeException("Not implemented!");
    }

    @Override
    public boolean addAll(Collection<? extends Map.Entry<String, Object>> c) {
        throw new RuntimeException("Not supported!");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new RuntimeException("Not implemented!");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new RuntimeException("Not implemented!");
    }

    @Override
    public void clear() {
        throw new RuntimeException("Not implemented!");
    }

    static class EntryIterator implements Iterator<Map.Entry<String, Object>> {

        Iterator<Map.Entry<String, JsonValue>> wrappedIterator;

        public EntryIterator(Iterator<Map.Entry<String, JsonValue>> wrappedIterator) {
            this.wrappedIterator = wrappedIterator;
        }

        @Override
        public boolean hasNext() {
            return wrappedIterator.hasNext();
        }

        @Override
        public Map.Entry<String, Object> next() {
            Map.Entry<String, JsonValue> jsonValueEntry = wrappedIterator.next();
            return new Entry(jsonValueEntry.getKey(), noJsonValueOf(jsonValueEntry.getValue()));
        }

        @Override
        public void remove() {
            throw new RuntimeException("Not supported!");

        }

        @Override
        public void forEachRemaining(Consumer<? super Map.Entry<String, Object>> action) {
            throw new RuntimeException("Not implemented!");
        }
    }

    static class Entry implements Map.Entry<String, Object> {

        private final String key;
        private final Object value;
        private String asString;

        public Entry(String key, Object value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public String getKey() {
            return key;
        }

        @Override
        public Object getValue() {
            return value;
        }

        @Override
        public Object setValue(Object value) {
            throw new RuntimeException("Not supported!");
        }

        @Override
        public String toString(){
            if (asString == null) {
                asString = key + " -> " + value;
            }
            return asString;
        }
    }
}
