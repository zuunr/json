/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import com.zuunr.json.pointer.JsonPointer;
import com.zuunr.json.tool.JsonUtil;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/*
 * Created by niklas on 25/01/16.
 */
class JsonObjectTest {

    private static final Logger logger = LoggerFactory.getLogger(JsonObjectFactory.class);

    @Test
    void equalsTest() {
        assertThat(JsonObject.EMPTY.equals(JsonObject.EMPTY), is(true));
        assertThat(JsonObject.EMPTY.put("a", "a").equals(JsonObject.EMPTY.put("a", "a")), is(true));
    }

    @Test
    void simple() {
        String value = JsonObject.EMPTY
                .put("key", "value")
                .get("key")
                .getValue(String.class);
        assertThat(value, is("value"));
    }

    @Test
    void asJson() throws Exception {
        String jsonStr = JsonObject.EMPTY.put("param1", "value1").put("param2", "value2").put("array", JsonArray.EMPTY.add("olle").add(2).add(JsonObject.EMPTY.put("val", false))).asPrettyJson();

        logger.info("json: {}", jsonStr);
        assertThat(jsonStr, notNullValue());
    }

    @Test
    void testRemove() {
        JsonObject tested = JsonObject
                .EMPTY
                .put("a", JsonObject
                        .EMPTY
                        .put("a-a", JsonObject
                                .EMPTY
                                .put("a-a-b", "leaf:a-a-b")
                                .put("a-a-a", "leaf:a-a-a"))
                        .put("a-b", JsonObject
                                .EMPTY
                                .put("a-b-a", "leaf:a-b-a"))

                )
                .put("b", JsonObject
                        .EMPTY
                        .put("b-a", JsonObject
                                .EMPTY
                                .put("b-a-a", "leaf:b-a-a")));

        JsonObject whenRemoved = tested.remove(JsonArray.EMPTY.add("a").add("a-a").add("a-a-a"));
        String src = JsonUtil.asJavaSourceCode(whenRemoved.jsonValue());
        assertThat(whenRemoved.get(JsonArray.EMPTY.add("a").add("a-a").add("a-a-b")), is(JsonValue.of("leaf:a-a-b")));
        assertThat(whenRemoved.get(JsonArray.EMPTY.add("a").add("a-a").add("a-a-a")), nullValue());

        assertThat(whenRemoved.jsonValue(), is(JsonObject.EMPTY.put("a", JsonObject.EMPTY.put("a-a", JsonObject.EMPTY.put("a-a-b", "leaf:a-a-b")).put("a-b", JsonObject.EMPTY.put("a-b-a", "leaf:a-b-a"))).put("b", JsonObject.EMPTY.put("b-a", JsonObject.EMPTY.put("b-a-a", "leaf:b-a-a"))).jsonValue()));
    }

    @Test
    void testReplaceInKeys() {
        JsonObject jsonObject = JsonObject.EMPTY.put("-- something to be replaced AND SOME TO REMAIN --", "someThing");
        JsonObject expected = JsonObject.EMPTY.put("-- ... AND SOME TO REMAIN --", "someThing");

        assertThat(jsonObject.replaceInKeys("something to be replaced", "...", false), is(expected));
    }

    //@Test
    void testJsonInString() {
        String jsonKey = JsonObject.EMPTY.put("key", "value").asJson();
        String jsonValue = JsonObject.EMPTY.put("key", "value").asJson();

        String from = "{\"key\":\"value\"}";
        String to = from.replace("\"", "\\\"");//.replace(" ",""));
        //"{\"\"}"
        JsonObject jsonObject = JsonObject.EMPTY.put(jsonKey, jsonValue);
        String src = jsonObject.asJson();
        assertThat("", is("..."));
    }

    @Test
    void testHashCode() {
        JsonObject jsonObject1 = JsonObject.EMPTY.put("name", "Kalle Karlsson");
        JsonObject jsonObject2 = JsonObject.EMPTY.put("name", "Kalle Karlsson");
        assertThat(jsonObject1.hashCode(), is(jsonObject2.hashCode()));
    }

    @Test
    void testEquals() {
        JsonObject jsonObject1 = JsonObject.EMPTY.put("name", "Kalle Karlsson");
        JsonObject jsonObject2 = JsonObject.EMPTY.put("name", "Kalle Karlsson");
        assertThat(jsonObject1.equals(jsonObject2), is(true));
    }

    @Test
    void testPairs_toString() {

        String string = JsonObject.EMPTY
                .put("name", "someName").getPairs().toString();
        assertThat(string, is(JsonArray.EMPTY
                .add(JsonObject.EMPTY
                        .put("name", "someName")).toString()));
    }

    @Test
    void toStringTest() {

        String allValues = "";//{"someValue":"\n"}
        JsonObject jsonObject = JsonObject.EMPTY.put("\"", "a");// {\"k\":\"\\n\"}", "{\"someValue\":\"\\n\"}");
        String json = jsonObject.asJson();
        JsonObject newJson = new JsonObjectFactory().createJsonObject(json);
        assertThat(newJson.asJson(), is(json));
    }

    @Test
    void diffTest() {
        JsonObject diff1 = JsonObject.EMPTY
                .put("a", "b")
                .put("c", "d")
                .diff(JsonObject.EMPTY
                        .put("a", "b")
                        .put("e", JsonObject.EMPTY
                                .put("f", "g")));
        JsonObject diff2 = JsonObject.EMPTY
                .put("a", JsonArray.EMPTY
                        .add("b")
                        .add("c"))
                .diff(
                        JsonObject.EMPTY
                                .put("a", JsonArray.EMPTY
                                        .add("b")
                                        .add("c")));
        JsonObject diff3 = JsonObject.EMPTY
                .put("a", JsonArray.EMPTY
                        .add("b")
                        .add("c"))
                .diff(JsonObject.EMPTY
                        .put("a", JsonArray.EMPTY
                                .add("b")
                                .add("c")
                                .add("d")));
        JsonObject diff5 =
                JsonObject.EMPTY
                        .put("a", "b")
                        .put("e", JsonObject.EMPTY
                                .put("a", "b")
                                .put("f", "h"))
                        .diff(
                                JsonObject.EMPTY
                                        .put("a", "b")
                                        .put("e", JsonObject.EMPTY
                                                .put("a", "b")
                                                .put("f", "g")
                                                .put("i", "j")));

        assertThat(diff1, is(JsonObject.EMPTY.put("reduction", JsonObject.EMPTY.put("e", JsonObject.EMPTY.put("f", "g"))).put("expansion", JsonObject.EMPTY.put("c", "d"))));
        assertThat(diff2, is(JsonObject.EMPTY.put("reduction", JsonValue.NULL).put("expansion", JsonValue.NULL)));
        assertThat(diff3, is(JsonObject.EMPTY.put("reduction", JsonObject.EMPTY.put("a", JsonArray.EMPTY.add("b").add("c").add("d"))).put("expansion", JsonObject.EMPTY.put("a", JsonArray.EMPTY.add("b").add("c")))));
        assertThat(diff5, is(JsonObject.EMPTY.put("reduction", JsonObject.EMPTY.put("e", JsonObject.EMPTY.put("i", "j").put("f", "g"))).put("expansion", JsonObject.EMPTY.put("e", JsonObject.EMPTY.put("f", "h")))));
    }

    @Test
    void diffTest2() {
        JsonObject first = JsonUtil.create("{'a':'b','c':{'d':'e'}}");
        JsonObject second = JsonUtil.create("{'a':'b','c':{'d':'f'}}");

        JsonValue howToGetToSecondFromFirst = first.expansionComparedTo(second.jsonValue(), first.jsonValue());

        assertThat(howToGetToSecondFromFirst.getJsonObject(), is(JsonObject.EMPTY.put("c", JsonObject.EMPTY.put("d", "f"))));
    }

    @Test
    void putArrayTest() {
        JsonArray path = JsonArray.of("a", 0);
        JsonValue result = JsonObject.EMPTY.jsonValue().put(path, JsonObject.EMPTY.jsonValue());
        JsonValue expected = JsonObject.EMPTY.put("a", JsonArray.EMPTY.add(JsonObject.EMPTY)).jsonValue();
        assertThat(result, is(expected));
    }

    @Test
    void testRemove2() {
        JsonObject original = JsonObject.EMPTY.put("equals", JsonObject.EMPTY.put("paths", JsonArray.EMPTY)).put("required", true);

        JsonArray keys = original.keys();
        JsonObject result = original;
        for (int i = 0; i < keys.size(); i++) {
            result = result.remove(keys.get(i).getValue(String.class));
        }
        assertThat(result.isEmpty(), is(true));

        JsonObject resultOfRemove = original.remove("required");
        JsonObject expected = JsonObject.EMPTY.put("equals", JsonObject.EMPTY.put("paths", JsonArray.EMPTY));
        assertThat(resultOfRemove, is(expected));
    }

    @Test
    void keysAndValuesTest() {
        JsonObject jsonObject = JsonObject.EMPTY.put("key1", "value1");

        JsonArray values = jsonObject.values();
        assertThat(values, is(JsonArray.of("value1")));

        JsonArray keys = jsonObject.keys();
        assertThat(keys, is(JsonArray.of("key1")));
    }

    @Test
    void asJsonSortedTest() {
        JsonObject jsonObject = JsonObject.EMPTY.put("d", 2).put("e", "2");
        String result = jsonObject.asJsonSorted();
        String expected = "{'d':2,'e':'2'}".replaceAll("[']", "\"");
        assertThat(result, is(expected));
    }

    @Test
    void getPathThatDoesNotExistInObject() {
        JsonObject jsonObject = JsonObject.EMPTY.builder().put("a", "b").build();
        JsonArray path = JsonArray.of("c", "d", "e");
        JsonValue value = jsonObject.jsonValue().get(path, null);
        assertThat(value, nullValue());
    }

    @Test
    void orderOfFields() {
        JsonObject jsonObject = JsonObject.EMPTY;

        for (int i = 0; i < 1000; i++) {
            jsonObject = jsonObject.put(UUID.randomUUID().toString(), i);
        }

        for (int i = 0; i < 1000; i++) {
            String headKey = jsonObject.keys().head().getString();
            JsonObject smallerJsonObject = jsonObject.remove(headKey);
            // System.out.println("" + i + ": " + jsonObject.values().tail());
            // System.out.println("" + i + ": " + smallerJsonObject.values());
            assertThat("" + i, jsonObject.values().tail(), is(smallerJsonObject.values()));
            jsonObject = smallerJsonObject;
        }
    }

    @Test
    void key_with_tab() {
        JsonObject jsonObject = JsonUtil.create("{'a\\tb': true}");
        assertThat(jsonObject.get(jsonObject.keys().get(0).getString()), is(JsonValue.TRUE));
    }

    @Test
    void removeByPointerSuccess() {
        JsonObject jsonObject = JsonObject.EMPTY
                .put("a", JsonObject.EMPTY
                        .put("b", JsonArray.of(
                                JsonObject.EMPTY.put("c", "hello"),
                                JsonObject.EMPTY.put("c", "world"))));

        JsonObject expected = JsonObject.EMPTY
                .put("a", JsonObject.EMPTY
                        .put("b", JsonArray.of(
                                JsonObject.EMPTY,
                                JsonObject.EMPTY.put("c", "world"))));

        assertThat(jsonObject.remove(JsonPointer.of("/a/b/0/c")), is(expected));
        assertThat(jsonObject.remove(JsonPointer.of("/a/b")), is(JsonObject.EMPTY.put("a", JsonObject.EMPTY)));
    }

    @Test
    void removeByPointerSuccess2() {
        assertThat(JsonObject.EMPTY.put("a", "b").remove(JsonPointer.of("/notPresentInObject")), is(JsonObject.EMPTY.put("a", "b")));
        assertThat(JsonObject.EMPTY.put("", "").remove(JsonPointer.of("/")), is(JsonObject.EMPTY));
    }

    @Test
    void removeByPointerSuccess3() {
        assertThat(JsonObject.EMPTY.put("0", "b").remove(JsonPointer.of("/0")), is(JsonObject.EMPTY));
    }

    @Test
    void removeByPointerFailure() {
        JsonObject jsonObject = JsonObject.EMPTY
                .put("a", JsonObject.EMPTY
                        .put("b", JsonArray.of(
                                JsonObject.EMPTY.put("c", "hello"),
                                JsonObject.EMPTY.put("c", "world"))));

        JsonObject expected = JsonObject.EMPTY
                .put("a", JsonObject.EMPTY
                        .put("b", JsonArray.of(
                                JsonObject.EMPTY,
                                JsonObject.EMPTY.put("c", "world"))));
        try {
            assertThat(jsonObject.remove(JsonPointer.of("/a/b/0")), is(expected));
            assertThat(true, is(false));
        } catch (PointerMismatchJsonValueException e) {
            assertThat(e.getPointer(), is(JsonPointer.of("/a/b")));
        }
    }
}