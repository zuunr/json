/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import java.util.Set;
import java.util.Spliterator;

/**
 * @author Niklas Eldberger
 */
public class NoJsonValueSet extends NoJsonValueCollection implements Set<Object> {

    public NoJsonValueSet(JsonValue jsonValue) {
        super(jsonValue);
    }

    @Override
    public Spliterator<Object> spliterator() {
        return null;
    }
}
