package com.zuunr.json;

import com.zuunr.json.schema.validation.JsonSchemaValidator;
import com.zuunr.json.schema.validation.OutputStructure;
import org.junit.jupiter.api.Test;

public class ReadmeTest {

    @Test
    void jsonValueFactory() {
        JsonObject jsonObject = JsonValueFactory
                .create("{\"name\": \"Peter\"}")  // creates JsonValue
                .getJsonObject();                 // gets the JsonObject which is wrapped by the JsonValue
    }

    @Test
    void JsonSchemaExample_success() {

        JsonSchemaValidator jsonSchemaValidator = new JsonSchemaValidator();

        JsonValue jsonSchema = JsonValue.of(
                JsonObject.EMPTY
                        .put("properties", JsonObject.EMPTY
                                .put("firstName", true))
                        .put("additionalProperties", false)
        );

        JsonValue validInstance = JsonObject.EMPTY
                .put("firstName", "Peter")
                .jsonValue();

        JsonObject validationResult = jsonSchemaValidator
                .validate(
                        validInstance,    // data to be validated
                        jsonSchema,               // schema use when validating
                        OutputStructure.DETAILED  // how to present result in case of errors
                );

        System.out.println(validationResult.asJson());
    }

    @Test
    void JsonSchemaExample_error() {

        JsonSchemaValidator jsonSchemaValidator = new JsonSchemaValidator();

        JsonValue jsonSchema = JsonValue.of(
                JsonObject.EMPTY
                        .put("properties", JsonObject.EMPTY
                                .put("firstName", true))
                        .put("additionalProperties", false)
        );

        JsonValue invalidInstance = JsonObject.EMPTY
                .put("firstName", "Peter")
                .put("age", 35)
                .jsonValue();


        JsonObject validationResult = jsonSchemaValidator
                .validate(
                        invalidInstance,          // data to be validated
                        jsonSchema,               // schema use when validating
                        OutputStructure.DETAILED  // how to present result in case of errors
                );

        System.out.println(validationResult.asJson());
    }

    @Test
    void JsonSchemaExample_filter() {

        JsonSchemaValidator jsonSchemaValidator = new JsonSchemaValidator();

        JsonValue jsonSchema = JsonValue.of(
                JsonObject.EMPTY
                        .put("properties", JsonObject.EMPTY
                                .put("firstName", true))
                        .put("additionalProperties", false)
        );

        JsonValue invalidInstance = JsonObject.EMPTY
                .put("firstName", "Peter")
                .put("age", 35)
                .jsonValue();


        JsonValue filtered = jsonSchemaValidator
                .filter(
                        invalidInstance,         // data to be filtered
                        jsonSchema               // schema use when validating
                );

        System.out.println(filtered.asJson());
    }


}
