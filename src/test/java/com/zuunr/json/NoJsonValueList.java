/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import static com.zuunr.json.NoJsonValueUtil.noJsonValueOf;

/**
 * @author Niklas Eldberger
 */
public class NoJsonValueList implements List<Object>, JsonArraySupport {

    private final JsonArray jsonArray;

    public NoJsonValueList(JsonValue jsonValue) {
        jsonArray = jsonValue.getValue(JsonArray.class);
    }

    @Override
    public JsonArray asJsonArray() {
        return jsonArray;
    }

    @Override
    public JsonValue asJsonValue() {
        return jsonArray.jsonValue();
    }

    @Override
    public int size() {
        return jsonArray.size();
    }

    @Override
    public boolean isEmpty() {
        return jsonArray.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        boolean result = false;
        for (JsonValue jsonValue: jsonArray){
            if (jsonValue.getValue() == null) {
                if (o == null) {
                    result = true;
                } else {
                    result = false;
                }
            } else {
                result = jsonValue.getValue().equals(o);
            }
        }
        return result;
    }

    @Override
    public Iterator<Object> iterator() {
        return new NoJsonValueIterator(jsonArray.persistentVector, 0);
    }

    @Override
    public Object[] toArray() {
        Object[] array = new Object[jsonArray.size()];
        int i = 0;
        for (JsonValue jsonValue: jsonArray.asList()){
            array[i] = noJsonValueOf(jsonValue);
            i++;
        }
        return array;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return (T[])toArray();
    }

    @Override
    public boolean add(Object o) {
        throw new RuntimeException("Not supported!");
    }

    @Override
    public boolean remove(Object o) {
        throw new RuntimeException("Not supported!");
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new RuntimeException("Not implemented yet!");
    }

    @Override
    public boolean addAll(Collection<?> c) {
        throw new RuntimeException("Not supported!");
    }

    @Override
    public boolean addAll(int index, Collection<?> c) {
        throw new RuntimeException("Not supported!");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new RuntimeException("Not supported!");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new RuntimeException("Not supported!");
    }

    @Override
    public void clear() {
        throw new RuntimeException("Not supported!");
    }

    @Override
    public Object get(int index) {
        return noJsonValueOf(jsonArray.get(index));
    }

    @Override
    public Object set(int index, Object element) {
        throw new RuntimeException("Not supported!");
    }

    @Override
    public void add(int index, Object element) {
        throw new RuntimeException("Not supported!");
    }

    @Override
    public Object remove(int index) {
        throw new RuntimeException("Not supported!");
    }

    @Override
    public int indexOf(Object o) {
        throw new RuntimeException("Not implemented yet!");
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new RuntimeException("Not implemented yet!");
    }

    @Override
    public ListIterator<Object> listIterator() {
        return listIterator(0);
    }

    @Override
    public ListIterator<Object> listIterator(int index) {
        return new NoJsonValueIterator(jsonArray.persistentVector, index);
    }

    @Override
    public List<Object> subList(int fromIndex, int toIndex) {
        return jsonArray.as(NoJsonValueList.class).subList(fromIndex, toIndex);
    }

    @Override
    public String toString(){
        return jsonArray.toString();
    }
}
