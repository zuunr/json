/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import com.zuunr.json.pointer.JsonPointer;
import com.zuunr.json.tool.JsonUtil;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

/**
 * @author Niklas Eldberger
 */
class JsonValueTest {

    private static final JsonValueUtil jsonValueUtil = new JsonValueUtil();

    @Test
    void asSourceCode() {
        JsonValue value = JsonObject
                .EMPTY
                .put("a", JsonArray
                        .EMPTY
                        .add("b")
                        .add("c"))
                .put("d", JsonObject
                        .EMPTY
                        .put("e", "f")
                        .put("g", JsonValue
                                .NULL)
                ).jsonValue();
        String sourceCode = JsonUtil.asJavaSourceCode(value);
        assertThat(sourceCode, is("JsonObject.EMPTY.put(\"d\", JsonObject.EMPTY.put(\"e\", \"f\").put(\"g\", JsonValue.NULL)).put(\"a\", JsonArray.EMPTY.add(\"b\").add(\"c\"))"));
    }

    @Test
    void putInArrayTest() {
        JsonValue original = JsonArray
                .EMPTY.add(JsonObject.EMPTY.put("param1", "param1beforePut").put("param2", "param2beforePut")).jsonValue();
        JsonValue afterPut = original.put(JsonArray.EMPTY.add(0), JsonObject.EMPTY.put("param2", "param2afterPut").jsonValue());
        String source = JsonUtil.asJavaSourceCode(afterPut);
        assertThat(afterPut, is(JsonArray.EMPTY.add(JsonObject.EMPTY.put("param2", "param2afterPut")).jsonValue()));
        //Make put work!!!! :)
    }

    @Test
    void putTest() {
        JsonValue original = JsonObject
                .EMPTY
                .put("one", JsonObject
                        .EMPTY
                        .put("two", JsonArray
                                .EMPTY.add(JsonObject
                                        .EMPTY
                                        .put("three", "3-value")
                                        .put("four", "4-value")))).jsonValue();
        JsonValue afterPut = original.put(JsonArray.EMPTY.add("one").add("two").add(0).add("three"), JsonObject.EMPTY.put("a", "a-value").put("b", "b-value").jsonValue());
        String source = JsonUtil.asJavaSourceCode(afterPut);
        JsonValue expected = JsonObject
                .EMPTY
                .put("one", JsonObject
                        .EMPTY.put("two", JsonArray
                                .EMPTY.add(JsonObject
                                        .EMPTY
                                        .put("three", JsonObject
                                                .EMPTY
                                                .put("a", "a-value")
                                                .put("b", "b-value"))
                                        .put("four", "4-value")))).jsonValue();
        assertThat(afterPut, is(expected));
    }

    @Test
    void getPathsTest() {
        JsonArray paths = JsonObject.EMPTY.put("a", JsonArray.EMPTY.add(JsonArray.EMPTY.add("value"))).jsonValue().getPaths();

        JsonArray expectedPaths = JsonArray.of(JsonArray.of("a", 0, 0));
        assertThat(paths, is(expectedPaths));

        JsonArray paths2 = JsonObject
                .EMPTY
                .put("object1-key1", JsonObject
                        .EMPTY
                        .put("object2-key1", "object2-key1-value"))
                .put("object1-key2", JsonObject
                        .EMPTY
                        .put("object3-key1", "object3-key1-value"))
                .put("object1-key3", JsonObject
                        .EMPTY
                        .put("object4-key1", JsonArray
                                .EMPTY
                                .add(JsonObject
                                        .EMPTY
                                        .put("object5-key1", "object5-key1-value")
                                        .put("object6-key1", "object6-key1-value")

                                )
                                .add(JsonObject
                                        .EMPTY
                                        .put("object7-key1", "object7-key1-value"))
                        ))

                .jsonValue().getPaths();

        JsonArray expected = JsonArray.EMPTY
                .add(JsonArray.EMPTY.add("object1-key1").add("object2-key1"))
                .add(JsonArray.EMPTY.add("object1-key2").add("object3-key1"))
                .add(JsonArray.EMPTY.add("object1-key3").add("object4-key1").add(0).add("object5-key1"))
                .add(JsonArray.EMPTY.add("object1-key3").add("object4-key1").add(0).add("object6-key1"))
                .add(JsonArray.EMPTY.add("object1-key3").add("object4-key1").add(1).add("object7-key1"));
        assertThat(paths2.sort(), is(expected));
    }

    @Test
    void getPathsAndValues() {
        JsonObject sample0 = JsonObject.EMPTY.put("myArray", JsonArray.EMPTY);
        JsonArray result0 = sample0
                .jsonValue()
                .getPathsAndValue(true);
        assertThat(result0.get(0).get(1), is(JsonArray.EMPTY.jsonValue()));

        // null value
        JsonObject sample1 = JsonObject.EMPTY.put("myArray", JsonValue.NULL);
        JsonArray result1 = sample1.jsonValue().getPathsAndValue(true);
        assertThat(result1.get(0).get(1), is(JsonValue.NULL));
    }

    @Test
    void testEscapedCitationCharacters() {
        String value = "value may be called \"value\", but not always.\nand this is a \\d{2} new row! :) ";
        JsonValue newJsonValue = JsonValue.of(value);


        String resultingValue = newJsonValue.getValue(String.class);
        assertThat(resultingValue, is(value));

        JsonObject object = JsonObject.EMPTY.put("value", value);
        String objectAsString = object.asJson();
        JsonObjectFactory factory = new JsonObjectFactory();
        assertThat(objectAsString, is("{\"value\":\"value may be called \\\"value\\\", but not always.\\nand this is a \\\\d{2} new row! :) \"}"));
        JsonObject newObject = factory.createJsonObject(objectAsString);
        assertThat(object, is(newObject));

    }

    @Test
    void testPut() {
        JsonValue jsonValue = JsonObject.EMPTY.jsonValue();
        JsonValue newJsonValue = jsonValue.put(JsonArray.EMPTY.add("key"), JsonValue.of("value"));
        assertThat(newJsonValue, is(JsonObject.EMPTY.jsonValue().put(JsonArray.EMPTY.add("key"), JsonValue.of("value"))));

    }

    @Test
    void testPutInNulled() {
        JsonValue jsonValue = JsonObject.EMPTY.jsonValue().put(JsonArray.EMPTY.add("key"), JsonValue.NULL);
        JsonValue newJsonValue = jsonValue.put(JsonArray.EMPTY.add("key"), JsonValue.of("value"));
        assertThat(newJsonValue, is(JsonObject.EMPTY.put("key", "value").jsonValue()));
    }

    @Test
    void testRemoveRegexPath() {
        JsonValue jsonValue = JsonObject.EMPTY
                .put("POST", JsonObject.EMPTY
                        .put("DOING", JsonObject.EMPTY
                                .put("TODO", true)))
                .put("PATCH", JsonObject.EMPTY
                        .put("DOING", JsonObject.EMPTY
                                .put("TODO", true))
                        .put("$any_status", JsonObject.EMPTY
                                .put("TODO", true)))
                .jsonValue();

        JsonValue result = jsonValueUtil.removeRegexPath(jsonValue, JsonArray.EMPTY.add(".*").add("DOING|DONE|\\$any_status").add("TODO"));
        assertThat(result.getValue(JsonObject.class), is(JsonObject.EMPTY
                .put("POST", JsonObject.EMPTY.put("DOING", JsonObject.EMPTY))
                .put("PATCH", JsonObject.EMPTY
                        .put("DOING", JsonObject.EMPTY)
                        .put("$any_status", JsonObject.EMPTY))));

        JsonValue result2 = jsonValueUtil.removeRegexPath(jsonValue, JsonArray.EMPTY.add("PATCH").add("DOING"));
        assertThat(result2.getValue(JsonObject.class), is(JsonObject.EMPTY
                .put("POST", JsonObject.EMPTY
                        .put("DOING", JsonObject.EMPTY
                                .put("TODO", true)))
                .put("PATCH", JsonObject.EMPTY
                        .put("$any_status", JsonObject.EMPTY
                                .put("TODO", true)))
        ));

    }

    @Test
    void testGetRegexPath() {

        JsonValue onePath = JsonObject.EMPTY.put(JsonArray.EMPTY.add("a").add("b"), "c").jsonValue();
        assertThat(jsonValueUtil.getPathsByRegex(onePath, JsonArray.EMPTY.add(".*")), is(JsonArray.EMPTY.add(JsonArray.EMPTY.add("a"))));
        assertThat(jsonValueUtil.getPathsByRegex(onePath, JsonArray.EMPTY.add(".*").add(".*").add(".*")), is(JsonArray.EMPTY));

        JsonValue jsonValue = JsonObject.EMPTY
                .put("param1", JsonObject.EMPTY
                        .put("nestedParam1", "nestedValue1")
                        .put("nestedParam2", "nestedValue2")
                )
                .put("param2", JsonObject.EMPTY
                        .put("nestedParam1", JsonObject.EMPTY.put("nestedValue1", "evenMoreNested"))
                        .put("nestedParam2", "nestedValue2")).jsonValue();


        JsonArray path = jsonValueUtil.getPathsByRegex(jsonValue, JsonArray.EMPTY.add(".*2").add(".*1"));
        assertThat(path, is(JsonArray.EMPTY.add(JsonArray.EMPTY.add("param2").add("nestedParam1"))));
        path = jsonValueUtil.getPathsByRegex(jsonValue, JsonArray.EMPTY.add(".*2").add(".*1").add(".*"));
        assertThat(path, is(JsonArray.EMPTY.add(JsonArray.EMPTY.add("param2").add("nestedParam1").add("nestedValue1"))));

        JsonValue withArray = JsonObject
                .EMPTY
                .put("a", JsonArray
                        .EMPTY
                        .add(JsonObject
                                .EMPTY
                                .put("b", "c"))
                        .add(JsonObject
                                .EMPTY
                                .put("d", "e")))
                .put("f", JsonArray
                        .EMPTY
                        .add(JsonObject
                                .EMPTY
                                .put("g", "h"))
                        .add(JsonObject
                                .EMPTY
                                .put("i", "j"))).jsonValue();
        JsonArray paths = jsonValueUtil.getPathsByRegex(withArray, JsonArray.EMPTY.add(".*").add(-1).add("d|i"));

        assertThat(paths, is(JsonArray.EMPTY.add(JsonArray.EMPTY.add("a").add(1).add("d")).add(JsonArray.EMPTY.add("f").add(1).add("i"))));

    }

    @Test
    void nonIdempotentPutTest() {
        JsonObject object = JsonObject.EMPTY;
        JsonArray newArray = JsonArray.EMPTY.add("Stina").put(JsonArray.EMPTY.add(-1), JsonValue.of("Olle"));
        assertThat(newArray, is(JsonArray.EMPTY.add("Stina").add("Olle")));
    }

    @Test
    void asJsonOrdered() {
        JsonObject toBeSorted = JsonObject.EMPTY.put("b", JsonArray.EMPTY.add(JsonObject.EMPTY.put("first", 1)).add(JsonObject.EMPTY.put("second", 2))).put("a", "Should be the first");
        String sortedString = toBeSorted.asJsonSorted();
        assertThat(sortedString, is("{\"a\":\"Should be the first\",\"b\":[{\"first\":1},{\"second\":2}]}"));
    }

    @Test
    void replaceAll() {
        JsonObject original = JsonObject.EMPTY
                .put("a", JsonObject.EMPTY
                        .put("b", "a"))
                .put("b", JsonObject.EMPTY
                        .put("c", "d"));

        JsonValue expected = JsonObject.EMPTY
                .put("A", JsonObject.EMPTY
                        .put("b", "A"))
                .put("b", JsonObject.EMPTY
                        .put("c", "d")).jsonValue();
        JsonValue result = original.jsonValue().replaceAll("a", "A");
        assertThat(result, is(expected));
    }

    @Test
    void replaceAllKeepExisting() {
        JsonObject original = JsonObject.EMPTY
                .put("b", JsonObject.EMPTY
                        .put("c", "d"));

        JsonObject result = original.jsonValue().replaceAll("a", "A").getValue(JsonObject.class);

        assertThat(result, is(original));
    }

    @Test
    void asJsonSorted() {
        JsonObject val = JsonObject.EMPTY.put("array", JsonArray.EMPTY.add(JsonObject.EMPTY).add(JsonObject.EMPTY));
        String pretty = val.asJsonSorted();
        String expected = "{\"array\":[{},{}]}";
        assertThat(pretty, is(expected));
    }

    @Test
    void asPrettyJson() throws Exception {


        JsonObject manyAttrib = JsonObject.EMPTY;
        for (int i = 0; i < 1000; i++) {
            manyAttrib = manyAttrib.put("" + System.currentTimeMillis(), i);

        }

        JsonObject toBePretty = JsonObject.EMPTY.put("f", JsonArray.EMPTY.add(JsonObject.EMPTY.put("g", "h")).add(JsonObject.EMPTY.put("i", "j" +
                ""))).put("a", JsonArray.EMPTY.add(JsonObject.EMPTY.put("b", "c")).add(JsonObject.EMPTY.put("d", "e")));
        String pretty = toBePretty.asPrettyJson();
        
        String expected = null;

        if (isWindowsLineSeparator()) {
            expected = "\n" +
                    "\t\t{\r\n" +
                    "\t\t  \"a\" : [ {\r\n" +
                    "\t\t    \"b\" : \"c\"\r\n" +
                    "\t\t  }, {\r\n" +
                    "\t\t    \"d\" : \"e\"\r\n" +
                    "\t\t  } ],\r\n" +
                    "\t\t  \"f\" : [ {\r\n" +
                    "\t\t    \"g\" : \"h\"\r\n" +
                    "\t\t  }, {\r\n" +
                    "\t\t    \"i\" : \"j\"\r\n" +
                    "\t\t  } ]\r\n" +
                    "\t\t}";
        } else {
            expected = "\n" +
                    "\t\t{\n" +
                    "\t\t  \"a\" : [ {\n" +
                    "\t\t    \"b\" : \"c\"\n" +
                    "\t\t  }, {\n" +
                    "\t\t    \"d\" : \"e\"\n" +
                    "\t\t  } ],\n" +
                    "\t\t  \"f\" : [ {\n" +
                    "\t\t    \"g\" : \"h\"\n" +
                    "\t\t  }, {\n" +
                    "\t\t    \"i\" : \"j\"\n" +
                    "\t\t  } ]\n" +
                    "\t\t}";
        }

        assertThat(pretty, is(expected));

    }

    private boolean isWindowsLineSeparator() {
        return System.getProperty("line.separator").equals("\r\n");
    }

    @Test
    void pathsEndingWithTest() {
        JsonArray end = JsonArray.EMPTY.add("my").add("h[r]ef");

        JsonValue jsonValue = JsonObject.EMPTY
                .put("you love", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("my", JsonObject.EMPTY
                                        .put("href", "")))
                        .add(JsonArray.EMPTY
                                .add(JsonObject.EMPTY
                                        .put("my", JsonObject.EMPTY
                                                .put("href", "")))))
                .put("i love", JsonObject.EMPTY
                        .put("my", JsonObject.EMPTY
                                .put("href", "")))
                .put("my", JsonObject.EMPTY
                        .put("not your", JsonObject.EMPTY
                                .put("href", "http://localhost:8080/v1/SYSTEM/api/friends/1233212"))
                        .put("href", "http://localhost:8080/v1/SYSTEM/api/friends/1233212"))
                .put("its", JsonObject.EMPTY
                        .put("my", JsonObject.EMPTY
                                .put("href", "http://localhost:8080/v1/SYSTEM/api/friends/1233212")))
                .put("someone", JsonObject.EMPTY.put("else calling it", JsonObject.EMPTY
                        .put("my", JsonObject.EMPTY
                                .put("href", "http://localhost:8080/v1/SYSTEM/api/friends/1233212"))))
                .jsonValue();


        JsonValueUtil jsonValueUtil = new JsonValueUtil();
        JsonArray paths = jsonValueUtil.pathsEndingWith(jsonValue, end, true);

        assertThat(paths.size(), is(6));
        assertThat(paths.contains(JsonArray.EMPTY.add("someone").add("else calling it").add("my").add("href").jsonValue()), is(true));
        assertThat(paths.contains(JsonArray.EMPTY.add("my").add("href").jsonValue()), is(true));
        assertThat(paths.contains(JsonArray.EMPTY.add("its").add("my").add("href").jsonValue()), is(true));
        assertThat(paths.contains(JsonArray.EMPTY.add("i love").add("my").add("href").jsonValue()), is(true));
        assertThat(paths.contains(JsonArray.EMPTY.add("you love").add(0).add("my").add("href").jsonValue()), is(true));
        assertThat(paths.contains(JsonArray.EMPTY.add("you love").add(1).add(0).add("my").add("href").jsonValue()), is(true));

        JsonArray paths2 = jsonValueUtil.pathsEndingWith(jsonValue, JsonArray.EMPTY.add(-1).add("my").add("h[r]ef"), true);

        assertThat(paths2.size(), is(2));
        assertThat(paths2.contains(JsonArray.EMPTY.add("you love").add(0).add("my").add("href").jsonValue()), is(true));
        assertThat(paths2.contains(JsonArray.EMPTY.add("you love").add(1).add(0).add("my").add("href").jsonValue()), is(true));

        JsonValue jsonValue1 = JsonObject.EMPTY.put("http://loclahost:8080/v1/iusdu/api/collection", JsonObject.EMPTY.put("status", "OPEN")).jsonValue();

        assertThat(jsonValueUtil.pathsEndingWith(jsonValue1, JsonArray.EMPTY.add("http.*").add(".*"), true), is(JsonArray.EMPTY.add(JsonArray.EMPTY.add("http://loclahost:8080/v1/iusdu/api/collection").add("status"))));

    }

    @Test
    void pathsStartingWithTest() {

        JsonObject object = JsonObject.EMPTY
                .put("a1", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("b", "b-value")
                                .put("c", "c-value"))
                        .add(JsonObject.EMPTY
                                .put("b2", "b2-value")
                                .put("c", "c-value"))
                        .add(JsonObject.EMPTY
                                .put("b", "b-value")
                                .put("c", "c-value")))
                .put("a2", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("b", "b-value")
                                .put("c", "c-value")));
        JsonArray path = JsonArray.EMPTY.add("a1").add(-1).add("b");
        JsonArray result = new JsonValueUtil().pathsStartingWith(object.jsonValue(), path);
        JsonArray expected = JsonArray.EMPTY.add(JsonArray.EMPTY.add("a1").add(0).add("b")).add(JsonArray.EMPTY.add("a1").add(2).add("b"));
        assertThat(result, is(expected));
    }

    @Test
    void isIntegerTest() {
        assertThat(JsonValue.of(1).isInteger(), is(true));
        assertThat(JsonValue.of(Long.valueOf(1)).isInteger(), is(true));
        assertThat(JsonValue.of(Long.valueOf(Integer.MAX_VALUE)).isInteger(), is(true));
        assertThat(JsonValue.of(Long.valueOf(1L + (long) Integer.MAX_VALUE)).isInteger(), is(false));
        assertThat(JsonValue.of(Long.valueOf(Integer.MIN_VALUE)).isInteger(), is(true));
        assertThat(JsonValue.of(Long.valueOf(-1L + (long) Integer.MIN_VALUE)).isInteger(), is(false));
    }

    @Test
    void compareTo() {
        assertThat(JsonValue.of(1).compareTo(JsonValue.of(1)), is(0));
        assertThat(JsonValue.of(1).compareTo(JsonValue.of(2)), is(-1));
        assertThat(JsonValue.of(2).compareTo(JsonValue.of(1)), is(1));
        assertThat(JsonValue.of(1L).compareTo(JsonValue.of(1L)), is(0));
        assertThat(JsonValue.of(1L).compareTo(JsonValue.of(2L)), is(-1));
        assertThat(JsonValue.of(2L).compareTo(JsonValue.of(1L)), is(1));
        assertThat("a".compareTo("a"), is(0));
        assertThat("a".compareTo("c") < 0, is(true));
        assertThat("c".compareTo("a") > 0, is(true));
        assertThat(JsonValue.of("a").compareTo(JsonValue.of("a")), is(0));
        assertThat(JsonValue.of("a").compareTo(JsonValue.of("c")) < 0, is(true));
        assertThat(JsonValue.of("c").compareTo(JsonValue.of("a")) > 0, is(true));
    }

    @Test
    void testGet() {
        JsonObject source = JsonUtil.create("{'a':{'b':{'c':{'d':[{'e':'f'},{'g':true}]}}}}");
        Boolean result = source.get(JsonArray.of("a", "b", "c", "d", 1, "g")).getValue(Boolean.class);
        assertThat(result, is(true));

        JsonValue nullResult = source.get(JsonArray.of("a", "b", "c", "d", 2, "g"));
        assertThat(nullResult, nullValue());
    }

    @Test
    void isLeaf() {
        assertThat(JsonObject.EMPTY.jsonValue().isLeaf(), is(true));
        assertThat(JsonArray.EMPTY.jsonValue().isLeaf(), is(true));
        assertThat(JsonValue.of("a").isLeaf(), is(true));
        assertThat(JsonValue.of(1).isLeaf(), is(true));
        assertThat(JsonValue.NULL.isLeaf(), is(true));
        assertThat(JsonValue.TRUE.isLeaf(), is(true));
        assertThat(JsonValue.FALSE.isLeaf(), is(true));

        assertThat(JsonObject.EMPTY.put("", "").jsonValue().isLeaf(), is(false));
        assertThat(JsonArray.EMPTY.add("").jsonValue().isLeaf(), is(false));
    }

    @Test
    void putPointerSuccessTest() {
        JsonValue original = JsonObject.EMPTY.put("q", true).put("a", JsonObject.EMPTY.put("b", "c")).jsonValue();
        assertThat(original.put(JsonPointer.of("/a/b"), JsonValue.of(2)), is(JsonObject.EMPTY.put("q", true).put("a", JsonObject.EMPTY.put("b", 2)).jsonValue()));
    }

    @Test
    void putPointerSuccessInArrayTest() {
        JsonValue original = JsonObject.EMPTY.put("q", true).put("a", JsonArray.of(JsonObject.EMPTY.put("b", "c"), JsonObject.EMPTY.put("b", "c"))).jsonValue();
        JsonValue expected = JsonObject.EMPTY.put("q", true).put("a", JsonArray.of(JsonObject.EMPTY.put("b", "c"), JsonObject.EMPTY.put("b", 2))).jsonValue();
        assertThat(original.put(JsonPointer.of("/a/1/b"), JsonValue.of(2)), is(expected));
    }

    @Test
    void putPointerFailureTest() {
        JsonValue original = JsonObject.EMPTY.put("a", JsonObject.EMPTY.put("b", "c")).jsonValue();
        try {
            assertThat(original.put(JsonPointer.of("/a/b/c"), JsonValue.of(2)), is(JsonObject.EMPTY.put("a", JsonObject.EMPTY.put("b", JsonObject.EMPTY.put("c", 2))).jsonValue()));
            assertThat(true, is(false));
        } catch (PointerMismatchJsonValueException e) {
            assertThat(e.getPointer(), is(JsonPointer.of("/a/b")));
            assertThat(e.getType(), is(JsonType.String));
        }
    }

    @Test
    void getByPointerSuccess1() {
        JsonValue original = JsonObject.EMPTY.put("q", true).put("a", JsonArray.of(JsonObject.EMPTY.put("b", "c"), JsonObject.EMPTY.put("b", "c"))).jsonValue();
        assertThat(original.get(JsonPointer.of("/a/1/b")), is(JsonValue.of("c")));
    }

    @Test
    void getByPointerReturnsNull() {
        JsonValue original = JsonObject.EMPTY.put("q", true).put("a", JsonArray.of(JsonObject.EMPTY.put("b", "c"), JsonObject.EMPTY.put("b", "c"))).jsonValue();
        assertThat(original.get(JsonPointer.of("/a/1/B")), nullValue());
    }

    @Test
    void getByPointerFailure() {
        JsonValue original = JsonObject.EMPTY.put("q", true).put("a", JsonArray.of(JsonObject.EMPTY.put("b", "c"), JsonObject.EMPTY.put("b", "c"))).jsonValue();
        try {
            original.get(JsonPointer.of("/a/01/b"));
            assertThat(true, is(false));
        } catch (PointerMismatchJsonValueException e) {
            assertThat(e.getPointer(), is(JsonPointer.of("/a")));
            assertThat(e.getType(), is(JsonType.Array));
        }
    }

    @Test
    void bigDecimalToDouble() {
        assertThat(translate(new BigDecimal("12345678910112")), is(12345678910112.0));
        assertThat(translate(new BigDecimal("123.000")), is(123.000));
        assertThat(translate(new BigDecimal("123.456")), is(123.456));
        assertThat(translate(new BigDecimal("12345678.1")), is(12345678.1));
        assertThat(translate(new BigDecimal("123456789.1")), is(123456789.1));
        assertThat(translate(new BigDecimal("123456789.101")), is(123456789.101));
        assertThat(translate(new BigDecimal("123456789.1011")), is(123456789.1011));
        assertThat(translate(new BigDecimal("123456789.101112")), is(123456789.101112));
        assertThat(translate(new BigDecimal("123456789.10111213")), is(123456789.10111213));
        assertThat(translate(new BigDecimal("12345678910.1112")), is(12345678910.1112));
        assertThat(translate(new BigDecimal("1234567891011.12")), is(1234567891011.12));
        assertThat(translate(new BigDecimal("123456789101112E-2")), is(1234567891011.12));
        //assertThat(translate(new BigDecimal("123456789.1011121314")), is(123456789.1011121314));
    }

    private Double translate(BigDecimal bigDecimal) {
        Double converted = bigDecimal.doubleValue();
        if (new BigDecimal(String.valueOf(converted)).compareTo(bigDecimal) == 0) {
            return converted;
        }
        return null;
    }
}