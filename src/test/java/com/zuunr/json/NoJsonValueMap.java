/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import static com.zuunr.json.NoJsonValueUtil.noJsonValueOf;

/**
 * @author Niklas Eldberger
 */
class NoJsonValueMap implements JsonObjectSupport, Map<String, Object> {

    private final JsonObject jsonObject;

    public NoJsonValueMap(JsonValue jsonValue) {
        this.jsonObject = jsonValue.getValue(JsonObject.class);
    }

    @Override
    public JsonObject asJsonObject() {
        return jsonObject;
    }

    @Override
    public JsonValue asJsonValue() {
        return jsonObject.jsonValue();
    }

    @Override
    public int size() {
        return jsonObject.size();
    }

    @Override
    public boolean isEmpty() {
        return jsonObject.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        if (key.getClass() == String.class) {
            return jsonObject.containsKey((String) key);
        } else {
            return false;
        }
    }

    @Override
    public boolean containsValue(Object value) {
        return jsonObject.values().as(NoJsonValueList.class).contains(value);
    }

    @Override
    public Object get(Object key) {
        return noJsonValueOf(jsonObject.get((String)key));
    }

    @Override
    public Object put(String key, Object value) {
        throw new RuntimeException("Method put is not supported. Map is immutable.");
    }

    @Override
    public Object remove(Object key) {
        throw new RuntimeException("Method remove is not supported. Map is immutable.");
    }

    @Override
    public void putAll(Map<? extends String, ?> m) {
        throw new RuntimeException("Method putAll is not supported. Map is immutable.");

    }

    @Override
    public void clear() {
        throw new RuntimeException("Method clear is not supported. Map is immutable.");

    }

    @Override
    public Set<String> keySet() {
        return jsonObject.keySet();
    }

    @Override
    public Collection<Object> values() {
        return jsonObject.values().as(NoJsonValueList.class);
    }

    @Override
    public Set<Map.Entry<String, Object>> entrySet() {
        return jsonObject.as(NoJsonValueMapEntrySet.class);
    }
}
