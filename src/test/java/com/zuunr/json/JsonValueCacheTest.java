/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.junit.jupiter.api.Test;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
class JsonValueCacheTest {

    @Test
    void testPersistent(){
        JsonObject empty1 = JsonObject.EMPTY;
        JsonObject empty2 = JsonObject.EMPTY.put("a", "a").remove("a");
        assertThat(empty1.persistentMap, is(empty2.persistentMap));

    }

    // @Test
    void makeSureNonReferencedAreRemoved(){
        Cache<String, JsonValue> cache = Caffeine.newBuilder()
                .expireAfterAccess(1000000, TimeUnit.DAYS)
                //.maximumSize(Long.MAX_VALUE)
                .softValues()
                .build();

        JsonValue uuid0 = new JsonValue(UUID.randomUUID().toString());
        String key0 = new String(uuid0.getString());
        cache.put(uuid0.getString(), uuid0);
        uuid0 = null;
        for (long i = 0; i < Long.MAX_VALUE; i++){
            JsonValue uuid = new JsonValue(UUID.randomUUID().toString());
            cache.put(uuid.getString(), uuid);
            if (i % 1000000 == 0) {
                System.out.println("cache size: " + cache.estimatedSize());
                System.out.println("uuid0 from cache: " + cache.getIfPresent(key0));
            }
        }
    }
}
