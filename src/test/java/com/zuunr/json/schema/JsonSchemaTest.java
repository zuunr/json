package com.zuunr.json.schema;


import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.pointer.JsonPointer;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


public class JsonSchemaTest {

    @Test
    void test1() {
        JsonSchema schema = JsonObject.EMPTY
                .put("properties", JsonObject.EMPTY
                        .put("nested", JsonObject.EMPTY
                                .put("$ref", "#/defs/ReferencedSchema")))
                .put("defs", JsonObject.EMPTY
                        .put("ReferencedSchema", JsonObject.EMPTY.put("const", "CONSTANT_VALUE"))).as(JsonSchema.class);
        JsonPointer keywordLocation = JsonValue.of("#/properties/nested/$ref/const").as(JsonPointer.class);
        assertThat(schema.get(keywordLocation, true), is(JsonValue.of("CONSTANT_VALUE")));
    }

    @Test
    void test2() {
        JsonSchema schema = JsonObject.EMPTY
                .put("properties", JsonObject.EMPTY
                        .put("nested", JsonObject.EMPTY
                                .put("$ref", "#/defs/ReferencedSchema")))
                .put("defs", JsonObject.EMPTY
                        .put("ReferencedSchema", JsonObject.EMPTY
                                .put("allOf", JsonArray.of(
                                        JsonObject.EMPTY
                                                .put("const", "CONSTANT_VALUE"))))).as(JsonSchema.class);
        JsonPointer keywordLocation = JsonValue.of("#/properties/nested/$ref/allOf/0/const").as(JsonPointer.class);
        assertThat(schema.get(keywordLocation, true), is(JsonValue.of("CONSTANT_VALUE")));
    }
}
