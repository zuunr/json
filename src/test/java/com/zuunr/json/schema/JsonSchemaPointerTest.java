package com.zuunr.json.schema;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonValue;
import com.zuunr.json.pointer.JsonPointer;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
public class JsonSchemaPointerTest {

    @Test
    void split0(){
        JsonArray result = JsonSchemaPointer.split("#//", '/', 0);
        assertThat(result, is(JsonArray.of("#","")));
    }

    @Test
    void split1(){
        JsonArray result = JsonSchemaPointer.split("//", '/', 1);
        assertThat(result, is(JsonArray.of("")));
    }

    @Test
    void split3(){
        JsonArray result = JsonSchemaPointer.split("/13/abc/", '/', 1);
        assertThat(result, is(JsonArray.of(13, "abc")));
    }

    @Test
    void asArray1(){
        JsonValue pointer = JsonValue.of("/");
        JsonArray result = pointer.as(JsonPointer.class).asArray();
        assertThat(result, is(JsonArray.of("")));
    }

    @Test
    void asArray2(){
        JsonValue pointer = JsonValue.of("//");
        JsonArray result = pointer.as(JsonPointer.class).asArray();
        assertThat(result, is(JsonArray.of("","")));
    }

    @Test
    void asArray3(){
        JsonValue pointer = JsonValue.of("/abc/");
        JsonArray result = pointer.as(JsonPointer.class).asArray();
        assertThat(result, is(JsonArray.of("abc","")));
    }

    @Test
    void asArray4(){
        JsonValue pointer = JsonValue.of("/abc/def");
        JsonArray result = pointer.as(JsonPointer.class).asArray();
        assertThat(result, is(JsonArray.of("abc","def")));
    }

    @Test
    void asArray5(){
        JsonValue pointer = JsonValue.of("/abc");
        JsonArray result = pointer.as(JsonPointer.class).asArray();
        assertThat(result, is(JsonArray.of("abc")));
    }
}
