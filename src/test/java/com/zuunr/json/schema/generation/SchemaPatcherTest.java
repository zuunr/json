/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json.schema.generation;

import com.zuunr.json.JsonObject;
import com.zuunr.json.tool.JsonUtil;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
class SchemaPatcherTest {

    private final SchemaPatcher schemaPatcher = new SchemaPatcher();

    @Test
    void test1() {

        JsonObject schema = JsonObject.EMPTY
                .put("type", "object")
                .put("properties", JsonObject.EMPTY
                        .put("name", JsonObject.EMPTY
                                .put("properties", JsonObject.EMPTY
                                        .put("first", JsonObject.EMPTY
                                                .put("type", "string")
                                                .put("minLength", 3)

                                        )
                                        .put("last", JsonObject.EMPTY
                                                .put("type", "string")
                                        )))
                        .put("birthDate", JsonObject.EMPTY
                                .put("type", "string")
                                .put("lexicalMin", "1900-01-01")
                ));

        JsonObject overrider = JsonObject.EMPTY
                .put("type", "object")
                .put("properties", JsonObject.EMPTY
                        .put("name", JsonObject.EMPTY
                                .put("properties", JsonObject.EMPTY
                                        .put("first", JsonObject.EMPTY
                                                .put("type", "string")
                                                .put("minLength", 0)
                                                .put("maxLength", 20)
                                        )
                                        .put("middle", JsonObject.EMPTY
                                                .put("type", "string")
                                                .put("pattern", "^Andersson$")
                                        )

                                ))
                        .put("birthDate", JsonObject.EMPTY
                                .put("type", "string")
                                .put("lexicalMin", "1900-01-01")
                        ));

        JsonObject expectedResult = JsonObject.EMPTY
                .put("type", "object")
                .put("properties", JsonObject.EMPTY
                        .put("name", JsonObject.EMPTY
                                .put("properties", JsonObject.EMPTY
                                        .put("first", JsonObject.EMPTY
                                                .put("type", "string")
                                                .put("minLength", 0)
                                                .put("maxLength", 20)

                                        )
                                        .put("last", JsonObject.EMPTY
                                                .put("type", "string")
                                        )))
                        .put("birthDate", JsonObject.EMPTY
                                .put("type", "string")
                                .put("lexicalMin", "1900-01-01")
                        ));

        JsonObject result = schemaPatcher.patch(schema, overrider);
        assertThat(result, is(expectedResult));
    }
}