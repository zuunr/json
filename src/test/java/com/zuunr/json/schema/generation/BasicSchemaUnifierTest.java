package com.zuunr.json.schema.generation;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.JsonValueFactory;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

/**
 * @author Niklas Eldberger
 */
public class BasicSchemaUnifierTest {

    @Test
    void _const_unionOfEqualConst() {
        SchemaUnifier schemaUnifier = new BasicSchemaUnifier();
        JsonValue union = schemaUnifier.unionOf(JsonObject.EMPTY.put("const", "A").jsonValue(), JsonObject.EMPTY.put("const", "A").jsonValue());
        assertThat(union.getJsonObject().get("const").getString(), is("A"));
    }

    @Test
    void _const_unionOfAandB() {
        SchemaUnifier schemaUnifier = new BasicSchemaUnifier();
        JsonValue union = schemaUnifier.unionOf(JsonObject.EMPTY.put("const", "B").jsonValue(), JsonObject.EMPTY.put("const", "A").jsonValue());
        assertThat(union.getJsonObject().get("const"), nullValue());
        assertThat(union.getJsonObject().get("enum").getJsonArray(), is(JsonArray.of("A", "B")));
    }

    @Test
    void _const_unionOfAandEnumB() {
        SchemaUnifier schemaUnifier = new BasicSchemaUnifier();
        JsonValue union = schemaUnifier.unionOf(JsonObject.EMPTY.put("enum", JsonArray.of("B")).jsonValue(), JsonObject.EMPTY.put("const", "A").jsonValue());
        assertThat(union.getJsonObject().get("const"), nullValue());
        assertThat(union.getJsonObject().get("enum").getJsonArray(), is(JsonArray.of("A", "B")));
    }

    @Test
    void _const_unionOfAandEnumBor1() {
        SchemaUnifier schemaUnifier = new BasicSchemaUnifier();
        JsonValue union = schemaUnifier.unionOf(JsonObject.EMPTY.put("enum", JsonArray.of("B", 1)).jsonValue(), JsonObject.EMPTY.put("const", "A").jsonValue());
        assertThat(union.getJsonObject().get("const"), nullValue());
        assertThat(union.getJsonObject().get("enum").getJsonArray(), is(JsonArray.of(1, "A", "B")));
    }

    @Test
    void defs_test() {
        JsonValue schema1 = JsonObject.EMPTY.put("$defs", JsonObject.EMPTY.put("TypeA", true).put("TypeB", JsonObject.EMPTY.put("type", "string"))).jsonValue();
        JsonValue schema2 = JsonObject.EMPTY.put("$defs", JsonObject.EMPTY.put("TypeA", false).put("TypeC", JsonObject.EMPTY.put("type", "integer"))).jsonValue();
        SchemaUnifier schemaUnifier = new BasicSchemaUnifier();
        assertThat(schemaUnifier.unionOf(schema1, schema2).get("$defs").get("TypeA"), is(JsonValue.TRUE));
        assertThat(schemaUnifier.unionOf(schema1, schema2).get("$defs").get("TypeA"), is(JsonValue.TRUE));
        assertThat(schemaUnifier.unionOf(schema2, schema1).get("$defs").get("TypeB"), is(JsonObject.EMPTY.put("type", "string").jsonValue()));
        assertThat(schemaUnifier.unionOf(schema2, schema1).get("$defs").get("TypeC"), is(JsonObject.EMPTY.put("type", "integer").jsonValue()));
    }

    @Test
    void additionalProperties() {
        String jsonString = "{\n" +
                "  \"schema1\": {\n" +
                "    \"type\": \"object\",\n" +
                "    \"properties\": {\n" +
                "      \"a\": {\n" +
                "        \"const\": \"A\"\n" +
                "      }\n" +
                "    },\n" +
                "    \"additionalProperties\": {\n" +
                "      \"enum\": [\n" +
                "        \"C\"\n" +
                "      ]\n" +
                "    }\n" +
                "  },\n" +
                "  \"schema2\": {\n" +
                "    \"type\": \"object\",\n" +
                "    \"properties\": {\n" +
                "      \"b\": {\n" +
                "        \"const\": \"B\"\n" +
                "      }\n" +
                "    },\n" +
                "    \"additionalProperties\": {\n" +
                "      \"enum\": [\n" +
                "        \"D\"\n" +
                "      ]\n" +
                "    }\n" +
                "  },\n" +
                "  \"expectedSchema\": {\n" +
                "    \"properties\": {\n" +
                "      \"a\": {\n" +
                "        \"enum\": [\n" +
                "          \"A\",\n" +
                "          \"D\"\n" +
                "        ]\n" +
                "      },\n" +
                "      \"b\": {\n" +
                "        \"enum\": [\n" +
                "          \"B\",\n" +
                "          \"C\"\n" +
                "        ]\n" +
                "      }\n" +
                "    },\n" +
                "    \"additionalProperties\": {\n" +
                "      \"enum\": [\n" +
                "        \"C\",\"D\"\n" +
                "      ]\n" +
                "    }\n" +
                "  }\n" +
                "}";
        JsonValue testData = JsonValueFactory.create(jsonString.replace("'", "\""));
        BasicSchemaUnifier basicSchemaUnifier = new BasicSchemaUnifier();
        JsonValue resultingSchema = basicSchemaUnifier.unionOf(
                testData.get("schema1"),
                testData.get("schema2")
        );
        JsonValue expected = JsonValueFactory.create("{\"properties\":{\"a\":{\"enum\":[\"A\",\"D\"]},\"b\":{\"enum\":[\"B\",\"C\"]}},\"type\":\"object\",\"additionalProperties\":{\"enum\":[\"C\",\"D\"]}}");
        assertThat(resultingSchema, is(expected));
    }
}
