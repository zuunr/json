package com.zuunr.json.schema.generation;

import com.zuunr.json.*;
import com.zuunr.json.pointer.JsonPointer;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static com.zuunr.json.schema.JsonSchema.TYPE_INTEGER_NULLABLE;
import static com.zuunr.json.schema.Keywords.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

/**
 * @author Niklas Eldberger
 */
public class SchemaGeneratorTest {

    SchemaGenerator schemaGenerator = new SchemaGenerator();
    SchemaUnifier schemaUnifier = new BasicSchemaUnifier();

    @Test
    void null1() {
        JsonObject schema = schemaGenerator.generateSchema(JsonValue.NULL);
        assertThat(schema.get(TYPE), is(JsonValue.of("null")));
    }

    @Test
    void null2() {
        JsonObject schema = schemaGenerator.generateSchema(JsonArray.of(1, JsonValue.NULL).jsonValue());
        assertThat(schema.get(ITEMS).get(TYPE).getJsonArray(), is(TYPE_INTEGER_NULLABLE));
    }

    @Test
    void integerAndStringType() {
        JsonObject schema = schemaGenerator.generateSchema(JsonArray.of(1, 2, "Pete", "Laura").jsonValue());
        assertThat(schema.get(ITEMS).get(TYPE).getJsonArray().contains("integer"), is(true));
        assertThat(schema.get(ITEMS).get(TYPE).getJsonArray().contains("string"), is(true));
        assertThat(schema.get(ITEMS).get(MAX_LENGTH), is(JsonValue.of(5)));
        assertThat(schema.get(ITEMS).get(MIN_LENGTH), is(JsonValue.of(4)));
        assertThat(schema.get(ITEMS).get(MAXIMUM), is(JsonValue.of(2)));
        assertThat(schema.get(ITEMS).get(MINIMUM), is(JsonValue.of(1)));
    }

    @Test
    void string1() {
        JsonObject schema = schemaGenerator.generateSchema(JsonValue.of("name"));
        assertThat(schema.get("type"), is(JsonValue.of("string")));
        assertThat(schema.get("minLength"), is(JsonValue.of(4)));
        assertThat(schema.get("maxLength"), is(JsonValue.of(4)));
    }

    @Test
    void stringWithConst() {
        SchemaGenerator generator = new SchemaGenerator(JsonObject.EMPTY
                .put("keywordMappers", JsonArray.of(JsonObject.EMPTY
                        .put("instanceValue", JsonObject.EMPTY.put("type", "string").put("pattern", "^[A-Z_]+$"))
                        .put("keywords", JsonObject.EMPTY.put(CONST, true))
                ))
                .put("keywordDefaults", JsonObject.EMPTY));
        JsonObject schema = generator.generateSchema(JsonValue.of("OLLE"));
        assertThat(schema.get("const"), is(JsonValue.of("OLLE")));
        JsonObject schema2 = generator.generateSchema(JsonValue.of("olle"));
        assertThat(schema2.get("const"), nullValue());
    }

    @Test
    void stringWithConstAndRef() {
        SchemaGenerator generator = new SchemaGenerator(JsonObject.EMPTY
                .put("keywordMappers", JsonArray.of(JsonObject.EMPTY
                        .put("instanceValue", JsonObject.EMPTY.put("type", "string").put("pattern", "^[A-Z_]+$"))
                        .put("keywords", JsonObject.EMPTY.put(CONST, JsonObject.EMPTY))
                ))
                .put("keywordDefaults", JsonObject.EMPTY.put(REF, true)));
        JsonObject schema = generator.generateSchema(JsonValue.of("OLLE"));
        assertThat(schema.get(JsonPointer.of("/$defs/root/const")), is(JsonValue.of("OLLE")));
        assertThat(schema.get(JsonPointer.of("/$ref")), is(JsonValue.of("/$defs/root")));
    }

    @Test
    void minAndMaxProperties() {
        SchemaGenerator schemaGenerator = new SchemaGenerator(JsonObject.EMPTY.put("keywordDefaults", JsonObject.EMPTY.put("minProperties", JsonObject.EMPTY).put("maxProperties", JsonObject.EMPTY)));
        JsonObject schema = schemaGenerator.generateSchema(JsonObject.EMPTY.put("a", 1).put("b", 1).put("c", 1));
        assertThat(schema.get("maxProperties"), is(JsonValue.of(3)));
        assertThat(schema.get("minProperties"), is(JsonValue.of(3)));
    }

    @Test
    void additionalProperties() {
        SchemaGenerator schemaGenerator = new SchemaGenerator(
                JsonObject.EMPTY.put("keywordDefaults", JsonObject.EMPTY.put("additionalProperties", JsonObject.EMPTY))
        );
        JsonObject schema = schemaGenerator.generateSchema(JsonObject.EMPTY.put("a", 1).put("b", 1).put("c", 1));
        assertThat(schema.get("additionalProperties"), is(JsonObject.EMPTY.jsonValue()));
    }

    //@Test
    void stringInObject1() {

        JsonObject config = JsonObject.EMPTY
                .put("keywordMappers", JsonArray.of(
                        JsonObject.EMPTY
                                .put("instanceLocation", "/name$")
                                .put("keywords", JsonObject.EMPTY.put("$ref", "/$defs/name")),
                        JsonObject.EMPTY
                                .put("instanceLocation", "^/?(.+)$")
                                .put("instanceValue", JsonObject.EMPTY.put("type", "string")),
                        JsonObject.EMPTY
                                .put("instanceLocation", "^/?(.*)$")
                                .put("instanceValue", JsonObject.EMPTY
                                        .put("type", "object")
                                        .put("required", JsonArray.of("href")))
                                .put("keywords", JsonObject.EMPTY
                                        .put("$ref", JsonObject.EMPTY
                                                .put("string", "{{$/instanceValue/href}}")
                                                .put("pattern", "^(https?[:][/][/][^/]+)?[/]([^?]*)[/][^/?]+([?].*)?")
                                                .put("template", "/$defs/$2Item"))),
                        JsonObject.EMPTY
                                .put("instanceLocation", "^/?(.*)$")
                                .put("instanceValue", JsonObject.EMPTY
                                        .put("type", "object")
                                        .put("properties", JsonObject.EMPTY.put("href", false)))
                                .put("keywords", JsonObject.EMPTY
                                        .put("$ref", "/$defs/personWithoutHref")),
                        JsonObject.EMPTY
                                .put("instanceLocation", "^$")
                                .put("keywords", JsonObject.EMPTY
                                        .put("$ref", "/$defs/root")))
                ).put("keywordDefaults", JsonObject.EMPTY
                        .put("required", true)
                        .put("minLength", true)
                        .put("maxLength", true)
                );

        SchemaGenerator schemaGenerator = new SchemaGenerator(config);
        JsonValue instance = JsonArray.of(
                JsonObject.EMPTY.put("name", "Laura").jsonValue(),
                JsonObject.EMPTY.put("name", "Anne").put("href", "https://example.com/persons/abc123").jsonValue(),
                JsonObject.EMPTY.put("name", "Peter the poop").jsonValue()
        ).jsonValue();
        JsonObject schema = schemaGenerator.generateSchema(instance);
        JsonObject expectedSchema = JsonValueFactory
                .create("{'$ref':'/$defs/root','$defs':{'root':{'properties':{'name':{'$ref':'/$defs/name'}},'required':['name'],'type':'object','additionalProperties':false},'name':{'minLength':4,'maxLength':4,'type':'string'}}}".replace("'", "\""))
                .getJsonObject();
        assertThat(schema, is(expectedSchema));
    }

    @Test
    void object2() {
        SchemaGenerator schemaGenerator = new SchemaGenerator(
                JsonObject.EMPTY
                        .put("keywordMappers", JsonArray.of(
                                JsonObject.EMPTY
                                        .put("instanceLocation", "^$")
                                        .put("keywords", JsonObject.EMPTY
                                                .put("$ref", "/$defs/root")),
                                JsonObject.EMPTY
                                        .put("instanceLocation", "^/?(.*)$")
                                        .put("keywords", JsonObject.EMPTY
                                                .put("$ref", "/$defs/$1"))))
                        .put("keywordDefaults", JsonObject.EMPTY
                        ));

        JsonValue instance = JsonObject.EMPTY
                .put("name", "Peter").jsonValue();

        JsonObject schema = schemaGenerator.generateSchema(instance);

        JsonObject expectedSchema = JsonValueFactory
                .create("{\"$ref\":\"/$defs/root\",\"$defs\":{\"root\":{\"properties\":{\"name\":{\"$ref\":\"/$defs/name\"}},\"type\":\"object\",\"additionalProperties\":false},\"name\":{\"type\":\"string\"}}}")
                .getJsonObject();
        assertThat(schema, is(expectedSchema));
    }

    @Test
    void objectAndArray1() {

        SchemaGenerator schemaGenerator = new SchemaGenerator(
                JsonObject.EMPTY
                        .put("keywordMappers", JsonArray.of(
                                JsonObject.EMPTY
                                        .put("instanceLocation", "^$")
                                        .put("keywords", JsonObject.EMPTY
                                                .put("$ref", "/$defs/root")),
                                JsonObject.EMPTY
                                        .put("instanceLocation", "^/?(.*)$")
                                        .put("instanceValue", JsonObject.EMPTY
                                                .put("type", JsonArray.of("object", "array")))
                                        .put("keywords", JsonObject.EMPTY
                                                .put("$ref", "/$defs/$1"))))
                        .put("keywordDefaults", JsonObject.EMPTY
                                //.put("required", JsonObject.EMPTY)
                                //.put("minLength", JsonObject.EMPTY)
                                //.put("maxLength", JsonObject.EMPTY)
                                //.put("minItems", JsonObject.EMPTY)
                                //.put("maxItems", JsonObject.EMPTY)
                        ));

        JsonValue instance = JsonObject.EMPTY.put("contacts", JsonArray.of(
                JsonObject.EMPTY
                        .put("name", "Peter")
                        .put("phone", "+4673999888"),
                JsonObject.EMPTY
                        .put("name", "Anne")
                        .put("phone", "+467312233400"))
        ).jsonValue();

        JsonObject schema = schemaGenerator.generateSchema(instance);

        JsonObject expectedSchema = JsonValueFactory
                .create("{\"$ref\":\"/$defs/root\",\"$defs\":{\"contacts\":{\"items\":{\"$ref\":\"/$defs/contacts_item\"},\"type\":\"array\"},\"root\":{\"properties\":{\"contacts\":{\"$ref\":\"/$defs/contacts\"}},\"type\":\"object\",\"additionalProperties\":false},\"contacts_item\":{\"properties\":{\"name\":{\"type\":\"string\"},\"phone\":{\"type\":\"string\"}},\"type\":\"object\",\"additionalProperties\":false}}}")
                .getJsonObject();
        assertThat(schema, is(expectedSchema));
    }


    @Test
    void array() {

        SchemaGenerator schemaGenerator = new SchemaGenerator(
                JsonObject.EMPTY
                        .put("keywordMappers", JsonArray.of(
                                JsonObject.EMPTY
                                        .put("instanceLocation", "^$")
                                        .put("keywords", JsonObject.EMPTY
                                                .put("$ref", "/$defs/company")),
                                JsonObject.EMPTY
                                        .put("instanceLocation", "^/?(.*)$")
                                        .put("keywords", JsonObject.EMPTY
                                                .put("$ref", "/$defs/$1"))

                        ))
                        .put("keywordDefaults", JsonObject.EMPTY));

        JsonValue instance = JsonArray.of("A", "B").jsonValue();
        JsonObject schema = schemaGenerator.generateSchema(instance);

        JsonObject expectedSchema = JsonValueFactory
                .create("{\"$ref\":\"/$defs/company\",\"$defs\":{\"company\":{\"items\":{\"$ref\":\"/$defs/item\"},\"type\":\"array\"},\"item\":{\"type\":\"string\"}}}")
                .getJsonObject();
        assertThat(schema, is(expectedSchema));
    }

    @Test
    void string2() {
        ExtensionsGenerator extensionsGenerator = new ExtensionsGenerator() {

            private final BasicSchemaPerceptor basicExampleToSchemaPerceptor = new BasicSchemaPerceptor();

            @Override
            public JsonObject generateObjectSchema(JsonValue rootValue, JsonArray path, JsonObject jsonObject, JsonObject pathsPerValue, KeywordMapperAndMatcher keywordMapperAndMatcher) {
                return basicExampleToSchemaPerceptor.generateObjectSchema(rootValue, path, jsonObject, pathsPerValue, keywordMapperAndMatcher);
            }

            @Override
            public JsonObject generateProperties(JsonValue rootValue, JsonArray path, JsonObject jsonObject, JsonObject pathsPerValue) {
                return basicExampleToSchemaPerceptor.generateProperties(rootValue, path, jsonObject, pathsPerValue);
            }

            @Override
            public JsonObject generateStringSchema(JsonValue rootValue, JsonArray path, String stringValue, JsonObject pathsPerValue) {
                JsonObject keywords = basicExampleToSchemaPerceptor.generateStringSchema(rootValue, path, stringValue, pathsPerValue);

                if (path.last().getString().equals("href")) {
                    keywords = keywords.put(MAX_LENGTH, 1000);
                }
                return keywords;
            }

            @Override
            public JsonObject generateNumericSchema(JsonValue rootValue, JsonArray path, JsonNumber numberValue, JsonObject pathsPerValue) {
                return basicExampleToSchemaPerceptor.generateNumericSchema(rootValue, path, numberValue, pathsPerValue);
            }

            @Override
            public SchemaUpdate unionOfKeywords(JsonObject schema1, JsonObject schema2) {
                return new SchemaUpdate(JsonObject.EMPTY, null);
            }

            @Override
            public JsonObject generateArraySchema(JsonValue rootValue, JsonArray path, JsonArray jsonArray, JsonObject pathsPerValue) {
                return basicExampleToSchemaPerceptor.generateArraySchema(rootValue, path, jsonArray, pathsPerValue);
            }

            @Override
            public JsonValue generateItemsSchema(JsonValue rootValue, JsonArray path, JsonArray jsonArrayInstance, JsonObject pathsPerValue) {
                return null;
            }

            @Override
            public JsonObject generateBooleanSchema(JsonValue rootValue, JsonArray path, Boolean value, JsonObject pathsPerValue) {
                return null;
            }
        };
        SchemaGenerator schemaGenerator = new SchemaGenerator(extensionsGenerator, schemaUnifier, new KeywordMapper());
        JsonObject schema = schemaGenerator.generateSchema(JsonArray.of(JsonObject.EMPTY.put("href", "https://example.com").jsonValue()));
        assertThat(schema.get(ITEMS).get(PROPERTIES).get("href").get(TYPE), is(JsonValue.of("string")));
        assertThat(schema.get(ITEMS).get(PROPERTIES).get("href").get(MIN_LENGTH), is(JsonValue.of("https://example.com".length())));
        assertThat(schema.get(ITEMS).get(PROPERTIES).get("href").get(MAX_LENGTH), is(JsonValue.of(1000)));
    }

    @Test
    void string3() {
        SchemaPerceptor schemaPerceptor = new SchemaPerceptor() {

            final BasicSchemaPerceptor basicExampleToSchemaPerceptor = new BasicSchemaPerceptor();

            @Override
            public JsonObject generateObjectSchema(JsonValue rootValue, JsonArray path, JsonObject jsonObject, JsonObject pathsPerValue, KeywordMapperAndMatcher keywordMapperAndMatcher) {
                return basicExampleToSchemaPerceptor.generateObjectSchema(rootValue, path, jsonObject, pathsPerValue, keywordMapperAndMatcher);
            }

            @Override
            public JsonObject generateProperties(JsonValue rootValue, JsonArray path, JsonObject jsonObject, JsonObject pathsPerValue) {
                return basicExampleToSchemaPerceptor.generateProperties(rootValue, path, jsonObject, pathsPerValue);
            }

            @Override
            public JsonObject generateStringSchema(JsonValue rootValue, JsonArray path, String stringValue, JsonObject pathsPerValue) {
                JsonObject keywords = basicExampleToSchemaPerceptor.generateStringSchema(rootValue, path, stringValue, pathsPerValue);
                keywords = keywords.put(LEXICAL_MAX, stringValue).put(LEXICAL_MIN, stringValue);
                return keywords;
            }

            @Override
            public JsonObject generateNumericSchema(JsonValue rootValue, JsonArray path, JsonNumber numberValue, JsonObject pathsPerValue) {
                return basicExampleToSchemaPerceptor.generateNumericSchema(rootValue, path, numberValue, pathsPerValue);
            }

            @Override
            public JsonObject generateArraySchema(JsonValue rootValue, JsonArray path, JsonArray jsonArray, JsonObject pathsPerValue) {
                return basicExampleToSchemaPerceptor.generateArraySchema(rootValue, path, jsonArray, pathsPerValue);
            }

            @Override
            public JsonValue generateItemsSchema(JsonValue rootValue, JsonArray path, JsonArray jsonArrayInstance, JsonObject pathsPerValue) {
                return null;
            }

            @Override
            public JsonObject generateBooleanSchema(JsonValue rootValue, JsonArray path, Boolean value, JsonObject pathsPerValue) {
                return null;
            }
        };

        SchemaUnifier specialSchemaUnifier = new LexicalBasicSchemaUnifier();

        SchemaGenerator schemaGenerator = new SchemaGenerator(schemaPerceptor, specialSchemaUnifier, new KeywordMapper());
        JsonObject schema = schemaGenerator.generateSchema(JsonArray.of(
                JsonObject.EMPTY.put("stringNumber", "30"),
                JsonObject.EMPTY.put("stringNumber", "1000"),
                JsonObject.EMPTY.put("stringNumber", "200")));
        assertThat(schema.get(ITEMS).get(PROPERTIES).get("stringNumber").get(TYPE), is(JsonValue.of("string")));
        assertThat(schema.get(ITEMS).get(PROPERTIES).get("stringNumber").get(LEXICAL_MAX), is(JsonValue.of("30")));
        assertThat(schema.get(ITEMS).get(PROPERTIES).get("stringNumber").get(LEXICAL_MIN), is(JsonValue.of("1000")));
    }

    @Test
    void string4() {

        SchemaGenerator mySchemaGenerator = new SchemaGenerator(new StatusPerceptor(), new BasicSchemaUnifier(), new KeywordMapper());

        JsonObject schema2 = mySchemaGenerator.generateSchema(JsonArray.of(JsonObject.EMPTY.put("status", "TODO"), JsonObject.EMPTY.put("status", "DONE")));
        assertThat(schema2.get(ITEMS).get(PROPERTIES).get("status").get(TYPE), is(JsonValue.of("string")));
        assertThat(schema2.get(ITEMS).get(PROPERTIES).get("status").get(ENUM).getJsonArray(), is(JsonArray.of("DONE", "TODO")));

        JsonObject schema3 = mySchemaGenerator.generateSchema(JsonArray.of(JsonObject.EMPTY.put("status", "todo"), JsonObject.EMPTY.put("status", "DONE")));
        assertThat(schema3.get(ITEMS).get(PROPERTIES).get("status").get(TYPE), is(JsonValue.of("string")));
        assertThat(schema3.get(ITEMS).get(PROPERTIES).get("status").get(CONST), nullValue());
        assertThat(schema3.get(ITEMS).get(PROPERTIES).get("status").get(ENUM), nullValue());
        assertThat(schema3.get(ITEMS).get(PROPERTIES).get("status").get(PATTERN).getString(), is("DONE|todo"));
    }

    @Test
    void string5() {
        SchemaGenerator schemaGenerator = new SchemaGenerator(
                new KeywordMapper(JsonObject.EMPTY
                        .put("keywordDefaults", KeywordMapper.KEYWORD_DEFAULTS
                                .put("maxLength", JsonObject.EMPTY))));
        JsonObject schema = schemaGenerator.generateSchema(JsonValue.of("name"));
        assertThat(schema.get("type"), is(JsonValue.of("string")));
        assertThat(schema.get("minLength"), is(JsonValue.of(4)));
        assertThat(schema.get("maxLength"), is(JsonValue.of(4)));
    }

    @Test
    void array1() {
        JsonObject schema = schemaGenerator.generateSchema(JsonArray.of("a", "aa").jsonValue());
        assertThat(schema.get("type"), is(JsonValue.of("array")));
        assertThat(schema.get("items").get("type"), is(JsonValue.of("string")));
    }

    @Test
    void arrayAndInteger1() {
        JsonObject schema = schemaGenerator.generateSchema(JsonArray.of(1, 2).jsonValue());
        assertThat(schema.get(TYPE), is(JsonValue.of(ARRAY)));
        assertThat(schema.get(ITEMS).get(TYPE), is(JsonValue.of("integer")));
        assertThat(schema.get(ITEMS).get(MINIMUM), is(JsonValue.of(1)));
        assertThat(schema.get(ITEMS).get(MAXIMUM), is(JsonValue.of(2)));
        assertThat(schema.get(UNIQUE_ITEMS), is(JsonValue.TRUE));
    }

    @Test
    void arrayAndArray1() {
        JsonObject schema = schemaGenerator.generateSchema(JsonArray.of(JsonArray.of(1), JsonArray.of(4, 4)).jsonValue());
        assertThat(schema.get("type"), is(JsonValue.of("array")));
        assertThat(schema.get("items").get("type"), is(JsonValue.of("array")));
        assertThat(schema.get("items").get("items").get("type"), is(JsonValue.of("integer")));
        assertThat(schema.get("items").get("items").get("minimum"), is(JsonValue.of(1)));
        assertThat(schema.get("items").get("items").get("maximum"), is(JsonValue.of(4)));
        assertThat(schema.get(ITEMS).get(MIN_ITEMS), is(JsonValue.of(1)));
        assertThat(schema.get(ITEMS).get(MAX_ITEMS), is(JsonValue.of(2)));
        assertThat(schema.get(ITEMS).get(UNIQUE_ITEMS), nullValue());
    }

    @Test
    void arrayAndBigDecimal1() {
        JsonObject schema = schemaGenerator.generateSchema(
                JsonArray.of(
                                new BigDecimal("1.01"),
                                new BigDecimal("2.02"))
                        .jsonValue());
        assertThat(schema.get("type").getString(), is("array"));
        assertThat(schema.get("items").get("type"), is(JsonValue.of("number")));
        assertThat(schema.get("items").get("minimum"), is(JsonValue.of(new BigDecimal("1.01"))));
        assertThat(schema.get("items").get("maximum"), is(JsonValue.of(new BigDecimal("2.02"))));
    }

    @Test
    void arrayAndObject1() {
        JsonObject schema = schemaGenerator.generateSchema(
                JsonArray.of(
                                JsonObject.EMPTY.put("name", "Pete"),
                                JsonObject.EMPTY.put("name", "Laura"))
                        .jsonValue());
        assertThat(schema.get("type"), is(JsonValue.of("array")));
        assertThat(schema.get("items").get("type"), is(JsonValue.of("object")));
        assertThat(schema.get("items").get("properties").get("name").get("type"), is(JsonValue.of("string")));
        assertThat(schema.get("items").get("properties").get("name").get("minLength"), is(JsonValue.of(4)));
        assertThat(schema.get("items").get("properties").get("name").get("maxLength"), is(JsonValue.of(5)));
    }

    @Test
    void boolean1() {
        JsonValue jsonValue = JsonValue.TRUE;
        JsonObject schema = schemaGenerator.generateSchema(jsonValue);
        assertThat(schema, is(JsonObject.EMPTY.put("type", JsonValue.of("boolean"))));
    }

    @Test
    void bigDecimal1() {
        JsonValue jsonValue = JsonValue.of(new BigDecimal("187136487126487126312873681236812763817245671235371238172368172547123681276387126381451.8726348772364872364823674"));
        JsonObject schema = schemaGenerator.generateSchema(jsonValue);
        assertThat(schema.get("type"), is(JsonValue.of("number")));
        assertThat(schema.get("minimum").getBigDecimal(), is(jsonValue.getBigDecimal()));
        assertThat(schema.get("maximum").getBigDecimal(), is(jsonValue.getBigDecimal()));
    }

    @Test
    void integer1() {
        JsonValue jsonValue = JsonValue.of(187136487126487L);
        JsonObject schema = schemaGenerator.generateSchema(jsonValue);
        assertThat(schema.get("type"), is(JsonValue.of("integer")));
        assertThat(schema.get("minimum").getLong(), is(jsonValue.getLong()));
        assertThat(schema.get("maximum").getLong(), is(jsonValue.getLong()));
    }

    @Test
    void object1() {
        JsonValue jsonValue = JsonObject.EMPTY.put("name", "Peter").jsonValue();
        JsonObject schema = schemaGenerator.generateSchema(jsonValue);
        assertThat(schema.get("type"), is(JsonValue.of("object")));
        assertThat(schema.get("properties").get("name").get("type"), is(JsonValue.of("string")));
    }

    static class LexicalBasicSchemaUnifier extends BasicSchemaUnifier {
        @Override
        public JsonValue unionOfStringType(JsonValue schema1, JsonValue schema2, JsonObject unionSchemaSoFar) {
            JsonValue union = super.unionOfStringType(schema1, schema2, unionSchemaSoFar);

            if (union.isBoolean()) {
                return union;
            }

            JsonObject unionKeywords = union.getJsonObject();

            JsonValue naturalMax1 = schema1.get(LEXICAL_MAX);
            JsonValue naturalMax2 = schema2.get(LEXICAL_MAX);
            JsonValue naturalMaxUnion = unionOfMaximum(naturalMax1, naturalMax2);
            if (naturalMaxUnion != null) {
                unionKeywords = unionKeywords.put(LEXICAL_MAX, naturalMaxUnion);
            }

            JsonValue naturalMin1 = schema1.get(LEXICAL_MIN);
            JsonValue naturalMin2 = schema2.get(LEXICAL_MIN);
            JsonValue naturalMinUnion = unionOfMinimum(naturalMin1, naturalMin2);
            if (naturalMinUnion != null) {
                unionKeywords = unionKeywords.put(LEXICAL_MIN, naturalMinUnion);
            }
            return unionKeywords.jsonValue();
        }
    }

    static class StatusPerceptor extends BasicSchemaPerceptor {
        @Override
        public JsonObject generateStringSchema(JsonValue rootValue, JsonArray path, String stringValue, JsonObject pathsPerValue) {
            JsonObject schema = super.generateStringSchema(rootValue, path, stringValue, pathsPerValue);
            if (JsonValue.of("status").equals(path.last())) {
                if (stringValue.toUpperCase().equals(stringValue)) {
                    schema = schema.put("const", stringValue);
                } else {
                    schema = schema.put("pattern", stringValue);
                }
            }
            return schema;
        }
    }

    @Test
    void generateNullSchema() {
        JsonValue jsonValue = JsonValue.NULL;
        JsonObject schema = schemaGenerator.generateSchema(jsonValue);
        assertThat(schema.get("type"), is(JsonValue.of("null")));
        assertThat(schema.get("const"), nullValue());
    }

    @Test
    void generateNullSchemaWithConst() {
        JsonValue jsonValue = JsonValue.NULL;
        SchemaGenerator schemaGenerator = new SchemaGenerator(JsonObject.EMPTY.put("keywordDefaults", JsonObject.EMPTY.put("const", true)));
        JsonObject schema = schemaGenerator.generateSchema(jsonValue);
        assertThat(schema.get("type"), is(JsonValue.of("null")));
        assertThat(schema.get("const"), is(JsonValue.NULL));
    }

    @Test
    void generateStringSchemaWithDefaultLimit() {
        JsonValue jsonValue = JsonValue.of("11_chars...");
        SchemaGenerator schemaGenerator = new SchemaGenerator(
                JsonObject.EMPTY
                        .put("keywordDefaults", JsonObject.EMPTY
                                .put("maxLength", JsonArray.of(10, 50, 100))
                                .put("minLength", JsonArray.of(100, 50, 10))
                        ));
        JsonObject schema = schemaGenerator.generateSchema(jsonValue);
        assertThat(schema.get("type"), is(JsonValue.of("string")));
        assertThat(schema.get("maxLength"), is(JsonValue.of(50)));
        assertThat(schema.get("minLength"), is(JsonValue.of(10)));
    }

    @Test
    void sameTypeForDifferentNestedObjects() {
        JsonObject config = JsonObject.EMPTY.put("keywordDefaults", JsonObject.EMPTY).put("keywordMappers", JsonArray.of(
                JsonObject
                        .EMPTY.put("instanceValue", JsonObject.EMPTY
                                .put("type", "object"))
                        .put("keywords", JsonObject.EMPTY
                                .put("$ref", JsonObject.EMPTY
                                        .put("template", "/$defs/THE_OBJECT"))),
                JsonObject
                        .EMPTY.put("instanceValue", JsonObject.EMPTY
                                .put("type", "string"))
                        .put("keywords", JsonObject.EMPTY
                                .put("const", true))));

        JsonObject schema = new SchemaGenerator(config).generateSchema(JsonObject.EMPTY.put("gender", "MALE").put("nested", JsonObject.EMPTY.put("gender", "FEMALE")));

        JsonValue expectedResult = JsonValueFactory.create("{'$ref':'/$defs/THE_OBJECT','$defs':{'THE_OBJECT':{'properties':{'gender':{'type':'string','enum':['FEMALE','MALE']},'nested':{'$ref':'/$defs/THE_OBJECT'}},'type':'object','additionalProperties':false}}}".replace("'", "\""));
        assertThat(schema.jsonValue(), is(expectedResult));
    }
}