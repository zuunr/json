package com.zuunr.json.schema.generation;

import com.zuunr.json.pointer.JsonPointer;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class KeywordMapperTest {

    @Test
    void test1(){
        assertThat(KeywordMapper.createDefinitionName("https://example.com"), is("https:__example.com"));
    }
}
