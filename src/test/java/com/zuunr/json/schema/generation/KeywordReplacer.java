package com.zuunr.json.schema.generation;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArrayBuilder;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectBuilder;
import com.zuunr.json.JsonValue;

import java.util.Iterator;

import static com.zuunr.json.schema.Keywords.ALL_OF;
import static com.zuunr.json.schema.Keywords.ANY_OF;
import static com.zuunr.json.schema.Keywords.ITEMS;
import static com.zuunr.json.schema.Keywords.KEYWORDS_WITHOUT_SCHEMAS;
import static com.zuunr.json.schema.Keywords.ONE_OF;
import static com.zuunr.json.schema.Keywords.PROPERTIES;
import static com.zuunr.json.schema.Keywords.TITLE;

/**
 * @author Niklas Eldberger
 */
public class KeywordReplacer {

    private static final JsonArray ADDITIONAL_SCHEMA_KEYWORDS = JsonArray.of(ANY_OF, ALL_OF, ONE_OF);

    private final JsonValue removeMe = JsonValue.NULL;


    public JsonObject replaceKeywords(JsonObject schema, JsonObject replacement) {
        JsonObjectBuilder schemaBuilder = schema.builder();
        replaceProperties(schema, replacement, schemaBuilder);
        replaceItems(schema, replacement, schemaBuilder);
        replaceKeywordsWithoutSchemas(schema, replacement, schemaBuilder);
        replaceAdditionalSchemas(schema, replacement, schemaBuilder);
        return schemaBuilder.build();
    }

    private void replaceAdditionalSchemas(JsonObject schema, JsonObject replacement, JsonObjectBuilder schemaBuilder) {

        for (JsonValue keywordJsonValue : ADDITIONAL_SCHEMA_KEYWORDS) {
            String keyword = keywordJsonValue.getString();
            JsonValue replacementValue = replacement.get(keyword);
            if (replacementValue != null) {
                JsonValue schemaValue = schema.get(keyword);
                if (schemaValue != null) {
                    schemaBuilder.put(keyword, replaceArrayOfSchemas(schemaValue.getJsonArray(), replacementValue.getJsonArray()));
                }
            }
        }
    }

    private JsonArray replaceArrayOfSchemas(JsonArray schemaArray, JsonArray replacementArray) {

        JsonArrayBuilder builder = JsonArray.EMPTY.builder();
        for (int i = 0; i < schemaArray.size(); i++) {
            JsonValue schemaItem = schemaArray.get(i);
            for (JsonValue replacementItem : replacementArray) {
                if (matchSchemaItems(replacementItem, schemaItem)) {
                    schemaItem = replaceKeywords(schemaItem.getJsonObject(), replacementItem.getJsonObject()).jsonValue();
                    break;
                }
            }
            builder.add(schemaItem);
        }
        return builder.build();
    }

    protected boolean matchSchemaItems(JsonValue schemaItem1, JsonValue schemaItem2) {
        if (schemaItem1.isJsonObject() && schemaItem2.isJsonObject()) {
            JsonValue title1 = schemaItem1.get(TITLE);
            return title1 != null && title1.equals(schemaItem2.get(TITLE));
        }
        return false;
    }


    private void replaceKeywordsWithoutSchemas(JsonObject schema, JsonObject replacement, JsonObjectBuilder schemaBuilder) {
        for (JsonValue keywordJsonValue : KEYWORDS_WITHOUT_SCHEMAS) {
            String keyword = keywordJsonValue.getString();
            JsonValue replacementValue = replacement.get(keyword);
            if (replacementValue != null) {
                if (isRemove(replacementValue)) {
                    schemaBuilder.remove(keyword);
                } else if (schema.get(keyword) != null) {
                    schemaBuilder.put(keyword, replacementValue);
                }
            }
        }

    }

    private void replaceItems(JsonObject schema, JsonObject replacement, JsonObjectBuilder schemaBuilder) {
        JsonValue replacementItems = replacement.get(ITEMS);
        if (replacementItems == null) {
            return;
        } else if (replacementItems.isJsonObject()) {
            JsonValue schemaItems = schema.get(ITEMS);
            if (schemaItems != null) {
                if (schemaItems.isJsonObject()) {
                    schemaBuilder.put(ITEMS, replaceKeywords(schemaItems.getJsonObject(), replacementItems.getJsonObject()));
                }
            }
        } else if (replacementItems.isJsonArray()) {
            throw new RuntimeException("Items as list is not implemented yet");
        } else if (replacementItems.isBoolean()) {
            JsonValue schemaItems = schema.get(ITEMS);
            if (schemaItems != null) {
                schemaBuilder.put(ITEMS, replacementItems);
            }
        }
    }

    private boolean isRemove(JsonValue replacementValue) {
        return removeMe.equals(replacementValue);
    }


    private void replace(String keyword, JsonObject schema, JsonObject replacement, JsonObjectBuilder schemaBuilder) {
        JsonValue keywordReplacement = replacement.get(keyword);
        if (keywordReplacement != null && schema.get(keyword) != null) {
            schemaBuilder.put(keyword, keywordReplacement);
        }
    }

    private void replaceProperties(JsonObject schema, JsonObject replacement, JsonObjectBuilder schemaBuilder) {
        JsonObject replacementProperties = replacement.get(PROPERTIES, JsonValue.NULL).getJsonObject();
        if (replacementProperties != null) {
            JsonObject schemaProperties = schema.get(PROPERTIES, JsonValue.NULL).getJsonObject();
            if (schemaProperties != null) {
                schemaBuilder.put(PROPERTIES, replacementPropertiesKeywords(replacementProperties, schemaProperties));
            }
        }
    }

    private JsonObject replacementPropertiesKeywords(JsonObject replacementProperties, JsonObject schemaProperties) {
        JsonObjectBuilder updatedSchemaProperties = schemaProperties.builder();
        Iterator<JsonValue> replacementValuesIter = replacementProperties.values().iterator();
        for (Iterator<JsonValue> replacementKeyIter = replacementProperties.keys().iterator(); replacementKeyIter.hasNext(); ) {
            String replacementKey = replacementKeyIter.next().getString();
            JsonValue replacementValue = replacementValuesIter.next();

            JsonValue schemaPropertyValue = schemaProperties.get(replacementKey);
            if (schemaPropertyValue != null && schemaPropertyValue.isJsonObject() && replacementValue.isJsonObject()) {
                updatedSchemaProperties.put(replacementKey, replaceKeywords(schemaPropertyValue.getJsonObject(), replacementValue.getJsonObject()));
            }
        }
        return updatedSchemaProperties.build();
    }


}
