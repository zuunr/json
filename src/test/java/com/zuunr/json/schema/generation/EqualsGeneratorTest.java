package com.zuunr.json.schema.generation;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.test.GivenWhen;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
public class EqualsGeneratorTest implements GivenWhen {

    EqualsGenerator equalsGenerator = new EqualsGenerator();

    @Test
    void test1() {

        JsonObject jsonObject = JsonObject.EMPTY
                .put("familyName", "Andersson")
                .put("children", JsonArray.of(
                        JsonObject.EMPTY.put("firstName", "Peter").put("lastName", "Andersson"),
                        JsonObject.EMPTY.put("firstName", "Laura").put("lastName", "Andersson")));

        JsonObject pathsByValue = equalsGenerator.equalValuePaths(jsonObject.jsonValue(), "-1");

        assertThat(pathsByValue.get(JsonValue.of("Andersson").asJson()).getJsonArray().contains(JsonArray.of("children", -1, "lastName").jsonValue()), is(true));
        assertThat(pathsByValue.get(JsonValue.of("Andersson").asJson()).getJsonArray().contains(JsonArray.of("familyName").jsonValue()), is(true));
    }

    @Test
    void test2() {

        JsonObject jsonObject = JsonObject.EMPTY
                .put("familyName", "Andersson")
                .put("children", JsonArray.of(
                        JsonObject.EMPTY.put("firstName", "Peter").put("lastName", "Andersson"),
                        JsonObject.EMPTY.put("firstName", "Laura").put("lastName", "Andersson"),
                        JsonObject.EMPTY.put("firstName", "Robert")));

        JsonObject pathsByValue = equalsGenerator.equalValuePaths(jsonObject.jsonValue(), "-1");

        assertThat(pathsByValue.get(JsonValue.of("Andersson").asJson()).getJsonArray().contains(JsonArray.of("children", -1, "lastName").jsonValue()), is(true));
        assertThat(pathsByValue.get(JsonValue.of("Andersson").asJson()).getJsonArray().contains(JsonArray.of("familyName").jsonValue()), is(true));
    }


    @Override
    public JsonValue doGivenWhen(JsonValue given, JsonValue when) {
        return equalsGenerator.pathsByValue(when).jsonValue();
    }
}
