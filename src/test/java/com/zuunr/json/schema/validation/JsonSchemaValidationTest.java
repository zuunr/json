package com.zuunr.json.schema.validation;

import com.zuunr.json.*;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.tool.JsonUtil;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

/**
 * @author Niklas Eldberger
 */
public class JsonSchemaValidationTest {

    JsonSchemaValidator validator = new JsonSchemaValidator(true);
    JsonObjectFactory jsonObjectFactory = new JsonObjectFactory();

    private void executeTest() {

        String methodName = new Throwable().getStackTrace()[1].getMethodName();
        JsonObject testCase = jsonObjectFactory.createJsonObject(this.getClass().getResourceAsStream(methodName + ".json"));
        assertThat(methodName, validator.validate(testCase.get("when"), testCase.get("given").as(JsonSchema.class), OutputStructure.DETAILED).jsonValue(), is(testCase.get("then")));
    }

    @Test
    void trueSchema() {
        assertThat(validator.validate(JsonValue.of("a"), JsonValue.TRUE, OutputStructure.FLAG).get("valid").getBoolean(), is(true));
    }

    @Test
    void emptySchema() {
        JsonValue instance = JsonValue.of("a");

        JsonValue schema = JsonObject.EMPTY.jsonValue();

        assertThat(validator.validate(instance, schema, OutputStructure.FLAG).get("valid").getBoolean(), is(true));
        assertThat(validator.validate(instance, schema, OutputStructure.DETAILED).get("valid").getBoolean(), is(true));
    }

    @Test
    void falseSchema() {
        JsonValue instance = JsonValue.of("a");

        JsonValue schema = JsonValue.FALSE;

        assertThat(validator.validate(instance, schema, OutputStructure.FLAG).get("valid").getBoolean(), is(false));
    }

    @Test
    void falseSchemaOutputStructureDetailed() {
        JsonObject error = validator.validate(JsonValue.of("a"), JsonValue.FALSE, OutputStructure.DETAILED);

        assertThat(error.get("valid").getBoolean(), is(false));
        assertThat(error.get("instanceLocation").getString(), is(""));
    }

    @Test
    void falseSchemaOutputStructureDetailed2() {
        JsonObject error = validator.validate(
                JsonObject.EMPTY
                        .put("a", 1)
                        .put("b", 2).jsonValue(),
                JsonObject.EMPTY
                        .put("type", JsonArray.of("object"))
                        .put("required", JsonArray.of("c"))
                        .put("properties", JsonObject.EMPTY
                                .put("a", false)
                        )
                        .put("patternProperties", JsonObject.EMPTY
                                .put("a", JsonObject.EMPTY.put("minimum", 10))
                        )
                        .put("additionalProperties", false)
                        .jsonValue(),
                OutputStructure.DETAILED);

        assertThat(error.get("valid").getBoolean(), is(false));
        assertThat(error.get(JsonArray.of("errors", 0, "keywordLocation")).getString(), is("/required"));
        assertThat(error.get(JsonArray.of("errors", 0, "instanceLocation")).getString(), is(""));
        assertThat(error.get(JsonArray.of("errors", 1, "errors", 0, "keywordLocation")).getString(), is("/properties/a"));
        assertThat(error.get(JsonArray.of("errors", 1, "errors", 0, "instanceLocation")).getString(), is("/a"));
        assertThat(error.get(JsonArray.of("errors", 1, "errors", 1, "keywordLocation")).getString(), is("/patternProperties/a/minimum"));
        assertThat(error.get(JsonArray.of("errors", 1, "errors", 1, "instanceLocation")).getString(), is("/a"));
        assertThat(error.get(JsonArray.of("errors", 2, "keywordLocation")).getString(), is("/additionalProperties"));
        assertThat(error.get(JsonArray.of("errors", 2, "instanceLocation")).getString(), is("/b"));
    }

    @Test
    void enumValid() {
        JsonObject result = validator.validate(JsonValue.of("TWO"), JsonObject.EMPTY.put("enum", JsonArray.of("ONE", "TWO")).jsonValue(), OutputStructure.FLAG);
        assertThat(result.get("valid").getBoolean(), is(true));
    }

    @Test
    void enumInvalidOutputStructureDetailed() {
        JsonObject result = validator.validate(
                JsonValue.of("THREE"),
                JsonObject.EMPTY.put("enum", JsonArray.of("ONE", "TWO")).jsonValue(),
                OutputStructure.DETAILED);

        assertThat(result.get("valid").getBoolean(), is(false));
        assertThat(result.get("instanceLocation").getString(), is(""));
        assertThat(result.get("keywordLocation").getString(), is(""));

        assertThat(result.get(JsonArray.of("errors", 0, "valid")).getBoolean(), is(false));
        assertThat(result.get(JsonArray.of("errors", 0, "instanceLocation")).getString(), is(""));
        assertThat(result.get(JsonArray.of("errors", 0, "keywordLocation")).getString(), is("/enum"));
        assertThat(result.get(JsonArray.of("errors", 0, "keywordValue")).getJsonArray(), is(JsonArray.of("ONE", "TWO")));
    }

    @Test
    void anyOf_empty() {
        JsonValue value = JsonValue.of("whatever");

        JsonValue schema = JsonObject.EMPTY
                .put("anyOf", JsonArray.EMPTY).jsonValue();

        assertThat(validator.validate(value, schema, OutputStructure.FLAG).get("valid").getBoolean(), is(false));
    }

    @Test
    void allOf_valid() {
        JsonValue value = JsonValue.of("whatever");

        JsonValue schema = JsonObject.EMPTY
                .put("allOf", JsonArray.of(true, true)).jsonValue();

        assertThat(validator.validate(value, schema, OutputStructure.FLAG).get("valid").getBoolean(), is(true));
    }

    @Test
    void allOf_invalid() {
        JsonValue value = JsonValue.of("whatever");

        JsonValue schema = JsonObject.EMPTY
                .put("allOf", JsonArray.of(true, false)).jsonValue();

        assertThat(validator.validate(value, schema, OutputStructure.FLAG).get("valid").getBoolean(), is(false));
    }

    @Test
    void oneOf_valid() {
        JsonValue value = JsonValue.of("whatever");

        JsonValue schema = JsonObject.EMPTY
                .put("oneOf", JsonArray.of(true, false)).jsonValue();

        assertThat(validator.validate(value, schema, OutputStructure.FLAG).get("valid").getBoolean(), is(true));
    }

    @Test
    void oneOf_invalid() {
        JsonValue value = JsonValue.of("whatever");

        JsonValue schema = JsonObject.EMPTY
                .put("oneOf", JsonArray.of(true, true)).jsonValue();

        assertThat(validator.validate(value, schema, OutputStructure.FLAG).get("valid").getBoolean(), is(false));
    }

    @Test
    void allOfAndOneOfCombined_invalid() {
        JsonValue value = JsonValue.of("whatever");

        JsonValue schema = JsonObject.EMPTY
                .put("oneOf", JsonArray.of(false, true))
                .put("allOf", JsonArray.of(true, false))
                .jsonValue();

        assertThat(validator.validate(value, schema, OutputStructure.FLAG).get("valid").getBoolean(), is(false));
    }

    @Test
    void oneOf_invalid2() {
        JsonValue value = JsonValue.of("whatever");

        JsonValue schema = JsonObject.EMPTY
                .put("oneOf", JsonArray.of(false, false)).jsonValue();

        assertThat(validator.validate(value, schema, OutputStructure.FLAG).get("valid").getBoolean(), is(false));
    }

    @Test
    void anyOf_empty_outputStructureDetailed() {
        JsonValue value = JsonValue.of("whatever");

        JsonValue schema = JsonObject.EMPTY
                .put("anyOf", JsonArray.EMPTY).jsonValue();

        JsonObject error = validator.validate(value, schema, OutputStructure.DETAILED);
        assertThat(error.get("valid").getBoolean(), is(false));
        assertThat(error.get("instanceLocation").getString(), is(""));
    }

    @Test
    void test4() {
        JsonValue value = JsonValue.of("TWO");

        JsonValue schema = JsonObject.EMPTY
                .put("anyOf", JsonArray.of(false, true)).jsonValue();

        assertThat(validator.validate(value, schema, OutputStructure.FLAG).get("valid").getBoolean(), is(true));
    }

    @Test
    void test5() {
        JsonValue value = JsonValue.of("TWO");

        JsonValue schema = JsonObject.EMPTY
                .put("anyOf", JsonArray.of(false)).jsonValue();

        assertThat(validator.validate(value, schema, OutputStructure.FLAG).get("valid").getBoolean(), is(false));
    }

    @Test
    void test6() {
        assertThat(validator.validate(JsonValue.of("B"), JsonObject.EMPTY
                .put("anyOf", JsonArray.of(
                        JsonObject.EMPTY
                                .put("enum", JsonArray.of("A")),
                        JsonObject.EMPTY
                                .put("enum", JsonArray.of("B")))).jsonValue(), OutputStructure.FLAG).get("valid").getBoolean(), is(true));
    }

    @Test
    void test7() {
        assertThat(validator.validate(JsonValue.of("C"), JsonObject.EMPTY
                .put("anyOf", JsonArray.of(
                        JsonObject.EMPTY
                                .put("enum", JsonArray.of("A")),
                        JsonObject.EMPTY
                                .put("enum", JsonArray.of("B")))).jsonValue(), OutputStructure.FLAG).get("valid").getBoolean(), is(false));
    }

    @Test
    void testTypeNullOk() {
        assertThat(validator.validate(JsonValue.NULL, JsonObject.EMPTY
                .put("type", JsonArray.of("null")).jsonValue(), OutputStructure.FLAG).get("valid").getBoolean(), is(true));
    }

    @Test
    void testTypeNullFails() {
        assertThat(validator.validate(JsonValue.of("C"), JsonObject.EMPTY
                .put("type", JsonArray.of("null")).jsonValue(), OutputStructure.FLAG).get("valid").getBoolean(), is(false));
    }

    @Test
    void testTypeStringOk() {
        assertThat(validator.validate(JsonValue.of("C"), JsonObject.EMPTY
                .put("type", JsonArray.of("null", "string")).jsonValue(), OutputStructure.FLAG).get("valid").getBoolean(), is(true));
    }

    @Test
    void testPatternStringOk() {
        assertThat(validator.validate(JsonValue.of("MATCH"), JsonObject.EMPTY
                .put("type", JsonArray.of("string"))
                .put("pattern", "MATCH")
                .jsonValue(), OutputStructure.FLAG).get("valid").getBoolean(), is(true));
    }

    @Test
    void testPatternStringNotOk() {
        assertThat(validator.validate(JsonValue.of("MATCH"), JsonObject.EMPTY
                .put("type", JsonArray.of("string"))
                .put("pattern", "NO MATCH")
                .jsonValue(), OutputStructure.FLAG).get("valid").getBoolean(), is(false));
    }

    @Test
    void testMinLengthStringOk() {
        assertThat(validator.validate(JsonValue.of("MATCH"), JsonObject.EMPTY
                .put("type", JsonArray.of("string"))
                .put("minLength", 2)
                .jsonValue(), OutputStructure.FLAG).get("valid").getBoolean(), is(true));
    }

    @Test
    void testMinLengthStringNotOk() {
        assertThat(validator.validate(JsonValue.of("MATCH"), JsonObject.EMPTY
                .put("type", JsonArray.of("string"))
                .put("minLength", 10)
                .jsonValue(), OutputStructure.FLAG).get("valid").getBoolean(), is(false));
    }


    @Test
    void testTypeStringFails() {
        assertThat(validator.validate(JsonValue.of("C"), JsonObject.EMPTY
                .put("type", JsonArray.of("null")).jsonValue(), OutputStructure.FLAG).get("valid").getBoolean(), is(false));
    }

    @Test
    void testTypeIntegerOk() {
        assertThat(validator.validate(JsonValue.of(1L), JsonObject.EMPTY
                .put("type", JsonArray.of("null", "integer")).jsonValue(), OutputStructure.FLAG).get("valid").getBoolean(), is(true));
    }

    @Test
    void testTypeIntegerFails() {
        assertThat(validator.validate(JsonValue.of(1L), JsonObject.EMPTY
                .put("type", JsonArray.of("null")).jsonValue(), OutputStructure.FLAG).get("valid").getBoolean(), is(false));
    }

    @Test
    void testTypeNumberOk() {
        assertThat(validator.validate(JsonValue.of(new BigDecimal("1.1")), JsonObject.EMPTY
                .put("type", JsonArray.of("null", "number")).jsonValue(), OutputStructure.FLAG).get("valid").getBoolean(), is(true));
    }

    @Test
    void testTypeNumberFails() {
        assertThat(validator.validate(JsonValue.of(new BigDecimal("1.1")), JsonObject.EMPTY
                .put("type", JsonArray.of("integer")).jsonValue(), OutputStructure.FLAG).get("valid").getBoolean(), is(false));
    }

    @Test
    void testTypeBooleanOk() {
        assertThat(validator.validate(JsonValue.of(true), JsonObject.EMPTY
                .put("type", JsonArray.of("boolean")).jsonValue(), OutputStructure.FLAG).get("valid").getBoolean(), is(true));
    }

    @Test
    void testTypeBooleanFails() {
        assertThat(validator.validate(JsonValue.of(1L), JsonObject.EMPTY
                .put("type", JsonArray.of("string")).jsonValue(), OutputStructure.FLAG).get("valid").getBoolean(), is(false));
    }

    @Test
    void testTypeArrayOk() {
        assertThat(validator.validate(JsonArray.EMPTY.jsonValue(), JsonObject.EMPTY
                .put("type", JsonArray.of("string", "array")).jsonValue(), OutputStructure.FLAG).get("valid").getBoolean(), is(true));
    }

    @Test
    void testTypeArrayFails() {
        assertThat(validator.validate(JsonArray.EMPTY.jsonValue(), JsonObject.EMPTY
                .put("type", JsonArray.of("string")).jsonValue(), OutputStructure.FLAG).get("valid").getBoolean(), is(false));
    }

    @Test
    void testRequiredOk() {
        assertThat(validator.validate(JsonObject.EMPTY.put("requiredField", JsonValue.NULL).jsonValue(), JsonObject.EMPTY
                .put("type", JsonArray.of("object")).put("required", JsonArray.of("requiredField")).jsonValue(), OutputStructure.FLAG).get("valid").getBoolean(), is(true));
    }

    @Test
    void testRequiredFails() {
        assertThat(validator.validate(JsonObject.EMPTY.jsonValue(), JsonObject.EMPTY
                .put("type", JsonArray.of("object")).put("required", JsonArray.of("requiredField")).jsonValue(), OutputStructure.FLAG).get("valid").getBoolean(), is(false));
    }

    @Test
    void testPropertyOk() {
        assertThat(validator.validate(JsonObject.EMPTY.put("field1", "value").jsonValue(), JsonObject.EMPTY
                .put("type", JsonArray.of("object")).put("properties", JsonObject.EMPTY.put("field1", true)).jsonValue(), OutputStructure.FLAG).get("valid").getBoolean(), is(true));
    }

    @Test
    void testPropertyFails() {
        assertThat(validator.validate(JsonObject.EMPTY.put("field1", "value").jsonValue(), JsonObject.EMPTY
                .put("type", JsonArray.of("object")).put("properties", JsonObject.EMPTY.put("field1", false)).jsonValue(), OutputStructure.FLAG).get("valid").getBoolean(), is(false));
    }

    @Test
    void testPropertyOkDueToAdditionalProperties() {
        assertThat(validator.validate(JsonObject.EMPTY.put("field1", "value").jsonValue(), JsonObject.EMPTY
                .put("type", JsonArray.of("object")).put("additionalProperties", true).jsonValue(), OutputStructure.FLAG).get("valid").getBoolean(), is(true));
    }

    @Test
    void testTwoProperties() {
        assertThat(
                validator.validate(
                        JsonObject.EMPTY
                                .put("field2", "value")
                                .put("field3", "value").jsonValue(),
                        JsonObject.EMPTY
                                .put("type", JsonArray.of("object"))
                                .put("properties", JsonObject.EMPTY.put("field2", true).put("field3", false))
                                .put("additionalProperties", true).jsonValue(),
                        OutputStructure.FLAG).get("valid").getBoolean(), is(false));
    }

    @Test
    void filterTwoProperties() {
        assertThat(
                validator.filter(
                        JsonObject.EMPTY
                                .put("field1", "value")
                                .put("field2", "value")
                                .put("field3", "value")
                                .put("field4", "value").jsonValue(),
                        JsonObject.EMPTY
                                .put("type", JsonArray.of("object"))
                                .put("properties", JsonObject.EMPTY
                                        .put("field1", false)
                                        .put("field2", false))
                                .jsonValue()),
                is(JsonObject.EMPTY.put("field3", "value").put("field4", "value").jsonValue()));
    }

    @Test
    void outputStructureDetailed() {

        JsonValue instance = JsonObject.EMPTY
                .put("a", true)
                .jsonValue();

        JsonValue schema = JsonObject.EMPTY
                .put("type", JsonArray.of("object"))
                .put("properties", JsonObject.EMPTY
                        .put("a", false)).jsonValue();

        JsonObject result = validator.validate(instance, schema, OutputStructure.DETAILED);

        assertThat(result.get("valid").getBoolean(), is(false));
        assertThat(result.get("instanceLocation").getString(), is(""));
        assertThat(result.get("keywordLocation").getString(), is(""));

        assertThat(result.get(JsonArray.of("errors", 0, "valid")).getBoolean(), is(false));
        assertThat(result.get(JsonArray.of("errors", 0, "instanceLocation")).getString(), is("/a"));
        assertThat(result.get(JsonArray.of("errors", 0, "keywordLocation")).getString(), is("/properties/a"));

    }

    @Test
    void nested_outputStructureDetailed() {

        JsonValue instance = JsonObject.EMPTY
                .put("a1", JsonObject.EMPTY
                        .put("b", "b-value"))
                .jsonValue();

        JsonValue schema = JsonObject.EMPTY
                .put("type", JsonArray.of("object"))
                .put("properties", JsonObject.EMPTY
                        .put("a1", JsonObject.EMPTY
                                .put("type", JsonArray.of("object"))
                                .put("properties", JsonObject.EMPTY
                                        .put("b", false)))
                ).jsonValue();

        JsonObject result = validator.validate(instance, schema, OutputStructure.DETAILED);

        assertThat(result.get("valid").getBoolean(), is(false));
        assertThat(result.get("instanceLocation").getString(), is(""));
        assertThat(result.get("keywordLocation").getString(), is(""));

        assertThat(result.get(JsonArray.of("errors", 0, "valid")).getBoolean(), is(false));
        assertThat(result.get(JsonArray.of("errors", 0, "instanceLocation")).getString(), is("/a1/b"));
        assertThat(result.get(JsonArray.of("errors", 0, "keywordLocation")).getString(), is("/properties/a1/properties/b"));
    }


    @Test
    void nestedValid_outputStructureDetailed() {

        JsonObject result = validator.validate(
                JsonObject.EMPTY
                        .put("a1", JsonObject.EMPTY
                                .put("b", "b-value"))
                        .jsonValue(),
                JsonObject.EMPTY
                        .put("type", JsonArray.of("object"))
                        .put("properties", JsonObject.EMPTY
                                .put("a1", JsonObject.EMPTY
                                        .put("type", JsonArray.of("object"))
                                        .put("properties", JsonObject.EMPTY
                                                .put("b", true)))
                        ).jsonValue()
                , OutputStructure.DETAILED);
        assertThat(result.get("valid").getBoolean(), is(true));
    }


    @Test
    void filterNested1() {

        JsonValue filtered = validator.filter(
                JsonObject.EMPTY
                        .put("a1", JsonObject.EMPTY
                                .put("b", "b-value"))
                        .jsonValue(),
                JsonObject.EMPTY
                        .put("type", JsonArray.of("object"))
                        .put("properties", JsonObject.EMPTY
                                .put("a1", JsonObject.EMPTY
                                        .put("type", JsonArray.of("object"))
                                        .put("properties", JsonObject.EMPTY
                                                .put("b", false)))
                        ).jsonValue()
        );
        assertThat(filtered, is(JsonObject.EMPTY.put("a1", JsonObject.EMPTY).jsonValue())
        );
    }

    @Test
    void filterRefSchema() {

        JsonValue schema1 = JsonObject.EMPTY
                .put("$ref", "#/$defs/Object")
                .put("$defs", JsonObject.EMPTY
                        .put("Object", JsonObject.EMPTY
                                .put("type", JsonArray.of("object"))
                                .put("required", JsonArray.of("status"))
                                .put("properties", JsonObject.EMPTY
                                        .put("status", true))
                                .put("additionalProperties", false)
                                .jsonValue()
                        )).jsonValue();

        JsonValue filtered = validator.filter(
                JsonObject.EMPTY
                        .put("id", "8716348913649")
                        .put("status", "INITIALIZING")
                        .jsonValue(),
                schema1
        );

        assertThat(filtered, is(JsonObject.EMPTY
                .put("status", "INITIALIZING")
                .jsonValue()));
    }


    @Test
    void filterNested2() {
        assertThat(
                validator.filter(
                        JsonObject.EMPTY
                                .put("a1", JsonObject.EMPTY
                                        .put("d", "d-value")
                                        .put("e", "e-value"))
                                .jsonValue(),
                        JsonObject.EMPTY
                                .put("type", JsonArray.of("object"))
                                .put("properties", JsonObject.EMPTY
                                        .put("a1", JsonObject.EMPTY
                                                .put("type", JsonArray.of("object"))
                                                .put("properties", JsonObject.EMPTY
                                                        .put("d", true)
                                                        .put("e", false)))
                                ).jsonValue()
                ),
                is(JsonObject.EMPTY
                        .put("a1", JsonObject.EMPTY.put("d", "d-value")).jsonValue())
        );
    }

    @Test
    void filterNested3() {
        assertThat(
                validator.filter(
                        JsonObject.EMPTY
                                .put("a1", JsonObject.EMPTY
                                        .put("b", "b-value")
                                        .put("c", "c-value"))
                                .put("a2", JsonObject.EMPTY
                                        .put("d", "d-value")
                                        .put("e", "e-value"))
                                .jsonValue(),
                        JsonObject.EMPTY
                                .put("type", JsonArray.of("object"))
                                .put("properties", JsonObject.EMPTY
                                        .put("a1", JsonObject.EMPTY
                                                .put("type", JsonArray.of("object"))
                                                .put("properties", JsonObject.EMPTY
                                                        .put("b", false)
                                                        .put("c", true)))
                                        .put("a2", JsonObject.EMPTY
                                                .put("type", JsonArray.of("object"))
                                                .put("properties", JsonObject.EMPTY
                                                        .put("d", false)
                                                        .put("e", true)))
                                ).jsonValue()
                ),
                is(JsonObject.EMPTY
                        .put("a1", JsonObject.EMPTY.put("c", "c-value"))
                        .put("a2", JsonObject.EMPTY.put("e", "e-value")).jsonValue())
        );
    }

    @Test
    void filterRequiredProperty1() {
        assertThat(
                validator.filter(
                        JsonObject.EMPTY
                                .put("field1", "value")
                                .put("field2", "value").jsonValue(),
                        JsonObject.EMPTY
                                .put("type", JsonArray.of("object"))
                                .put("required", JsonArray.of("field2"))
                                .put("properties", JsonObject.EMPTY
                                        .put("field1", true)
                                        .put("field2", false))
                                .jsonValue()),
                nullValue());
    }

    @Test
    void filterRequiredProperty2() {
        assertThat(
                validator.filter(
                        JsonObject.EMPTY
                                .put("field1", "value")
                                .put("field2", "value").jsonValue(),
                        JsonObject.EMPTY
                                .put("type", JsonArray.of("object"))
                                .put("required", JsonArray.of("field3"))
                                .put("properties", JsonObject.EMPTY
                                        .put("field1", true))
                                .jsonValue()),
                nullValue());
    }

    @Test
    void validRef1() {
        assertThat(
                validator.filter(
                        JsonValue.of("hello"),
                        JsonObject.EMPTY
                                .put("type", JsonArray.of("string"))
                                .put("anyOf", JsonArray.of(
                                        JsonObject.EMPTY
                                                .put("$ref", "#/$defs/mystring")))
                                .put("$defs", JsonObject.EMPTY.put("mystring", true))
                                .jsonValue()),
                is(JsonValue.of("hello")));
    }

    @Test
    void filterInvalidRef1() {
        assertThat(
                validator.filter(
                        JsonValue.of("hello"),
                        JsonObject.EMPTY
                                .put("type", JsonArray.of("string"))
                                .put("anyOf", JsonArray.of(
                                        JsonObject.EMPTY
                                                .put("$ref", "#/$defs/mystring")))
                                .put("$defs", JsonObject.EMPTY.put("mystring", false))
                                .jsonValue()),
                nullValue());
    }

    @Test
    void invalidRef1() {
        JsonObject result = validator.validate(
                JsonValue.of("hello"),
                JsonObject.EMPTY
                        .put("type", JsonArray.of("string"))
                        .put("anyOf", JsonArray.of(
                                JsonObject.EMPTY
                                        .put("$ref", "#/$defs/mystring")))
                        .put("$defs", JsonObject.EMPTY.put("mystring", false))
                        .jsonValue(),
                OutputStructure.DETAILED);

        assertThat(result.get("valid").getBoolean(), is(false));
        assertThat(result.get("instanceLocation").getString(), is(""));
        assertThat(result.get("keywordLocation").getString(), is(""));

        assertThat(result.get(JsonArray.of("errors", 0, "valid")).getBoolean(), is(false));
        assertThat(result.get(JsonArray.of("errors", 0, "instanceLocation")).getString(), is(""));
        assertThat(result.get(JsonArray.of("errors", 0, "keywordLocation")).getString(), is("/anyOf/0/$ref"));
    }

    @Test
    void itemsOfInvalidType1() {
        JsonValue value = JsonArray.of("Hello").jsonValue();

        JsonValue schema = JsonObject.EMPTY
                .put("type", JsonArray.of("array"))
                .put("items", JsonObject.EMPTY
                        .put("type", JsonArray.of("boolean"))).jsonValue();

        JsonObject result = validator.validate(value, schema, OutputStructure.DETAILED);

        assertThat(result.get("valid").getBoolean(), is(false));
        assertThat(result.get(JsonArray.of("errors", 0, "instanceLocation")).getString(), is("/0"));
        assertThat(result.get(JsonArray.of("errors", 0, "keywordLocation")).getString(), is("/items/type"));
    }

    @Test
    void itemsOfInvalidType2() {
        JsonValue value = JsonArray.of(true, "Hello").jsonValue();

        JsonValue schema = JsonObject.EMPTY
                .put("type", JsonArray.of("array"))
                .put("items", JsonObject.EMPTY
                        .put("type", JsonArray.of("boolean"))).jsonValue();

        JsonObject result = validator.validate(value, schema, OutputStructure.DETAILED);

        assertThat(result.get("valid").getBoolean(), is(false));
        assertThat(result.get(JsonArray.of("errors", 0, "instanceLocation")).getString(), is("/1"));
        assertThat(result.get(JsonArray.of("errors", 0, "keywordLocation")).getString(), is("/items/type"));
    }

    @Test
    void itemsOfvalidType1() {
        JsonValue value = JsonArray.of("Hello").jsonValue();

        JsonValue schema = JsonObject.EMPTY
                .put("type", JsonArray.of("array"))
                .put("items", JsonObject.EMPTY
                        .put("type", JsonArray.of("string"))).jsonValue();

        assertThat(validator.validate(value, schema, OutputStructure.FLAG).get("valid").getBoolean(), is(true));
    }

    @Test
    void equalsTest() {
        JsonValue value = JsonObject.EMPTY
                .put("a", 1)
                .put("b", 1)
                .jsonValue();

        JsonValue schema = JsonObject.EMPTY
                .put("type", JsonArray.of("object"))
                .put("properties", JsonObject.EMPTY
                        .put("a", JsonObject.EMPTY.put("equals", JsonArray.of("#/b")))).jsonValue();

        assertThat(validator.validate(value, schema, OutputStructure.FLAG).get("valid").getBoolean(), is(true));
    }

    @Test
    void equalsTest_failing() {
        JsonValue value = JsonObject.EMPTY
                .put("a", 1)
                .put("b", 2)
                .jsonValue();

        JsonValue schema = JsonObject.EMPTY
                .put("type", JsonArray.of("object"))
                .put("properties", JsonObject.EMPTY
                        .put("a", JsonObject.EMPTY.put("equals", JsonArray.of("#/b")))).jsonValue();

        assertThat(validator.validate(value, schema, OutputStructure.FLAG).get("valid").getBoolean(), is(false));
    }

    @Test
    void minItemsInvalid() {
        JsonValue value = JsonArray.EMPTY.jsonValue();

        JsonValue schema = JsonObject.EMPTY
                .put("type", JsonArray.of("array"))
                .put("minItems", 1).jsonValue();

        assertThat(validator.validate(value, schema, OutputStructure.FLAG).get("valid").getBoolean(), is(false));
    }

    @Test
    void minItemsValid() {
        JsonValue value = JsonArray.EMPTY.add("item 1").jsonValue();

        JsonValue schema = JsonObject.EMPTY
                .put("type", JsonArray.of("array"))
                .put("minItems", 1).jsonValue();

        assertThat(validator.validate(value, schema, OutputStructure.FLAG).get("valid").getBoolean(), is(true));
    }

    @Test
    void maxItemsValid() {
        JsonValue value = JsonArray.EMPTY.jsonValue();

        JsonValue schema = JsonObject.EMPTY
                .put("type", JsonArray.of("array"))
                .put("maxItems", 1).jsonValue();

        assertThat(validator.validate(value, schema, OutputStructure.FLAG).get("valid").getBoolean(), is(true));
    }

    @Test
    void maxItemsInvalid() {
        JsonValue value = JsonArray.EMPTY.add("item 1").jsonValue();

        JsonValue schema = JsonObject.EMPTY
                .put("type", JsonArray.of("array"))
                .put("maxItems", 0).jsonValue();

        assertThat(validator.validate(value, schema, OutputStructure.FLAG).get("valid").getBoolean(), is(false));
    }

    @Test
    void uniqueItemsValid() {
        JsonValue value = JsonArray.EMPTY.add("item 1").add("item 2").jsonValue();

        JsonValue schema = JsonObject.EMPTY
                .put("type", JsonArray.of("array"))
                .put("uniqueItems", true).jsonValue();

        assertThat(validator.validate(value, schema, OutputStructure.FLAG).get("valid").getBoolean(), is(true));
    }

    @Test
    void uniqueItemsInvalid() {
        JsonValue value = JsonArray.EMPTY.add("item 1").add("item 1").jsonValue();

        JsonValue schema = JsonObject.EMPTY
                .put("type", JsonArray.of("array"))
                .put("uniqueItems", true).jsonValue();

        assertThat(validator.validate(value, schema, OutputStructure.FLAG).get("valid").getBoolean(), is(false));
    }

    @Test
    void subschema() {
        JsonValue value = JsonObject.EMPTY.put("key", "value1").jsonValue();

        JsonValue schema = JsonObject.EMPTY
                .put("type", JsonArray.of("object"))
                .put("anyOf", JsonArray.of(JsonObject.EMPTY))
                .put("additionalProperties", false).jsonValue();

        JsonObject validationResult = validator.validate(value, schema, OutputStructure.DETAILED);
        assertThat(validationResult.get("valid").getBoolean(), is(false));
        assertThat(validationResult.get(JsonArray.of("errors", 0, "instanceLocation")), is(JsonValue.of("/key")));
        assertThat(validationResult.get(JsonArray.of("errors", 0, "keywordLocation")), is(JsonValue.of("/additionalProperties")));
        assertThat(validationResult.get(JsonArray.of("errors", 0, "instanceProperty")), nullValue());
        assertThat(validationResult.get(JsonArray.of("errors", 0, "keywordValue")), is(JsonValue.of(false)));
        assertThat(validationResult.get(JsonArray.of("errors", 0, "valid")), is(JsonValue.of(false)));
    }

    @Test
    void subschemaWithTwoErrors() {
        JsonValue value = JsonObject.EMPTY.put("key", "value1").jsonValue();

        JsonValue schema = JsonObject.EMPTY
                .put("type", JsonArray.of("object"))
                .put("allOf", JsonArray.of(true, false))
                //.put("additionalProperties", true)
                .jsonValue();

        JsonObject validationResult = validator.validate(value, schema, OutputStructure.DETAILED);
        assertThat(validationResult.get("valid").getBoolean(), is(false));
        assertThat(validationResult.get(JsonArray.of("errors", 0, "instanceLocation")), is(JsonValue.of("")));
        assertThat(validationResult.get(JsonArray.of("errors", 0, "keywordLocation")), is(JsonValue.of("/allOf/1")));
        assertThat(validationResult.get(JsonArray.of("errors", 0, "keywordValue")), is(JsonValue.of(false)));
        assertThat(validationResult.get(JsonArray.of("errors", 0, "valid")), is(JsonValue.of(false)));
    }

    @Test
    void patternPropertiesOneFailing() {
        JsonValue value = JsonObject.EMPTY.put("abc123", true).jsonValue();

        JsonValue schema = JsonObject.EMPTY
                .put("patternProperties", JsonObject.EMPTY
                        .put("abc123", true)
                        .put(".*", false)
                )
                .put("additionalProperties", false)
                .jsonValue();

        JsonObject validationResult = validator.validate(value, schema, OutputStructure.DETAILED);
        assertThat(validationResult.get("valid").getBoolean(), is(false));

    }

    @Test
    void patternPropertiesOk() {
        JsonValue value = JsonObject.EMPTY.put("abc123", true).jsonValue();

        JsonValue schema = JsonObject.EMPTY
                .put("patternProperties", JsonObject.EMPTY
                        .put("abc123", true)
                        .put(".*", true)
                )
                .put("additionalProperties", false)
                .jsonValue();

        assertThat(validator.validate(value, schema, OutputStructure.FLAG).get("valid").getBoolean(), is(true));
    }

    @Test
    void singlePatternPropertyOk() {
        JsonValue value = JsonObject.EMPTY.put("abc123", "mystring").jsonValue();

        JsonValue schema = JsonObject.EMPTY
                .put("patternProperties", JsonObject.EMPTY
                        .put(".*", JsonObject.EMPTY.put("type", "string"))
                )
                .put("additionalProperties", false)
                .jsonValue();

        assertThat(validator.validate(value, schema, OutputStructure.FLAG).get("valid").getBoolean(), is(true));
    }

    @Test
    void emptyProperties() {
        JsonValue value = JsonObject.EMPTY.put("abc123", "mystring").jsonValue();

        JsonValue schema = JsonObject.EMPTY
                .put("properties", JsonObject.EMPTY)
                .put("patternProperties", JsonObject.EMPTY)
                .jsonValue();

        assertThat(validator.validate(value, schema, OutputStructure.FLAG).get("valid").getBoolean(), is(true));
    }

    @Test
        // TODO: Make this work!
    void recursiveSchema() {
        JsonArray invalidJsonArray = JsonArray.of("invalid string");
        JsonArray validJsonArray = JsonArray.of(1);
        for (int i = 0; i < 100; i++) {
            invalidJsonArray = JsonArray.of(invalidJsonArray);
            validJsonArray = JsonArray.of(validJsonArray);
        }

        /*
        {"$ref":"#/$defs/recursive","$defs":{"recursive":{"anyOf":[{"items":{"$ref":"#/$defs/recursive"}},{"type":"integer"}]}}}
         */

        JsonValue schema = JsonObject.EMPTY
                .put("$ref", "#/$defs/recursive")
                .put("$defs", JsonObject.EMPTY.put(
                        "recursive", JsonObject.EMPTY
                                .put("anyOf", JsonArray.of(
                                        JsonObject.EMPTY
                                                .put("type", "array")
                                                .put("items", JsonObject.EMPTY
                                                        .put("$ref", "#/$defs/recursive")),
                                        JsonObject.EMPTY.put("type", "integer")))))
                .jsonValue();

        long start1 = System.currentTimeMillis();
        JsonValue detailedResult = validator.filter(invalidJsonArray.jsonValue(), schema);//, OutputStructure.DETAILED);
        System.out.printf("execution time: %d\n", System.currentTimeMillis() - start1);
        long start2 = System.currentTimeMillis();
        detailedResult = validator.validate(invalidJsonArray.jsonValue(), schema, OutputStructure.DETAILED).jsonValue();
        System.out.printf("execution time: %d\n", System.currentTimeMillis() - start2);

        assertThat(detailedResult.get("valid").getBoolean(), is(false));
        long start3 = System.currentTimeMillis();
        detailedResult = validator.validate(validJsonArray.jsonValue(), schema, OutputStructure.DETAILED).jsonValue();
        System.out.printf("execution time of valid: %d\n", System.currentTimeMillis() - start3);


        assertThat(detailedResult.get("valid").getBoolean(), is(true));
    }

    @Test
    void filter() {
        JsonValue schema = JsonUtil.create("{\n" +
                "  \"anyOf\": [\n" +
                "    {\n" +
                "      \"properties\": {\n" +
                "        \"a\": {\n" +
                "          \"const\": \"a\"\n" +
                "        },\n" +
                "        \"b\": false\n" +
                "      }\n" +
                "    },\n" +
                "    {\n" +
                "      \"properties\": {\n" +
                "        \"a\": false,\n" +
                "        \"b\": {\n" +
                "          \"const\": \"b\"\n" +
                "        }\n" +
                "      }\n" +
                "    }\n" +
                "  ]\n" +
                "}").jsonValue();


        JsonValue toBeValidated1 = JsonObject.EMPTY
                .put("b", "b")
                .put("a", "a")
                .jsonValue();

        JsonValue filtrate1 = validator.filter(toBeValidated1, schema);

        assertThat(filtrate1, nullValue());

        JsonValue toBeValidated2 = JsonObject.EMPTY
                .put("b", "b")
                .jsonValue();

        JsonValue filtrate2 = validator.filter(toBeValidated2, schema);
        assertThat(filtrate2, is(toBeValidated2));
    }

    @Test
    void filter_2() {
        JsonValue value =
                JsonObject.EMPTY
                        .put("firstName", "Peter")
                        .put("lastName", "Andersson").jsonValue();

        JsonObject schema = JsonObject.EMPTY
                .put("properties", JsonObject.EMPTY
                        .put("firstName", false));

        JsonValue filtrate = new JsonSchemaValidator().filter(value, schema.jsonValue());
        assertThat(filtrate, is(JsonObject.EMPTY.put("lastName", "Andersson").jsonValue()));
    }

    @Test
    void filter_3() {
        JsonArray value = JsonArray.of(
                JsonObject.EMPTY
                        .put("firstName", "Peter")
                        .put("lastName", "Andersson"));

        JsonObject schema = JsonObject.EMPTY
                .put("items", JsonObject.EMPTY
                        .put("properties", JsonObject.EMPTY
                                .put("firstName", false)));

        JsonValue filtrate = new JsonSchemaValidator().filter(value.jsonValue(), schema.jsonValue());
        assertThat(filtrate, is(JsonArray.of(
                JsonObject.EMPTY
                        .put("lastName", "Andersson")).jsonValue()));
    }

    @Test
    void filter_4() {
        JsonArray value = JsonArray.of(
                JsonObject.EMPTY
                        .put("firstName", "Peter")
                        .put("lastName", "Andersson"),
                JsonObject.EMPTY
                        .put("firstName", "Laura")
                        .put("lastName", "Andersson")
        );

        JsonObject schema = JsonObject.EMPTY
                .put("items", JsonObject.EMPTY
                        .put("properties", JsonObject.EMPTY
                                .put("lastName", false)));

        JsonValue filtrate = new JsonSchemaValidator().filter(value.jsonValue(), schema.jsonValue());
        assertThat(filtrate, is(JsonArray.of(
                        JsonObject.EMPTY
                                .put("firstName", "Peter"),
                        JsonObject.EMPTY
                                .put("firstName", "Laura"))
                .jsonValue()));
    }

    @Test
    void filter_5() {
        JsonArray value = JsonArray.of(
                JsonObject.EMPTY.put("name", "Peter"),
                JsonObject.EMPTY.put("name", "Laura")
        );

        JsonObject schema = JsonObject.EMPTY
                .put("items", JsonObject.EMPTY
                        .put("additionalProperties", false));

        JsonValue filtrate = new JsonSchemaValidator().filter(value.jsonValue(), schema.jsonValue());
        assertThat(filtrate, is(JsonArray.of(JsonObject.EMPTY, JsonObject.EMPTY).jsonValue()));
    }

    @Test
    void error_5() {
        JsonArray value = JsonArray.of(
                JsonObject.EMPTY.put("name", "Peter"),
                JsonObject.EMPTY.put("name", "Laura"));

        JsonObject schema = JsonObject.EMPTY
                .put("items", JsonObject.EMPTY
                        .put("additionalProperties", false));

        String expectedString = "{\"instanceLocation\":\"\",\"errors\":[{\"instanceLocation\":\"\",\"errors\":[{\"instanceLocation\":\"/0/name\",\"keywordLocation\":\"/items/additionalProperties\",\"valid\":false,\"keywordValue\":false},{\"instanceLocation\":\"/1/name\",\"keywordLocation\":\"/items/additionalProperties\",\"valid\":false,\"keywordValue\":false}],\"keywordLocation\":\"/items\",\"valid\":false,\"keywordValue\":{\"additionalProperties\":false}}],\"keywordLocation\":\"\",\"valid\":false}";

        JsonObject expected = JsonValueFactory.create(expectedString).getJsonObject();

        JsonObject result = new JsonSchemaValidator().validate(value.jsonValue(), schema.jsonValue(), OutputStructure.DETAILED);
        assertThat(result, is(expected));
    }


    @Test
    void filterIfThenElse() {
        JsonValue value =
                JsonObject.EMPTY.put("name", "Peter").jsonValue();

        JsonObject schema = JsonObject.EMPTY
                .put("if", false)
                .put("then", false)
                .put("else", JsonObject.EMPTY
                        .put("additionalProperties", false) // should remove all fields but return {} as filtrate
                );

        JsonValue filtrate = new JsonSchemaValidator().filter(value, schema.jsonValue());

        assertThat(filtrate, is(JsonObject.EMPTY.jsonValue()));
    }

    @Test
    void filterIfThenElseItems() {
        JsonValue value =
                JsonArray.of(
                        JsonObject.EMPTY
                                .put("name", "Peter")
                                .put("customer","Company A"),
                        JsonObject.EMPTY
                                .put("name", "Laura")
                                .put("customer","Company B"))
                        .jsonValue();

        JsonObject schema = JsonObject.EMPTY
                .put("description", "Only info of customers of Company B should show in the filtered list")
                .put("items", JsonObject.EMPTY
                        .put("if", JsonObject.EMPTY
                                .put("properties", JsonObject.EMPTY
                                        .put("customer", JsonObject.EMPTY
                                                .put("const", "Company B"))))
                        .put("then", true)
                        .put("else", JsonObject.EMPTY
                                .put("additionalProperties", false) // should remove all fields but return {} as filtrate
                        ));

        JsonValue filtrate = new JsonSchemaValidator().filter(value, schema.jsonValue());
        assertThat(filtrate, is(JsonArray.of(JsonObject.EMPTY.jsonValue(), JsonObject.EMPTY.put("name", "Laura").put("customer", "Company B")).jsonValue()));
    }

    @Test
    void minPropertiesTest() {
        JsonValue schema = JsonObject.EMPTY.put("minProperties", 1).jsonValue();
        JsonObject validationResult = validator.validate(JsonObject.EMPTY.jsonValue(), schema, OutputStructure.DETAILED);
        assertThat(validationResult.asJson(), validationResult.get("valid"), is(JsonValue.FALSE));
    }

    @Test
    void maxPropertiesTest() {
        JsonValue schema = JsonObject.EMPTY.put("maxProperties", 1).jsonValue();
        JsonObject validationResult = validator.validate(JsonObject.EMPTY.put("prop1", "prop1Value").put("prop2", "prop2Value").jsonValue(), schema, OutputStructure.DETAILED);
        assertThat(validationResult.asJson(), validationResult.get("valid"), is(JsonValue.FALSE));
    }

    @Test
    void refToDefsInIf() {
        JsonValue schema = JsonObject.EMPTY.put("$defs", JsonObject.EMPTY.put("SomeType", true).put("if", JsonObject.EMPTY.put("$ref", "#/$defs/SomeType"))).jsonValue();
        JsonObject result = validator.validate(JsonValue.of(2), schema, OutputStructure.DETAILED);
        assertThat(result.get("valid").getBoolean(), is(true));
    }

    @Test
    void contains() {
        JsonValue schema = JsonObject.EMPTY.put("contains", JsonObject.EMPTY.put("const", "hello")).jsonValue();
        JsonObject result = validator.validate(JsonArray.of("a", "b", 2, false).jsonValue(), schema, OutputStructure.DETAILED);
        assertThat(result.get(JsonArray.of("errors", 0, "keywordLocation")).getString(), is("/contains"));
    }

    @Test
    void dependent_schemas_1() {
        executeTest();
    }

    @Test
    void testEquals() {
        JsonSchema schema = JsonObject.EMPTY
                .put("properties", JsonObject.EMPTY
                        .put("val", JsonObject.EMPTY
                                .put("equals", JsonArray.of("/a/0/b")))).as(JsonSchema.class);

        JsonObject instance = JsonObject.EMPTY.put("val", 34).put("a", JsonArray.of(JsonObject.EMPTY.put("b", 34)));

        JsonObject validationResult = new JsonSchemaValidator().validate(instance.jsonValue(), schema, OutputStructure.DETAILED);
        assertThat(validationResult.get("valid"), is(JsonValue.of(true)));
    }

    @Test
    void testIfAndEquals() {
        JsonSchema schema = JsonObject.EMPTY
                .put("properties", JsonObject.EMPTY
                        .put("val", JsonObject.EMPTY
                                .put("if", JsonObject.EMPTY
                                        .put("equals", JsonArray.of("/a/0/b")))
                                .put("then", true)
                                .put("else", false)
                        ))
                .as(JsonSchema.class);

        JsonObject instance = JsonObject.EMPTY.put("val", 34).put("a", JsonArray.of(JsonObject.EMPTY.put("b", 34)));

        JsonObject validationResult = new JsonSchemaValidator().validate(instance.jsonValue(), schema, OutputStructure.DETAILED);
        assertThat(validationResult.get("valid"), is(JsonValue.of(true)));
    }
}