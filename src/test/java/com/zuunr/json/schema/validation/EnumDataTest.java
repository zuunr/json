package com.zuunr.json.schema.validation;

import com.zuunr.json.*;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

/**
 * @author Niklas Eldberger
 */
public class EnumDataTest {

    JsonSchemaValidator validator = new JsonSchemaValidator(false);

    @Test
    void enumData(){
        JsonValue validModel = JsonValueFactory.create("""
                {
                    "dynamicEnum": ["A","B"],
                    "value": "A"
                }
                """);
        JsonValue invalidModel = JsonValueFactory.create("""
                {
                    "dynamicEnum": ["B", "C"],
                    "value": "A"
                }
                """);
        JsonValue jsonSchema = JsonValueFactory.create("""
                {
                    "properties": {
                        "value": {
                            "enum": {"$data": "/dynamicEnum"}
                        }
                    }
                }
                """);
        JsonObject validResult = validator.validate(validModel, jsonSchema, OutputStructure.BASIC);
        assertThat(validResult.get("valid").getBoolean(), is(true));
        JsonObject invalidResult = validator.validate(invalidModel, jsonSchema, OutputStructure.BASIC);
        assertThat(invalidResult.get("valid").getBoolean(), is(false));

    }
}