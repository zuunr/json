package com.zuunr.json.schema.validation.object;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.node.object.SchemaPropertyNode;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * @author Niklas Eldberger
 */
public class SchemaPropertyStepTest {

    @Test
    void test() {
        JsonSchema schema = JsonObject.EMPTY
                .put("properties", JsonObject.EMPTY
                        .put("field1", true))
                .put("patternProperties", JsonObject.EMPTY
                        .put("^.*1$", true)
                        .put("no", true)
                        .put("^.*$", true))
                .put("additionalProperties", false).as(JsonSchema.class);

        SchemaPropertyNode.State result1 = SchemaPropertyNode.nextState("field1", JsonValue.of("valueOfField1"), schema, null, -1);

        assertThat(result1.currentKeyword, is("properties"));

        SchemaPropertyNode.State result2 = SchemaPropertyNode.nextState("field1", JsonValue.of("valueOfField1"), schema, "properties", -1);
        assertThat(result2.currentKeyword, is("patternProperties"));
        assertThat(result2.currentPatternPropertyIndex, greaterThan(-1));

        SchemaPropertyNode.State result3 = SchemaPropertyNode
                .nextState(
                        "field1",
                        JsonValue.of("valueOfField1"),
                        schema,
                        "patternProperties",
                        result2.currentPatternPropertyIndex);

        assertThat(result3.currentKeyword, is("patternProperties"));
        assertThat(result3.currentPatternPropertyIndex, greaterThan(-1));
        assertThat(result3.currentPatternPropertyIndex, not(result2.currentPatternPropertyIndex));

        SchemaPropertyNode.State result4 = SchemaPropertyNode.nextState("field1", JsonValue.of("valueOfField1"), schema, "patternProperties", result3.currentPatternPropertyIndex);
        assertThat(result4, nullValue());
    }


}
