package com.zuunr.json.schema.validation;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.pointer.JsonPointer;
import com.zuunr.json.schema.validation.node.EqualsNode;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
public class EqualsStepTest {

    @Test
    void testSuccess() {
        boolean result = EqualsNode.equals(
                JsonValue.of("d"),
                JsonValue.of("/a/b/c").as(JsonPointer.class).asArray(),
                JsonObject.EMPTY
                        .put("a", JsonObject.EMPTY
                                .put("b", JsonObject.EMPTY
                                        .put("c", "d"))).jsonValue()
        );
        assertThat(result, is(true));
    }

    @Test
    void testFailure() {
        boolean result = EqualsNode.equals(
                JsonValue.of("d"),
                JsonValue.of("/a/b/c").as(JsonPointer.class).asArray(),
                JsonObject.EMPTY
                        .put("a", JsonObject.EMPTY
                                .put("b", JsonObject.EMPTY
                                        .put("c", "q"))).jsonValue()
        );
        assertThat(result, is(false));
    }

    @Test
    void emptyArrayTest() {
        boolean result = EqualsNode.equals(
                JsonValue.of("d"),
                JsonValue.of("/a/-1/c").as(JsonPointer.class).asArray(),
                JsonObject.EMPTY
                        .put("a", JsonArray.EMPTY).jsonValue());
        assertThat(result, is(true));
    }

    @Test
    void exactIndexSuccess() {
        boolean result = EqualsNode.equals(
                JsonValue.of("d"),
                JsonValue.of("/a/1").as(JsonPointer.class).asArray(),
                JsonObject.EMPTY
                        .put("a", JsonArray.EMPTY
                                .add(true)
                                .add("d")).jsonValue());
        assertThat(result, is(true));
    }

    @Test
    void exactIndexFailure() {
        boolean result = EqualsNode.equals(
                JsonValue.of("d"),
                JsonValue.of("/a/1").as(JsonPointer.class).asArray(),
                JsonObject.EMPTY
                        .put("a", JsonArray.EMPTY
                                .add(true)
                                .add(JsonObject.EMPTY)).jsonValue());
        assertThat(result, is(false));
    }

    @Test
    void multiIndexSuccess() {
        boolean result = EqualsNode.equals(
                JsonValue.of("d"),
                JsonValue.of("/a/-1").as(JsonPointer.class).asArray(),
                JsonObject.EMPTY
                        .put("a", JsonArray.EMPTY
                                .add("d")
                                .add("d")).jsonValue());
        assertThat(result, is(true));
    }

    @Test
    void multiIndexSuccess2() {
        boolean result = EqualsNode.equals(
                JsonValue.of("Andersson"),
                JsonValue.of("/a/-1/name").as(JsonPointer.class).asArray(),
                JsonObject.EMPTY
                        .put("a", JsonArray.EMPTY
                                .add(JsonObject.EMPTY.put("name", "Andersson"))
                                .add(JsonObject.EMPTY) // This will match successfully because there is nothing to match
                        ).jsonValue());
        assertThat(result, is(true));
    }

    @Test
    void multiIndexFailure() {
        boolean result = EqualsNode.equals(
                JsonValue.of("d"),
                JsonValue.of("/a/-1").as(JsonPointer.class).asArray(),
                JsonObject.EMPTY
                        .put("a", JsonArray.EMPTY
                                .add("d")
                                .add(false)).jsonValue());
        assertThat(result, is(false));
    }
}
