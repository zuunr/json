package com.zuunr.json.schema.validation.string;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.JsonValueFactory;
import com.zuunr.json.schema.validation.JsonSchemaValidator;
import com.zuunr.json.schema.validation.OutputStructure;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
public class PatternDataTest {

    JsonSchemaValidator validator = new JsonSchemaValidator(false);

    @Test
    void test(){
        JsonValue validModel = JsonValueFactory.create("""
                {
                    "dynamicValue": ".B.",
                    "value": "ABC"
                }
                """);
        JsonValue invalidModel = JsonValueFactory.create("""
                {
                    "dynamicValue": ".B.",
                    "value": "BAC"
                }
                """);
        JsonValue jsonSchema = JsonValueFactory.create("""
                {
                    "properties": {
                        "value": {
                            "pattern": {"$data": "/dynamicValue"}
                        }
                    }
                }
                """);
        JsonObject validResult = validator.validate(validModel, jsonSchema, OutputStructure.BASIC);
        assertThat(validResult.get("valid").getBoolean(), is(true));
        JsonObject invalidResult = validator.validate(invalidModel, jsonSchema, OutputStructure.BASIC);
        assertThat(invalidResult.get("valid").getBoolean(), is(false));
    }
}