package com.zuunr.json.schema.validation;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectMerger;
import com.zuunr.json.JsonValue;
import com.zuunr.json.test.FileUtil;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
public class JsonSchemaCompabilityTest {

    JsonSchemaValidator validator = new JsonSchemaValidator();
    FileUtil fileUtil = new FileUtil();
    JsonObjectMerger jsonObjectMerger = new JsonObjectMerger();

    @Test
    void additionalProperties() {
        runTests("/com/zuunr/json/schema/validation/draft2020-12/additionalProperties.json", JsonArray.EMPTY);
    }

    @Test
    void allOf() {
        runTests("/com/zuunr/json/schema/validation/draft2020-12/allOf.json", JsonArray.EMPTY);
    }

    //@Test
    void anchor() {
        runTests("/com/zuunr/json/schema/validation/draft2020-12/anchor.json", JsonArray.EMPTY);
    }

    @Test
    void anyOf() {
        runTests("/com/zuunr/json/schema/validation/draft2020-12/anyOf.json", JsonArray.EMPTY);
    }

    @Test
    void boolean_schema() {
        runTests("/com/zuunr/json/schema/validation/draft2020-12/boolean_schema.json", JsonArray.EMPTY);
    }

    @Test
    void _const() {
        runTests("/com/zuunr/json/schema/validation/draft2020-12/const.json", JsonArray.EMPTY);
    }

    @Test
    void contains() {
        runTests("/com/zuunr/json/schema/validation/draft2020-12/contains.json", JsonArray.of(
                //"contains keyword validation: array without items matching schema is invalid",
                //"contains keyword validation: empty array is invalid",
                //"contains keyword with const keyword: array without item 5 is invalid",
                //"contains keyword with boolean schema true: empty array is invalid",
                //"contains keyword with boolean schema false: any non-empty array is invalid",
                //"contains keyword with boolean schema false: empty array is invalid",
                //"items + contains: matches items, does not match contains"
        ));
    }

    //@Test
    void content() {
        runTests("/com/zuunr/json/schema/validation/draft2020-12/content.json", JsonArray.EMPTY);
    }

    //@Test
    void _default() {
        runTests("/com/zuunr/json/schema/validation/draft2020-12/default.json", JsonArray.EMPTY);
    }

    @Test
    void defs() {
        runTests("/com/zuunr/json/schema/validation/draft2020-12/defs.json", JsonArray.of(
                "validate definition against metaschema: valid definition schema",
                "validate definition against metaschema: invalid definition schema"
        ));
    }

    @Test
    void _enum() {
        runTests("/com/zuunr/json/schema/validation/draft2020-12/enum.json", JsonArray.EMPTY);
    }

    @Test
    void oneOf() {
        runTests("/com/zuunr/json/schema/validation/draft2020-12/oneOf.json", JsonArray.EMPTY);
    }

    @Test
    void patternProperties() {
        runTests("/com/zuunr/json/schema/validation/draft2020-12/patternProperties.json", JsonArray.EMPTY);
    }

    @Test
    void type() {
        runTests("/com/zuunr/json/schema/validation/draft2020-12/type.json", JsonArray.EMPTY);
    }

    @Test
    void required() {
        runTests("/com/zuunr/json/schema/validation/draft2020-12/required.json", JsonArray.EMPTY);
    }

    @Test
    void properties() {
        runTests("/com/zuunr/json/schema/validation/draft2020-12/properties.json",
                JsonArray.of(
                        //"properties, patternProperties, additionalProperties interaction: patternProperty invalidates property",
                        //"properties, patternProperties, additionalProperties interaction: patternProperty validates nonproperty"
                ));
    }


    @Test
    void maxLength() {
        runTests("/com/zuunr/json/schema/validation/draft2020-12/maxLength.json",
                JsonArray.of(
                        "maxLength validation: two supplementary Unicode code points is long enough"
                ));
    }

    @Test
    void minLength() {
        runTests("/com/zuunr/json/schema/validation/draft2020-12/minLength.json",
                JsonArray.of(
                        "minLength validation: one supplementary Unicode code point is not long enough"
                ));
    }

    @Test
    void maxItems() {
        runTests("/com/zuunr/json/schema/validation/draft2020-12/maxItems.json", JsonArray.EMPTY);
    }

    @Test
    void ifThenElse() {
        runTests("/com/zuunr/json/schema/validation/draft2020-12/if-then-else.json", JsonArray.of(
                "if and else without then: invalid through else",
                "validate against correct branch, then vs else: invalid through else"
                ));
    }

    @Test
    void dependentSchemas() {
        runTests("/com/zuunr/json/schema/validation/draft2020-12/dependentSchemas.json", JsonArray.of());
    }

    @Test
    void BigDecimal() {
        JsonValue jsonValue1 = JsonValue.of(new BigDecimal("0.003536460000"));
        JsonValue jsonValue2 = JsonValue.of(new BigDecimal("0.003536460"));

        assertThat(jsonValue1.equals(jsonValue2), is(true));

        JsonValue jsonValue3 = JsonValue.of(new BigDecimal("0.3536460E-2"));
        assertThat(jsonValue2.equals(jsonValue3), is(true));
        assertThat(jsonValue2.toString(), is("0.00353646"));

    }

    private void runTests(String testFilePath, JsonArray excludedTests) {
        JsonArray tests = fileUtil.getJsonValue(testFilePath).getJsonArray();

        for (JsonValue testJsonValue : tests) {
            JsonObject test = testJsonValue.getJsonObject();
            JsonValue schema = test.get("schema");
            String testsDescription = test.get("description").getString();
            JsonArray schemaTests = test.get("tests", JsonArray.EMPTY).getJsonArray();

            for (JsonValue schemaTest : schemaTests) {
                JsonValue data = schemaTest.getJsonObject().get("data");
                JsonValue valid = schemaTest.getJsonObject().get("valid");

                String schemaTestDescription = schemaTest.get("description").getString();
                String fullTestDescription = testsDescription + ": " + schemaTestDescription;
                if (!excludedTests.contains(fullTestDescription)) {
                    System.out.println("Executing JSON Schema test: " + fullTestDescription);
                    JsonObject flagResult = validator.validate(data, schema, OutputStructure.FLAG);
                    assertThat(fullTestDescription, flagResult.get("valid"), is(valid));
                    JsonObject detailedResult = validator.validate(data, schema, OutputStructure.DETAILED);

                    if (!valid.getBoolean()) {
                        JsonObject requiredResultAndMore = jsonObjectMerger.merge(detailedResult, schemaTest.get("detailed", JsonObject.EMPTY).getJsonObject());
                        assertThat(fullTestDescription, detailedResult, is(requiredResultAndMore));
                    }
                } else {
                    System.out.println("Ignoring JSON Schema test: " + fullTestDescription);

                }
            }
        }
    }
}