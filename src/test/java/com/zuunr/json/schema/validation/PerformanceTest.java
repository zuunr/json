package com.zuunr.json.schema.validation;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.UUID;

/**
 * @author Niklas Eldberger
 */
public class PerformanceTest {

    private static final JsonObject NESTED_GENERATED_SCHEMA = generateNestedSchema();

    int testIterations = 10000;
    BigDecimal _1000_nanos = new BigDecimal(1000000L);

    @Test
    void testValidation() {

        JsonObject singleInstance = createInstance().getJsonObject();

        JsonObject schema = getSchema().getJsonObject();

        JsonSchemaValidator validator = new JsonSchemaValidator();

        System.out.println("Init done");
        long start = System.nanoTime();

        for (int i = 0; i < testIterations; i++) {
            JsonObject result = validator.validate(singleInstance.jsonValue(), schema.jsonValue(), OutputStructure.DETAILED);
        }

        System.out.printf("Validation: Execution time: %s ms/validation\n", millisPerIteration(System.nanoTime() - start, testIterations));
    }

    @Test
    void testFilter() {

        JsonObject singleInstance = createInstance().getJsonObject();
        JsonObject schema = getSchema().getJsonObject();
        JsonSchemaValidator validator = new JsonSchemaValidator();

        System.out.println("Init done");
        long start = System.nanoTime();

        for (int i = 0; i < testIterations; i++) {
            JsonValue result = validator.filter(singleInstance.jsonValue(), schema.jsonValue());
            if (result == null) {
                throw new NullPointerException("filter is failing!");
            }
        }

        System.out.printf("Filter: Execution time: %s ms/validation\n", millisPerIteration(System.nanoTime() - start, testIterations));

    }

    private BigDecimal millisPerIteration(long fullTimeInNanos, int iterations) {
        return new
                BigDecimal(fullTimeInNanos).divide(new BigDecimal(iterations)).divide(_1000_nanos);
    }

    private JsonValue getSchema() {
        JsonObject schema = JsonObject.EMPTY
                .put("type", "object")
                .put("properties", JsonObject.EMPTY
                        .put("field0", NESTED_GENERATED_SCHEMA)
                        .put("field1", NESTED_GENERATED_SCHEMA)
                        .put("field2", NESTED_GENERATED_SCHEMA)
                        .put("field3", NESTED_GENERATED_SCHEMA)
                        .put("field4", NESTED_GENERATED_SCHEMA)
                        .put("field5", NESTED_GENERATED_SCHEMA)
                        .put("field6", NESTED_GENERATED_SCHEMA)
                        .put("field7", NESTED_GENERATED_SCHEMA)
                        .put("field8", NESTED_GENERATED_SCHEMA)
                        .put("field9", NESTED_GENERATED_SCHEMA));
        return schema.jsonValue();
    }

    private JsonValue createInstance() {
        JsonObject instance = JsonObject.EMPTY
                .put("field0", generateNested())
                .put("field1", generateNested())
                .put("field2", generateNested())
                .put("field3", generateNested())
                .put("field4", generateNested())
                .put("field5", generateNested())
                .put("field6", generateNested())
                .put("field7", generateNested())
                .put("field8", generateNested())
                .put("field9", generateNested());
        return instance.jsonValue();
    }

    private JsonObject generateNested() {
        JsonObject instance = JsonObject.EMPTY
                .put("field0", UUID.randomUUID().toString())
                .put("field1", UUID.randomUUID().toString())
                .put("field2", UUID.randomUUID().toString())
                .put("field3", UUID.randomUUID().toString())
                .put("field4", UUID.randomUUID().toString())
                .put("field5", UUID.randomUUID().toString())
                .put("field6", UUID.randomUUID().toString())
                .put("field7", UUID.randomUUID().toString())
                .put("field8", UUID.randomUUID().toString())
                .put("field9", UUID.randomUUID().toString());
        return instance;
    }

    private static JsonObject generateNestedSchema() {
        return JsonObject.EMPTY.put("type", "object")
                .put("properties", JsonObject.EMPTY
                        .put("field0", JsonObject.EMPTY.put("type", "string").put("minLength", 0).put("maxLength", 100))
                        .put("field1", JsonObject.EMPTY.put("type", "string").put("minLength", 0).put("maxLength", 100))
                        .put("field2", JsonObject.EMPTY.put("type", "string").put("minLength", 0).put("maxLength", 100))
                        .put("field3", JsonObject.EMPTY.put("type", "string").put("minLength", 0).put("maxLength", 100))
                        .put("field4", JsonObject.EMPTY.put("type", "string").put("minLength", 0).put("maxLength", 100))
                        .put("field5", JsonObject.EMPTY.put("type", "string").put("minLength", 0).put("maxLength", 100))
                        .put("field6", JsonObject.EMPTY.put("type", "string").put("minLength", 0).put("maxLength", 100))
                        .put("field7", JsonObject.EMPTY.put("type", "string").put("minLength", 0).put("maxLength", 100))
                        .put("field8", JsonObject.EMPTY.put("type", "string").put("minLength", 0).put("maxLength", 100))
                        .put("field9", JsonObject.EMPTY.put("type", "string").put("minLength", 0).put("maxLength", 100)));
    }
}

