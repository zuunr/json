package com.zuunr.json.schema.validation;

import com.zuunr.json.JsonObject;
import com.zuunr.json.tool.JsonUtil;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
public class SelfValidationTest {

    @Test
    void jsonSchemaOfJsonSchema() {

        JsonSchemaValidator validator = new JsonSchemaValidator();

        JsonObject validationResult = null;

        validator.validate(validator.getJsonSchemaForJsonSchema().asJsonValue(), validator.getJsonSchemaForJsonSchema(), OutputStructure.DETAILED);
        long start = System.nanoTime();
        long numberOfIterations = 1000;
        for (int i = 0; i < numberOfIterations; i++) {

            validationResult = validator.validate(validator.getJsonSchemaForJsonSchema().asJsonValue(), validator.getJsonSchemaForJsonSchema(), OutputStructure.DETAILED);


        }
        System.out.printf("Execution time: %d\n", (System.nanoTime() - start) / numberOfIterations);

        assertThat(validationResult.get("valid").getBoolean(), is(true));


    }
}
