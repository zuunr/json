package com.zuunr.json.schema.validation;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.JsonValueFactory;
import com.zuunr.json.pointer.JsonPointer;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.node.object.SchemaPropertyNode;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * @author Niklas Eldberger
 */
public class RefValidationTest {



    @Test
    void combineRefWithOtherKeyword() {

        String jsonSchemaString = """
                {
                  "$ref": "#/$defs/Reftype",
                  "type": "string",
                  "pattern": "^HELLO_.*$",
                  "$defs": {
                    "Reftype": {
                      "pattern": "^.*_YOU$"
                    }
                  }
                }
                
                """;
        JsonSchema jsonSchema = JsonValueFactory.create(jsonSchemaString).as(JsonSchema.class);

        JsonObject result;

        result = (doGivenWhen(JsonValue.of("HELLO_YOU"), jsonSchema));
        assertThat(result.asJson(), result.get("valid"), is(JsonValue.TRUE));

        result = (doGivenWhen(JsonValue.of("HI_ME"), jsonSchema));
        assertThat(result.asJson(), result.get("valid"), is(JsonValue.FALSE));

        result = (doGivenWhen(JsonValue.of("HI_YOU"), jsonSchema));
        assertThat(result.asJson(), result.get("valid"), is(JsonValue.FALSE));

        result = (doGivenWhen(JsonValue.of("HELLO_ME"), jsonSchema));
        assertThat(result.asJson(), result.get("valid"), is(JsonValue.FALSE));

        result = (doGivenWhen(JsonValue.of("YOU_HELLO"), jsonSchema));
        assertThat(result.asJson(), result.get("valid"), is(JsonValue.FALSE));
        assertThat(result.asJson(), result.get(JsonPointer.of("/errors/0/keywordLocation")), is(JsonValue.of("/pattern")));
        assertThat(result.asJson(), result.get(JsonPointer.of("/errors/0/keywordValue")), is(JsonValue.of("^HELLO_.*$")));
        assertThat(result.asJson(), result.get(JsonPointer.of("/errors/1/keywordLocation")), is(JsonValue.of("/$ref/pattern")));
        assertThat(result.asJson(), result.get(JsonPointer.of("/errors/1/keywordValue")), is(JsonValue.of("^.*_YOU$")));
    }

    JsonObject doGivenWhen(JsonValue instance, JsonSchema schema) {
        JsonSchemaValidator jsonSchemaValidator = new JsonSchemaValidator(true);

        return jsonSchemaValidator.validate(instance,            // data to be validated
                schema,               // schema use when validating
                OutputStructure.DETAILED  // how to present result in case of errors
        );
    }
}
