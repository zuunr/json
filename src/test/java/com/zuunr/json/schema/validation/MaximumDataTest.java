package com.zuunr.json.schema.validation;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectFactory;
import com.zuunr.json.JsonValue;
import com.zuunr.json.JsonValueFactory;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
public class MaximumDataTest {

    JsonSchemaValidator validator = new JsonSchemaValidator(false);

    @Test
    void test(){
        JsonValue validModel = JsonValueFactory.create("""
                {
                    "dynamicMaximum": 11,
                    "value": 10
                }
                """);
        JsonValue invalidModel = JsonValueFactory.create("""
                {
                    "dynamicMaximum": 9,
                    "value": 10
                }
                """);
        JsonValue jsonSchema = JsonValueFactory.create("""
                {
                    "properties": {
                        "value": {
                            "maximum": {"$data": "/dynamicMaximum"}
                        }
                    }
                }
                """);
        JsonObject validResult = validator.validate(validModel, jsonSchema, OutputStructure.BASIC);
        assertThat(validResult.get("valid").getBoolean(), is(true));
        JsonObject invalidResult = validator.validate(invalidModel, jsonSchema, OutputStructure.BASIC);
        assertThat(invalidResult.get("valid").getBoolean(), is(false));
    }
}