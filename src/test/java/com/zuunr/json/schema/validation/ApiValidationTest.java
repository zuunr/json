package com.zuunr.json.schema.validation;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.pointer.JsonPointer;
import com.zuunr.json.tool.JsonUtil;
import org.junit.jupiter.api.Test;

import java.util.ArrayDeque;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
class ApiValidationTest {

    JsonSchemaValidator validator = new JsonSchemaValidator();

    @Test
    void complexOutput(){
        JsonObject instance = JsonObject.EMPTY
                .put("a", JsonObject.EMPTY
                        .put("a1", "a-value"))
                .put("b", "b-value");

        JsonValue schema = JsonObject.EMPTY
                .put("properties", JsonObject.EMPTY
                        .put("a", JsonObject.EMPTY
                                .put("properties", JsonObject.EMPTY
                                        .put("a1", JsonObject.EMPTY
                                                .put("maxLength", 5))))
                        .put("b", JsonObject.EMPTY
                                .put("pattern", "ACB123")
                                .put("maxLength", 3))
                ).jsonValue();

        JsonObject validationResult = validator.validate(instance.jsonValue(), schema, OutputStructure.DETAILED);

        String expectedApiErrorString = "{\n" +
                "  \"/b\": {\n" +
                "    \"badValue\": \"b-value\",\n" +
                "    \"violations\": {\n" +
                "      \"/properties/b/pattern\": \"ACB123\",\n" +
                "      \"/properties/b/maxLength\": 3\n" +
                "    }\n" +
                "  },\n" +
                "  \"/a/a1\": {\n" +
                "    \"badValue\": \"a-value\",\n" +
                "    \"violations\": {\n" +
                "      \"/properties/a/properties/a1/maxLength\": 5\n" +
                "    }\n" +
                "  }\n" +
                "}";

        JsonObject expectedApiError = JsonUtil.create(expectedApiErrorString);
        JsonObject apiError = apiError(validationResult, instance.jsonValue(), schema);
        assertThat(apiError, is(expectedApiError));
    }

    JsonObject apiError(JsonObject detailedJsonSchemaOutput, JsonValue validatedInstance, JsonValue schema){

        JsonObject apiError = JsonObject.EMPTY;

        ArrayDeque<JsonObject> stack = new ArrayDeque<>();

        stack.push(detailedJsonSchemaOutput);

        while (!stack.isEmpty()){
            JsonObject topError = stack.pop();
            JsonArray errors = topError.get("errors", JsonValue.NULL).getJsonArray();
            if (errors == null || errors.isEmpty()){

                JsonPointer instanceLocation = topError.get("instanceLocation").as(JsonPointer.class);
                JsonObject fieldInfo = apiError.get(instanceLocation.getJsonPointerString().getString(), JsonObject.EMPTY).getJsonObject();

                JsonObject violations;
                if (fieldInfo.isEmpty()) {
                    JsonValue badValue = validatedInstance.get(instanceLocation.asArray());
                    if (badValue != null) {
                        fieldInfo = fieldInfo.put("badValue", badValue);
                    }
                    violations = JsonObject.EMPTY;
                } else {
                    violations = fieldInfo.get("violations").getJsonObject();
                }
                JsonPointer keywordLocation = topError.get("keywordLocation").as(JsonPointer.class);
                violations = violations.put(keywordLocation.getJsonPointerString().getString(), schema.get(keywordLocation.asArray()));


                apiError = apiError
                        .put(topError.get("instanceLocation").getString(), fieldInfo.put("violations", violations));
            } else {
                for (JsonValue errorJsonValue: errors){
                    stack.push(errorJsonValue.getJsonObject());
                }
            }
        }

        return apiError;
    }
}