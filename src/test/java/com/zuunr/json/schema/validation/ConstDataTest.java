package com.zuunr.json.schema.validation;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectFactory;
import com.zuunr.json.JsonValue;
import com.zuunr.json.JsonValueFactory;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
public class ConstDataTest {

    JsonSchemaValidator validator = new JsonSchemaValidator(false);

    @Test
    void constData(){
        JsonValue validModel = JsonValueFactory.create("""
                {
                    "dynamicConst": "A",
                    "value": "A"
                }
                """);
        JsonValue invalidModel = JsonValueFactory.create("""
                {
                    "dynamicConst": "B",
                    "value": "A"
                }
                """);
        JsonValue jsonSchema = JsonValueFactory.create("""
                {
                    "properties": {
                        "value": {
                            "const": {"$data": "/dynamicConst"}
                        }
                    }
                }
                """);
        JsonObject validResult = validator.validate(validModel, jsonSchema, OutputStructure.DETAILED);
        assertThat(validResult.get("valid").getBoolean(), is(true));
        JsonObject invalidResult = validator.validate(invalidModel, jsonSchema, OutputStructure.DETAILED);
        assertThat(invalidResult.get("valid").getBoolean(), is(false));

    }
}