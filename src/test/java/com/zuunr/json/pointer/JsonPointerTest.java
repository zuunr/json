package com.zuunr.json.pointer;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonValue;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
public class JsonPointerTest {

    @Test
    void testEmptyString() {
        assertThat(
                JsonValue.of("/").as(JsonPointer.class).asArray(),
                is(JsonArray.of("")));
    }

    @Test
    void testEmptyStrings() {
        assertThat(
                JsonValue.of("//").as(JsonPointer.class).asArray(),
                is(JsonArray.of("","")));
    }

    @Test
    void testEmptyStrings2() {
        assertThat(
                JsonValue.of("/body/").as(JsonPointer.class).asArray(),
                is(JsonArray.of("body","")));
    }

    @Test
    void test1() {
        assertThat(
                JsonValue.of("/some/2/path").as(JsonPointer.class).asArray(),
                is(JsonArray.of("some", "2", "path")));
    }

    @Test
    void test2() {
        assertThat(
                JsonPointer.parseJsonPointer("/a~1b/m~0n"),
                is(JsonArray.of("a/b", "m~n")));
    }

    @Test
    void test3() {
        assertThat(
                JsonPointer.parseJsonPointer("#/c%25d"),
                is(JsonArray.of("c%d")));
    }

    @Test
    void test4() {
        assertThat(
                JsonPointer.parseJsonPointer("#"),
                is(JsonArray.EMPTY));
    }

    @Test
    void test5() {
        assertThat(
                JsonPointer.parseJsonPointer(""),
                is(JsonArray.EMPTY));
    }

    @Test
    void test6() {
        assertThat(
                JsonPointer.parseJsonPointer("#/"),
                is(JsonArray.of("")));
    }

    @Test
    void test7() {
        assertThat(
                JsonPointer.parseJsonPointer("/"),
                is(JsonArray.of("")));
    }

    @Test
    void test8() {
        assertThat(
                JsonPointer.parseJsonPointer("/ "),
                is(JsonArray.of(" ")));
    }

    @Test
    void test9() {
        assertThat(
                JsonPointer.parseJsonPointer("/1-2"),
                is(JsonArray.of("1-2")));
    }

    @Test
    void test10() {
        assertThat(
                JsonPointer.parseJsonPointer("/"),
                is(JsonArray.of("")));
    }

    @Test
    void jsonPointerStringOf0() {
        assertThat(
                JsonPointer.jsonPointerStringOf(JsonArray.of("some", 2, "path")), is("/some/2/path"));
    }

    @Test
    void jsonPointerStringOf1() {
        assertThat(
                JsonPointer.jsonPointerStringOf(JsonArray.of("a/b", "m~n")), is("/a~1b/m~0n"));
    }

    @Test
    void jsonPointerStringOf2() {
        assertThat(
                JsonPointer.jsonPointerStringOf(JsonArray.of("c%d")), is("/c%d"));
    }


    @Test
    void jsonPointerStringOf4() {
        assertThat(
                JsonPointer.jsonPointerStringOf(JsonArray.EMPTY), is(""));
    }


    @Test
    void jsonPointerStringOf5() {
        assertThat(
                JsonPointer.jsonPointerStringOf(JsonArray.of("")), is("/"));
    }

    @Test
    void jsonPointerStringOf6() {
        assertThat(
                JsonPointer.jsonPointerStringOf(JsonArray.of(" ")), is("/ "));
    }

    @Test
    void uriFragmentJsonPointerStringOf0() {
        assertThat(
                JsonPointer.uriFragmentJsonPointerStringOf(JsonArray.of("some", 2, "path")), is("#/some/2/path"));
    }

    @Test
    void uriFragmentJsonPointerStringOf1() {
        assertThat(
                JsonPointer.uriFragmentJsonPointerStringOf(JsonArray.of("a/b", "m~n")), is("#/a~1b/m~0n"));
    }

    @Test
    void uriFragmentJsonPointerStringOf1b() {
        assertThat(
                JsonPointer.parseJsonPointer("#/a~1b/m~0n"), is(JsonArray.of("a/b", "m~n")));
    }

    @Test
    void uriFragmentJsonPointerStringOf1c() {
        assertThat(
                JsonPointer.parseJsonPointer("#/a~1b~0~1c/m~0n/a~1b/m~0n"), is(JsonArray.of("a/b~/c", "m~n", "a/b", "m~n")));
    }

    @Test
    void uriFragmentJsonPointerStringOf2() {
        assertThat(
                JsonPointer.uriFragmentJsonPointerStringOf(JsonArray.of("c%d")), is("#/c%25d"));
    }

    @Test
    void uriFragmentJsonPointerStringOf4() {
        assertThat(
                JsonPointer.uriFragmentJsonPointerStringOf(JsonArray.EMPTY), is("#"));
    }

    @Test
    void uriFragmentJsonPointerStringOf5() {
        assertThat(
                JsonPointer.uriFragmentJsonPointerStringOf(JsonArray.of("")), is("#/"));
    }

    @Test
    void uriFragmentJsonPointerStringOf6() {
        assertThat(
                JsonPointer.uriFragmentJsonPointerStringOf(JsonArray.of(" ")), is("#/%20"));
        assertThat(
                JsonPointer.parseJsonPointer("#/%20"), is(JsonArray.of(" ")));
        assertThat(
                JsonPointer.parseJsonPointer("#/+"), is(JsonArray.of(" ")));
    }

    @Test
    void invalidJsonPointer() {
        try {
            JsonPointer jsonPointer = JsonValue.of("kalle").as(JsonPointer.class);
            assertThat(false, is(true));
        } catch (Exception e) {
            assertThat(true, is(true));

        }
    }
}
