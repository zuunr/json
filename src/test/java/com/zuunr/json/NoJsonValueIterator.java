/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import clojure.lang.APersistentVector;

import java.util.ListIterator;

import static com.zuunr.json.NoJsonValueUtil.noJsonValueOf;

/**
 * @author Niklas Eldberger
 */
public class NoJsonValueIterator implements ListIterator<Object> {

    private final ListIterator<Object> wrappedIterator;

    NoJsonValueIterator(APersistentVector persistentVector, int index) {
        this.wrappedIterator = persistentVector.listIterator(index);
    }

    @Override
    public boolean hasNext() {
        return wrappedIterator.hasNext();
    }

    @Override
    public Object next() {
        JsonValue jsonValue = (JsonValue) wrappedIterator.next();
        return noJsonValueOf(jsonValue);
    }

    @Override
    public boolean hasPrevious() {
        return wrappedIterator.hasPrevious();
    }

    @Override
    public Object previous() {
        return noJsonValueOf((JsonValue) wrappedIterator.previous());
    }

    @Override
    public int nextIndex() {
        return wrappedIterator.nextIndex();
    }

    @Override
    public int previousIndex() {
        return wrappedIterator.previousIndex();
    }

    @Override
    public void remove() {
        wrappedIterator.remove();
    }

    @Override
    public void set(Object t) {
        throw new RuntimeException("Not supported!");
    }

    @Override
    public void add(Object t) {
        throw new RuntimeException("Not supported!");
    }
}