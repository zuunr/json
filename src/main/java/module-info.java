module com.zuunr.json {
    requires org.slf4j;
    requires com.github.benmanes.caffeine;
    requires clojure;
    requires com.fasterxml.jackson.databind;
    exports com.zuunr.json;
    exports com.zuunr.json.schema;
    exports com.zuunr.json.schema.validation;
    exports com.zuunr.json.schema.generation    ;
    exports com.zuunr.json.util;
    exports com.zuunr.json.pointer;
    exports com.zuunr.json.schema.validation.node.string;
}