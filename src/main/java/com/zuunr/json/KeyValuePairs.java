package com.zuunr.json;

public interface KeyValuePairs {

    public JsonValue getKey(int index);

    public JsonValue getValue(int index);

    public int getSize();
}
