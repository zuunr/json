/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import clojure.lang.ITransientVector;
import clojure.lang.PersistentVector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author Niklas Eldberger
 */
public class JsonArrayBuilder {
    private ITransientVector transientVector;

    public JsonArrayBuilder(JsonArray initialJsonArray) {
        transientVector = initialJsonArray.persistentVector.asTransient();
    }

    public JsonArrayBuilder add(JsonValue jsonValue) {
        validateNull(jsonValue);
        transientVector = transientVector.assocN(transientVector.count(), jsonValue);
        return this;
    }

    public JsonArrayBuilder set(int index, JsonValue jsonValue){
        validateNull(jsonValue);
        transientVector.assocN(index, jsonValue);
        return this;
    }

    public JsonArrayBuilder add(JsonValueSupport jsonValueSupport) {
        return add(jsonValueSupport.asJsonValue());
    }

    public JsonArrayBuilder add(JsonObject jsonObject) {
        return this.add(jsonObject.jsonValue());
    }

    public JsonArrayBuilder add(JsonArray jsonArray) {
        return this.add(jsonArray.jsonValue());
    }

    public JsonArrayBuilder add(String string) {
        return add(JsonValue.of(string));
    }

    public JsonArrayBuilder add(Integer integer) {
        return add(JsonValue.of(integer));
    }

    public JsonArrayBuilder addAll(JsonArray jsonArrayToAdd) {
        for (int i = 0; i < jsonArrayToAdd.size(); i++) {
            add(jsonArrayToAdd.get(i));
        }
        return this;
    }

    public JsonArrayBuilder removeLast() {
        throw new UnsupportedOperationException("removeLast() is not implemented!");
    }

    public int size() {
        return transientVector.count();
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public JsonArray build() {
        return JsonArray.of((PersistentVector) transientVector.persistent());
    }

    public JsonArray buildSorted(Comparator<JsonValue> comparator) {
        List<JsonValue> list = new ArrayList<>((PersistentVector) transientVector.persistent());
        Collections.sort(list, comparator);
        return JsonArray.of(PersistentVector.create(list));
    }

    private static void validateNull(JsonValue jsonValue) {
        if (jsonValue == null) {
            throw new NullPointerException("Not allowed to add null, use JsonValue.NULL instead");
        }
    }
}
