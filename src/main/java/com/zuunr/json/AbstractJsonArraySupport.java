/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

/**
 * @author Niklas Eldberger
 */
public class AbstractJsonArraySupport implements JsonArraySupport {

    private final JsonArray jsonArray;

    protected AbstractJsonArraySupport(JsonArray jsonArray) {
        this.jsonArray = jsonArray;
    }

    @Override
    public JsonArray asJsonArray() {
        return jsonArray;
    }

    @Override
    public JsonValue asJsonValue() {
        return jsonArray.jsonValue();
    }

    @Override
    public boolean equals(Object o) {
        return o != null &&
                getClass().equals(o.getClass()) &&
                asJsonArray().equals(((JsonArraySupport)o).asJsonArray());
    }

    @Override
    public int hashCode(){
        return jsonArray.hashCode();
    }

    @Override
    public String toString(){
        return jsonArray.toString();
    }
}
