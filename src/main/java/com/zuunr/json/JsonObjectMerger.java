/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import com.zuunr.json.merge.ArrayMergeStrategy;
import com.zuunr.json.merge.MergeStrategy;
import com.zuunr.json.merge.NullMergeStrategy;
import com.zuunr.json.merge.UnsupportedMergeStrategyException;

import java.util.Iterator;

/**
 * @author Niklas Eldberger
 */
public class JsonObjectMerger {

    private final MergeStrategy mergeStrategy;

    /**
     * <p>Same as JsonObjectMergeStrategy(MergeStrategy.NULL_AS_VALUE_AND_ARRAY_BY_ELEMENT)</p>
     */
    public JsonObjectMerger() {
        this(MergeStrategy.NULL_AS_VALUE_AND_ARRAY_BY_ELEMENT);
    }

    public JsonObjectMerger(MergeStrategy mergeStrategy) {
        this.mergeStrategy = mergeStrategy;
    }

    /**
     * <p>Will merge according to the merge strategy used when creating this instance.</p>
     * 
     * @param jsonObject
     * @param overrider
     * @return
     */
    public JsonObject merge(JsonObject jsonObject, JsonObject overrider) {
        return merge(jsonObject, overrider, mergeStrategy);
    }

    /**
     *
     * @param jsonObject
     * @param overrider
     * @param mergeStrategy is the strategy that will be used regardless of what merge strategy was used when creating this JsonObjectMerger instance.
     * @return
     */
    public JsonObject merge(JsonObject jsonObject, JsonObject overrider, MergeStrategy mergeStrategy) {
        if (jsonObject == null) {
            jsonObject = JsonObject.EMPTY;
        }

        if (overrider == null) {
            overrider = JsonObject.EMPTY;
        }

        if (jsonObject.isEmpty()) {
            return overrider;
        } else if (overrider.isEmpty()) {
            return jsonObject;
        }

        JsonObjectBuilder merged = jsonObject.builder();

        JsonArray overriderValues = overrider.values();
        JsonArray overriderKeys = overrider.keys();

        Iterator<JsonValue> valueIterator = overriderValues.asList().iterator();
        for (String key : overriderKeys.asList(String.class)) {
            JsonValue overriderValue = valueIterator.next();
            JsonValue originalValue = jsonObject.get(key, JsonValue.NULL);
            if (overriderValue.isNull() && NullMergeStrategy.NULL_AS_DELETE == mergeStrategy.nullMergeStrategy) {
                merged.remove(key);
            } else {
                merged.put(key, mergeJsonValue(originalValue, overriderValue, mergeStrategy));
            }
        }
        return merged.build();
    }


    /**
     * @param jsonObject
     * @param overrider
     * @param mergeArrays
     * @return
     * @deprecated use merge instead
     */
    @Deprecated
    public JsonObject merge2(JsonObject jsonObject, JsonObject overrider, boolean mergeArrays) {

        if (jsonObject == null) {
            jsonObject = JsonObject.EMPTY;
        }

        if (overrider == null) {
            overrider = JsonObject.EMPTY;
        }

        if (jsonObject.isEmpty()) {
            return overrider;
        } else if (overrider.isEmpty()) {
            return jsonObject;
        }

        JsonObjectBuilder merged = jsonObject.builder();

        JsonArray overriderValues = overrider.values();
        JsonArray overriderKeys = overrider.keys();

        Iterator<JsonValue> valueIterator = overriderValues.asList().iterator();
        for (String key : overriderKeys.asList(String.class)) {
            JsonValue overriderValue = valueIterator.next();
            JsonValue originalValue = jsonObject.get(key, JsonValue.NULL);
            merged.put(key, mergeJsonValue2(originalValue, overriderValue, mergeArrays));
        }
        return merged.build();
    }

    protected JsonArray mergeJsonArray(JsonArray jsonArray, JsonArray overrider, MergeStrategy mergeStrategy) {
        JsonArray result;

        if (ArrayMergeStrategy.ARRAY == mergeStrategy.arrayMergeStrategy) {
            result = overrider;
        } else if (ArrayMergeStrategy.ELEMENT == mergeStrategy.arrayMergeStrategy) {
            JsonArrayBuilder resultBuilder = JsonArray.EMPTY.builder();

            for (int index = 0; index < overrider.size(); index++) {
                if (jsonArray.size() > index) {
                    JsonValue merged = mergeJsonValue(jsonArray.get(index), overrider.get(index), mergeStrategy);
                    resultBuilder = resultBuilder.add(merged);
                } else if (overrider.size() > index) {
                    resultBuilder = resultBuilder.add(overrider.get(index));
                } else {
                    break;
                }
            }
            if (jsonArray.size() > overrider.size()) {
                resultBuilder.addAll(jsonArray.subArray(overrider.size()));
            }
            result = resultBuilder.build();
        } else {
            throw new UnsupportedMergeStrategyException("Only ARRAY and ELEMENT is supported");
        }
        return result;
    }

    /**
     * @deprecated Use mergeJsonArray instead
     * @param jsonArray
     * @param overrider
     * @param mergeArrays
     * @return
     */
    @Deprecated
    protected JsonArray mergeJsonArray2(JsonArray jsonArray, JsonArray overrider, boolean mergeArrays) {
        JsonArrayBuilder result = JsonArray.EMPTY.builder();

        for (int index = 0; index < overrider.size(); index++) {
            if (jsonArray.size() > index) {
                JsonValue merged = mergeJsonValue2(jsonArray.get(index), overrider.get(index), mergeArrays);
                result = result.add(merged);
            } else if (overrider.size() > index) {
                result = result.add(overrider.get(index));
            } else {
                break;
            }
        }
        if (jsonArray.size() > overrider.size()) {
            result.addAll(jsonArray.subArray(overrider.size()));
        }

        return result.build();
    }


    protected JsonValue mergeJsonValue(JsonValue jsonValue, JsonValue overrider, MergeStrategy mergeStrategy) {
        JsonValue merged;

        if (jsonValue == null) {
            merged = overrider;
        } else if (overrider == null) {
            merged = jsonValue;
        } else if (ArrayMergeStrategy.ELEMENT == mergeStrategy.arrayMergeStrategy && overrider.isJsonArray()) {
            if (jsonValue.isJsonArray()) {
                merged = mergeJsonArray(jsonValue.getJsonArray(), overrider.getJsonArray(), mergeStrategy).jsonValue();
            } else {
                merged = overrider;
            }
        } else if (overrider.isJsonObject()) {
            if (jsonValue.isJsonObject()) {
                merged = merge(jsonValue.getJsonObject(), overrider.getJsonObject(), mergeStrategy).jsonValue();
            } else {
                merged = overrider;
            }
        } else {
            merged = overrider;
        }
        return merged;
    }

    /**
     * @deprecated use mergeJsonValue instead
     * @param jsonValue
     * @param overrider
     * @param mergeArrays
     * @return
     */
    @Deprecated
    protected JsonValue mergeJsonValue2(JsonValue jsonValue, JsonValue overrider, boolean mergeArrays) {
        JsonValue merged;

        if (jsonValue == null) {
            merged = overrider;
        } else if (overrider == null) {
            merged = jsonValue;
        } else if (mergeArrays && overrider.isJsonArray()) {
            if (jsonValue.isJsonArray()) {
                merged = mergeJsonArray2(jsonValue.getJsonArray(), overrider.getJsonArray(), mergeArrays).jsonValue();
            } else {
                merged = overrider;
            }
        } else if (overrider.isJsonObject()) {
            if (jsonValue.isJsonObject()) {
                merged = merge2(jsonValue.getJsonObject(), overrider.getJsonObject(), mergeArrays).jsonValue();
            } else {
                merged = overrider;
            }
        } else {
            merged = overrider;
        }
        return merged;
    }

    /**
     * @param jsonObject
     * @param overrider
     * @param nullOverrides any value (incl null) for a key overrides the value of the same key in jsonObject
     * @return
     * @deprecated Use merge(JsonObject, JsonObject) instead. There is no support for nulloverrides = false
     */

    @Deprecated
    public JsonObject merge(JsonObject jsonObject, JsonObject overrider, boolean nullOverrides) {
        if (!nullOverrides) {
            throw new UnsupportedMergeStrategyException("nulloverrides must be true");
        }
        return merge2(jsonObject, overrider, true);
    }

    /**
     * This one merges objects in json arrays
     * @deprecated use merge with MergeStrategy.NULL_AS_VALUE_AND_ARRAY_BY_ELEMENT instead
     * @param original
     * @param overrider
     * @return
     */
    @Deprecated
    public JsonObject mergeAll(JsonObject original, JsonObject overrider) {
        return merge2(original, overrider, true);
    }
}
