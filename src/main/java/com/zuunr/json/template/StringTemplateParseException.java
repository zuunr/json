package com.zuunr.json.template;

public class StringTemplateParseException extends RuntimeException {
    public StringTemplateParseException(String message) {
        super(message);
    }
}
