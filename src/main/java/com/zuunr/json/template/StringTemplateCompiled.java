package com.zuunr.json.template;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonValue;
import com.zuunr.json.pointer.JsonPointer;

public class StringTemplateCompiled {

    public final JsonArray staticFragments;
    public final JsonArray dynamicFragments;

    public StringTemplateCompiled(JsonArray staticFragments, JsonArray dynamicFragments) {
        this.staticFragments = staticFragments;
        this.dynamicFragments = dynamicFragments;
    }

    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder(staticFragments.get(0).getString());
        for (int i = 0; i < dynamicFragments.size(); i++) {
                builder.append(dynamicFragments.get(i));
                builder.append(staticFragments.get(i+1));
        }
        return builder.toString();
    }

    private static final JsonArray TEMPLATE_VALUE_ONLY = JsonArray.of(JsonValue.EMPTY_STRING, JsonValue.EMPTY_STRING);

    public JsonValue render(JsonValue model) throws ModelValueUndefinedException {
        return render(model, '$', true);
    }

    public JsonValue render(JsonValue model, char jsonPointerPrefix) throws ModelValueUndefinedException {
        return render(model, jsonPointerPrefix, true);
    }

    public JsonValue render(JsonValue model, char jsonPointerPrefix, boolean stopOnMissingModelValue) throws ModelValueUndefinedException {

        if (staticFragments.isEmpty()){
            return JsonValue.EMPTY_STRING;
        }
        if (TEMPLATE_VALUE_ONLY.equals(staticFragments)) {
            String theOnlyTemplate = dynamicFragments.get(0).getString();
            if (theOnlyTemplate.length() > 0 && theOnlyTemplate.charAt(0) == jsonPointerPrefix) {
                return model.get(JsonPointer.of(dynamicFragments.get(0).getString().substring(1)));
            }
            return JsonValue.of("{{" + theOnlyTemplate + "}}");
        }

        StringBuilder stringBuilder = new StringBuilder();

        String previousStatic = staticFragments.get(0).getString(); // Cannot append before the type of the model value to be appended

        for (int dynamicIndex = 0; dynamicIndex < dynamicFragments.size(); dynamicIndex++) {
            int previousStaticStart = 0;
            String dynamicString = dynamicFragments.get(dynamicIndex).getString(); // TODO: dynamicString could be cached in class like DynamicString to avoid cpu for substring and JsonPointer.of and all that...
            if ("".equals(dynamicString) || dynamicString.charAt(0) != jsonPointerPrefix) {
                stringBuilder.append(previousStatic);
                stringBuilder.append("{{").append(dynamicString).append("}}");
            } else {
                if (dynamicString.charAt(0) == jsonPointerPrefix && dynamicString.length() > 1 && dynamicString.charAt(1) == jsonPointerPrefix) {
                    String unescaped = dynamicString.substring(1);
                    stringBuilder.append(previousStatic);
                    stringBuilder.append("{{").append(unescaped).append("}}");
                } else {
                    JsonPointer pointer = JsonPointer.of(dynamicString.substring(1));
                    JsonValue modelValue = model.get(pointer);
                    if (modelValue == null) {
                        if (stopOnMissingModelValue) {
                            throw new ModelValueUndefinedException("Value cannot be found in model at " + pointer.getJsonPointerString(), pointer);
                        }
                        stringBuilder.append(previousStatic);
                        stringBuilder.append("{{").append(dynamicString).append("}}");

                    } else if (modelValue.isString()) {
                        stringBuilder.append(previousStatic);
                        stringBuilder.append(modelValue.getString());
                    } else {

                        String nextStaticFragment = staticFragments.get(dynamicIndex + 1).getString();
                        if (previousStatic.endsWith("\"")
                                && nextStaticFragment.startsWith("\"")
                                && !previousStatic.endsWith("\\\"")
                                && !nextStaticFragment.startsWith("\\\"")) {
                            stringBuilder.append(previousStatic.substring(0, previousStatic.length() - 1));
                            stringBuilder.append(modelValue.asJson());
                            previousStaticStart = 1;
                        } else {
                            throw new ModelValueUndefinedException("Value is to be inserted inside JSON string value " + pointer.getJsonPointerString() + " is not string but " + modelValue.asJson(), pointer);
                        }
                    }
                }
            }

            // Cannot append before the type of the model value to be appended
            previousStatic = staticFragments.get(dynamicIndex + 1).getString().substring(previousStaticStart);
        }
        stringBuilder.append(previousStatic);
        return JsonValue.of(stringBuilder.toString());
    }


}
