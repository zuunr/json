package com.zuunr.json.template;

import com.zuunr.json.pointer.JsonPointer;

public class ModelValueUndefinedException extends RuntimeException {
    public final JsonPointer pointerToUndefinedValue;
    public ModelValueUndefinedException(String message, JsonPointer pointerToUndefinedValue) {
        super(message);
        this.pointerToUndefinedValue = pointerToUndefinedValue;
    }


}
