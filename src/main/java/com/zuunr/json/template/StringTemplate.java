package com.zuunr.json.template;

import com.zuunr.json.JsonValue;

public class StringTemplate {

    private static StringTemplateCompiler compiler = new StringTemplateCompiler();

    public final StringTemplateCompiled compiled;

    private StringTemplate(JsonValue jsonValue){

        compiled = compiler.compile(jsonValue);
    }
}
