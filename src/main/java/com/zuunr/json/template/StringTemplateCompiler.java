package com.zuunr.json.template;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArrayBuilder;
import com.zuunr.json.JsonValue;

public class StringTemplateCompiler {

    public StringTemplateCompiled compile(JsonValue template) {
        JsonArrayBuilder staticFragments = JsonArray.EMPTY.builder();
        JsonArrayBuilder dynamicFragments = null;
        String templateString = template.getString();

        char prevChar = ' ';
        int staticStart = 0;
        int candidateStart = -1;

        for (int i = 0; i < templateString.length(); i++) {
            char currentChar = templateString.charAt(i);


            if (candidateStart == -1 && currentChar == '{' && prevChar == '{') {
                candidateStart = i - 1;
            }

            if (candidateStart > -1 && currentChar == '}' && prevChar == '}') {
                if (dynamicFragments == null) {
                    dynamicFragments = JsonArray.EMPTY.builder();
                }
                String staticString = templateString.substring(staticStart, candidateStart);
                String dynamicString = templateString.substring(candidateStart + 2, i + 1 - 2);
                dynamicFragments = dynamicFragments.add(dynamicString);
                staticFragments = staticFragments.add(staticString);
                candidateStart = -1;
                staticStart = i + 1;
            }
            if (i + 1 == templateString.length()) {
                staticFragments = staticFragments.add(templateString.substring(staticStart));
            }
            prevChar = currentChar;
        }
        return new StringTemplateCompiled(staticFragments.build(), dynamicFragments == null ? JsonArray.EMPTY : dynamicFragments.build());
    }
}
