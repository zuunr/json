/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.util.Map;

/**
 * @author Niklas Eldberger
 */
public class JsonValueSerializer extends StdSerializer<JsonValue> {

    private static final long serialVersionUID = 1L;
    private final JsonObjectSerializer jsonObjectSerializer = new JsonObjectSerializer();
    private final JsonArraySerializer jsonArraySerializer = new JsonArraySerializer();

    public JsonValueSerializer() {
        this(null);
    }

    public JsonValueSerializer(Class<JsonValue> t) {
        super(t);
    }

    @Override
    public void serialize(JsonValue jsonValue, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {

        if (jsonValue.isJsonObject()) {
            jsonObjectSerializer.serialize(jsonValue.getJsonObject(), jsonGenerator, serializerProvider);
        } else if (jsonValue.isJsonArray()) {
            jsonArraySerializer.serialize(jsonValue.getJsonArray(), jsonGenerator, serializerProvider);
        } else {
            jsonGenerator.writeRaw(jsonValue.asJson());
        }
    }
}
