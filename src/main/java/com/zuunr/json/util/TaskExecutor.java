package com.zuunr.json.util;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;

/**
 * Implements Divide and Conquer for use when recursion may cause StackOverflow.
 *
 * Each Task is divided into three (3) parts:
 *
 * 1. Basecase - The base case where it is possible to find the solution without dividing into subtasks
 * 2. Subtasks - The subtasks of the Task where each Subtask is a smaller task than the Task
 * 3. Joining results - Creating a result by joining the results of the Subtasks
 * T - Input
 * U - Output
 * V - Context of Subtask in Task (e.g "firstName" when Subtask is nested JsonValue "Peter", of a JsonObject)
 * @author Niklas Eldberger
 */
public class TaskExecutor<T, U, V> {


    private final static Iterator EMPTY_ITERATOR = new ArrayList<>(0).iterator();

    /**
     *
     * @param initialTask
     * @return initialTask.getOut() is returned when calculated
     */
    public final U solve(Task<T, U, V> initialTask) {

        Deque<Task> stack = new ArrayDeque<>();
        stack.push(initialTask);

        U topTaskResult = null;

        ArrayList<Subtask<T, U, V>> subtasks = null;

        while (!stack.isEmpty()) {
            Task<T, U, V> topTask = stack.peek();

            topTaskResult = topTask.getOut();

            if (topTaskResult == null) { // Check if it is possible to create the result based on the subtasks results

                if (topTask.getSubtasks() == null) {
                    // Check if task is basecase that can be solved without further division into subtasks
                    topTaskResult = topTask.getOutFromBasecase(topTask.getIn());
                } else {
                    // If not basecase - try to create result already executed subtasks
                    topTaskResult = topTask.getOutFromFinishedSubtasks(topTask.getSubtasks());
                }
            }

            if (topTaskResult != null) {
                topTask.setOut(topTaskResult); // because topTask may be referenced from subtasks
                stack.poll();
            } else {
                // Split task into subtasks
                subtasks = new Subtasks(stack);
                topTask.splitIntoSubtasks(topTask.getSubtasks() == null ? EMPTY_ITERATOR : topTask.getSubtasks().iterator(), subtasks); // Adds tasks to stack via
                topTask.setSubtasks(subtasks);
            }
        }
        return topTaskResult;
    }

    /**
     *
     * @param <T> Input
     * @param <U> Output
     * @param <V> Context of this subtask (e.g "firstName")
     */
    static class Subtasks<T, U, V> extends ArrayList<Subtask<T, U, V>> {

        private final Deque<Task<T, U, V>> stack;

        public Subtasks(Deque<Task<T, U, V>> stack) {
            this.stack = stack;
        }

        @Override
        public boolean add(Subtask<T, U, V> subtask) {
            stack.push(subtask.getTask());
            return super.add(subtask);
        }
    }
}
