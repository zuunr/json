package com.zuunr.json.util;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArrayBuilder;

public class StringSplitter {

    public static JsonArray splitString(String string, char separator) {
        JsonArrayBuilder builder = JsonArray.EMPTY.builder();
        int startIndex = 0;
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == separator) {
                builder.add(string.substring(startIndex, i));
                startIndex = i + 1;
            }
        }
        builder.add(string.substring(startIndex));
        return builder.build();
    }
}