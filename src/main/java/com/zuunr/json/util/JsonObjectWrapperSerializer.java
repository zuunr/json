/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json.util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.zuunr.json.JsonObject;

/**
 * <p>The JsonObjectWrapperSerializer is responsible for
 * serializing a {@link JsonObjectWrapper} object, by
 * calling its underlying {@link JsonObject#asJson()}
 * method.</p>
 * 
 * <p>The json string is written with {@link JsonGenerator#writeRaw(char)}
 * to avoid escaping.</p>
 *
 * @author Mikael Ahlberg
 */
public class JsonObjectWrapperSerializer extends StdSerializer<JsonObjectWrapper> {
    
    private static final long serialVersionUID = 1L;
    
    public JsonObjectWrapperSerializer() {
        this(null);
    }
    
    protected JsonObjectWrapperSerializer(Class<JsonObjectWrapper> t) {
        super(t);
    }

    @Override
    public void serialize(JsonObjectWrapper value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeRaw(value.asJsonObject().asJson());
    }
}
