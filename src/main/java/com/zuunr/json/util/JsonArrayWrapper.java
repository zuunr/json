/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json.util;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArraySupport;
import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public class JsonArrayWrapper implements JsonArraySupport {

    private JsonArray jsonArray;

    public static JsonArrayWrapper of(JsonArray jsonArray) {
        return new JsonArrayWrapper(jsonArray);
    }

    /**
     * @param jsonValue
     */
    private JsonArrayWrapper(JsonValue jsonValue) { // NOSONAR
        this.jsonArray = jsonValue.getValue(JsonArray.class);
    }

    private JsonArrayWrapper(JsonArray jsonArray) {
        this.jsonArray = jsonArray;
    }

    @Override
    public JsonArray asJsonArray() {
        return jsonArray;
    }
}
