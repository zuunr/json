package com.zuunr.json.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 *
 * @param <T> Input
 * @param <U> Output (result)
 * @param <V> Context of Subtask
 * @author Niklas Eldberger
 */
public abstract class Task<T, U, V> {
    private final T in;
    private U out;
    private ArrayList<Subtask<T, U, V>> subtasks;

    public Task(T in) {
        this.in = in;
    }

    /**
     * Input to task
     * @return
     */
    public final T getIn() {
        return in;
    }

    /**
     * Result of task
     * @return
     */
    public final U getOut() {
        return out;
    }

    final void setOut(U out) {
        this.out = out;
    }

    final ArrayList<Subtask<T, U, V>> getSubtasks() {
        return subtasks;
    }

    /**
     *
     * @param finishedSubtasks - Subtasks that are already finished but were not enough to combine to a result of this Task in the getOutFromFinishedSubtasks method
     * @param subtasks - The collection where new Subtasks will be put by this method
     */
    public abstract void splitIntoSubtasks(Iterator<Subtask<T, U, V>> finishedSubtasks, Collection<Subtask<T, U, V>> subtasks);

    /**
     * Gets the result of this Task by combining the results of its already executed Subtasks. Is called when its Subtasks are executed
     * @param finishedSubtasks
     * @return Result if could be created, null otherwise.
     */
    public abstract U getOutFromFinishedSubtasks(Collection<Subtask<T, U, V>> finishedSubtasks);

    /**
     * Gets the result of this Task without using any Subtasks
     * @param in
     * @return Result if could be created, null otherwise.
     */
    public abstract U getOutFromBasecase(T in);

    final void setSubtasks(ArrayList<Subtask<T, U, V>> subtasks) {
        this.subtasks = subtasks;
    }
}