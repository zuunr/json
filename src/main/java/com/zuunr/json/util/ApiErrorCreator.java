package com.zuunr.json.util;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArrayBuilder;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.pointer.JsonPointer;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.Keywords;

import java.util.ArrayDeque;
import java.util.Comparator;


/**
 * @author Niklas Eldberger
 */
public class ApiErrorCreator {

    private final JsonValue ADDITIONAL_PROPERTIES = JsonValue.of(Keywords.ADDITIONAL_PROPERTIES);
    public final static ApiErrorCreator ERROR_ARRAY_WITH_VIOLATIONS_ARRAY = new ApiErrorCreator(
            true,
            "field",
            "rejectedValue",
            "violations",
            true,
            "keyword",
            "value");

    public final static ApiErrorCreator ERROR_OBJECT_WITH_VIOLATIONS_OBJECT = new ApiErrorCreator(
            false,
            null,
            "rejectedValue",
            "violations",
            false,
            null,
            null);


    private final String nameOfInstanceLocation;
    private final String nameOfRejectedValue;
    private final String nameOfViolations;
    private final boolean arrayViolations;
    private final boolean arrayErrors;
    private final String nameOfViolationLocation;
    private final String nameOfViolationValue;

    public ApiErrorCreator(boolean arrayErrors, String nameOfInstanceLocation, String nameOfRejectedValue, String nameOfViolations, boolean arrayViolations, String nameOfViolationLocation, String nameOfViolationValue) {
        this.arrayErrors = arrayErrors;
        this.nameOfInstanceLocation = nameOfInstanceLocation;
        this.nameOfRejectedValue = nameOfRejectedValue;
        this.nameOfViolationLocation = nameOfViolationLocation;
        this.nameOfViolations = nameOfViolations;
        this.arrayViolations = arrayViolations;
        this.nameOfViolationValue = nameOfViolationValue;
    }

    public ApiErrorCreator() {
        this(
                false,
                null,
                "rejectedValue",
                "violations",
                false,
                null,
                null);
    }

    public JsonValue createErrors(JsonObject detailedJsonSchemaOutput, JsonValue validatedInstance, JsonSchema schema) {
        return arrayErrors
                ? createErrorsArray(detailedJsonSchemaOutput, validatedInstance, schema).jsonValue()
                : createErrorsObject(detailedJsonSchemaOutput, validatedInstance, schema).jsonValue();
    }

    public JsonObject createErrorsAndSchemaObject(JsonObject detailedJsonSchemaOutput, JsonValue validatedInstance, JsonSchema schema) {
        return JsonObject.EMPTY.put("errors", createErrorsObject(detailedJsonSchemaOutput, validatedInstance, schema)).put("schema", schema.asJsonValue());
    }

    public JsonObject createErrorsObject(JsonObject detailedJsonSchemaOutput, JsonValue validatedInstance, JsonSchema schema) {

        JsonObject apiError = JsonObject.EMPTY;

        ArrayDeque<JsonObject> stack = new ArrayDeque<>();

        stack.push(detailedJsonSchemaOutput);

        while (!stack.isEmpty()) {
            JsonObject topError = stack.pop();
            JsonArray errors = topError.get("errors", JsonValue.NULL).getJsonArray();
            if (errors == null || errors.isEmpty()) {

                JsonPointer instanceLocation = topError.get("instanceLocation").as(JsonPointer.class);
                JsonObject fieldInfo = apiError.get(instanceLocation.getJsonPointerString().getString(), JsonObject.EMPTY).getJsonObject();

                JsonValue violations;
                if (fieldInfo.isEmpty()) {
                    JsonValue badValue = validatedInstance.get(instanceLocation);
                    if (badValue != null) {
                        fieldInfo = fieldInfo.put(nameOfRejectedValue, badValue);
                    }
                    violations = arrayViolations ? JsonArray.EMPTY.jsonValue() : JsonObject.EMPTY.jsonValue();
                } else {
                    violations = fieldInfo.get(nameOfViolations);
                }
                JsonPointer keywordLocation = topError.get("keywordLocation").as(JsonPointer.class);

                JsonArray keywordLocationAsArray = keywordLocation.asArray();
                JsonPointer locationOfKeywordParent = keywordLocationAsArray.allButLast().as(JsonPointer.class);
                JsonValue schemaOfParent = schema.get(locationOfKeywordParent, true);
                JsonValue keywordValue = schema.get(keywordLocation, true);

                violations = arrayViolations
                        ? violations.getJsonArray().add(
                        JsonObject.EMPTY
                                .put(nameOfViolationLocation, keywordLocation.getJsonPointerString().getString())
                                .put(nameOfViolationValue, keywordValue)).jsonValue()
                        : violations.getJsonObject().put(keywordLocation.getJsonPointerString().getString(), keywordValue).jsonValue();

                // Description as part of violations object
                JsonValue description = schemaOfParent.get("description");
                JsonPointer locationOfDescription = locationOfKeywordParent.asArray().add(Keywords.DESCRIPTION).as(JsonPointer.class);

                if (description == null) {
                    if (!keywordLocationAsArray.isEmpty() && keywordLocationAsArray.last().equals(ADDITIONAL_PROPERTIES) && JsonValue.FALSE.equals(keywordValue)) {

                        if (arrayViolations) {
                            violations = violations.getJsonArray()
                                    .add(JsonObject.EMPTY
                                            .put(nameOfViolationLocation, "description")
                                            .put(nameOfViolationValue, "Property '" + instanceLocation.asArray().last().getString() + "' has not been defined and schema does not allow additional properties")
                                    ).jsonValue();
                        } else {
                            violations =
                                    violations.getJsonObject().put("description", "Property '" + instanceLocation.asArray().last().getString() + "' has not been defined and schema does not allow additional properties").jsonValue();
                        }
                    }
                } else {

                    if (arrayViolations) {
                        violations = violations.getJsonArray()
                                .add(JsonObject.EMPTY
                                        .put(nameOfViolationLocation, locationOfDescription.getJsonPointerString().getString())
                                        .put(nameOfViolationValue, description)
                                ).jsonValue();
                    } else {
                        violations = violations.getJsonObject()
                                .put(locationOfDescription.getJsonPointerString().getString(), description)
                                .jsonValue();
                    }
                }

                apiError = apiError.put(topError.get("instanceLocation").getString(), fieldInfo.put(nameOfViolations, violations));
            } else {
                for (JsonValue errorJsonValue : errors) {
                    stack.push(errorJsonValue.getJsonObject());
                }
            }
        }
        return apiError;
    }

    public JsonArray createErrorsArray(JsonObject detailedJsonSchemaOutput, JsonValue validatedInstance, JsonSchema schema) {
        JsonObject errorObject = createErrorsObject(detailedJsonSchemaOutput, validatedInstance, schema);

        JsonArrayBuilder builder = JsonArray.EMPTY.builder();

        for (int i = 0; i < errorObject.size(); i++) {
            builder.add(errorObject.values().get(i).getJsonObject().put(nameOfInstanceLocation, errorObject.keys().get(i)));
        }

        return builder.buildSorted(new Comparator<JsonValue>() {
            @Override
            public int compare(JsonValue o1, JsonValue o2) {
                return o1.getJsonObject().get(nameOfInstanceLocation).getString().compareTo(o1.getJsonObject().get(nameOfInstanceLocation).getString());
            }
        });
    }
}