package com.zuunr.json.util;

import com.zuunr.json.JsonValue;
import com.zuunr.json.util.Task;

/**
 * @author Niklas Eldberger
 */
public final class Subtask<T, U, V> {
    final V context;
    final private Task<T, U, V> task;

    public Subtask(V context, Task<T, U, V> task) {
        this.context = context;
        this.task = task;
    }

    public Task<T, U, V> getTask() {
        return task;
    }

    public V getContext() {
        return context;
    }
}
