package com.zuunr.json.util;

import com.zuunr.json.*;

public class JsonArrayPatcher {

    public JsonArray patch(JsonArray original, JsonArray overrider, JsonArray itemKeyProperties) {
        JsonObjectPatcher jsonObjectPatcher = new JsonObjectPatcher();

        JsonObject toBePatchedObject = arrayAsObject(original, itemKeyProperties, null);
        JsonObject overriderObject = arrayAsObject(overrider, itemKeyProperties, toBePatchedObject);

        JsonObject resultObject = jsonObjectPatcher.patch(toBePatchedObject, overriderObject, JsonObjectPatcher.Strategy.REPLACE_ARRAY);

        JsonArray result = arrayOfObject(resultObject, itemKeyProperties);
        return result;
    }

    public JsonObject arrayAsObject(JsonArray arrayToPatch, JsonArray propertyInfo) {
        return arrayAsObject(arrayToPatch, propertyInfo, null);
    }

    private JsonObject arrayAsObject(JsonArray arrayToPatch, JsonArray propertyInfo, JsonObject whitelist) {
        JsonObject result = JsonObject.EMPTY;
        for (int i = 0; i < arrayToPatch.size(); i++) {
            JsonValue parametersItem = arrayToPatch.get(i);
            JsonArray path = createPath(parametersItem, propertyInfo);

            if (whitelist == null || whitelist.get(path) != null) {
                result = result.put(path, parametersItem);
            }
        }
        return result;
    }

    private JsonArray createPath(JsonValue itemToBePatched, JsonArray propertySpec) {
        JsonArrayBuilder builder = JsonArray.EMPTY.builder();
        for (int i = 0; i < propertySpec.size(); i++) {
            builder.add(itemToBePatched.get(propertySpec.get(i).getJsonArray()));
        }
        return builder.build();
    }


    JsonArray arrayOfObject(JsonObject rootObject, JsonArray meta) {
        return getItems(rootObject, meta, JsonArray.EMPTY);
    }


    private JsonArray getItems(JsonObject object, JsonArray meta, JsonArray toBeAddedToItem) {
        if (meta.isEmpty()) {
            for (int i = 0; i < toBeAddedToItem.size(); i++) {
                JsonArray propertyPath = toBeAddedToItem.get(i).get(0).getJsonArray();
                JsonValue value = toBeAddedToItem.get(i).get(1);
                object = object.put(propertyPath, value);
            }
            return JsonArray.of(object);
        }

        JsonArrayBuilder builder = JsonArray.EMPTY.builder();
        for (int i = 0; i < object.size(); i++) {
            builder.addAll(getItems(object.values().get(i).getJsonObject(), meta.tail(), toBeAddedToItem.add(JsonArray.of(meta.head(), object.keys().get(i)))));
        }
        return builder.build();
    }
}
