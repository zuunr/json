package com.zuunr.json.util;

import com.zuunr.json.JsonValue;

public class ApiErrorException extends RuntimeException {

    public final JsonValue errors;

    public ApiErrorException(JsonValue errors){
        this.errors = errors;
    }
}
