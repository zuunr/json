/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json.util;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectSupport;
import com.zuunr.json.JsonValue;

@JsonDeserialize(using = JsonObjectWrapperDeserializer.class)
@JsonSerialize(using = JsonObjectWrapperSerializer.class)
public class JsonObjectWrapper implements JsonObjectSupport {

    private final JsonObject jsonObject;

    public static JsonObjectWrapper of(JsonObject jsonObject) {
        JsonObjectWrapper result = null;
        if (jsonObject != null) {
            result = new JsonObjectWrapper(jsonObject);
        }
        return result;
    }

    /**
     *
     * @param jsonValue
     */
    private JsonObjectWrapper(JsonValue jsonValue) { // NOSONAR
        this.jsonObject = jsonValue.getValue(JsonObject.class);
    }

    private JsonObjectWrapper(JsonObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    @Override
    public JsonObject asJsonObject() {
        return jsonObject;
    }

    @Override
    public JsonValue asJsonValue() {
        return jsonObject.jsonValue();
    }
    
    @Override
    public String toString() {
        return jsonObject.toString();
    }
}
