/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

/**
 * @author Niklas Eldberger
 */
public class JsonArraySerializer extends StdSerializer<JsonArray> {

    private static final long serialVersionUID = 1L;

    public JsonArraySerializer() {
        this(null);
    }

    public JsonArraySerializer(Class<JsonArray> t) {
        super(t);
    }

    @Override
    public void serialize(JsonArray jsonArray, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {

        jsonGenerator.writeStartArray();

        for (int i = 0; i < jsonArray.size(); i++) {
            jsonGenerator.writeObject(jsonArray.get(i).getValue());
        }

        jsonGenerator.writeEndArray();
    }
}
