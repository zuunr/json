/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;

/**
 * @author Niklas Eldberger
 */
public class JsonObjectFactory {

    private ObjectMapper mapper;

    public JsonObjectFactory() {
        mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(JsonObject.class, new JsonObjectDeserializer());
        module.addDeserializer(JsonValue.class, new JsonValueDeserializer());
        mapper.registerModule(module);
        mapper.enable(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS);
        mapper.enable(DeserializationFeature.USE_LONG_FOR_INTS);
        mapper.setNodeFactory(JsonNodeFactory.withExactBigDecimals(true));
    }

    public JsonObject createJsonObject(File file) {
        try (InputStream fileStream = new FileInputStream(file)) {
            return createJsonObject(fileStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public JsonObject createJsonObject(InputStream inputStream) {
        return createJsonValue(inputStream).getJsonObject();
    }

    public JsonValue createJsonValue(InputStream inputStream) {

        try {
            return getObjectMapper().readValue(inputStream, JsonValue.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public JsonObject createJsonObject(Object pojo) {
        return getObjectMapper().convertValue(pojo, JsonObject.class);
    }

    public JsonObject createJsonObject(String json) {
        return createJsonValue(json).getJsonObject();
    }

    public JsonValue createJsonValue(String json) {
        try {
            return getObjectMapper().readValue(json, JsonValue.class);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private ObjectMapper getObjectMapper() {
        return mapper;
    }
}
