/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import java.util.Iterator;

/*
 *
 * Implementation is close to RFC 7396 - JSON Merge Patch
 * Difference is null will remain as value after patch and not
 * result in deletion of JSON field.
 */
public class JsonObjectPatcher {

    public JsonObject patch(JsonObject original, JsonObject overrider) {
        return patch(original, overrider, Strategy.REPLACE_ARRAY);
    }

    public JsonObject patch(JsonObject original, JsonObject overrider, Strategy strategy) {
        JsonObjectBuilder builder = original.builder();

        JsonArray keys = overrider.keys();
        Iterator<JsonValue> valueIter = overrider.values().asList().iterator();

        for (String overriderKey : keys.asList(String.class)) {

            JsonValue overriderValue = valueIter.next();
            if (overriderValue.is(JsonArray.class)) {
                if (strategy == Strategy.REPLACE_ARRAY) {
                    builder.put(overriderKey, overriderValue);
                }
            } else if (overriderValue.is(JsonObject.class)) {
                builder.put(
                        overriderKey,
                        patch(original.get(overriderKey, JsonObject.EMPTY).getValue(JsonObject.class), overriderValue.getValue(JsonObject.class), strategy));
            } else {
                builder.put(overriderKey, overriderValue);
            }
        }
        return builder.build();
    }

    public enum Strategy {
        MERGE_ARRAY,
        REPLACE_ARRAY
    }


}