/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import clojure.lang.PersistentHashMap;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Niklas Eldberger
 */
public class JsonSerializer {

    private static final Logger logger = LoggerFactory.getLogger(JsonSerializer.class);

    private static final JsonSerializer jsonSerializer = new JsonSerializer();

    private static final ObjectMapper prettyMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);

    public static String serialize(String string2beSerialized) {
        return jsonSerializer.escapeString(string2beSerialized);
    }

    protected String serialize(Object object) {

        if (object == null) {
            return "null";
        } else if (object.getClass().equals(JsonValue.class)) {
            JsonValue jsonValue = ((JsonValue) object);
            if (jsonValue.isJsonObject()) {
                return jsonValue.getJsonObject().toString();
            } else if (jsonValue.isJsonArray()) {
                return jsonValue.getJsonArray().toString();
            } else {
                return serialize(((JsonValue) object).getValue());
            }
        } else if (object.getClass().equals(PersistentHashMap.class)) {
            return serialize((Map) object);
        } else if (object.getClass() == String.class) {
            return '\"' + serialize((String) object) + '\"';
        } else if (object.getClass() == ArrayList.class) {
            return serialize((List) object);
        } else if (object.getClass() == Boolean.class) {
            return object.toString();
        } else if (object instanceof Number) {
            return object.toString();
        } else {
            throw new UnsupportedTypeException("class not supported: " + object.getClass());
        }
    }

    protected String serialize(Map<String, Object> map2serialize) {
        String commaSeparatedPairs = map2serialize.entrySet().stream()
                .map(entry -> '\"' + serialize(entry.getKey()) + "\":" + serialize(entry.getValue()))
                .reduce((pair1, pair2) -> pair1 + "," + pair2)
                .orElse("");
        return '{' + commaSeparatedPairs + '}';
    }

    protected String serialize(List<Object> list) {
        String commaSeparatedElements = list.stream().sequential()
                .map(this::serialize)
                .reduce((element1, element2) -> element1 + ',' + element2)
                .orElse("");
        return '[' + commaSeparatedElements + ']';
    }

    /**
     * <p>Escapes String as described in https://json.org</p>
     * 
     * @param strVal
     * @return
     */
    public String escapeString(String strVal) {

        StringBuilder escapedValueBuilder = new StringBuilder();

        for (int i = 0; i < strVal.length(); i++) {
            char current = strVal.charAt(i);
            if (current == '\n') {
                escapedValueBuilder.append("\\n");
            } else if (current == '\r') {
                escapedValueBuilder.append("\\r");
            } else if (current == '\f') {
                escapedValueBuilder.append("\\f");
            } else if (current == '\b') {
                escapedValueBuilder.append("\\b");
            } else if (current == '\t') {
                escapedValueBuilder.append("\\t");
            } else if (current == '\"') {
                escapedValueBuilder.append("\\\"");
            } else if (current == '\\') {
                escapedValueBuilder.append("\\\\");
            } else {
                escapedValueBuilder.append(current);
            }
        }
        return escapedValueBuilder.toString();
    }


    public String asPrettyJson(JsonObject jsonObject) {
        String result = null;
        try {
            String json = prettyMapper.writeValueAsString(jsonObject.asMapsAndLists(true));
            result = "\n\t\t" + json.replace("\n", "\n\t\t");
        } catch (Exception e) {
            String msg = "Failed to pretty print json";
            logger.error(msg, e);
            throw new RuntimeException(msg, e);
        }
        return result;
    }
}
