package com.zuunr.json;

import java.math.BigDecimal;

/**
 * @author Niklas Eldberger
 */
public final class JsonNumber extends Number implements Comparable {

    private Number nativeNumber;
    private JsonValue asJsonValue;
    private BigDecimal asBigDecimal;

    private String asString;

    static JsonNumber of(Number value) {
        if (value.getClass() == Integer.class ||
                value.getClass() == Long.class ||
                value.getClass() == Short.class ||
                value.getClass() == BigDecimal.class) {
            return new JsonNumber(value);
        }
        throw new UnsupportedTypeException("Not supported subclass of Number: " + value.getClass());
    }

    static JsonNumber of(Integer value) {
        return new JsonNumber(value);
    }

    static JsonNumber of(BigDecimal value) {
        return new JsonNumber(value);
    }

    static JsonNumber of(Long value) {
        return new JsonNumber(value);
    }

    static JsonNumber of(Short value) {
        return new JsonNumber(value);
    }

    private JsonNumber(Number nativeNumber) {
        this.nativeNumber = nativeNumber;
        this.asJsonValue = new JsonValue(this);
    }

    @Override
    public int intValue() {
        return nativeNumber.intValue();
    }

    @Override
    public long longValue() {
        return nativeNumber.longValue();
    }

    @Override
    public float floatValue() {
        return nativeNumber.floatValue();
    }

    @Override
    public double doubleValue() {
        return nativeNumber.doubleValue();
    }

    @Override
    public byte byteValue() {
        return nativeNumber.byteValue();
    }

    @Override
    public short shortValue() {
        return nativeNumber.shortValue();
    }

    @Override
    public String toString() {
        if (asString == null) {
            if (nativeNumber.getClass() == BigDecimal.class) {
                asString = ((BigDecimal) nativeNumber).stripTrailingZeros().toPlainString();
            } else {
                asString = nativeNumber.toString();
            }
        }
        return asString;
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj.getClass() == JsonNumber.class &&
                toString().equals(obj.toString());
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    public BigDecimal getBigDecimal() {
        return (BigDecimal) nativeNumber;
    }

    @Override
    public int compareTo(Object o) {

        // TODO: This one must be implemented more efficient!

        if (o == null || o.getClass() != JsonNumber.class) {
            throw new UnsupportedTypeException("Cannot compare!");
        }

        JsonNumber other = (JsonNumber) o;

        if (nativeNumber.getClass() != other.nativeNumber.getClass()) {
            return asBigDecimal().compareTo(((JsonNumber) o).asBigDecimal());
        } else if (nativeNumber instanceof Integer) {
            return ((Integer) nativeNumber).compareTo((Integer) other.nativeNumber);
        } else if (nativeNumber instanceof Long) {
            return ((Long) nativeNumber).compareTo((Long) other.nativeNumber);
        } else if (nativeNumber instanceof Short) {
            return ((Short) nativeNumber).compareTo((Short) other.nativeNumber);
        } else if (nativeNumber instanceof BigDecimal) {
            return ((BigDecimal) nativeNumber).compareTo((BigDecimal) other.nativeNumber);
        } else {
            throw new UnsupportedTypeException("Cannot compare!");
        }
    }

    public BigDecimal asBigDecimal() {
        if (asBigDecimal == null) {
            if (nativeNumber.getClass() == BigDecimal.class) {
                asBigDecimal = (BigDecimal) nativeNumber;
            } else {
                asBigDecimal = new BigDecimal(toString());
            }
        }
        return asBigDecimal;
    }


    public Number getWrappedNumber() {
        return nativeNumber;
    }

    public boolean isJsonInteger() {
        Class wrappedClass = nativeNumber.getClass();
        return wrappedClass == Integer.class ||
                wrappedClass == Long.class ||
                wrappedClass == Short.class ||
                getBigDecimal().remainder(BigDecimal.ONE).compareTo(BigDecimal.ZERO) == 0;
    }


    public JsonValue jsonValue() {
        return asJsonValue;
    }

    public String toIntString() {
        return ""+intValue();
    }


    public JsonNumber multiplyWith(Number operand){
        return multiplyWith(JsonValue.of(operand).getJsonNumber());
    }

    public JsonNumber multiplyWith(JsonNumber other) { // TODO: Add test cases for all combinations
        if (nativeNumber.getClass() != other.nativeNumber.getClass()) {
            return JsonValue.of(asBigDecimal().multiply(other.asBigDecimal())).getJsonNumber();
        } else if (nativeNumber instanceof Integer) {
            Integer result = ((Integer) nativeNumber) * (Integer) other.nativeNumber;
            return JsonValue.of(result).getJsonNumber();
        } else if (nativeNumber instanceof Long) {
            return JsonValue.of(((Long) nativeNumber) * (Long) other.nativeNumber).getJsonNumber();
        } else if (nativeNumber instanceof Short) {
            return JsonValue.of(((Short) nativeNumber) * (Short) other.nativeNumber).getJsonNumber();
        } else if (nativeNumber instanceof BigDecimal) {
            return JsonValue.of(((BigDecimal) nativeNumber).multiply((BigDecimal) other.nativeNumber)).getJsonNumber();
        } else {
            throw new UnsupportedTypeException("Cannot multiply!");
        }
    }

    public JsonNumber divideWith(JsonNumber other) {
        if (nativeNumber.getClass() != other.nativeNumber.getClass()) {
            return JsonValue.of(asBigDecimal().divide(other.asBigDecimal())).getJsonNumber();
        } else if (nativeNumber instanceof Integer && (Integer) nativeNumber % (Integer) other.nativeNumber == 0) {
            return JsonValue.of(((Integer) nativeNumber) / (Integer) other.nativeNumber).getJsonNumber();
        } else if (nativeNumber instanceof Long && (Long) nativeNumber % (Long) other.nativeNumber == 0) {
            return JsonValue.of(((Long) nativeNumber) / (Long) other.nativeNumber).getJsonNumber();
        } else if (nativeNumber instanceof Short && (Short) nativeNumber % (Short) other.nativeNumber == 0) {
            return JsonValue.of(((Short) nativeNumber) / (Short) other.nativeNumber).getJsonNumber();
        } else if (nativeNumber instanceof BigDecimal) {
            return JsonValue.of(((BigDecimal) nativeNumber).divide((BigDecimal) other.nativeNumber)).getJsonNumber();
        } else {
            throw new UnsupportedTypeException("Cannot divide!");
        }
    }


}
