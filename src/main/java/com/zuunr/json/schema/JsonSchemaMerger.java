package com.zuunr.json.schema;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectMerger;
import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.validation.JsonSchemaValidator;

public final class JsonSchemaMerger {

    private static final JsonSchemaValidator JSON_SCHEMA_VALIDATOR = new JsonSchemaValidator();
    private static final JsonObjectMerger merger = new JsonObjectMerger();

    public static JsonSchema mergeMetaJsonSchemaWith(JsonSchema jsonSchema) {

        JsonValue schemaJsonValue = jsonSchema.asJsonValue();
        if (schemaJsonValue.isBoolean()) {
            return jsonSchema; // true or false schemas will never result in anything but true resp. false (no need to extend)
        }

        JsonObject resultSchema = schemaJsonValue.getJsonObject();

        JsonObject defs = resultSchema.get(Keywords.DEFS, JsonObject.EMPTY).getJsonObject();

        JsonObject commonSchema = JSON_SCHEMA_VALIDATOR.getJsonSchemaForJsonSchema().asJsonValue().getJsonObject();

        JsonObject commonDefs = commonSchema.get(Keywords.DEFS, JsonObject.EMPTY).getJsonObject();

        JsonObject mergedDefs = merger.merge(commonDefs, defs);

        if (mergedDefs.size() != defs.size() + commonDefs.size()) {
            throw new RuntimeException("Colliding definitions in schemas to be merged!\n jsonSchemaForJsonSchema: \n" + commonSchema + "\njsonSchema: \n" + jsonSchema);
        }

        return resultSchema.put(Keywords.DEFS, mergedDefs).as(JsonSchema.class);
    }
}
