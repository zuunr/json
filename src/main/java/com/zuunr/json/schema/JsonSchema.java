package com.zuunr.json.schema;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.pointer.JsonPointer;
import com.zuunr.json.schema.validation.*;

/**
 * @author Niklas Eldberger
 */
public class JsonSchema {

    private static final JsonSchemaValidator VALIDATOR = new JsonSchemaValidator();

    public static final JsonArray TYPE_NULL = JsonArray.of("null");
    public static final JsonArray TYPE_ARRAY = JsonArray.of("array");
    public static final JsonArray TYPE_ARRAY_NULLABLE = TYPE_ARRAY.addAll(TYPE_NULL);
    public static final JsonArray TYPE_BOOLEAN = JsonArray.of("boolean");
    public static final JsonArray TYPE_BOOLEAN_NULLABLE = TYPE_BOOLEAN.addAll(TYPE_NULL);
    public static final JsonArray TYPE_INTEGER = JsonArray.of("integer");
    public static final JsonArray TYPE_INTEGER_NULLABLE = TYPE_INTEGER.addAll(TYPE_NULL);
    public static final JsonArray TYPE_NUMBER = JsonArray.of("number");
    public static final JsonArray TYPE_NUMBER_NULLABLE = TYPE_NULL.addAll(TYPE_NUMBER);
    public static final JsonArray TYPE_NUMERIC = TYPE_INTEGER.addAll(TYPE_NUMBER);
    public static final JsonArray TYPE_NUMERIC_NULLABLE = TYPE_INTEGER.addAll(TYPE_NULL).addAll(TYPE_NUMBER);
    public static final JsonArray TYPE_OBJECT = JsonArray.of("object");
    public static final JsonArray TYPE_OBJECT_NULLABLE = TYPE_NULL.addAll(TYPE_OBJECT);
    public static final JsonArray TYPE_STRING = JsonArray.of("string");
    public static final JsonArray TYPE_STRING_NULLABLE = TYPE_NULL.addAll(TYPE_STRING);

    private static final JsonValue REF = JsonValue.of(Keywords.REF);

    private JsonObject schemaValidationResult = null;
    private final JsonValue schema;
    private final JsonSchemaKeywordsOrderer keywordsOrderer;

    private NullableCachedValue<JsonValue> type;
    private NullableCachedValue<JsonObject> properties;

    private NullableCachedValue<JsonObject> patternProperties;
    private NullableCachedValue<JsonSchema> additionalProperties;
    private NullableCachedValue<JsonObject> items;

    public JsonSchema(JsonValue schema) {
        this.schema = schema;
        this.keywordsOrderer = new JsonSchemaKeywordsOrderer(schema);
    }

    public ValidationNodeCreator getValidationNodeCreator(JsonValue instance, int keywordIndex) {
        return keywordsOrderer.getValidationNodeCreator(instance, keywordIndex);
    }

    public JsonObject getDependentSchemas() {
        return schema.get(Keywords.DEPENDENT_SCHEMAS).getJsonObject();
    }

    public JsonValue getKeyword(int index, JsonValue instance) {
        return keywordsOrderer.getKeyword(index, instance);
    }

    public JsonValue getType() {
        if (type == null) {
            type = new NullableCachedValue<>(schema.get(Keywords.TYPE));
        }
        return type.value;
    }

    public JsonObject getProperties() {
        if (properties == null) {
            var propertiesJsonValue = schema.get(Keywords.PROPERTIES);
            if (propertiesJsonValue == null) {
                properties = new NullableCachedValue<>(null);
            } else {
                properties = new NullableCachedValue<>(propertiesJsonValue.getJsonObject().optimizeGet(true));
            }
        }
        return properties.value;
    }

    public JsonObject getItems() {
        if (items == null) {
            var itemsJsonValue = schema.get(Keywords.ITEMS);
            if (itemsJsonValue == null) {
                items = new NullableCachedValue<>(null);
            } else {
                items = new NullableCachedValue<>(itemsJsonValue.getJsonObject().optimizeGet(true));
            }
        }
        return items.value;
    }

    public JsonObject getPatternProperties() {
        if (patternProperties == null) {
            var patternPropertiesJsonValue = schema.get(Keywords.PATTERN_PROPERTIES);
            if (patternPropertiesJsonValue == null) {
                patternProperties = new NullableCachedValue<>(null);
            } else {
                patternProperties = new NullableCachedValue<>(patternPropertiesJsonValue.getJsonObject().optimizeGet(true));
            }
        }
        return patternProperties.value;
    }

    public JsonSchema getAdditionalProperties() {
        if (additionalProperties == null) {
            additionalProperties = new NullableCachedValue<>(schema.get(Keywords.ADDITIONAL_PROPERTIES, JsonValue.NULL).as(JsonSchema.class));
        }
        return additionalProperties.value;
    }

    public JsonValue getKeyword(String keyword) {
        return schema.get(keyword);
    }

    public JsonValue getKeyword(String keyword, JsonValue defaultIfNull) {
        return schema.get(keyword, defaultIfNull);
    }

    public JsonValue getKeyword(String keyword, JsonArray defaultIfNull) {
        return schema.get(keyword, defaultIfNull);
    }

    public JsonValue getKeyword(String keyword, JsonObject defaultIfNull) {
        return schema.get(keyword, defaultIfNull);
    }

    /**
     * @param jsonPointer a JSON Pointer
     * @param followRef   true means that each time "$ref" is found the value of "$ref" in the JSON Schema document (e.g "#/defs/A_Referenced_Schema") will be used as the new path from that point.
     * @return
     */
    public JsonValue get(JsonPointer jsonPointer, boolean followRef) {

        JsonValue schemaOrKeyword = schema;
        JsonArray jsonArray = jsonPointer.asArray();
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonValue currentKey = jsonArray.get(i);
            if (followRef && REF.equals(currentKey)) {
                JsonValue refPointer = schemaOrKeyword.get(Keywords.REF);
                schemaOrKeyword = schema.get(refPointer.as(JsonPointer.class).asArray());
            } else {
                if (schemaOrKeyword.isJsonArray()) {
                    try {
                        currentKey = JsonValue.of(Integer.parseInt(currentKey.getString()));
                    } catch (NumberFormatException e) {
                        return null;
                    }

                }
                schemaOrKeyword = schemaOrKeyword.get(currentKey);
            }
            if (schemaOrKeyword == null) {
                break;
            }
        }
        return schemaOrKeyword;
    }

    public JsonValue asJsonValue() {
        return schema;
    }

    private static class NullableCachedValue<T> {
        final T value;

        public NullableCachedValue(T value) {
            this.value = value;
        }
    }

    @Override
    public String toString() {
        return schema.asJson();
    }

    /**
     * Validates this JsonSchema according to what is supported in this implementation
     * @throws JsonSchemaValidationException when the JsonValue is not describing a supported JSON Schema
     */
    public void validate() throws JsonSchemaValidationException {

        if (schemaValidationResult == null) {
            schemaValidationResult = VALIDATOR.validate(asJsonValue(), VALIDATOR.getJsonSchemaForJsonSchema(), OutputStructure.DETAILED);
        }

        if (!schemaValidationResult.get("valid").getBoolean()) {
            throw new JsonSchemaValidationException(schemaValidationResult);
        }
    }
}
