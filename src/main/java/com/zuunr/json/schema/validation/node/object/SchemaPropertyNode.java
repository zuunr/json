package com.zuunr.json.schema.validation.node.object;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.validation.ValidationContext;
import com.zuunr.json.schema.validation.node.SchemaNode;
import com.zuunr.json.schema.validation.node.ValidationNode;
import com.zuunr.json.schema.validation.node.string.Pattern;

/**
 * @author Niklas Eldberger
 */
public class SchemaPropertyNode extends ValidationNode {


    private final State state;
    private final int instanceFieldNameIndex;

    public SchemaPropertyNode(int instanceFieldNameIndex, JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, keywordIndex, validationContext, rootInstance);
        this.instanceFieldNameIndex = instanceFieldNameIndex;
        state = firstState(instance.getJsonObject().keys().get(instanceFieldNameIndex).getString(), instance.getJsonObject().values().get(instanceFieldNameIndex), schema);
    }

    public SchemaPropertyNode(int instanceFieldNameIndex, JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance, State state) {
        super(instance, schema, keywordIndex, validationContext, rootInstance);
        this.instanceFieldNameIndex = instanceFieldNameIndex;
        this.state = state;
    }

    @Override
    protected void childNodeCompleted(ValidationNode subnode) {
        if (!subnode.getValid()) {
            setValid(false);
        }
        if (validationContext().returnFiltrate()) {
            filtrate = subnode.filtrate();
        }
    }

    @Override
    protected void doAfterAllChildNodesAreCompleted() {
        if (getValid() == null) {
            setValid(true);
        }
    }

    @Override
    protected Boolean calculateValid() {
        return null;
    }

    @Override
    protected ValidationNode createFirstChildNode() {
        return subnodeOf(state);
    }

    @Override
    protected ValidationNode createNextChildNodeOfParent() {

        State nextState = nextState(
                state.fieldName,
                state.fieldValue,
                schema,
                state.currentKeyword,
                state.currentPatternPropertyIndex);
        if (nextState == null) {
            return null;
        } else {
            return new SchemaPropertyNode(instanceFieldNameIndex, instance, schema, keywordSchemaIndex(), validationContext(), rootInstance, nextState);
        }
    }

    @Override
    public Location location() {
        if (location == null) {

            String currentKeyword = state.currentKeyword;

            JsonValue keywordValue = null;
            int currentPatternPropertyIndex = state.currentPatternPropertyIndex == null ? -1 : state.currentPatternPropertyIndex;

            JsonArray keywordLocation;
            if (currentKeyword.equals(Keywords.ADDITIONAL_PROPERTIES)) {
                keywordLocation = parentNode.location.keyword.add(Keywords.ADDITIONAL_PROPERTIES);
                keywordValue = schema.getKeyword(currentKeyword);
            } else if (currentKeyword.equals(Keywords.PROPERTIES)) {
                keywordLocation = parentNode.location.keyword.add(Keywords.PROPERTIES).add(state.fieldName);
                keywordValue = schema.getKeyword(currentKeyword).getJsonObject().get(state.fieldName);
            } else if (currentKeyword.equals(Keywords.PATTERN_PROPERTIES)) {
                JsonValue pattern = schema.getKeyword(currentKeyword).getJsonObject().keys().get(currentPatternPropertyIndex);
                keywordLocation = parentNode.location.keyword.add(Keywords.PATTERN_PROPERTIES).add(pattern);
                if (currentPatternPropertyIndex > -1) {
                    keywordValue = schema.getKeyword(currentKeyword).getJsonObject().values().get(currentPatternPropertyIndex);
                }
            } else {
                throw new RuntimeException("Only " + Keywords.PROPERTIES + ", " + Keywords.PATTERN_PROPERTIES + " and " + Keywords.ADDITIONAL_PROPERTIES + " are supported");
            }

            location = new Location(
                    parentNode.location.instance,
                    parentNode.location.instanceProperty,
                    keywordLocation,
                    keywordValue
            );
        }
        return location;
    }

    @Override
    protected JsonValue keyword() {
        return null;
    }

    public static State firstState(String fieldName, JsonValue fieldValue, JsonSchema schema) {
        return nextState(fieldName, fieldValue, schema, null, -1);
    }

    public static State nextState(String fieldName, JsonValue fieldValue, JsonSchema schema, String currentKeyword, int currentPatternPropertyIndex) {
        if (currentKeyword == null) {
            JsonObject properties = schema.getProperties();
            JsonValue propertySchema = properties == null ? null : properties.get(fieldName);

            if (propertySchema != null) {
                return new State(fieldName, fieldValue, schema, Keywords.PROPERTIES, -1);
            }
        }

        if (currentKeyword == null || Keywords.PROPERTIES.equals(currentKeyword)) {
            int nextIndex = nextPatternIndex(fieldName, schema, -1);
            if (nextIndex != -1) {
                return new State(fieldName, fieldValue, schema, Keywords.PATTERN_PROPERTIES, nextIndex);
            }
        }

        if (Keywords.PATTERN_PROPERTIES.equals(currentKeyword)) {
            int nextIndex = nextPatternIndex(fieldName, schema, currentPatternPropertyIndex);
            if (nextIndex != -1) {
                return new State(fieldName, fieldValue, schema, Keywords.PATTERN_PROPERTIES, nextIndex);
            }
        }

        if (currentKeyword == null) {
            JsonSchema additionalProperties = schema.getAdditionalProperties();
            if (additionalProperties != null) {
                return new State(fieldName, fieldValue, schema, Keywords.ADDITIONAL_PROPERTIES, -1);
            }
        }
        return null;
    }

    private static int nextPatternIndex(String fieldName, JsonSchema schema, int currentPatternPropertyIndex) {
        JsonObject patternProperties = schema.getPatternProperties();
        JsonArray fieldNamePatterns = patternProperties == null ? JsonArray.EMPTY : patternProperties.keys();
        int start = currentPatternPropertyIndex == -1 ? 0 : currentPatternPropertyIndex + 1;
        for (int i = start; i < fieldNamePatterns.size(); i++) {
            if (fieldNamePatterns.get(i).as(Pattern.class).compiled().matcher(fieldName).find()) {
                return i;
            }
        }
        return -1;
    }

    private SchemaNode subnodeOf(State state) {
        String keyword = state.currentKeyword == null ? Keywords.PROPERTIES : state.currentKeyword;
        if (Keywords.PROPERTIES.equals(keyword)) {
            JsonValue propertySchema = schema.getKeyword(Keywords.PROPERTIES, JsonObject.EMPTY).get(state.fieldName);
            if (propertySchema != null) {
                return new SchemaNode(state.fieldValue, propertySchema.as(JsonSchema.class), validationContext(), rootInstance);
            }
        }

        if (Keywords.PATTERN_PROPERTIES.equals(keyword)) {
            return new SchemaNode(
                    state.fieldValue,
                    state.schema.getKeyword(Keywords.PATTERN_PROPERTIES).getJsonObject().values().get(state.currentPatternPropertyIndex).as(JsonSchema.class),
                    validationContext(),
                    rootInstance);
        }

        if (Keywords.ADDITIONAL_PROPERTIES.equals(keyword)) {
            return new SchemaNode(state.fieldValue, state.schema.getKeyword(Keywords.ADDITIONAL_PROPERTIES).as(JsonSchema.class), validationContext(), rootInstance);
        }

        return null;
    }

    public static class State {

        public final String fieldName;
        public final JsonValue fieldValue;
        public final JsonSchema schema;
        public final String currentKeyword;
        public final Integer currentPatternPropertyIndex;

        public State(String fieldName, JsonValue fieldValue, JsonSchema schema, String currentKeyword, Integer currentPatternPropertyIndex) {
            this.fieldName = fieldName;
            this.fieldValue = fieldValue;
            this.schema = schema;
            this.currentKeyword = currentKeyword;
            this.currentPatternPropertyIndex = currentPatternPropertyIndex;
        }
    }

}
