package com.zuunr.json.schema.validation;

/**
 * @author Niklas Eldberger
 */
public enum OutputStructure {
    FLAG,
    BASIC,
    DETAILED,
    VERBOSE
}
