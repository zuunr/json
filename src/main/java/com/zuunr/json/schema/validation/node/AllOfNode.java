package com.zuunr.json.schema.validation.node;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.ValidationContext;

/**
 * @author Niklas Eldberger
 */
public class AllOfNode extends ValidationNode {

    private static final JsonValue ALL_OF = JsonValue.of(Keywords.ALL_OF);

    public AllOfNode(JsonValue instance, JsonSchema schema, int schemaKeywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, schemaKeywordIndex, validationContext, rootInstance);
    }

    protected void doAfterAllChildNodesAreCompleted() {
        if (getValid() == null) {
            setValid(true);
        }
    }

    @Override
    protected void childNodeCompleted(ValidationNode subnode) {

        if (!subnode.getValid()) {
            setValid(false);
        }
        super.childNodeCompleted(subnode);
    }

    protected ValidationNode createFirstChildNode() {

        if (schema.getKeyword(Keywords.ALL_OF, JsonArray.EMPTY).getJsonArray().size() > 0) {
            return new AllOfIndexNode(instance, schema, keywordSchemaIndex(), validationContext(), rootInstance, 0);
        }
        return null;
    }

    @Override
    protected ValidationNode createNextChildNodeOfParent() {
        return ((SchemaNode) parentNode).createKeywordNode(keywordSchemaIndex() + 1);
    }

    @Override
    protected Boolean calculateValid() {
        return null; //NOSONAR
    }

    @Override
    protected JsonValue keyword() {
        return ALL_OF;
    }

    @Override
    public Location location() {
        if (location == null) {
            location = new Location(
                    parentNode.location.instance,
                    parentNode.location.instanceProperty,
                    parentNode.location.keyword.add(ALL_OF),
                    schema.getKeyword(Keywords.ALL_OF));
        }
        return location;
    }
}
