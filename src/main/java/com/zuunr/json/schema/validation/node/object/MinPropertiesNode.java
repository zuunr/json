package com.zuunr.json.schema.validation.node.object;

import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.validation.ValidationContext;
import com.zuunr.json.schema.validation.node.KeywordNode;

/**
 * @author Niklas Eldberger
 */
public class MinPropertiesNode extends KeywordNode {

    public MinPropertiesNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, keywordIndex, validationContext, rootInstance);
    }

    @Override
    protected Boolean calculateValid() {
        int minProperties =  schema.getKeyword(Keywords.MIN_PROPERTIES).getJsonNumber().intValue();
        return minProperties <= instance.getJsonObject().size();

    }
}
