package com.zuunr.json.schema.validation.node;

import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.ValidationContext;

/**
 * @author Niklas Eldberger
 */
public class AnyOfNode extends ValidationNode {

    private static final JsonValue ANY_OF = JsonValue.of(Keywords.ANY_OF);

    public AnyOfNode(JsonValue instance, JsonSchema schema, int schemaKeywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, schemaKeywordIndex, validationContext, rootInstance);
    }

    protected void doAfterAllChildNodesAreCompleted() {
        if (getValid() == null) {
            setValid(false);
        }
    }

    @Override
    protected void childNodeCompleted(ValidationNode subnode) {

        if (subnode.getValid()) {
            setValid(true);
        }
        super.childNodeCompleted(subnode);
    }

    protected ValidationNode createFirstChildNode() {

        if (schema.getKeyword(Keywords.ANY_OF).getJsonArray().size() > 0) {
            return new AnyOfIndexNode(instance, schema, keywordSchemaIndex(), validationContext(), rootInstance, 0);
        }
        return null;
    }

    @Override
    protected ValidationNode createNextChildNodeOfParent() {
        return ((SchemaNode) parentNode).createKeywordNode(keywordSchemaIndex() + 1);
    }

    @Override
    protected Boolean calculateValid() {
        return null; //NOSONAR
    }

    @Override
    protected JsonValue keyword() {
        return ANY_OF;
    }

    @Override
    public Location location() {
        if (location == null) {
            location = new Location(
                    parentNode.location.instance,
                    parentNode.location.instanceProperty,
                    parentNode.location.keyword.add(ANY_OF),
                    schema.getKeyword(Keywords.ANY_OF));
        }
        return location;
    }
}
