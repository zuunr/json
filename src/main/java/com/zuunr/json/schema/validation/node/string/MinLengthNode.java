package com.zuunr.json.schema.validation.node.string;

import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.ValidationContext;
import com.zuunr.json.schema.validation.node.KeywordNode;

/**
 * @author Niklas Eldberger
 */
public class MinLengthNode extends KeywordNode {

    public MinLengthNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, keywordIndex, validationContext, rootInstance);
    }

    @Override
    protected Boolean calculateValid() {
        JsonValue minLengthJsonValue = schema.getKeyword(Keywords.MIN_LENGTH);
        JsonValue data = dataKeywordValue(minLengthJsonValue);
        if (data != null){
            minLengthJsonValue = data;
        }

        Long minLength = minLengthJsonValue.getLong();
        return instance.getString().length() >= minLength;
    }
}
