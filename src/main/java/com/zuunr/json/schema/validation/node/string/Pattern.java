/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json.schema.validation.node.string;

import com.zuunr.json.JsonValue;

public class Pattern {

    private final JsonValue me;

    private String asString;
    private java.util.regex.Pattern compiled;


    public Pattern(JsonValue stringJsonValue) {
        me = stringJsonValue;
    }

    public String asString() {

        if (asString == null) {
            asString = me.getValue(String.class);
        }
        return asString;
    }

    public java.util.regex.Pattern compiled() {

        if (asString() != null && compiled == null) {
            compiled = java.util.regex.Pattern.compile(this.asString);
        }
        return compiled;
    }
}
