package com.zuunr.json.schema.validation.node;

import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.validation.OutputStructure;
import com.zuunr.json.schema.validation.ValidationContext;

/**
 * @author Niklas Eldberger
 */
public abstract class AbstractNodesExecutor {

    protected ValidationNode validateNodes(JsonValue instance, ValidationContext validationContext, JsonValue rootInstance) {

        ValidationNode node = new SchemaNode(instance, validationContext.rootSchema(), validationContext, rootInstance);

        while (true) {

            Boolean validationResult = node.investigateIfValid();

            if (validationResult != null) {

                if (validationContext.outputStructure() == OutputStructure.FLAG) {
                    node.cleanCompletedChildNodesList(); // GC
                }

                ValidationNode parentNode = node.parentNode();

                if (parentNode == null) {
                    break;
                }

                parentNode.childNodeCompleted(node);

                if (validationContext.outputStructure() == OutputStructure.FLAG && parentNode.resultIsKnown()) {
                    node = parentNode;
                } else {
                    ValidationNode nextnode = node.nextSiblingNode();
                    if (nextnode == null) {
                        parentNode = node.parentNode();
                        parentNode.allChildNodesAreCompleted();
                        node = parentNode;
                    } else {
                        nextnode.parentNode = node.parentNode;
                        node = nextnode;
                    }
                }
            } else {
                // assertion to be removed
                if (node.isChildNodesCompleted()) {
                    throw new RuntimeException("This step should have been done!");
                }

                ValidationNode childNode = node.firstChildNode();
                if (childNode == null) {
                    node.allChildNodesAreCompleted();
                } else {
                    childNode.parentNode = node;
                    node = childNode;
                }
            }
        }
        return node;
    }
}