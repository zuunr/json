package com.zuunr.json.schema.validation.node;

import com.zuunr.json.JsonValue;
import com.zuunr.json.pointer.JsonPointer;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.ValidationContext;

/**
 * @author Niklas Eldberger
 */
public abstract class KeywordNode extends ValidationNode {


    public KeywordNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, keywordIndex, validationContext, rootInstance);
    }

    @Override
    protected final ValidationNode createFirstChildNode() {
        return null;
    }

    @Override
    protected final ValidationNode createNextChildNodeOfParent() {
        return ((SchemaNode) parentNode).createKeywordNode(keywordSchemaIndex() + 1);
    }

    @Override
    protected final void doAfterAllChildNodesAreCompleted() {
        // Nothing more to do
    }

    @Override
    protected void childNodeCompleted(ValidationNode subnode) {
        super.childNodeCompleted(subnode);
    }

    @Override
    protected JsonValue keyword() {
        return JsonValue.of(schema.getKeyword(keywordSchemaIndex(), instance)); // TODO: Refactor this for ok performance!!!!
    }

    @Override
    public Location location() {
        if (location == null) {
            location = new Location(
                    parentNode.location.instance,
                    parentNode.location.instanceProperty,
                    parentNode.location.keyword.add(keyword()),
                    schema.getKeyword(keyword().getString()));
        }
        return location;
    }

    protected JsonValue dataKeywordValue(JsonValue keywordValue) {
        if (keywordValue != null && keywordValue.isJsonObject()) {
            JsonValue data = keywordValue.getJsonObject().get("$data", JsonValue.NULL);
            if (data.isString()) {
                return rootInstance.get(data.as(JsonPointer.class));
            }
        }
        return null;
    }
}
