package com.zuunr.json.schema.validation.node;

import com.zuunr.json.JsonValue;
import com.zuunr.json.pointer.JsonPointer;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.ValidationContext;

/**
 * @author Niklas Eldberger
 */
public class EqualsIndexNode extends SubschemaIndexNode<EqualsIndexNode> {


    public EqualsIndexNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance, int index) {
        super(instance, schema, keywordIndex, validationContext, rootInstance, index);
    }

    @Override
    EqualsIndexNode create(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance, int index) {
        return new EqualsIndexNode(instance, schema, keywordIndex, validationContext, rootInstance, index);
    }

    @Override
    protected ValidationNode createFirstChildNode() {
        return null;
    }

    @Override
    public Boolean getValid() {
        return instance.equals(rootInstance.get(schema.getKeyword(Keywords.EQUALS).get(index).as(JsonPointer.class).asArray()));
    }

    @Override
    protected void doAfterAllChildNodesAreCompleted() {
        // Do nothing
    }
}
