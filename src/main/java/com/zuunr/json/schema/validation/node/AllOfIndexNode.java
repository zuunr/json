package com.zuunr.json.schema.validation.node;

import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.ValidationContext;

/**
 * @author Niklas Eldberger
 */
public class AllOfIndexNode extends SubschemaIndexNode<AllOfIndexNode> {


    public AllOfIndexNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance, int index) {
        super(instance, schema, keywordIndex, validationContext, rootInstance, index);
    }

    @Override
    AllOfIndexNode create(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance, int index) {
        return new AllOfIndexNode(instance, schema, keywordIndex, validationContext, rootInstance, index);
    }
}
