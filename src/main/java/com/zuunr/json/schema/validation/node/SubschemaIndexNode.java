package com.zuunr.json.schema.validation.node;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.ValidationContext;

/**
 * @author Niklas Eldberger
 */
public abstract class SubschemaIndexNode<T extends ValidationNode> extends ValidationNode {


    protected int index = 0;

    protected SubschemaIndexNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance, int index) {
        super(instance, schema, keywordIndex, validationContext, rootInstance);
        this.index = index;
    }

    @Override
    protected void doAfterAllChildNodesAreCompleted() {
        setValid(firstChildNode().getValid());
    }

    @Override
    protected ValidationNode createFirstChildNode() {
        return new SchemaNode(instance, schema.getKeyword(keyword().getString()).get(index).as(JsonSchema.class), validationContext(), rootInstance);
    }

    abstract T create(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance, int index);

    @Override
    protected ValidationNode createNextChildNodeOfParent() {
        if (index < maxIndex()) {
            return create(instance, schema, keywordSchemaIndex(), validationContext(), rootInstance, index + 1);
        }
        return null;
    }

    private int maxIndex() {
        return schema.getKeyword(keyword().getString(), JsonArray.EMPTY).getJsonArray().size() - 1;
    }

    @Override
    protected Boolean calculateValid() {
        return null; //NOSONAR
    }

    @Override
    protected JsonValue keyword() {
        return parentNode.keyword();
    }

    JsonValue index() {
        return JsonValue.of(index);
    }

    @Override
    public Location location() {
        if (location == null) {
            location = new Location(
                    parentNode.location.instance,
                    parentNode.location.instanceProperty,
                    parentNode.location.keyword.add(index),
                    schema.getKeyword(keyword().getString()).get(index));
        }
        return location;
    }
}
