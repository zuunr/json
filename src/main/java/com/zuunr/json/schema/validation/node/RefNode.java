package com.zuunr.json.schema.validation.node;

import com.zuunr.json.JsonValue;
import com.zuunr.json.pointer.JsonPointer;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.ValidationContext;

/**
 * @author Niklas Eldberger
 */
public class RefNode extends ValidationNode {

    private static final JsonValue REF = JsonValue.of(Keywords.REF);

    private final JsonPointer jsonPointer;

    public RefNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, keywordIndex, validationContext, rootInstance);
        this.jsonPointer = schema.getKeyword("$ref").as(JsonPointer.class);
    }

    @Override
    protected void childNodeCompleted(ValidationNode subnode) {
        super.childNodeCompleted(subnode);
        Boolean isRefNodeValid = subnode.getValid();
        setValid(isRefNodeValid);
        ValidationNode parentNode = parentNode();
        Boolean isSchemaParentValid = parentNode.getValid();
        if (isSchemaParentValid == null && !isRefNodeValid) {
            parentNode().setValid(false);
        }
        filtrate = subnode.filtrate;
    }

    @Override
    protected void doAfterAllChildNodesAreCompleted() {
        // Completed already when first subnode is completed
    }

    @Override
    protected ValidationNode createFirstChildNode() {
        return new SchemaNode(instance, validationContext().getSchema(jsonPointer).as(JsonSchema.class), validationContext(), rootInstance);
    }

    @Override
    protected ValidationNode createNextChildNodeOfParent() {
        return null;
    }

    @Override
    protected Boolean calculateValid() {
        return null; //NOSONAR
    }

    @Override
    protected JsonValue keyword() {
        return REF;
    }

    @Override
    public Location location() {
        if (location == null) {
            location = new Location(
                    parentNode.location.instance,
                    parentNode.location.instanceProperty,
                    parentNode.location.keyword.add(REF),
                    schema.getKeyword(Keywords.REF));
        }
        return location;
    }
}
