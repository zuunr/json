package com.zuunr.json.schema.validation.node.number;

import com.zuunr.json.JsonNumber;
import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.ValidationContext;
import com.zuunr.json.schema.validation.node.KeywordNode;

/**
 * @author Niklas Eldberger
 */
public class MultipleOfNode extends KeywordNode {

    public MultipleOfNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, keywordIndex, validationContext, rootInstance);
    }

    @Override
    protected Boolean calculateValid() {

        JsonValue multipleOfJsonValue = schema.getKeyword(Keywords.MULTIPLE_OF);
        JsonValue data = dataKeywordValue(multipleOfJsonValue);
        if (data != null) {
            multipleOfJsonValue = data;
        }

        JsonNumber multipleOf = multipleOfJsonValue.getJsonNumber();
        long modulo = instance.getJsonNumber().longValue() % multipleOf.longValue();
        return instance.getJsonNumber().isJsonInteger() && modulo == 0;
    }
}
