package com.zuunr.json.schema.validation.node;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.validation.JsonSchemaValidator;
import com.zuunr.json.schema.validation.OutputStructure;
import com.zuunr.json.schema.validation.ValidationContext;

/**
 * @author Niklas Eldberger
 */
public class ContainsNode extends ValidationNode {

    private static JsonSchemaValidator jsonSchemaValidator = new JsonSchemaValidator();

    private static final JsonValue IF = JsonValue.of(Keywords.IF);
    private static final JsonValue THEN = JsonValue.of(Keywords.THEN);
    private static final JsonValue ELSE = JsonValue.of(Keywords.ELSE);

    private static JsonValue KEYWORD_CONTAINS = JsonValue.of(Keywords.CONTAINS);

    public ContainsNode(JsonValue instance, JsonSchema schema, int schemaKeywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, schemaKeywordIndex, validationContext, rootInstance);
    }

    protected void doAfterAllChildNodesAreCompleted() {
        if (getValid() == null) {
            setValid(true);
        }
    }

    @Override
    protected void childNodeCompleted(ValidationNode subnode) {
        setValid(subnode.getValid());
    }

    protected ValidationNode createFirstChildNode() {

        JsonValue schemaOfContains = schema.getKeyword(Keywords.CONTAINS);
        if (schemaOfContains == null) {
            return null;
        }

        JsonArray arrayInstance = instance.getJsonArray();

        for (int i = 0; i < arrayInstance.size(); i++) {
            JsonValue rootSchemaDefs = validationContext().rootSchema().getKeyword("$defs");
            if (!schemaOfContains.isBoolean() && rootSchemaDefs != null) {
                schemaOfContains = schemaOfContains.getJsonObject().put("$defs", rootSchemaDefs).jsonValue(); // TODO: May be possible to get better performance if we cache the result on the _if schema, but may be just suboptimization
            }

            JsonObject containsResult = jsonSchemaValidator.validate(arrayInstance.get(i), schemaOfContains, OutputStructure.FLAG);

            if (containsResult.get("valid").getBoolean()){
                setValid(true);
                return null;
            }
        }
        setValid(false);
        return null;
    }

    @Override
    protected ValidationNode createNextChildNodeOfParent() {
        return ((SchemaNode) parentNode).createKeywordNode(keywordSchemaIndex() + 1);
    }

    @Override
    protected Boolean calculateValid() {
        return null; //NOSONAR
    }

    @Override
    protected JsonValue keyword() {
        return KEYWORD_CONTAINS;
    }

    @Override
    public Location location() {
        if (location == null) {
            location = new Location(
                    parentNode.location.instance,
                    parentNode.location.instanceProperty,
                    parentNode.location.keyword.add(KEYWORD_CONTAINS),
                    schema.getKeyword(Keywords.CONTAINS));
        }
        return location;
    }
}
