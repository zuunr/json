package com.zuunr.json.schema.validation.node;

import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.ValidationContext;

/**
 * @author Niklas Eldberger
 */
public class OneOfIndexNode extends SubschemaIndexNode<OneOfIndexNode> {


    public OneOfIndexNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance, int index) {
        super(instance, schema, keywordIndex, validationContext, rootInstance, index);
    }

    @Override
    OneOfIndexNode create(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance, int index) {
        return new OneOfIndexNode(instance, schema, keywordIndex, validationContext, rootInstance, index);
    }
}
