package com.zuunr.json.schema.validation.node.string;

import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.ValidationContext;
import com.zuunr.json.schema.validation.node.KeywordNode;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * @author Niklas Eldberger
 */
public class FormatNode extends KeywordNode {

    private static final DateTimeFormatter DATETIME = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
    private static final DateTimeFormatter DATE = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public FormatNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, keywordIndex, validationContext, rootInstance);
    }

    @Override
    protected Boolean calculateValid() {
        String format = schema.getKeyword(Keywords.FORMAT).getString();
        switch (format) {
            case "date-time":
                return validateDateTime(instance.getString(), DATETIME);
            case "date":
                return validateDateTime(instance.getString(), DATE);
            default:
                return true;
        }
    }

    private Boolean validateDateTime(String toBeValidated, DateTimeFormatter dateTimeFormatter) {
        try {
            LocalDate.parse(toBeValidated, dateTimeFormatter);
            return true;
        } catch (DateTimeParseException e) {
            return false;
        }
    }
}
