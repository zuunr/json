package com.zuunr.json.schema.validation.node.array;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArrayBuilder;
import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.validation.ValidationContext;
import com.zuunr.json.schema.validation.node.ValidationNode;

public class ItemsNode extends ValidationNode {

    private JsonArrayBuilder filtrateBuilder;

    public ItemsNode(JsonValue instance, JsonSchema schema, int schemaKeywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, schemaKeywordIndex, validationContext, rootInstance);
        if (validationContext.returnFiltrate()) {
            filtrateBuilder = JsonArray.EMPTY.builder();
        }
    }

    @Override
    protected void doAfterAllChildNodesAreCompleted() {
        if (filtrateBuilder != null) {
            filtrate = filtrateBuilder.build().jsonValue();
        }
        if (getValid() == null) {
            setValid(true);
        }
    }

    @Override
    protected Boolean calculateValid() {
        return getValid();
    }

    @Override
    protected ValidationNode createFirstChildNode() {
        return createNextChild(-1);
    }

    @Override
    protected ValidationNode createNextChildNodeOfParent() {
        return null;
    }

    @Override
    public Location location() {
        if (location == null) {
            if (parentNode == null) {
                location = new Location(JsonArray.EMPTY, null, JsonArray.EMPTY.add(keyword()), schema.getKeyword(Keywords.ITEMS));
            } else {
                location = new Location(
                        parentNode.location.instance,
                        parentNode.location.instanceProperty,
                        parentNode.location.keyword.add(keyword()),
                        schema.getKeyword(Keywords.ITEMS));
            }
        }
        return location;
    }

    @Override
    protected JsonValue keyword() {
        return JsonValue.of("items");
    }

    public ValidationNode createNextChild(int currentInstanceIndex) {
        int nextIndex = currentInstanceIndex + 1;
        JsonArray array = instance.getJsonArray();
        if (nextIndex >= array.size()) {
            return null;
        }
        return new InstanceItemNode(
                array.get(nextIndex), // instance
                schema.getKeyword(Keywords.ITEMS).as(JsonSchema.class), // items schema
                keywordSchemaIndex(),  // -1
                validationContext(),
                rootInstance,
                nextIndex);
    }

    @Override
    protected void childNodeCompleted(ValidationNode subnode) {
        if (!subnode.getValid()) {
            setValid(false);
        }
        if (filtrateBuilder != null) {
            JsonValue subnodeFiltrate = subnode.filtrate();
            if (subnodeFiltrate == null) {
                // If an item is invalid and cannot become valid by removing some failing field(s), then no useful filtrate can be returned
                // (if we choose to allow for JSON null in these cases we may change the behavior in the future :)
                filtrateBuilder = null;
            } else {
                filtrateBuilder.add(subnodeFiltrate);
            }
        }
    }
}
