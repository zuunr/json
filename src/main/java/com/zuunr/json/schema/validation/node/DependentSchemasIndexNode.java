package com.zuunr.json.schema.validation.node;

import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.ValidationContext;

/**
 * @author Niklas Eldberger
 */
public class DependentSchemasIndexNode extends SubschemaIndexNode<DependentSchemasIndexNode> {

    private int dependentPropertyIndex;

    public DependentSchemasIndexNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance, int index, int dependentPropertyIndex) {
        super(instance, schema, keywordIndex, validationContext, rootInstance, index);
        this.dependentPropertyIndex = dependentPropertyIndex;
    }

    @Override
    DependentSchemasIndexNode create(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance, int index) {
        return new DependentSchemasIndexNode(instance, schema, keywordIndex, validationContext, rootInstance, index, dependentPropertyIndex);
    }

    @Override
    protected ValidationNode createFirstChildNode() {
        return new SchemaNode(instance, schema.getKeyword(keyword().getString()).getJsonObject().values().get(dependentPropertyIndex).as(JsonSchema.class), validationContext(), rootInstance);
    }


    @Override
    protected void doAfterAllChildNodesAreCompleted() {
        // Do nothing
    }

    protected ValidationNode createNextChildNodeOfParent() {
        int nextDependentSchemaIndex = DependentSchemasNode.nextDependentSchema(schema, dependentPropertyIndex + 1, instance.getJsonObject());
        if (nextDependentSchemaIndex > -1) {
            return new DependentSchemasIndexNode(instance, schema, keywordSchemaIndex(), validationContext(), rootInstance, index, nextDependentSchemaIndex);
        } else {
            return null;
        }
    }

    @Override
    public Location location() {
        if (location == null) {
            JsonValue dependentPropertyKey = schema.getKeyword(keyword().getString()).getJsonObject().keys().get(dependentPropertyIndex);

            JsonValue keywordValue = schema.getKeyword(keyword().getString()).get(dependentPropertyKey.getString());
            location = new Location(
                    parentNode.location.instance,
                    parentNode.location.instanceProperty,
                    parentNode.location.keyword.add(dependentPropertyKey),
                    keywordValue);
        }
        return location;
    }

    @Override
    protected void childNodeCompleted(ValidationNode subnode) {
        setValid(subnode.getValid());
    }
}