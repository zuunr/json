package com.zuunr.json.schema.validation.node;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.ValidationContext;

/**
 * @author Niklas Eldberger
 */
public class RequiredNode extends KeywordNode {

    public RequiredNode(JsonValue instance, JsonSchema schema, int schemaKeywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, schemaKeywordIndex, validationContext, rootInstance);
    }

    @Override
    protected Boolean calculateValid() {
        JsonArray required = schema.getKeyword(Keywords.REQUIRED, JsonArray.EMPTY).getJsonArray();

        for (JsonValue requiredPrioperty : required) {
            if (instance.get(requiredPrioperty.getString()) == null) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }
}
