package com.zuunr.json.schema.validation.node;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.ValidationContext;

/**
 * @author Niklas Eldberger
 */
public class OneOfNode extends ValidationNode {

    private static final JsonValue ONE_OF = JsonValue.of(Keywords.ONE_OF);
    private boolean oneIsConfirmedValid;

    public OneOfNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, keywordIndex, validationContext, rootInstance);
    }

    protected void doAfterAllChildNodesAreCompleted() {
        if (getValid() == null) {
            if (oneIsConfirmedValid) {
                setValid(true);
            } else {
                setValid(false);
            }

        }
    }

    @Override
    protected void childNodeCompleted(ValidationNode subnode) {

        if (subnode.getValid()) {
            if (oneIsConfirmedValid) {
                setValid(false);
            }
            oneIsConfirmedValid = true;
        }
    }

    protected ValidationNode createFirstChildNode() {

        if (schema.getKeyword(Keywords.ONE_OF, JsonArray.EMPTY).getJsonArray().size() > 0) {
            return new OneOfIndexNode(instance, schema, keywordSchemaIndex(), validationContext(), rootInstance, 0);
        }
        return null;
    }

    @Override
    protected ValidationNode createNextChildNodeOfParent() {
        return ((SchemaNode) parentNode).createKeywordNode(keywordSchemaIndex() + 1);
    }

    @Override
    protected Boolean calculateValid() {
        return null; // NOSONAR
    }

    @Override
    protected JsonValue keyword() {
        return ONE_OF;
    }

    @Override
    public Location location() {

        if (location == null) {
            JsonArray keywordLocation = parentNode.location.keyword.add(ONE_OF);
            JsonValue keywordValue = schema.getKeyword(Keywords.ONE_OF);

            JsonArray instanceLocation = parentNode.location.instance;
            String instanceProperty = parentNode.location.instanceProperty;

            location = new Location(instanceLocation, instanceProperty, keywordLocation, keywordValue);
        }
        return location;
    }
}
