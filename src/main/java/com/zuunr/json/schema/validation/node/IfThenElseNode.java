package com.zuunr.json.schema.validation.node;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.validation.JsonSchemaValidator;
import com.zuunr.json.schema.validation.OutputStructure;
import com.zuunr.json.schema.validation.ValidationContext;

/**
 * @author Niklas Eldberger
 */
public class IfThenElseNode extends ValidationNode {

    private static JsonSchemaValidator jsonSchemaValidator = new JsonSchemaValidator();

    private static final JsonValue IF = JsonValue.of(Keywords.IF);
    private static final JsonValue THEN = JsonValue.of(Keywords.THEN);
    private static final JsonValue ELSE = JsonValue.of(Keywords.ELSE);

    private JsonValue keyword;

    public IfThenElseNode(JsonValue instance, JsonSchema schema, int schemaKeywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, schemaKeywordIndex, validationContext, rootInstance);
    }

    protected void doAfterAllChildNodesAreCompleted() {
        if (getValid() == null) {
            setValid(true);
        }
    }

    @Override
    protected void childNodeCompleted(ValidationNode subnode) {
        setValid(subnode.getValid());
        filtrate = subnode.filtrate;
    }

    protected ValidationNode createFirstChildNode() {

        JsonValue _if = schema.getKeyword(Keywords.IF);
        if (_if == null) {
            return null;
        }

        JsonValue rootSchemaDefs = validationContext().rootSchema().getKeyword("$defs");
        if (!_if.isBoolean() && rootSchemaDefs != null){
            _if = _if.getJsonObject().put("$defs", rootSchemaDefs).jsonValue(); // May be possible to get better performance if we cache the result on the _if schema, but may be just suboptimization
        }

        ValidationNode ifResultNode = jsonSchemaValidator.validateNodes(instance, new ValidationContext(OutputStructure.FLAG, false, _if), rootInstance);

        keyword = ifResultNode.getValid() ? THEN : ELSE;

        if (keyword == THEN) {
            JsonValue _then = schema.getKeyword(Keywords.THEN);
            if (_then == null) {
                return null;
            } else {
                return new SchemaNode(instance, _then.as(JsonSchema.class), validationContext(), rootInstance);
            }
        } else {
            JsonValue _else = schema.getKeyword(Keywords.ELSE);
            if (_else == null) {
                return null;
            } else {
                return new SchemaNode(instance, _else.as(JsonSchema.class), validationContext(), rootInstance);
            }
        }
    }

    @Override
    protected ValidationNode createNextChildNodeOfParent() {
        return ((SchemaNode) parentNode).createKeywordNode(keywordSchemaIndex() + 1);
    }

    @Override
    protected Boolean calculateValid() {
        return null; //NOSONAR
    }

    @Override
    protected JsonValue keyword() {
        return keyword;
    }

    @Override
    public Location location() {
        if (location == null) {
            location = new Location(
                    parentNode.location.instance,
                    parentNode.location.instanceProperty,
                    parentNode.location.keyword.add(keyword),
                    schema.getKeyword(keyword.getString()));
        }
        return location;
    }


}
