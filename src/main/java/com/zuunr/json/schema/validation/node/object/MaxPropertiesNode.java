package com.zuunr.json.schema.validation.node.object;

import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.validation.ValidationContext;
import com.zuunr.json.schema.validation.node.KeywordNode;

/**
 * @author Niklas Eldberger
 */
public class MaxPropertiesNode extends KeywordNode {

    public MaxPropertiesNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, keywordIndex, validationContext, rootInstance);
    }

    @Override
    protected Boolean calculateValid() {
        int maxProperties =  schema.getKeyword(Keywords.MAX_PROPERTIES).getJsonNumber().intValue();
        return maxProperties >= instance.getJsonObject().size();

    }
}
