package com.zuunr.json.schema.validation.node;

import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.ValidationContext;

/**
 * @author Niklas Eldberger
 */
public class AnyOfIndexNode extends SubschemaIndexNode<AnyOfIndexNode> {


    public AnyOfIndexNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance, int index) {
        super(instance, schema, keywordIndex, validationContext, rootInstance, index);
    }

    @Override
    AnyOfIndexNode create(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance, int index) {
        return new AnyOfIndexNode(instance, schema, keywordIndex, validationContext, rootInstance, index);
    }
}
