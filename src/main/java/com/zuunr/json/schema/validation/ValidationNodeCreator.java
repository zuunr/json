package com.zuunr.json.schema.validation;

import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.node.ValidationNode;

/**
 * @author Niklas Eldberger
 */
public interface ValidationNodeCreator {
    ValidationNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance);
}
