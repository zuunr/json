package com.zuunr.json.schema.validation.node;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.ValidationContext;
import com.zuunr.json.schema.validation.ValidationNodeCreator;
import com.zuunr.json.schema.validation.node.array.*;
import com.zuunr.json.schema.validation.node.object.MaxPropertiesNode;
import com.zuunr.json.schema.validation.node.object.ObjectMemberIndexNode;
import com.zuunr.json.schema.validation.node.number.MaximumNode;
import com.zuunr.json.schema.validation.node.number.MinimumNode;
import com.zuunr.json.schema.validation.node.number.MultipleOfNode;
import com.zuunr.json.schema.validation.node.object.MinPropertiesNode;
import com.zuunr.json.schema.validation.node.string.MaxLengthNode;
import com.zuunr.json.schema.validation.node.string.MinLengthNode;
import com.zuunr.json.schema.validation.node.string.PatternNode;

/**
 * @author Niklas Eldberger
 */
public class SchemaNode extends ValidationNode {

    public SchemaNode(JsonValue instance, JsonSchema schema, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, -1, validationContext, rootInstance);
    }

    @Override
    protected Boolean calculateValid() {

        Boolean result = null;
        if (schema.asJsonValue().isBoolean()) {
            result = schema.asJsonValue().getBoolean();
        } else if (schema.asJsonValue().isJsonObject() && schema.asJsonValue().getJsonObject().isEmpty()) {
            result = Boolean.TRUE;
        } else {
            if (isChildNodesCompleted()) {
                result = Boolean.TRUE; // Invalid cases are handled as each subnode is completed
            }
        }
        return result;
    }

    protected ValidationNode createFirstChildNode() {
        return firstSubnodeOfSchema();
    }

    private ValidationNode firstSubnodeOfSchema() {
        return createKeywordNode(0);
    }

    protected ValidationNode createNextChildNodeOfParent() {
        return nextKeywordNode();
    }

    private ValidationNode nextKeywordNode() {
        return null;
    }

    public final ValidationNode createKeywordNode(int keywordIndex) {
        ValidationNodeCreator validationNodeCreator = schema.getValidationNodeCreator(instance, keywordIndex);

        return validationNodeCreator == null ? null : validationNodeCreator.createValidationNode(instance, schema, keywordIndex, validationContext(), rootInstance);
    }

    public static ValidationNode createSchemaSubnode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        JsonValue nextKeyword = schema.getKeyword(keywordIndex, instance);
        if (nextKeyword != null) {
            switch (nextKeyword.getString()) {

                case "$ref": {
                    return new RefNode(instance, schema, keywordIndex, validationContext, rootInstance);
                }
                case "const": {
                    return new ConstNode(instance, schema, keywordIndex, validationContext, rootInstance);
                }
                case "dependentSchemas": {
                    return new DependentSchemasNode(instance, schema, keywordIndex, validationContext, rootInstance);
                }
                case "enum": {
                    return new EnumNode(instance, schema, keywordIndex, validationContext, rootInstance);
                }
                case "anyOf": {
                    AnyOfNode anyOfNode = new AnyOfNode(instance, schema, keywordIndex, validationContext, rootInstance);
                    return anyOfNode;
                }
                case "allOf": {
                    AllOfNode allOfNode = new AllOfNode(instance, schema, keywordIndex, validationContext, rootInstance);
                    return allOfNode;
                }
                case "oneOf": {
                    OneOfNode oneOfNode = new OneOfNode(instance, schema, keywordIndex, validationContext, rootInstance);
                    return oneOfNode;
                }
                case "equals": {
                    EqualsNode equalsNode = new EqualsNode(instance, schema, keywordIndex, validationContext, rootInstance);
                    return equalsNode;
                }
                case "type": {
                    TypeNode typeNode = new TypeNode(instance, schema, keywordIndex, validationContext, rootInstance);
                    return typeNode;
                }
                case "required": {
                    RequiredNode requiredNode = new RequiredNode(instance, schema, keywordIndex, validationContext, rootInstance);
                    return requiredNode;
                }
                case "maxLength": {
                    MaxLengthNode maxLengthNode = new MaxLengthNode(instance, schema, keywordIndex, validationContext, rootInstance);
                    return maxLengthNode;
                }
                case "minLength": {
                    MinLengthNode minLengthNode = new MinLengthNode(instance, schema, keywordIndex, validationContext, rootInstance);
                    return minLengthNode;
                }
                case "pattern": {
                    PatternNode patternNode = new PatternNode(instance, schema, keywordIndex, validationContext, rootInstance);
                    return patternNode;
                }
                case "property": {
                    ObjectMemberIndexNode objectMemberIndexNode = new ObjectMemberIndexNode(instance, schema, keywordIndex, validationContext, rootInstance, 0);
                    return objectMemberIndexNode;
                }
                case "maxProperties": {
                    MaxPropertiesNode maxPropertiesNode = new MaxPropertiesNode(instance, schema, keywordIndex, validationContext, rootInstance);
                    return maxPropertiesNode;
                }
                case "minProperties": {
                    MinPropertiesNode minPropertiesNode = new MinPropertiesNode(instance, schema, keywordIndex, validationContext, rootInstance);
                    return minPropertiesNode;
                }
                case "maxItems": {
                    MaxItemsNode maxItemsNode = new MaxItemsNode(instance, schema, keywordIndex, validationContext, rootInstance);
                    return maxItemsNode;
                }
                case "minItems": {
                    MinItemsNode minItemsNode = new MinItemsNode(instance, schema, keywordIndex, validationContext, rootInstance);
                    return minItemsNode;
                }
                case "uniqueItems": {
                    UniqueItemsNode uniqueItemsNode = new UniqueItemsNode(instance, schema, keywordIndex, validationContext, rootInstance);
                    return uniqueItemsNode;
                }
                case "item": {
                    ItemNode itemNode = new ItemNode(instance, schema, keywordIndex, validationContext, rootInstance, 0);
                    return itemNode;
                }
                case "items": {
                    ItemsNode itemsNode = new ItemsNode(instance, schema, keywordIndex, validationContext, rootInstance);
                    return itemsNode;
                }
                case "minimum": {
                    MinimumNode minimumNode = new MinimumNode(instance, schema, keywordIndex, validationContext, rootInstance);
                    return minimumNode;
                }
                case "maximum": {
                    MaximumNode maximumNode = new MaximumNode(instance, schema, keywordIndex, validationContext, rootInstance);
                    return maximumNode;
                }
                case "multipleOf": {
                    MultipleOfNode multipleOfNode = new MultipleOfNode(instance, schema, keywordIndex, validationContext, rootInstance);
                    return multipleOfNode;
                }
                case "if": {
                    IfThenElseNode ifThenElseNode = new IfThenElseNode(instance, schema, keywordIndex, validationContext, rootInstance);
                    return ifThenElseNode;
                }
                default: {
                    return null;
                }
            }
        }
        return null;
    }


    @Override
    protected void doAfterAllChildNodesAreCompleted() {
        // No need for action here. Already handled in subnodeCompleted
    }

    // TODO: Refactor this method
    @Override
    protected void childNodeCompleted(ValidationNode childnode) {
        if (!childnode.getValid()) {
            setValid(false);
            if (validationContext().returnFiltrate()) {

                Class childnodeClass = childnode.getClass();
                if (childnodeClass == RefNode.class ||
                        childnodeClass == ItemsNode.class ||
                        childnodeClass == IfThenElseNode.class
                ) {
                    filtrate = childnode.filtrate;
                } else if (childnode.getClass() == ObjectMemberIndexNode.class) {

                    ObjectMemberIndexNode objectMemberIndexNode = (ObjectMemberIndexNode) childnode;
                    if (filtrate == null) {
                        filtrate = instance; // Starts with the full object and removes props if any is wrong
                    }
                    String subnodePropertyName = objectMemberIndexNode.getObjectMemberName();

                    JsonValue subnodeFiltrate = objectMemberIndexNode.filtrate();
                    if (subnodeFiltrate == null) {
                        if (schema.getKeyword("required", JsonArray.EMPTY).getJsonArray().contains(subnodePropertyName)) {
                            filtrate = null;
                            setValid(false);
                            if (parentNode() != null) {
                                parentNode().childNodeCompleted(this);
                            }
                        } else {
                            filtrate = filtrate.getJsonObject().remove(subnodePropertyName).jsonValue();
                        }
                    } else if (subnodeFiltrate != childnode.instance) {
                        filtrate = filtrate.getJsonObject().put(subnodePropertyName, subnodeFiltrate).jsonValue();
                    }
                }
            }
        }
    }

    @Override
    protected JsonValue keyword() {
        return null;
    }

    @Override
    public Location location() {
        if (location == null) {
            if (parentNode == null) {
                location = new Location(JsonArray.EMPTY, null, JsonArray.EMPTY, null);
            } else {
                location = new Location(parentNode.location.instance, parentNode.location.instanceProperty, parentNode.location.keyword, parentNode.location.keywordValue);
            }
        }
        return location;
    }
}
