package com.zuunr.json.schema.validation.node.object;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.ValidationContext;
import com.zuunr.json.schema.validation.node.ValidationNode;
import com.zuunr.json.schema.validation.node.SchemaNode;

/**
 * There will be one ObjectMemberIndexNode for each field in a JSON Object, e.g {"a",true, "b":4} will result in two instances, one for "a" and one for "b"
 * @author Niklas Eldberger
 */
public class ObjectMemberIndexNode extends ValidationNode {

    int objectMemberIndex;

    public ObjectMemberIndexNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance, int objectMemberIndex) {
        super(instance, schema, keywordIndex, validationContext, rootInstance);
        this.objectMemberIndex = objectMemberIndex;
    }

    public String getObjectMemberName() {
        return instance.getJsonObject().keys().get(objectMemberIndex).getString();
    }

    public JsonValue getObjectMemberValue() {
        return instance.getJsonObject().values().get(objectMemberIndex);
    }


    @Override
    protected void childNodeCompleted(ValidationNode subnode) {
        if (!subnode.getValid()) {
            setValid(false);
        }

        if (validationContext().returnFiltrate()) {
            filtrate = subnode.filtrate();
        }
    }

    @Override
    protected void doAfterAllChildNodesAreCompleted() {
        if (getValid() == null) {
            setValid(true);
        }
    }

    @Override
    protected Boolean calculateValid() {
        return null;
    }

    @Override
    protected ValidationNode createFirstChildNode() {

        if (!instance.getJsonObject().isEmpty()) {
            String fieldName = instance.getJsonObject().keys().get(objectMemberIndex).getString();
            JsonValue fieldValue = instance.getJsonObject().values().get(objectMemberIndex);
            SchemaPropertyNode.State firstState = SchemaPropertyNode.firstState(fieldName, fieldValue, schema);
            if (firstState != null) {
                return new SchemaPropertyNode(objectMemberIndex, instance, schema, keywordSchemaIndex(), validationContext(), rootInstance);
            }
        }
        return null;

    }

    @Override
    protected ValidationNode createNextChildNodeOfParent() {
        int nextObjectMemberIndex = nextObjectMemberIndex(instance.getJsonObject(), objectMemberIndex);
        if (nextObjectMemberIndex > -1) {
            return new ObjectMemberIndexNode(instance, schema, keywordSchemaIndex(), validationContext(), rootInstance, nextObjectMemberIndex);
        } else {
            return SchemaNode.createSchemaSubnode(instance, schema, keywordSchemaIndex() + 1, validationContext(), rootInstance);
        }
    }

    @Override
    public Location location() {
        if (location == null) {
            location = new Location(
                    parentNode.location.instance.add(instance.getJsonObject().keys().get(objectMemberIndex)),
                    parentNode.location.instanceProperty,
                    parentNode.location.keyword,
                    schema.asJsonValue());
        }
        return location;
    }

    @Override
    protected JsonValue keyword() {
        return location.keyword.last();
    }

    public static int nextObjectMemberIndex(JsonObject object, int currentIndex) {
        JsonArray keys = object.keys();
        int i = currentIndex + 1;

        if (i < keys.size()) {
            return i;
        }
        return -1;
    }

}
