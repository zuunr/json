package com.zuunr.json.schema.validation.node.string;

import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.ValidationContext;
import com.zuunr.json.schema.validation.node.KeywordNode;

/**
 * @author Niklas Eldberger
 */
public class PatternNode extends KeywordNode {

    public PatternNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, keywordIndex, validationContext, rootInstance);
    }

    @Override
    protected Boolean calculateValid() {
        JsonValue patternJsonValue = schema.getKeyword(Keywords.PATTERN);
        JsonValue data = dataKeywordValue(patternJsonValue);
        if (data != null) {
            patternJsonValue = data;
        }
        Pattern pattern = patternJsonValue.as(Pattern.class);
        return pattern.compiled().matcher(instance.getString()).matches();
    }
}
