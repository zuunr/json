package com.zuunr.json.schema.validation;

import com.zuunr.json.JsonObject;

public class JsonSchemaValidationException extends RuntimeException {

    private final JsonObject schemaValidationResult;

    public JsonSchemaValidationException(JsonObject schemaValidationResult) {
        this.schemaValidationResult = schemaValidationResult;
    }

    public JsonObject getSchemaValidationResult() {
        return schemaValidationResult;
    }
}
