package com.zuunr.json.schema.validation.node.array;

import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.ValidationContext;
import com.zuunr.json.schema.validation.node.KeywordNode;

/**
 * @author Niklas Eldberger
 */
public class MaxItemsNode extends KeywordNode {

    public MaxItemsNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, keywordIndex, validationContext, rootInstance);
    }

    @Override
    protected Boolean calculateValid() {
        int maxLength = schema.getKeyword(Keywords.MAX_ITEMS, JsonValue.NULL).getJsonNumber().intValue();
        return instance.getJsonArray().size() <= maxLength;
    }
}
