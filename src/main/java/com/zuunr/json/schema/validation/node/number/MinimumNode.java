package com.zuunr.json.schema.validation.node.number;

import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.validation.ValidationContext;
import com.zuunr.json.schema.validation.node.KeywordNode;

/**
 * @author Niklas Eldberger
 */
public class MinimumNode extends KeywordNode {

    public MinimumNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, keywordIndex, validationContext, rootInstance);
    }

    @Override
    protected Boolean calculateValid() {

        JsonValue minimum = schema.getKeyword(Keywords.MINIMUM);
        JsonValue data = dataKeywordValue(minimum);
        if (data != null) {
            minimum = data;
        }
        return minimum.getJsonNumber().compareTo(instance.getJsonNumber()) <= 0;
    }
}
