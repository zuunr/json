package com.zuunr.json.schema.validation.node;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonValue;
import com.zuunr.json.pointer.JsonPointer;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.validation.ValidationContext;

import java.util.ArrayDeque;

/**
 * @author Niklas Eldberger
 */
public class EqualsNode extends KeywordNode {

    public EqualsNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, keywordIndex, validationContext, rootInstance);
    }

    @Override
    protected Boolean calculateValid() {

        for (JsonValue jsonPointerJsonValue : schema.getKeyword(Keywords.EQUALS, JsonArray.EMPTY).getJsonArray()) {
            JsonPointer jsonPointer = jsonPointerJsonValue.as(JsonPointer.class);
            JsonValue toCompareWith = rootInstance.get(jsonPointer);
            if (!instance.equals(toCompareWith)) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

    public static boolean equals(JsonValue value, JsonArray pointer, JsonValue pointerTargetInstance) {
        ArrayDeque<JsonValue[]> stack = new ArrayDeque<>();
        JsonValue[] pointerAndTargetTuple = new JsonValue[2]; //
        pointerAndTargetTuple[0] = pointer.jsonValue();
        pointerAndTargetTuple[1] = pointerTargetInstance;
        stack.push(pointerAndTargetTuple);
        while (!stack.isEmpty()) {


            pointerAndTargetTuple = stack.pop();
            JsonArray currentPointer = pointerAndTargetTuple[0].getJsonArray();
            JsonValue currentTarget = pointerAndTargetTuple[1];

            if (currentPointer.isEmpty()) {
                if (currentTarget == null) { // Only check equal value if there is a value
                    continue;
                } else if (!value.equals(currentTarget)) {
                    return false;
                } else {
                    continue;
                }
            }

            JsonValue currentPointerHead = currentPointer.head();
            JsonArray currentPointerTail = currentPointer.tail();
            if (currentPointerHead.isString()) {
                if (currentPointerHead.getString().equals("-1") && currentTarget.isJsonArray()) {
                    for (int i = 0; i < currentTarget.getJsonArray().size(); i++) {
                        pointerAndTargetTuple = new JsonValue[2];
                        pointerAndTargetTuple[0] = currentPointerTail.addFirst(i).jsonValue();
                        pointerAndTargetTuple[1] = currentTarget;
                        stack.push(pointerAndTargetTuple);
                    }
                } else if (currentTarget.isJsonObject()) {
                    pointerAndTargetTuple = new JsonValue[2];
                    pointerAndTargetTuple[0] = currentPointerTail.jsonValue();
                    pointerAndTargetTuple[1] = currentTarget.get(currentPointerHead.getString());
                    stack.push(pointerAndTargetTuple);
                } else if (currentTarget.isJsonArray()) {
                    pointerAndTargetTuple = new JsonValue[2];
                    pointerAndTargetTuple[0] = currentPointerTail.jsonValue();
                    int index;
                    try {
                        index = Integer.parseInt(currentPointerHead.getString());
                    } catch (NumberFormatException e) {
                        //throw new RuntimeException(e); // TODO: Create good Exception with info of what went wrong
                        return false;
                    }
                    pointerAndTargetTuple[1] = currentTarget.get(index);
                    stack.push(pointerAndTargetTuple);
                }
            } else if (currentPointerHead.isJsonNumber() &&
                    currentPointerHead.getJsonNumber().isJsonInteger() &&
                    currentTarget.isJsonArray()) {
                JsonArray currentTargetJsonArray = currentTarget.getJsonArray();
                int index = currentPointerHead.getJsonNumber().intValue();
                if (currentTargetJsonArray.size() > index) {
                    pointerAndTargetTuple = new JsonValue[2];
                    pointerAndTargetTuple[0] = currentPointer.tail().jsonValue();
                    pointerAndTargetTuple[1] = currentTargetJsonArray.get(index);
                    stack.push(pointerAndTargetTuple);
                }
            } else {
                return false;
            }
        }
        return true;
    }
}
