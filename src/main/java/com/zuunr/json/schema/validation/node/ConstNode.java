package com.zuunr.json.schema.validation.node;

import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.validation.ValidationContext;

/**
 * @author Niklas Eldberger
 */
public class ConstNode extends KeywordNode {

    public ConstNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, keywordIndex, validationContext, rootInstance);
    }

    @Override
    protected Boolean calculateValid() {
        JsonValue constantValue = schema.getKeyword(Keywords.CONST);
        JsonValue data = dataKeywordValue(constantValue);
        if (data != null) {
            constantValue = data;
        }
        return instance.equals(constantValue);
    }
}
