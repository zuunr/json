package com.zuunr.json.schema.validation.node.number;

import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.validation.ValidationContext;
import com.zuunr.json.schema.validation.node.KeywordNode;

/**
 * @author Niklas Eldberger
 */
public class MaximumNode extends KeywordNode {


    public MaximumNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, keywordIndex, validationContext, rootInstance);
    }

    @Override
    protected Boolean calculateValid() {
        JsonValue maximum = schema.getKeyword(Keywords.MAXIMUM);
        JsonValue data = dataKeywordValue(maximum);
        if (data != null) {
            maximum = data;
        }
        return maximum.getJsonNumber().compareTo(instance.getJsonNumber()) >= 0;
    }
}
