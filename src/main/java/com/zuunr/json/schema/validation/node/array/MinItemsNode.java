package com.zuunr.json.schema.validation.node.array;

import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.ValidationContext;
import com.zuunr.json.schema.validation.node.KeywordNode;

/**
 * @author Niklas Eldberger
 */
public class MinItemsNode extends KeywordNode {


    public MinItemsNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, keywordIndex, validationContext, rootInstance);
    }

    @Override
    protected Boolean calculateValid() {
        int minItems = schema.getKeyword(Keywords.MIN_ITEMS, JsonValue.NULL).getJsonNumber().intValue();
        return instance.getJsonArray().size() >= minItems;
    }
}
