package com.zuunr.json.schema.validation;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonValue;
import com.zuunr.json.pointer.JsonPointer;
import com.zuunr.json.schema.JsonSchema;

/**
 * @author Niklas Eldberger
 */
public class ValidationContext {

    private final boolean returnFiltrate;
    private final JsonSchema rootSchema;
    private final JsonValue jsonDocument;
    private final OutputStructure outputStructure;

    public ValidationContext(OutputStructure outputStructure, boolean returnFiltrate, JsonValue rootSchema) {
        this.outputStructure = outputStructure;
        this.returnFiltrate = returnFiltrate;
        this.rootSchema = rootSchema.as(JsonSchema.class);
        this.jsonDocument = rootSchema;
    }

    public ValidationContext(OutputStructure outputStructure, boolean returnFiltrate, JsonSchema rootSchema) {
        this.outputStructure = outputStructure;
        this.returnFiltrate = returnFiltrate;
        this.rootSchema = rootSchema;
        this.jsonDocument = rootSchema.asJsonValue();
    }

    public ValidationContext(OutputStructure outputStructure, boolean returnFiltrate, JsonArray pathToRootSchema, JsonValue jsonDocument) {
        this.outputStructure = outputStructure;
        this.returnFiltrate = returnFiltrate;
        this.jsonDocument = jsonDocument;
        this.rootSchema = jsonDocument.get(pathToRootSchema).as(JsonSchema.class);
    }



    public boolean returnFiltrate() {
        return returnFiltrate;
    }

    public JsonSchema rootSchema() {
        return rootSchema;
    }

    public JsonValue jsonDocument() {
        return jsonDocument;
    }

    public JsonValue getSchema(JsonPointer jsonPointer) {
        return jsonDocument.get(jsonPointer.asArray());
    }

    public OutputStructure outputStructure() {
        return outputStructure;
    }
}
