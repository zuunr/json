package com.zuunr.json.schema.validation.node.array;

import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.ValidationContext;
import com.zuunr.json.schema.validation.node.KeywordNode;

/**
 * @author Niklas Eldberger
 */
public class UniqueItemsNode extends KeywordNode {


    public UniqueItemsNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, keywordIndex, validationContext, rootInstance);
    }

    @Override
    protected Boolean calculateValid() {
        Boolean uniqueItems = schema.getKeyword(Keywords.UNIQUE_ITEMS, JsonValue.FALSE).getBoolean();
        return uniqueItems && instance.getJsonArray().asSet().size() == instance.getJsonArray().size();
    }
}
