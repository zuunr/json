package com.zuunr.json.schema.validation.node.string;

import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.validation.ValidationContext;
import com.zuunr.json.schema.validation.node.KeywordNode;

/**
 * @author Niklas Eldberger
 */
public class MaxLengthNode extends KeywordNode {


    public MaxLengthNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, keywordIndex, validationContext, rootInstance);
    }

    @Override
    protected Boolean calculateValid() {
        JsonValue maxLengthJsonValue = schema.getKeyword(Keywords.MAX_LENGTH);
        JsonValue data = dataKeywordValue(maxLengthJsonValue);
        if (data != null) {
            maxLengthJsonValue = data;
        }
        Long maxLength = maxLengthJsonValue.getLong();
        return instance.getString().length() <= maxLength;
    }
}
