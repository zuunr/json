package com.zuunr.json.schema.validation;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectFactory;
import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.node.AbstractNodesExecutor;

import java.io.InputStream;

/**
 * @author Niklas Eldberger
 */
public class JsonSchemaValidator extends AbstractNodesExecutor {

    private static final JsonObjectFactory jsonObjectFactory = new JsonObjectFactory();
    private static JsonSchema JSON_SCHEMA_FOR_JSON_SCHEMA;

    private static final JsonSchemaValidator SCHEMA_VALIDATOR = new JsonSchemaValidator(false);

    private final boolean validateSchema;

    /**
     * @deprecated Use {@link com.zuunr.json.schema.JsonSchema#validate()} instead.
     * @param validateSchema true means the schema is validated before the JsonValue instance is validated when using this JsonSchemaValidator
     */

    public JsonSchemaValidator(boolean validateSchema) {
        this.validateSchema = validateSchema;
    }

    public JsonSchemaValidator() {
        this.validateSchema = false;
    }

    public JsonObject validate(JsonValue instance, JsonSchema schema, OutputStructure outputStructure) {
        validateIfJsonSchemaIsOk(schema);
        return validateNodes(instance, new ValidationContext(outputStructure, false, schema), instance).validationResult();
    }

    public JsonObject validate(JsonValue instance, JsonValue schema, OutputStructure outputStructure) {
        validateIfJsonSchemaIsOk(schema.as(JsonSchema.class));
        return validateNodes(instance, new ValidationContext(outputStructure, false, JsonArray.EMPTY, schema), instance).validationResult();
    }

    public JsonObject validate(JsonValue instance, JsonArray pathToRootSchema, JsonValue jsonDocument, OutputStructure outputStructure) {
        JsonValue schema = jsonDocument.get(pathToRootSchema);
        validateIfJsonSchemaIsOk(schema.as(JsonSchema.class));
        return validateNodes(instance, new ValidationContext(outputStructure, false, JsonArray.EMPTY, schema), instance).validationResult();
    }

    public JsonValue filter(JsonValue instance, JsonSchema schema) {
        return filter(instance, schema.asJsonValue());
    }

    public JsonValue filter(JsonValue instance, JsonValue schema) {
        validateIfJsonSchemaIsOk(schema.as(JsonSchema.class));
        return validateNodes(instance, new ValidationContext(null, true, JsonArray.EMPTY, schema), instance).filtrate();
    }

    public JsonValue filter(JsonValue instance, JsonArray pathToRootSchema, JsonValue jsonDocument) {
        JsonValue schema = jsonDocument.get(pathToRootSchema);
        validateIfJsonSchemaIsOk(schema.as(JsonSchema.class));
        return validateNodes(instance, new ValidationContext(null, true, JsonArray.EMPTY, schema), instance).filtrate();
    }

    private void validateIfJsonSchemaIsOk(JsonSchema schema){
        if (validateSchema) {
            schema.validate();
        }
    }

    public JsonSchema getJsonSchemaForJsonSchema(){
        if (JSON_SCHEMA_FOR_JSON_SCHEMA == null) {
            InputStream inStream = this.getClass().getResourceAsStream("JsonSchemaForJsonSchema.json");
            JSON_SCHEMA_FOR_JSON_SCHEMA = jsonObjectFactory.createJsonValue(inStream).as(JsonSchema.class);
        }
        return JSON_SCHEMA_FOR_JSON_SCHEMA;
    }
}
