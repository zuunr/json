package com.zuunr.json.schema.validation.node;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonType;
import com.zuunr.json.JsonValue;
import com.zuunr.json.UnsupportedTypeException;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.ValidationContext;

/**
 * @author Niklas Eldberger
 */
public class TypeNode extends KeywordNode {

    public TypeNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, keywordIndex, validationContext, rootInstance);
    }

    @Override
    protected Boolean calculateValid() {

        JsonValue schemaType = schema.getKeyword(Keywords.TYPE);
        return validateType(schemaType, instance);
    }

    Boolean validateType(JsonValue schemaTypeJsonValue, JsonValue instance) {

        JsonArray schemaType = schemaTypeJsonValue.isJsonArray()
                ? schemaTypeJsonValue.getJsonArray()
                : JsonArray.of(schemaTypeJsonValue);

        JsonType instanceType = instance.type();
        String type;
        if (instanceType == JsonType.String) {
            type = "string";
        } else if (instanceType == JsonType.Object) {
            type = "object";
        } else if (instanceType == JsonType.Array) {
            type = "array";
        } else if (instanceType == JsonType.Boolean) {
            type = "boolean";
        } else if (instanceType == JsonType.Null) {
            type = "null";
        } else if (instanceType == JsonType.Number) {
            if (instance.getJsonNumber().isJsonInteger()) {
                type = "integer";
            } else {
                type = "number";
            }
        } else {
            throw new UnsupportedTypeException("Supported types are: string, object, array, boolean, null, number");
        }

        if (schemaType.contains(type) || "integer".equals(type) && schemaType.contains("number")) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

}
