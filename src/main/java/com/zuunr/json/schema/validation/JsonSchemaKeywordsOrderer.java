package com.zuunr.json.schema.validation;

import com.zuunr.json.*;
import com.zuunr.json.impl.NullableCachedValue;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.validation.node.*;
import com.zuunr.json.schema.validation.node.array.*;
import com.zuunr.json.schema.validation.node.number.MaximumNode;
import com.zuunr.json.schema.validation.node.number.MinimumNode;
import com.zuunr.json.schema.validation.node.number.MultipleOfNode;
import com.zuunr.json.schema.validation.node.object.MaxPropertiesNode;
import com.zuunr.json.schema.validation.node.object.MinPropertiesNode;
import com.zuunr.json.schema.validation.node.object.ObjectMemberIndexNode;
import com.zuunr.json.schema.validation.node.string.MaxLengthNode;
import com.zuunr.json.schema.validation.node.string.MinLengthNode;
import com.zuunr.json.schema.validation.node.string.PatternNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Niklas Eldberger
 */
public class JsonSchemaKeywordsOrderer {

    private final JsonValue schema;

    private JsonArray commonStartKeywords;
    private JsonArray commonEndKeywords;
    private JsonArray commonKeywords;
    private JsonArray objectKeywords;
    private JsonArray stringKeywords;
    private JsonArray arrayKeywords;
    private JsonArray numberKeywords;

    private NullableCachedValue<List<ValidationNodeCreator>> commonValidationNodeCreators;
    private NullableCachedValue<List<ValidationNodeCreator>> objectValidationNodeCreators;
    private NullableCachedValue<List<ValidationNodeCreator>> stringValidationNodeCreators;
    private NullableCachedValue<List<ValidationNodeCreator>> arrayValidationNodeCreators;
    private NullableCachedValue<List<ValidationNodeCreator>> numberValidationNodeCreators;

    public JsonSchemaKeywordsOrderer(JsonValue schema) {
        this.schema = schema;
    }

    private JsonArray getCommonKeywords() {
        if (commonKeywords == null) {
            commonKeywords = getCommonStartKeywords().addAll(getCommonEndKeywords());
        }
        return commonKeywords;
    }

    private JsonArray getCommonStartKeywords() {

        if (commonStartKeywords == null) {
            JsonArrayBuilder keywords = JsonArray.EMPTY.builder();
            addKeyword(Keywords.TYPE, keywords);
            addKeyword(Keywords.CONST, keywords);
            addKeyword(Keywords.ENUM, keywords);
            addKeyword(Keywords.EQUALS, keywords);
            commonStartKeywords = keywords.build();
        }
        return commonStartKeywords;
    }

    private JsonArray getCommonEndKeywords() {

        if (commonEndKeywords == null) {
            JsonArrayBuilder keywords = JsonArray.EMPTY.builder();
            addKeyword(Keywords.ALL_OF, keywords);
            addKeyword(Keywords.ANY_OF, keywords);
            addKeyword(Keywords.ONE_OF, keywords);
            addKeyword(Keywords.REF, keywords);
            addKeyword(Keywords.IF, keywords);
            commonEndKeywords = keywords.build();
        }
        return commonEndKeywords;
    }

    private JsonArray getObjectKeywords() {
        if (objectKeywords == null) {
            JsonArrayBuilder keywords = getCommonStartKeywords().builder();
            addKeyword(Keywords.REQUIRED, keywords);
            addKeyword(Keywords.MIN_PROPERTIES, keywords);
            addKeyword(Keywords.MAX_PROPERTIES, keywords);
            addKeyword(Keywords.DEPENDENT_SCHEMAS, keywords);

            JsonValue properties = schema.get(Keywords.PROPERTIES);
            JsonValue patternProperties = schema.get(Keywords.PATTERN_PROPERTIES);
            JsonValue additionalProperties = schema.get(Keywords.ADDITIONAL_PROPERTIES);

            if (properties != null || patternProperties != null || additionalProperties != null) {
                JsonObjectBuilder property = JsonObject.EMPTY.builder();

                if (properties != null) {
                    property.put(Keywords.PROPERTIES, properties);
                }

                if (patternProperties != null) {
                    property.put(Keywords.PATTERN_PROPERTIES, patternProperties);
                }

                if (additionalProperties != null) {
                    property.put(Keywords.ADDITIONAL_PROPERTIES, additionalProperties);
                }
                keywords.add(JsonObject.EMPTY.put("property", property.build()));
            }

            objectKeywords = keywords.addAll(getCommonEndKeywords()).build();
        }
        return objectKeywords;
    }

    private JsonArray getArrayKeywords() {
        if (arrayKeywords == null) {
            JsonArrayBuilder keywords = getCommonStartKeywords().builder();
            addKeyword(Keywords.MAX_ITEMS, keywords);
            addKeyword(Keywords.MIN_ITEMS, keywords);
            addKeyword(Keywords.UNIQUE_ITEMS, keywords);
            addKeyword(Keywords.CONTAINS, keywords);
            addKeyword(Keywords.ITEMS, keywords);

            /*
            JsonObjectBuilder item = null;

            JsonValue items = schema.getJsonObject().get(Keywords.ITEMS);
            if (items != null) {
                item = JsonObject.EMPTY.builder();
                item.put(Keywords.ITEMS, items);
            }

            JsonValue unevaluatedItems = schema.getJsonObject().get(Keywords.UNEVALUATED_ITEMS);
            if (unevaluatedItems != null) {
                if (item == null) {
                    item = JsonObject.EMPTY.builder();
                }
                item.put(Keywords.ITEMS, unevaluatedItems);
            }

            if (item != null) {
                keywords.add(JsonObject.EMPTY.put("item", item.build()));
            }

             */

            arrayKeywords = keywords.addAll(getCommonEndKeywords()).build();
        }
        return arrayKeywords;
    }

    private JsonArray getStringKeywords() {
        if (stringKeywords == null) {
            JsonArrayBuilder keywords = getCommonStartKeywords().builder();
            addKeyword(Keywords.MAX_LENGTH, keywords);
            addKeyword(Keywords.MIN_LENGTH, keywords);
            addKeyword(Keywords.LEXICAL_MAX, keywords);
            addKeyword(Keywords.LEXICAL_MIN, keywords);
            addKeyword(Keywords.PATTERN, keywords);
            stringKeywords = keywords.addAll(getCommonEndKeywords()).build();
        }
        return stringKeywords;
    }

    private JsonArray getNumberKeywords() {
        if (numberKeywords == null) {
            JsonArrayBuilder keywords = getCommonStartKeywords().builder();
            addKeyword(Keywords.MAXIMUM, keywords);
            addKeyword(Keywords.MINIMUM, keywords);
            addKeyword(Keywords.MULTIPLE_OF, keywords);
            numberKeywords = keywords.addAll(getCommonEndKeywords()).build();
        }
        return numberKeywords;
    }

    private void addKeyword(String keyword, JsonArrayBuilder builder) {
        JsonValue keywordValue = schema.get(keyword);
        if (keywordValue != null) {
            builder.add(JsonObject.EMPTY.put(keyword, keywordValue));
        }
    }

    public JsonValue getKeyword(int index, JsonValue instance) {
        JsonArray keywords = getKeywordPairs(instance);

        if (keywords.size() > index) {
            return keywords.get(index).getJsonObject().keys().get(0);
        }
        return null;
    }

    public JsonArray getKeywordPairs(JsonValue instance) {
        JsonArray keywords = JsonArray.EMPTY;
        if (instance.isJsonObject()) {
            keywords = getObjectKeywords();
        } else if (instance.isJsonArray()) {
            keywords = getArrayKeywords();
        } else if (instance.isString()) {
            keywords = getStringKeywords();
        } else if (instance.isBoolean()) {
            keywords = getCommonKeywords();
        } else if (instance.isJsonNumber()) {
            keywords = getNumberKeywords();
        } else if (instance.isNull()) {
            keywords = getCommonKeywords();
        }
        return keywords;
    }

    public ValidationNodeCreator getValidationNodeCreator(JsonValue instance, int keywordIndex) {
        if (instance.isJsonObject()) {
            if (objectValidationNodeCreators == null) {
                objectValidationNodeCreators = new NullableCachedValue<>(getValidationNodeCreators(instance));
            }
            return objectValidationNodeCreators.value == null
                    ? null
                    : objectValidationNodeCreators.value.size() <= keywordIndex
                    ? null
                    : objectValidationNodeCreators.value.get(keywordIndex);
        } else if (instance.isString()) {
            if (stringValidationNodeCreators == null) {
                stringValidationNodeCreators = new NullableCachedValue<>(getValidationNodeCreators(instance));
            }
            return stringValidationNodeCreators.value == null
                    ? null
                    : stringValidationNodeCreators.value.size() <= keywordIndex
                    ? null
                    : stringValidationNodeCreators.value.get(keywordIndex);
        } else if (instance.isJsonArray()) {
            if (arrayValidationNodeCreators == null) {
                arrayValidationNodeCreators = new NullableCachedValue<>(getValidationNodeCreators(instance));
            }
            return arrayValidationNodeCreators.value == null
                    ? null
                    : arrayValidationNodeCreators.value.size() <= keywordIndex
                    ? null
                    : arrayValidationNodeCreators.value.get(keywordIndex);
        } else if (instance.isJsonNumber()) {
            if (numberValidationNodeCreators == null) {
                numberValidationNodeCreators = new NullableCachedValue<>(getValidationNodeCreators(instance));
            }
            return numberValidationNodeCreators.value == null
                    ? null
                    : numberValidationNodeCreators.value.size() <= keywordIndex
                    ? null
                    : numberValidationNodeCreators.value.get(keywordIndex);
        } else {
            if (commonValidationNodeCreators == null) {
                commonValidationNodeCreators = new NullableCachedValue<>(getValidationNodeCreators(instance));
            }
            return commonValidationNodeCreators.value == null
                    ? null
                    : commonValidationNodeCreators.value.size() <= keywordIndex
                    ? null
                    : commonValidationNodeCreators.value.get(keywordIndex);
        }
    }

    private ArrayList<ValidationNodeCreator> getValidationNodeCreators(JsonValue instance) {
        ArrayList<ValidationNodeCreator> creators = new ArrayList();
        for (JsonValue keywordPair : getKeywordPairs(instance)) {
            String keyword = keywordPair.getJsonObject().keys().get(0).getString();
            switch (keyword) {
                case Keywords.ANY_OF: {
                    creators.add(
                            new ValidationNodeCreator() {
                                @Override
                                public ValidationNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
                                    return new AnyOfNode(instance, schema, keywordIndex, validationContext, rootInstance);
                                }
                            }
                    );
                    break;
                }
                case Keywords.ALL_OF: {
                    creators.add(
                            new ValidationNodeCreator() {
                                @Override
                                public ValidationNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
                                    return new AllOfNode(instance, schema, keywordIndex, validationContext, rootInstance);
                                }
                            }
                    );
                    break;
                }
                case Keywords.CONST: {
                    creators.add(
                            new ValidationNodeCreator() {
                                @Override
                                public ValidationNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
                                    return new ConstNode(instance, schema, keywordIndex, validationContext, rootInstance);
                                }
                            }
                    );
                    break;
                }
                case Keywords.CONTAINS: {
                    creators.add(
                            new ValidationNodeCreator() {
                                @Override
                                public ContainsNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
                                    return new ContainsNode(instance, schema, keywordIndex, validationContext, rootInstance);
                                }
                            }
                    );
                    break;
                }
                case Keywords.DEPENDENT_SCHEMAS: {
                    creators.add(
                            new ValidationNodeCreator() {
                                @Override
                                public ValidationNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
                                    return new DependentSchemasNode(instance, schema, keywordIndex, validationContext, rootInstance);
                                }
                            }
                    );
                    break;
                }
                case Keywords.ENUM: {
                    creators.add(
                            new ValidationNodeCreator() {
                                @Override
                                public ValidationNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
                                    return new EnumNode(instance, schema, keywordIndex, validationContext, rootInstance);
                                }
                            }
                    );
                    break;
                }
                case Keywords.EQUALS: {
                    creators.add(
                            new ValidationNodeCreator() {
                                @Override
                                public ValidationNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
                                    return new EqualsNode(instance, schema, keywordIndex, validationContext, rootInstance);
                                }
                            }
                    );
                    break;
                }

                case Keywords.IF: {
                    creators.add(
                            new ValidationNodeCreator() {
                                @Override
                                public ValidationNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
                                    return new IfThenElseNode(instance, schema, keywordIndex, validationContext, rootInstance);
                                }
                            }
                    );
                    break;
                }
                case "item": {
                    creators.add(
                            new ValidationNodeCreator() {
                                @Override
                                public ValidationNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
                                    return new ItemNode(instance, schema, keywordIndex, validationContext, rootInstance, 0);
                                }
                            }
                    );
                    break;
                }
                case Keywords.ITEMS: {
                    creators.add(
                            new ValidationNodeCreator() {
                                @Override
                                public ValidationNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
                                    return new ItemsNode(instance, schema, keywordIndex, validationContext, rootInstance);
                                }
                            }
                    );
                    break;
                }
                case Keywords.ONE_OF: {
                    creators.add(
                            new ValidationNodeCreator() {
                                @Override
                                public ValidationNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
                                    return new OneOfNode(instance, schema, keywordIndex, validationContext, rootInstance);
                                }
                            }
                    );
                    break;
                }
                case Keywords.MAX_ITEMS: {
                    creators.add(
                            new ValidationNodeCreator() {
                                @Override
                                public ValidationNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
                                    return new MaxItemsNode(instance, schema, keywordIndex, validationContext, rootInstance);
                                }
                            }
                    );
                    break;
                }
                case Keywords.MAX_LENGTH: {
                    creators.add(
                            new ValidationNodeCreator() {
                                @Override
                                public ValidationNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
                                    return new MaxLengthNode(instance, schema, keywordIndex, validationContext, rootInstance);
                                }
                            }
                    );
                    break;
                }
                case Keywords.MAX_PROPERTIES: {
                    creators.add(
                            new ValidationNodeCreator() {
                                @Override
                                public ValidationNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
                                    return new MaxPropertiesNode(instance, schema, keywordIndex, validationContext, rootInstance);
                                }
                            }
                    );
                    break;
                }
                case Keywords.MAXIMUM: {
                    creators.add(
                            new ValidationNodeCreator() {
                                @Override
                                public ValidationNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
                                    return new MaximumNode(instance, schema, keywordIndex, validationContext, rootInstance);
                                }
                            }
                    );
                    break;
                }

                case Keywords.MIN_ITEMS: {
                    creators.add(
                            new ValidationNodeCreator() {
                                @Override
                                public ValidationNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
                                    return new MinItemsNode(instance, schema, keywordIndex, validationContext, rootInstance);
                                }
                            }
                    );
                    break;
                }
                case Keywords.MIN_LENGTH: {
                    creators.add(
                            new ValidationNodeCreator() {
                                @Override
                                public ValidationNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
                                    return new MinLengthNode(instance, schema, keywordIndex, validationContext, rootInstance);
                                }
                            }
                    );
                    break;
                }
                case Keywords.MIN_PROPERTIES: {
                    creators.add(
                            new ValidationNodeCreator() {
                                @Override
                                public ValidationNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
                                    return new MinPropertiesNode(instance, schema, keywordIndex, validationContext, rootInstance);
                                }
                            }
                    );
                    break;
                }
                case Keywords.MINIMUM: {
                    creators.add(
                            new ValidationNodeCreator() {
                                @Override
                                public ValidationNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
                                    return new MinimumNode(instance, schema, keywordIndex, validationContext, rootInstance);
                                }
                            }
                    );
                    break;
                }
                case Keywords.MULTIPLE_OF: {
                    creators.add(
                            new ValidationNodeCreator() {
                                @Override
                                public ValidationNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
                                    return new MultipleOfNode(instance, schema, keywordIndex, validationContext, rootInstance);
                                }
                            }
                    );
                    break;
                }
                case Keywords.PATTERN: {
                    creators.add(
                            new ValidationNodeCreator() {
                                @Override
                                public ValidationNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
                                    return new PatternNode(instance, schema, keywordIndex, validationContext, rootInstance);
                                }
                            }
                    );
                    break;
                }
                case "property": {
                    creators.add(
                            new ValidationNodeCreator() {
                                @Override
                                public ValidationNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
                                    return new ObjectMemberIndexNode(instance, schema, keywordIndex, validationContext, rootInstance, 0);
                                }
                            }
                    );
                    break;
                }
                case Keywords.REF: {
                    creators.add(
                            new ValidationNodeCreator() {
                                @Override
                                public ValidationNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
                                    return new RefNode(instance, schema, keywordIndex, validationContext, rootInstance);
                                }
                            }
                    );
                    break;
                }
                case Keywords.REQUIRED: {
                    creators.add(
                            new ValidationNodeCreator() {
                                @Override
                                public ValidationNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
                                    return new RequiredNode(instance, schema, keywordIndex, validationContext, rootInstance);
                                }
                            }
                    );
                    break;
                }
                case Keywords.TYPE: {
                    creators.add(
                            new ValidationNodeCreator() {
                                @Override
                                public ValidationNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
                                    return new TypeNode(instance, schema, keywordIndex, validationContext, rootInstance);
                                }
                            }
                    );
                    break;
                }
                case Keywords.UNIQUE_ITEMS: {
                    creators.add(
                            new ValidationNodeCreator() {
                                @Override
                                public ValidationNode createValidationNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
                                    return new UniqueItemsNode(instance, schema, keywordIndex, validationContext, rootInstance);
                                }
                            }
                    );
                    break;
                }
            }
        }
        return creators;
    }
}
