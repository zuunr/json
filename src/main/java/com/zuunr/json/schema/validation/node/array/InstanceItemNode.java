package com.zuunr.json.schema.validation.node.array;

import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.validation.ValidationContext;
import com.zuunr.json.schema.validation.node.SchemaNode;
import com.zuunr.json.schema.validation.node.ValidationNode;

public class InstanceItemNode extends ValidationNode {

    public final int instanceIndex;

    public InstanceItemNode(JsonValue instance, JsonSchema schema, int schemaKeywordIndex, ValidationContext validationContext, JsonValue rootInstance, int instanceIndex) {
        super(instance, schema, schemaKeywordIndex, validationContext, rootInstance);
        this.instanceIndex = instanceIndex;
    }

    @Override
    protected void doAfterAllChildNodesAreCompleted() {

    }

    @Override
    protected Boolean calculateValid() {
        return null;
    }

    @Override
    protected ValidationNode createFirstChildNode() {
        return new SchemaNode(instance, schema, validationContext(), rootInstance);
    }

    @Override
    protected ValidationNode createNextChildNodeOfParent() {
        return ((ItemsNode) parentNode).createNextChild(instanceIndex);
    }

    @Override
    public Location location() {
        if (location == null) {
            location = new Location(
                    parentNode.location.instance.add(instanceIndex),
                    null,
                    parentNode.location.keyword,
                    null);
        }
        return location;
    }

    @Override
    protected JsonValue keyword() {
        return null;
    }

    @Override
    protected void childNodeCompleted(ValidationNode subnode) {

        setValid(subnode.getValid());
        filtrate = subnode.filtrate();

    }


}
