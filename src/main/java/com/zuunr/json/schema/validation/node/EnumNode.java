package com.zuunr.json.schema.validation.node;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.validation.ValidationContext;

/**
 * @author Niklas Eldberger
 */
public class EnumNode extends KeywordNode {

    public EnumNode(JsonValue instance, JsonSchema schema, int keywordIndex, ValidationContext validationContext, JsonValue rootInstance) {
        super(instance, schema, keywordIndex, validationContext, rootInstance);
    }

    @Override
    protected Boolean calculateValid() {
        JsonArray enm;
        JsonValue enumValue = schema.getKeyword(Keywords.ENUM, JsonValue.NULL);
        JsonValue data = dataKeywordValue(enumValue);
        if (data == null) {
            enm = enumValue.getJsonArray();
        } else {
            enm = data.getJsonArray();
        }
        return enm.contains(instance);
    }
}
