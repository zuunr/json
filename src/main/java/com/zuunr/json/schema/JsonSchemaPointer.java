package com.zuunr.json.schema;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArrayBuilder;
import com.zuunr.json.JsonValue;
import com.zuunr.json.UnsupportedTypeException;

/**
 * @author Niklas Eldberger
 */
public class JsonSchemaPointer {

    private JsonArray asArray;
    private JsonValue asJsonString;

    public JsonSchemaPointer(JsonValue self) {
        if (self.isJsonArray()) {
            asArray = self.getJsonArray();
        } else if (self.isString()) {
            asJsonString = self;
        }
    }

    public JsonValue asJsonString() {
        if (asJsonString == null) {
            StringBuilder stringBuilder = new StringBuilder();
            for (JsonValue element : asArray) {
                stringBuilder
                        .append("/")
                        .append(element.getValue());
            }
            String result = stringBuilder.toString();
            asJsonString = JsonValue.of(result.isEmpty() ? "/": result);
        }
        return asJsonString;
    }

    public String toString() {
        if (asJsonString == null) {
            StringBuilder builder = null;
            for (JsonValue element : asArray) {

                if (builder == null) {
                    builder = new StringBuilder("#");
                } else {
                    builder.append('/');
                }

                if (element.isString()) {
                    builder.append(element.getString());
                } else if (element.isInteger()) {
                    builder.append(element.getInteger());
                } else {
                    throw new UnsupportedTypeException("element must be either integer or string");
                }
            }
            asJsonString = JsonValue.of(builder.toString());
        }
        return asJsonString.getString();
    }


    public JsonArray asJsonArray() {
        if (asArray == null) {
            asArray = split(asJsonString().getString(), '/', 1);
        }
        return asArray;
    }

    public static JsonArray split(String string, char separator, int startOffset) {

        JsonArrayBuilder builder = JsonArray.EMPTY.builder();

        StringBuilder stringBuilder = new StringBuilder();

        boolean onlyDigits = true;

        for (int i = startOffset; i < string.length(); i++) {

            char currentChar = string.charAt(i);
            if (currentChar == separator) {

                String substring = stringBuilder.toString();

                if (substring.isEmpty()) {
                    builder.add(substring);
                } else if (onlyDigits) {
                    builder.add(Integer.valueOf(substring));
                } else {
                    builder.add(substring);
                }
                stringBuilder = new StringBuilder();

                onlyDigits = true;

            } else {
                if (Character.getType(currentChar) != Character.DECIMAL_DIGIT_NUMBER) {
                    onlyDigits = false;
                }
                stringBuilder.append(currentChar);
            }
        }
        return builder.build();
    }


}
