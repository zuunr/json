package com.zuunr.json.schema;

import com.zuunr.json.JsonObject;
import com.zuunr.json.schema.Keywords;

/**
 * @author Niklas Eldberger
 */
public class Schemas {

    public static final JsonObject ARRAY = JsonObject.EMPTY.put(Keywords.TYPE, Keywords.ARRAY);
    public static final JsonObject BOOLEAN = JsonObject.EMPTY.put(Keywords.TYPE, Keywords.BOOLEAN);
    public static final JsonObject INTEGER = JsonObject.EMPTY.put(Keywords.TYPE, Keywords.INTEGER);
    public static final JsonObject OBJECT = JsonObject.EMPTY.put(Keywords.TYPE, Keywords.OBJECT);
    public static final JsonObject NUMBER = JsonObject.EMPTY.put(Keywords.TYPE, Keywords.NUMBER);
    public static final JsonObject STRING = JsonObject.EMPTY.put(Keywords.TYPE, Keywords.STRING);

    public static final JsonObject NULL = JsonObject.EMPTY.put(Keywords.TYPE, Keywords.NULL);
}
