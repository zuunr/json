package com.zuunr.json.schema.generation;

import com.zuunr.json.JsonObject;

/**
 * @author Niklas Eldberger
 */
public interface SchemaUnionGenerator {
    SchemaUpdate unionOfKeywords(JsonObject schema1, JsonObject schema2);
}
