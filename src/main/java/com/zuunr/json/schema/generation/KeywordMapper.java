package com.zuunr.json.schema.generation;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArrayBuilder;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.pointer.JsonPointer;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.validation.JsonSchemaValidator;
import com.zuunr.json.schema.validation.OutputStructure;
import com.zuunr.json.schema.validation.node.string.Pattern;
import com.zuunr.json.template.StringTemplate;
import com.zuunr.json.util.StringSplitter;

import java.util.regex.Matcher;

public class KeywordMapper {

    private static final JsonSchemaValidator JSON_SCHEMA_VALIDATOR = new JsonSchemaValidator();
    private static final SchemaUnifier SCHEMA_UNIFIER = new BasicSchemaUnifier();

    public static final JsonObject KEYWORD_DEFAULTS = JsonObject.EMPTY.builder()
            .put(Keywords.MAX_ITEMS, JsonObject.EMPTY)
            .put(Keywords.MAX_LENGTH, JsonObject.EMPTY)
            .put(Keywords.MAXIMUM, JsonObject.EMPTY)
            .put(Keywords.MIN_ITEMS, JsonObject.EMPTY)
            .put(Keywords.MIN_LENGTH, JsonObject.EMPTY)
            .put(Keywords.MINIMUM, JsonObject.EMPTY)
            .put(Keywords.REQUIRED, JsonObject.EMPTY)
            .put(Keywords.UNIQUE_ITEMS, JsonObject.EMPTY)
            .build();

    public static final JsonObject DEFAULT_CONFIG = JsonObject.EMPTY
            .put("keywordMappers", JsonArray.EMPTY)
            .put("keywordDefaults", KEYWORD_DEFAULTS);

    private final JsonObject config;
    public final JsonArray effectiveKeywordMappers;
    public final JsonObject keywords;


    public KeywordMapper(JsonObject config) {
        this.config = config;
        this.effectiveKeywordMappers = effectiveKeywordMappers(config);
        this.keywords = config.get("keywordDefaults", DEFAULT_CONFIG).getJsonObject();
    }

    public KeywordMapper() {
        this(DEFAULT_CONFIG);
    }

    public KeywordMapperAndMatcher chooseMapper(JsonValue rootValue, JsonArray path, JsonValue value, JsonObject pathsPerValue) {

        JsonPointer instanceLocationPointer = path.as(JsonPointer.class);
        JsonObject model = JsonObject.EMPTY
                .put("rootValue", rootValue)
                .put("instanceLocation", instanceLocationPointer.getJsonPointerString())
                .put("instanceValue", value);

        for (int i = 0; i < effectiveKeywordMappers.size(); i++) {
            JsonObject mapper = effectiveKeywordMappers.get(i).getJsonObject();
            Pattern locationPattern = mapper.get("instanceLocation", JsonValue.NULL).as(Pattern.class);
            Matcher matcher = locationPattern == null ? null : locationPattern.compiled().matcher(instanceLocationPointer.getJsonPointerString().getString());

            if (matcher == null || matcher.find()) {
                JsonSchema instanceValueSchema = mapper.get("instanceValue", JsonValue.NULL).as(JsonSchema.class);
                JsonObject validationResult = instanceValueSchema == null
                        ? JsonObject.EMPTY.put("valid", true)
                        : JSON_SCHEMA_VALIDATOR.validate(value, instanceValueSchema, OutputStructure.FLAG);

                if (validationResult.get("valid").getBoolean()) {
                    return new KeywordMapperAndMatcher(mapper, matcher, model);
                }
            }
        }
        throw new RuntimeException("This is a bug! The last mapper in the effectiveKeywordMappers should be one that is always valid");
    }

    private static JsonArray effectiveKeywordMappers(JsonObject config) {

        JsonObject keywordDefaults = config.get("keywordDefaults", JsonObject.EMPTY).getJsonObject();

        JsonValue defaultRef = keywordDefaults.get(Keywords.REF);
        if (defaultRef != null && !defaultRef.isString()) {
            if (defaultRef.isBoolean() && !defaultRef.getBoolean()) {
                defaultRef = JsonValue.NULL;
            } else {
                defaultRef = JsonValue.of("/$defs/$0");
            }
            keywordDefaults = keywordDefaults.put(Keywords.REF, defaultRef);
        }

        JsonArray keywordMappers = config.get("keywordMappers", JsonArray.EMPTY).getJsonArray();
        JsonArrayBuilder keywordMappersBuilder = JsonArray.EMPTY.builder();

        for (int i = 0; i < keywordMappers.size(); i++) {
            JsonObject keywordMapper = keywordMappers.get(i).getJsonObject();
            keywordMappersBuilder.add(
                    keywordMapper
                            .put("keywords", keywordDefaults.putAll(keywordMapper.get("keywords", JsonObject.EMPTY).getJsonObject())));
        }

        // Adding a last mapper which will catch any value at any location
        keywordMappersBuilder.add(JsonObject.EMPTY
                .put("instanceLocation", "^.*$")
                .put("instanceValue", true)
                .put("keywords", keywordDefaults));
        return keywordMappersBuilder.build();
    }


    public static JsonObject applyRef(KeywordMapperAndMatcher keywordMapperAndMatcher, JsonObject schema) {
        JsonObject result = applyRefOnly(keywordMapperAndMatcher, schema);

        JsonValue defs = result.get(Keywords.DEFS);
        if (defs != null && defs.getJsonObject().isEmpty()) {
            result = result.remove(Keywords.DEFS);
        }
        return result;
    }

    private static JsonObject applyRefOnly(KeywordMapperAndMatcher keywordMapperAndMatcher, JsonObject schema) {
        JsonValue refJsonValue = keywordMapperAndMatcher.keywordMapper.get("keywords", JsonObject.EMPTY).get(Keywords.REF);

        if (refJsonValue == null || refJsonValue.isNull() || JsonValue.FALSE.equals(refJsonValue)) {
            return schema;
        }

        String ref;
        String templatePrefix = "/$defs/";
        if (refJsonValue.isString()) {
            ref = refJsonValue.getString();
            if (!ref.startsWith(templatePrefix)) {
                throw new RuntimeException("$ref must start with /" + Keywords.DEFS + "/");
            }
        } else {
            ref = refJsonValue.get("template", JsonValue.NULL).getString();
            if (ref == null) {
                String instanceLocation = keywordMapperAndMatcher.instanceModel.get("instanceLocation").getString();
                ref = instanceLocation.startsWith("/")
                        ? "/$defs" + instanceLocation
                        : "/$defs/" + instanceLocation;
            }
            if (!ref.startsWith(templatePrefix)) {
                throw new RuntimeException("template must start with /" + Keywords.DEFS + "/");
            }
        }
        ref = ref.substring(templatePrefix.length());

        JsonObject originalSchemaDefs = schema.get(Keywords.DEFS, JsonObject.EMPTY).getJsonObject();

        Matcher matcher = keywordMapperAndMatcher.matcher;

        if (refJsonValue.isJsonObject()) {

            StringTemplate stringTemplate = refJsonValue.get("string", JsonValue.NULL).as(StringTemplate.class);
            if (stringTemplate == null) {
                stringTemplate = keywordMapperAndMatcher.instanceModel.get("instanceLocation").as(StringTemplate.class);
            }
            JsonValue renderedString = stringTemplate.compiled.render(keywordMapperAndMatcher.instanceModel.jsonValue());
            matcher = keywordMapperAndMatcher.keywordMapper.get("keywords", JsonObject.EMPTY).get(Keywords.REF, JsonObject.EMPTY).get("pattern", ".*").as(Pattern.class).compiled().matcher(renderedString.getString());
            matcher = matcher.find() ? matcher : null;
        }

        if (matcher == null) {
            ref = keywordMapperAndMatcher.instanceModel.get("instanceLocation").as(JsonPointer.class).getJsonPointerString().getString();
        } else {
            int numberOfGroups = matcher.groupCount();
            for (int group = 0; group <= numberOfGroups; group++) {
                String valueOfGroup;
                try {
                    valueOfGroup = matcher.group(group);
                } catch (Exception e) {
                    throw new RuntimeException("There is no group of number " + group, e);
                }
                valueOfGroup = valueOfGroup == null ? "" : valueOfGroup;
                ref = ref.replace("$" + group, valueOfGroup);
            }
        }

        String definitionNameJsonPointerEscaped = createDefinitionName(ref);
        JsonPointer prefixedPointer = JsonPointer.of(templatePrefix + definitionNameJsonPointerEscaped);
        String unescapedDefinitionName = prefixedPointer.asArray().get(1).getString();
        JsonObject defsUnion = unionOfDDefs(JsonObject.EMPTY.put(unescapedDefinitionName, schema.remove(Keywords.DEFS)), originalSchemaDefs);
        return JsonObject.EMPTY
                .put(Keywords.REF, prefixedPointer.getJsonPointerString())
                .put(Keywords.DEFS, defsUnion);
    }

    static String createDefinitionName(String refTemplate) {
        JsonArray slashSplit = StringSplitter.splitString(refTemplate, '/');
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < slashSplit.size(); i++) {
            String fragment = slashSplit.get(i).getString();
            if (i != 0) {
                builder.append("_");
            }
            if ("0".equals(fragment)) {
                builder.append("item");
            } else if (fragment.startsWith("0")) {
                builder.append(fragment);
            } else {
                try {
                    Integer.parseInt(fragment);
                    builder.append("item");
                } catch (Exception e) {
                    builder.append(fragment);
                }
            }
        }
        String definitionName = builder.toString();
        if ("".equals(definitionName)) {
            definitionName = "root";
        }
        return definitionName.charAt(0) == '_' ? definitionName.substring(1) : definitionName;
    }

    private static JsonObject unionOfDDefs(JsonObject defs1, JsonObject defs2) {
        return SCHEMA_UNIFIER.unionOf(
                JsonObject.EMPTY.put(Keywords.DEFS, defs1).jsonValue(),
                JsonObject.EMPTY.put(Keywords.DEFS, defs2).jsonValue()
        ).get(Keywords.DEFS, JsonObject.EMPTY).getJsonObject();
    }
}
