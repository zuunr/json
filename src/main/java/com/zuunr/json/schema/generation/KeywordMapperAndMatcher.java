package com.zuunr.json.schema.generation;

import com.zuunr.json.JsonObject;
import com.zuunr.json.pointer.JsonPointer;

import java.util.regex.Matcher;

public class KeywordMapperAndMatcher {
    final JsonObject keywordMapper;
    final Matcher matcher;
    final JsonObject instanceModel;

    KeywordMapperAndMatcher(JsonObject keywordMapper, Matcher matcher, JsonObject instanceModel) {
        this.keywordMapper = keywordMapper;
        this.matcher = matcher;
        this.instanceModel = instanceModel;
    }


}