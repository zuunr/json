package com.zuunr.json.schema.generation;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;

/**
 * @author Niklas Eldberger
 */
public class SchemaUpdate {
    final JsonObject additions;
    final JsonArray removals;

    public SchemaUpdate(JsonObject additions, JsonArray removals) {
        this.additions = additions;
        this.removals = removals == null ? JsonArray.EMPTY : removals;
    }

    /**
     * Same as SchemaUpdate(additions, JsonArray.EMPTY)
     * @param additions
     */
    public SchemaUpdate(JsonObject additions) {
        this.additions = additions;
        this.removals = JsonArray.EMPTY;
    }

}
