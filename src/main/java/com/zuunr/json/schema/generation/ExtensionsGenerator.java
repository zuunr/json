package com.zuunr.json.schema.generation;

/**
 * @author Niklas Eldberger
 */
public interface ExtensionsGenerator extends SchemaPerceptor, SchemaUnionGenerator {
}
