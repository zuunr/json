package com.zuunr.json.schema.generation;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonNumber;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.Schemas;

/**
 * @author Niklas Eldberger
 */
public class TypeSchemaPerceptor implements SchemaPerceptor {

    private final boolean additionalProperties;

    /**
     * @param additionalPropertiesDefault <code>true</code> JsonObject.EMPTY.put("additionalProperties", true), <code>true</code> JsonObject.EMPTY.put("additionalProperties", false)
     */
    public TypeSchemaPerceptor(boolean additionalPropertiesDefault) {
        this.additionalProperties = additionalPropertiesDefault;
    }

    @Override
    public JsonObject generateProperties(JsonValue rootValue, JsonArray path, JsonObject jsonObject, JsonObject pathsPerValue) {
        return null;
    }

    @Override
    public JsonObject generateStringSchema(JsonValue rootValue, JsonArray path, String stringValue, JsonObject pathsPerValue) {
        return Schemas.STRING;
    }

    @Override
    public JsonObject generateNumericSchema(JsonValue rootValue, JsonArray path, JsonNumber numberValue, JsonObject pathsPerValue) {

        if (numberValue.isJsonInteger()) {
            return Schemas.INTEGER;
        } else {
            return Schemas.NUMBER;
        }
    }

    @Override
    public JsonObject generateObjectSchema(JsonValue rootValue, JsonArray path, JsonObject jsonObject, JsonObject pathsPerValue, KeywordMapperAndMatcher keywordMapperAndMatcher) {
        return Schemas.OBJECT.put(Keywords.ADDITIONAL_PROPERTIES, additionalProperties);
    }

    @Override
    public JsonObject generateArraySchema(JsonValue rootValue, JsonArray path, JsonArray jsonArray, JsonObject pathsPerValue) {
        return Schemas.ARRAY;
    }

    @Override
    public JsonValue generateItemsSchema(JsonValue rootValue, JsonArray path, JsonArray jsonArrayInstance, JsonObject pathsPerValue) {
        return null;
    }

    @Override
    public JsonObject generateBooleanSchema(JsonValue rootValue, JsonArray path, Boolean value, JsonObject pathsPerValue) {
        return Schemas.BOOLEAN;
    }
}
