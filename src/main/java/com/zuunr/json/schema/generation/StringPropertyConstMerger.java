/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json.schema.generation;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArrayBuilder;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public class StringPropertyConstMerger {

    private static final JsonObject ENUM_TO_PATTERN_CHAR_TRANSLATIONS = JsonObject.EMPTY
            .put("A", "A")
            .put("B", "B")
            .put("C", "C")
            .put("D", "D")
            .put("E", "E")
            .put("F", "F")
            .put("G", "G")
            .put("H", "H")
            .put("I", "I")
            .put("J", "J")
            .put("K", "K")
            .put("L", "L")
            .put("M", "M")
            .put("N", "N")
            .put("O", "O")
            .put("P", "P")
            .put("Q", "Q")
            .put("R", "R")
            .put("S", "S")
            .put("T", "T")
            .put("U", "U")
            .put("V", "V")
            .put("W", "W")
            .put("X", "X")
            .put("Y", "Y")
            .put("Z", "Z")
            .put("a", "a")
            .put("b", "b")
            .put("c", "c")
            .put("d", "d")
            .put("e", "e")
            .put("f", "f")
            .put("g", "g")
            .put("h", "h")
            .put("i", "i")
            .put("j", "j")
            .put("k", "k")
            .put("l", "l")
            .put("m", "m")
            .put("n", "n")
            .put("o", "o")
            .put("p", "p")
            .put("q", "q")
            .put("r", "r")
            .put("s", "s")
            .put("t", "t")
            .put("u", "u")
            .put("v", "v")
            .put("w", "w")
            .put("x", "x")
            .put("y", "y")
            .put("z", "z")
            .put("0", "0")
            .put("1", "1")
            .put("2", "2")
            .put("3", "3")
            .put("4", "4")
            .put("5", "5")
            .put("6", "6")
            .put("7", "7")
            .put("8", "8")
            .put("9", "9")
            .put("_", "_")
            .put(" ", " ")
            .put(".", "[.]")
            .put("*", "[*]")
            .put("?", "[?]")
            .put("+", "[+]")
            .put("(", "[(]")
            .put(")", "[)]")
            .put("[", "[[]")
            .put("{", "[{]")
            .put("}", "[}]")
            .put("\\", "[\\]")
            .put("^","\\^")
            .put("]", "\\]");

    public String regexOf(JsonArray enums) {
        StringBuilder builder = null;
        for (String enumString : enums.asList(String.class)) {
            if (builder == null) {
                builder = new StringBuilder();
            } else {
                builder.append("|");
            }

            builder.append(enumValueAsPattern(enumString));
        }
        return builder == null ? null : builder.toString();
    }

    /**
     * @param enum1
     * @param enum2
     * @return
     */
    public JsonArray unionOf(JsonArray enum1, JsonArray enum2) {

        JsonArrayBuilder union = enum2.builder();
        for (JsonValue enm : enum1) {
            if (!enum2.contains(enm)) {
                union.add(enm);
            }
        }
        return union.build().sort();
    }

    public String enumValueAsPattern(String enumValue) {

        StringBuilder result = new StringBuilder();
        for (int i = 0; i < enumValue.length(); i++) {
            String translation = ENUM_TO_PATTERN_CHAR_TRANSLATIONS
                    .get("" + enumValue.charAt(i), JsonValue.NULL).getString();
            if (translation == null) {
                result.append("[").append(enumValue.charAt(i)).append("]");
            } else {
                result.append(translation);
            }
        }
        return result.toString();
    }

    /**
     * @param enums1
     * @param enums2 order of elements in enum2 is kept
     * @return
     */
    public JsonArray intersectionOf(JsonArray enums1, JsonArray enums2) {
        JsonArrayBuilder intersection = JsonArray.EMPTY.builder();
        for (JsonValue enm : enums2) {
            if (enums1.contains(enm)) {
                intersection.add(enm);
            }
        }
        return intersection.build().sort();
    }
}
