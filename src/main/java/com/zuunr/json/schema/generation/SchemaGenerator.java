package com.zuunr.json.schema.generation;

import com.zuunr.json.*;
import com.zuunr.json.pointer.JsonPointer;
import com.zuunr.json.schema.JsonSchema;
import com.zuunr.json.schema.Keywords;
import com.zuunr.json.schema.Type;
import com.zuunr.json.schema.validation.JsonSchemaValidator;
import com.zuunr.json.schema.validation.OutputStructure;

import java.util.Iterator;

/**
 * @author Niklas Eldberger
 */
public final class SchemaGenerator {

    private static final JsonArray ITEMS_DEFS_ARRAY = JsonArray.of(Keywords.ITEMS, Keywords.DEFS);
    private static final JsonSchemaValidator jsonSchemaValidator = new JsonSchemaValidator();
    private static final EqualsGenerator equalsGenerator = new EqualsGenerator();
    private final SchemaUnifier schemaUnifier;
    private final SchemaPerceptor schemaPerceptor;


    private KeywordMapper keywordMapper;

    /**
     * Creates a SchemaGenerator with BasicSchemaPerceptor and BasicSchemaUnifier
     */
    public SchemaGenerator() {
        this(new KeywordMapper());
    }

    public SchemaGenerator(KeywordMapper keywordMapper) {
        this(new BasicSchemaPerceptor(keywordMapper), new BasicSchemaUnifier(), keywordMapper);
    }

    public SchemaGenerator(JsonObject config) {
        this(new KeywordMapper(config));
    }

    /**
     * Creates a SchemaGenerator based on custom SchemaPerceptor and SchemaUnifier.
     *
     * @param schemaPerceptor     - used by SchemaGenerator to understand how to create a JSON Schema by looking at (percept) one example JSON structure
     * @param schemaSchemaUnifier - used by SchemaGenerator to understand how to merge two different JSON Schemas into one unified schema
     */
    public SchemaGenerator(SchemaPerceptor schemaPerceptor, SchemaUnifier schemaSchemaUnifier, KeywordMapper keywordMapper) {
        this.schemaPerceptor = schemaPerceptor;
        this.schemaUnifier = schemaSchemaUnifier;

        this.keywordMapper = keywordMapper;
    }

    public JsonObject generateSchema(JsonArray value) {
        return generateSchema(value.jsonValue(), value.jsonValue());
    }

    public JsonObject generateSchema(JsonObject value) {
        return generateSchema(value.jsonValue(), value.jsonValue());
    }

    public JsonObject generateSchema(JsonValue value) {
        return generateSchema(value, value);
    }

    public JsonObject generateSchema(JsonArray value, JsonArray extendedValueForReflexiveRelations) {
        return generateSchema(value.jsonValue(), extendedValueForReflexiveRelations.jsonValue());
    }

    public JsonObject generateSchema(JsonObject value, JsonObject extendedValueForReflexiveRelations) {
        return generateSchema(value.jsonValue(), extendedValueForReflexiveRelations.jsonValue());
    }

    private JsonObject generateSchema(JsonValue value, JsonValue extendedValueForReflexiveRelations) {
        JsonObject pathsPerValue = equalsGenerator.equalValuePaths(extendedValueForReflexiveRelations, "-1");
        return generateSchema(value, JsonArray.EMPTY, value, pathsPerValue);
    }

    private JsonObject generateSchema(JsonValue rootValue, JsonArray path, JsonValue jsonValue, JsonObject pathsPerValue) {

        JsonObject schema;

        if (jsonValue.isNull()) {
            schema = schemaPerceptor.generateNullSchema(rootValue, path, pathsPerValue);
        } else if (jsonValue.isString()) {
            schema = generateStringSchema(rootValue, path, jsonValue.getString(), pathsPerValue);
        } else if (jsonValue.isJsonObject()) {
            schema = generateObjectSchema(rootValue, path, jsonValue.getJsonObject(), pathsPerValue);
        } else if (jsonValue.isJsonArray()) {
            schema = generateArraySchema(rootValue, path, jsonValue.getJsonArray(), pathsPerValue);
        } else if (jsonValue.isBoolean()) {
            schema = schemaPerceptor.generateBooleanSchema(rootValue, path, jsonValue.getBoolean(), pathsPerValue);
        } else if (jsonValue.isJsonNumber()) {
            schema = generateNumericSchema(rootValue, path, jsonValue, pathsPerValue);
        } else {
            throw new UnsupportedTypeException("type is not a json type!?");
        }
        return schema;
    }

    private JsonObject unionOfDDefs(JsonObject defs1, JsonObject defs2) {
        return schemaUnifier.unionOf(
                JsonObject.EMPTY.put(Keywords.DEFS, defs1).jsonValue(),
                JsonObject.EMPTY.put(Keywords.DEFS, defs2).jsonValue()
        ).get(Keywords.DEFS, JsonObject.EMPTY).getJsonObject();
    }

    private JsonObject generateStringSchema(JsonValue rootValue, JsonArray path, String value, JsonObject pathsPerValue) {
        return schemaPerceptor.generateStringSchema(rootValue, path, value, pathsPerValue);
    }

    private JsonObject generateObjectSchema(JsonValue rootValue, JsonArray path, JsonObject jsonObject, JsonObject pathsPerValue) {

        KeywordMapperAndMatcher keywordMapperAndMatcher = keywordMapper.chooseMapper(rootValue, path, jsonObject.jsonValue(), pathsPerValue);
        JsonObject objectSchema = schemaPerceptor.generateObjectSchema(rootValue, path, jsonObject, pathsPerValue, keywordMapperAndMatcher);
        JsonObject generatedProperties = generateProperties(rootValue, path, jsonObject, pathsPerValue);

        JsonObject allOfObjectAndNestedProps = objectSchema.putAll(generatedProperties);
        JsonObject schema = KeywordMapper.applyRef(keywordMapperAndMatcher, allOfObjectAndNestedProps);

        return schema;
    }

    private JsonObject generateProperties(JsonValue rootValue, JsonArray path, JsonObject jsonObject, JsonObject pathsPerValue) {

        JsonObject objectMemberSchemas = schemaPerceptor.generateProperties(rootValue, path, jsonObject, pathsPerValue);

        if (objectMemberSchemas == null) {

            JsonObjectBuilder objectMemberKeywordSchemas = JsonObject.EMPTY.builder();
            JsonObjectBuilder propertiesBuilder = JsonObject.EMPTY.builder();
            Iterator<JsonValue> valuesIter = jsonObject.values().iterator();

            objectMemberKeywordSchemas.put(Keywords.ADDITIONAL_PROPERTIES, keywordMapper.keywords.get(Keywords.ADDITIONAL_PROPERTIES, JsonValue.FALSE));

            JsonObject mergedDefs = JsonObject.EMPTY;
            for (JsonValue jsonValueKey : jsonObject.keys()) {
                JsonObject generatedSchema = generateSchema(rootValue, path.add(jsonValueKey), valuesIter.next(), pathsPerValue);
                mergedDefs = unionOfDDefs(mergedDefs, generatedSchema.get(Keywords.DEFS, JsonObject.EMPTY).getJsonObject());
                propertiesBuilder.put(jsonValueKey.getString(), generatedSchema.remove(Keywords.DEFS));
            }
            return objectMemberKeywordSchemas
                    .put(Keywords.TYPE, Keywords.OBJECT)
                    .put(Keywords.PROPERTIES, propertiesBuilder.build())
                    .put(Keywords.DEFS, mergedDefs)
                    .build();
        } else {
            return objectMemberSchemas;
        }
    }

    private JsonObject generateArraySchema(JsonValue rootValue, JsonArray path, JsonArray jsonArrayInstance, JsonObject pathsPerValue) {

        JsonObject schema = schemaPerceptor.generateArraySchema(rootValue, path, jsonArrayInstance, pathsPerValue);
        JsonValue itemsSchema = jsonArrayInstance.isEmpty() ? null : generateItemsSchema(rootValue, path, jsonArrayInstance, pathsPerValue);

        if (itemsSchema != null) {
            JsonObject itemsDefs = itemsSchema.get(Keywords.DEFS, JsonObject.EMPTY).getJsonObject();
            JsonObject schemaDefs = schema.get(Keywords.DEFS, JsonObject.EMPTY).getJsonObject();

            JsonObject unionOfDefs = unionOfDDefs(schemaDefs, itemsDefs);
            if (unionOfDefs != null && unionOfDefs.isEmpty()) {
                schema = schema.remove(Keywords.DEFS);
            } else {
                schema = schema.put(Keywords.DEFS, unionOfDefs);
            }

            JsonValue refJsonValue = schema.get(Keywords.REF);
            if (refJsonValue == null) {
                schema = schema.put(Keywords.ITEMS, itemsSchema.getJsonObject().remove(Keywords.DEFS));
            } else {
                schema = schema.put(refJsonValue.as(JsonPointer.class).asArray().add("items"), itemsSchema.getJsonObject().remove(Keywords.DEFS));
            }
        }
        return schema;
    }

    private JsonValue generateItemsSchema(JsonValue rootValue, JsonArray path, JsonArray jsonArrayInstance, JsonObject pathsPerValue) {

        JsonValue itemsSchema = schemaPerceptor.generateItemsSchema(rootValue, path, jsonArrayInstance, pathsPerValue);
        if (itemsSchema != null) {
            return itemsSchema;
        } else {
            JsonValue allItemsSchema = null;
            int index = 0;
            for (JsonValue item : jsonArrayInstance) {
                JsonArray nextPath = path.add(index++);
                if (allItemsSchema == null) {
                    allItemsSchema = generateSchema(rootValue, nextPath, item, pathsPerValue).jsonValue();
                } else {
                    JsonObject validationResult = jsonSchemaValidator.validate(item, allItemsSchema, OutputStructure.FLAG);
                    if (!validationResult.get("valid").getBoolean()) {
                        JsonObject itemSchema = generateSchema(rootValue, nextPath, item, pathsPerValue);
                        allItemsSchema = schemaUnifier.unionOf(allItemsSchema, itemSchema.jsonValue());
                    }
                }
            }
            return allItemsSchema;
        }
    }

    private JsonObject generateNumericSchema(JsonValue rootValue, JsonArray path, JsonValue jsonValue, JsonObject pathsPerValue) {
        return schemaPerceptor.generateNumericSchema(rootValue, path, jsonValue.getJsonNumber(), pathsPerValue);
    }
}
