package com.zuunr.json.schema.generation;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonNumber;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.Schemas;

/**
 * @author Niklas Eldberger
 */
public interface SchemaPerceptor {

    /**
     * @param rootValue
     * @param path
     * @param jsonObject
     * @param pathsPerValue
     * @return null means default behaviour of additionalProperties: false and properties one nested schema for each object member be generated
     */
    JsonObject generateProperties(JsonValue rootValue, JsonArray path, JsonObject jsonObject, JsonObject pathsPerValue);

    JsonObject generateStringSchema(JsonValue rootValue, JsonArray path, String stringValue, JsonObject pathsPerValue);

    JsonObject generateNumericSchema(JsonValue rootValue, JsonArray path, JsonNumber numberValue, JsonObject pathsPerValue);

    JsonObject generateObjectSchema(JsonValue rootValue, JsonArray path, JsonObject jsonObject, JsonObject pathsPerValue, KeywordMapperAndMatcher keywordMapperAndMatcher);

    JsonObject generateArraySchema(JsonValue rootValue, JsonArray path, JsonArray jsonArray, JsonObject pathsPerValue);

    /**
     * @param rootValue
     * @param path
     * @param jsonArrayInstance
     * @param pathsPerValue
     * @return null means default behaviour (unifying schemas for each item)
     */
    JsonValue generateItemsSchema(JsonValue rootValue, JsonArray path, JsonArray jsonArrayInstance, JsonObject pathsPerValue);

    JsonObject generateBooleanSchema(JsonValue rootValue, JsonArray path, Boolean value, JsonObject pathsPerValue);

    default JsonObject generateNullSchema(JsonValue rootValue, JsonArray path, JsonObject pathsPerValue){
        return Schemas.NULL;
    }

}
