package com.zuunr.json.schema.generation;

import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public interface SchemaUnifier {

    public JsonValue unionOf(JsonValue schema1, JsonValue schema2);
}
