package com.zuunr.json.schema.generation;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.pointer.JsonPointer;

/**
 * @author Niklas Eldberger
 */
public class EqualsGenerator {

    public JsonObject equalValuePaths(JsonValue jsonValue, String indexEscapeString) {
        JsonObject pathsByValue = JsonObject.EMPTY;

        for (JsonValue path : unindexedPathsWithEqualValue(jsonValue, indexEscapeString)) {

            JsonValue value = path.getJsonArray().last();
            JsonArray pathsOfValue = pathsByValue.get(value.asJson(), JsonArray.EMPTY).getJsonArray();
            pathsOfValue = pathsOfValue.add(path.getJsonArray().allButLast().replace("-1", -1)).sort();
            pathsByValue = pathsByValue.put(value.asJson(), pathsOfValue);
        }
        return pathsByValue;
    }

    JsonArray unindexedPathsWithEqualValue(JsonValue jsonValue, String indexEscapeString) {

        JsonValue escapedIndex = JsonValue.of(indexEscapeString);

        JsonObject valuesObject = JsonObject.EMPTY;
        JsonObject equalPaths = JsonObject.EMPTY;
        for (JsonValue path : jsonValue.getPaths(true)) {
            JsonValue newValue = path.getJsonArray().last();

            JsonArray unindexedPath = path.getJsonArray().allButLast().replaceIntegers(escapedIndex);
            JsonValue existingValue = valuesObject.get(unindexedPath);

            if (existingValue == null) {
                try {
                    valuesObject = valuesObject.put(unindexedPath, newValue);
                } catch (Exception e) {

                }
                equalPaths = equalPaths.put(unindexedPath, newValue);
            } else if (!existingValue.equals(newValue)) {
                equalPaths = equalPaths.remove(unindexedPath);
            }
        }
        return equalPaths.jsonValue().getPaths(true);
    }

    public JsonObject pathsByValueFromPaths(JsonArray pathsAndValues) {
        JsonObject pathByValue = JsonObject.EMPTY;
        for (int i = 0; i < pathsAndValues.size(); i++) {

            JsonArray pathAndValueArray = pathsAndValues.get(i).getJsonArray();
            JsonValue value = pathAndValueArray.last();
            JsonArray path = pathAndValueArray.allButLast();

            String valueAsJson = value.asJson();
            JsonPointer jsonPointer = path.as(JsonPointer.class);

            System.out.println(jsonPointer.getJsonPointerString() + ": " + valueAsJson);
            JsonArray allPathsOfValueSoFar = pathByValue.get(valueAsJson, JsonArray.EMPTY).getJsonArray();
            pathByValue = pathByValue.put(valueAsJson, allPathsOfValueSoFar.add(jsonPointer.getJsonPointerString()));
        }
        return pathByValue;
    }

    public JsonObject pathsByValue(JsonValue jsonValue) {
        return pathsByValueFromPaths(jsonValue.getPaths(true));
    }
}
