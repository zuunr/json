package com.zuunr.json.schema;

import com.zuunr.json.JsonArray;

/**
 * @author Niklas Eldberger
 */
public class Keywords {

    public static final String ADDITIONAL_PROPERTIES = "additionalProperties";
    public static final String ALL_OF = "allOf";
    public static final String ANY_OF = "anyOf";
    public static final String CONST = "const";
    public static final String CONTAINS = "contains";
    public static final String DEFS = "$defs";
    public static final String DEPENDENT_SCHEMAS = "dependentSchemas";
    public static final String DESCRIPTION = "description";
    public static final String ELSE = "else";
    public static final String ENUM = "enum";
    public static final String EQUALS = "equals"; // JSON Schema extension
    public static final String FORMAT = "format";
    public static final String IF = "if";
    public static final String ITEMS = "items";
    public static final String LEXICAL_MAX = "lexicalMax"; // JSON Schema extension
    public static final String LEXICAL_MIN = "lexicalMin"; // JSON Schema extension
    public static final String MAX_ITEMS = "maxItems";
    public static final String MAX_LENGTH = "maxLength";
    public static final String MAX_PROPERTIES = "maxProperties";
    public static final String MAXIMUM = "maximum";
    public static final String MIN_ITEMS = "minItems";
    public static final String MIN_LENGTH = "minLength";
    public static final String MIN_PROPERTIES = "minProperties";
    public static final String MINIMUM = "minimum";
    public static final String MULTIPLE_OF = "multipleOf";
    public static final String ONE_OF = "oneOf";
    public static final String PATHS = "paths";
    public static final String PATTERN = "pattern";
    public static final String PATTERN_PROPERTIES = "patternProperties";
    public static final String PROPERTIES = "properties";
    public static final String REF = "$ref";
    public static final String REQUIRED = "required";
    public static final String THEN = "then";
    public static final String TITLE = "title";
    public static final String TYPE = "type";
    public static final String UNIQUE_ITEMS = "uniqueItems";
    public static final String UNEVALUATED_ITEMS = "unevaluatedItems";

    public static final String ARRAY = "array";
    public static final String BOOLEAN = "boolean";
    public static final String INTEGER = "integer";
    public static final String OBJECT = "object";
    public static final String NUMBER = "number";
    public static final String NULL = "null";
    public static final String STRING = "string";

    public static final JsonArray KEYWORDS_WHERE_VALUE_IS_SCHEMA = JsonArray.of(ADDITIONAL_PROPERTIES, ALL_OF, ANY_OF, ITEMS, ONE_OF, PROPERTIES, PATTERN_PROPERTIES);
    public static final JsonArray KEYWORDS_WITHOUT_SCHEMAS = JsonArray.of(CONST, DESCRIPTION, ENUM, EQUALS, LEXICAL_MAX, LEXICAL_MIN, MAX_LENGTH, MAX_ITEMS, MAX_PROPERTIES, MIN_ITEMS, MIN_LENGTH, MIN_PROPERTIES, MINIMUM, PATTERN, REQUIRED, TITLE, TYPE, UNIQUE_ITEMS);
}