package com.zuunr.json.schema;

/**
 * @author Niklas Eldberger
 */
public enum Type {
    STRING, OBJECT, ARRAY, BOOLEAN, NULL, NUMBER, INTEGER
}
