/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json.tool;

import com.zuunr.json.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author Niklas Eldberger
 */
public class Json2JavaGenerator {

    public File makeZip(String zipFilePath, JsonObject models) {

        JsonArray keys = models.keys();
        JsonArray values = models.values();

        File outputFile = new File(zipFilePath);
        try (ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(outputFile))) {
            for (int i = 0; i < values.size(); i++) {
                try {
                    ZipEntry e = new ZipEntry(keys.get(i).getValue(String.class).replace('.', '/') + ".java");
                    zos.putNextEntry(e);
                    byte[] data = values.get(i).getValue(String.class).getBytes();
                    zos.write(data, 0, data.length);
                } finally {
                    zos.closeEntry();
                }
            }
            
            zos.finish();
            
            return outputFile;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public JsonObject renderModels(JsonObject models) {

        JsonObjectBuilder jsonObjectBuilder = JsonObject.EMPTY.builder();

        JsonArray values = models.values();

        for (int i = 0; i < values.size(); i++) {
            JsonObject model = values.get(i).getValue(JsonObject.class);

            String rendered;
            if (model.get("implementsJsonObjectSupport", false).getValue(Boolean.class)) {
                rendered = renderJsonObjectSupportModel(model);
            } else {
                rendered = renderJsonArraySupportModel(model);
            }
            String className = model.get("package").getValue(String.class) + "." + model.get("modelName").getValue(String.class);
            jsonObjectBuilder.put(className, rendered);
        }

        return jsonObjectBuilder.build();
    }

    public JsonObject generateModels(String packageName, String modelPrefix, JsonValue sample) {

        if (sample.is(JsonObject.class)) {
            return generateModels(packageName, modelPrefix, sample.getValue(JsonObject.class));
        } else if (sample.is(JsonArray.class)) {
            return generateModels(packageName, modelPrefix, sample.getValue(JsonArray.class));
        } else {
            throw new UnsupportedTypeException("Only JsonObject and JsonArray is supported here");
        }

    }

    public JsonObject generateModels(String packageName, String modelPrefix, JsonObject sample) {

        JsonArray keys = sample.keys();
        JsonArray values = sample.values();

        JsonObjectBuilder modelBuilder = JsonObject.EMPTY.builder();
        JsonArrayBuilder fieldsBuilder = JsonArray.EMPTY.builder();
        JsonObjectBuilder fieldBuilder = JsonObject.EMPTY.builder();


        for (int i = 0; i < keys.size(); i++) {

            String fieldName = keys.get(i).getValue(String.class);
            String fieldtype;

            JsonValue value = values.get(i);
            if (value.is(JsonObject.class)) {

                fieldtype = new StringBuilder(modelPrefix.substring(0, 1).toUpperCase()).append(modelPrefix.substring(1, modelPrefix.length())).append('_').append(fieldName).toString();

                modelBuilder.putAll(generateModels(packageName, fieldtype, value.getValue(JsonObject.class)));
            } else if (value.is(JsonArray.class)) {
                fieldtype = new StringBuilder(modelPrefix.substring(0, 1).toUpperCase()).append(modelPrefix.substring(1, modelPrefix.length())).append('_').append(fieldName).toString();
                modelBuilder.putAll(generateModels(packageName, fieldtype, value.getValue(JsonArray.class)));
            } else {
                if (value.is(String.class)) {
                    fieldtype = "String";
                } else if (value.is(Boolean.class)) {
                    fieldtype = "Boolean";
                } else if (value.isInteger()) {
                    fieldtype = "Integer";
                } else if (value.is(Double.class)) {
                    fieldtype = "Double";
                } else {
                    throw new UnsupportedTypeException("Unsupported field type: " + value.getValue().getClass());
                }
            }
            fieldBuilder
                    .put("fieldType", fieldtype)
                    .put("FieldName", fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1, fieldName.length()))
                    .put("fieldName", fieldName.substring(0, 1).toLowerCase() + fieldName.substring(1, fieldName.length()));


            JsonObject field = fieldBuilder.build();
            fieldsBuilder.add(field);
            fieldBuilder = field.builder();
        }
        return modelBuilder
                .put(modelPrefix, JsonObject.EMPTY
                        .put("package", packageName)
                        .put("implementsJsonObjectSupport", true)
                        .put("modelName", modelPrefix)
                        .put("fields", fieldsBuilder.build()))
                .build();
    }

    public JsonObject generateModels(String packageName, String modelPrefix, JsonArray sample) {

        JsonObjectBuilder modelBuilder = JsonObject.EMPTY.builder();

        String elementType = modelPrefix + "_elem";

        modelBuilder
                .put(modelPrefix, JsonObject.EMPTY
                        .put("package", packageName)
                        .put("implementsJsonArraySupport", true)
                        .put("modelName", modelPrefix)
                        .put("elementType", elementType));

        modelBuilder.putAll(generateModels(packageName, elementType, sample.get(0)));

        return modelBuilder.build();
    }

    private String renderJsonArraySupportModel(JsonObject model) {

        String template = "package ##package##;\n" +
                "\n" +
                "import com.zuunr.json.JsonArray;\n" +
                "import com.zuunr.json.JsonArraySupport;\n" +
                "import com.zuunr.json.JsonValue;\n" +
                "import java.util.List;\n" +
                "\n" +
                "import static com.zuunr.json.JsonArrayBuilder;\n" +
                "\n" +
                "/* This class is generated from JSON **/\n" +
                "\n" +
                "public class ##modelName## implements JsonArraySupport {\n" +
                "    private JsonArray jsonArray;\n" +
                "\n" +
                "    public static Builder builder() {\n" +
                "        return new Builder();\n" +
                "    }\n" +
                "\n" +
                "    private ##modelName##(JsonArray jsonArray) {\n" +
                "        this.jsonArray = jsonArray;\n" +
                "    }\n" +
                "\n" +
                "    private ##modelName##(JsonValue jsonValue) {\n" +
                "        this.jsonArray = jsonValue.getValue(JsonArray.class);\n" +
                "    }\n" +
                "\n" +
                "    @Override\n" +
                "    public JsonArray asJsonArray() {\n" +
                "        return jsonArray;\n" +
                "    }\n" +
                "\n" +
                "    public List<##elementType##> asList() {\n" +
                "        return jsonArray.asList(##elementType##.class);\n" +
                "    }\n" +
                "\n" +
                "    public static class Builder {\n" +
                "\n" +
                "        JsonArrayBuilder jsonArrayBuilder;\n" +
                "\n" +
                "        public Builder() {\n" +
                "            jsonArrayBuilder = JsonArray.EMPTY.builder();\n" +
                "        }\n" +
                "\n" +
                "        public Builder add(##elementType## element) {\n" +
                "            jsonArrayBuilder.add(element);\n" +
                "            return this;\n" +
                "        }\n" +
                "\n" +
                "        public ##modelName## build() {\n" +
                "            return new ##modelName##(jsonArrayBuilder.build());\n" +
                "        }\n" +
                "\n" +
                "    }\n" +
                "}\n";
        return template
                .replace("##package##", model.get("package").getValue(String.class))
                .replace("##modelName##", model.get("modelName").getValue(String.class))
                .replace("##elementType##", model.get("elementType").getValue(String.class));
    }

    private String renderJsonObjectSupportModel(JsonObject model) {
        String template = "package ##package##;\n" +
                "\n" +
                "import com.zuunr.json.JsonObject;\n" +
                "import com.zuunr.json.JsonValue;\n" +
                "import com.zuunr.json.TypedObject;\n" +
                "\n" +
                "import static com.zuunr.json.JsonObjectBuilder;\n" +
                "\n" +
                "/** This class is generated from JSON **/\n" +
                "\n" +
                "public class ##modelName## extends TypedObject<##modelName##> {\n" +
                "\n" +
                "    public static Builder builder() {\n" +
                "        return new Builder();\n" +
                "    }\n" +
                "\n" +
                "    private ##modelName##(JsonObject jsonObject) {\n" +
                "        super(jsonObject);\n" +
                "    }\n" +
                "\n" +
                "    private ##modelName##(JsonValue jsonValue) {\n" +
                "        super(jsonValue.getValue(JsonObject.class));\n" +
                "    }\n" +
                "\n" +
                "    @Override\n" +
                "    protected ##modelName## createNew(JsonObject jsonObject) {\n" +
                "        ##modelName## model = new ##modelName##(jsonObject);\n" +
                "        return model;\n" +
                "    }\n" +
                "\n" +
                "##modelGetters##\n" +
                "\n" +
                "##modelBuilder##\n" +
                "\n" +
                "}";

        return template
                .replace("##package##", model.get("package").getValue(String.class))
                .replace("##modelName##", model.get("modelName").getValue(String.class))
                .replace("##modelGetters##", renderGetters(model))
                .replace("##modelBuilder##", renderModelBuilder(model));
    }

    private String renderGetters(JsonObject model) {


        StringBuilder stringBuilder = null;
        JsonArray fields = model.get("fields").getValue(JsonArray.class);

        for (int i = 0; i < fields.size(); i++) {
            if (stringBuilder == null) {
                stringBuilder = new StringBuilder();
            } else {
                stringBuilder.append('\n');
            }
            stringBuilder.append(getterMethod(fields.get(i).getValue(JsonObject.class)));
        }
        return stringBuilder == null ? "" : stringBuilder.toString();
    }

    private String getterMethod(JsonObject field) {
        String getterMethod =
                "    public ##fieldType## get##FieldName##() {\n" +
                        "        return jsonObject.get(\"##fieldName##\", JsonValue.NULL).getValue(##fieldType##.class);\n" +
                        "    }\n";
        return getterMethod.replace("##fieldType##", field.get("fieldType").getValue(String.class)).replace("##fieldName##", field.get("fieldName").getValue(String.class)).replace("##FieldName##", field.get("FieldName").getValue(String.class));
    }


    private String renderModelBuilder(JsonObject model) {
        String builder = "    public static final class Builder {\n" +
                "        private JsonObjectBuilder jsonObjectBuilder = JsonObject.EMPTY.builder();\n" +
                "        \n" +
                "##builderMethods##" +
                "        \n" +
                "        public " + model.get("modelName").getValue(String.class) + " build() {\n" +
                "            return new ##modelName##(jsonObjectBuilder.build());\n" +
                "        }\n" +
                "\n" +
                "    }\n";

        String builderMethods = builderMethods(model);

        builder = builder
                .replace("##modelName##", model.get("modelName").getValue(String.class))
                .replace("##builderMethods##", builderMethods);
        return builder;
    }

    private String builderMethods(JsonObject model) {

        StringBuilder stringBuilder = null;

        JsonArray fields = model.get("fields").getValue(JsonArray.class);

        for (int i = 0; i < fields.size(); i++) {
            JsonObject field = fields.get(i).getValue(JsonObject.class);
            String fieldType = field.get("fieldType").getValue(String.class);
            String fieldName = field.get("fieldName").getValue(String.class);

            if (stringBuilder == null) {
                stringBuilder = new StringBuilder();
            } else {
                stringBuilder.append("\n");
            }
            stringBuilder.append(builderMethod(fieldType, fieldName));

        }
        return stringBuilder == null ? "" : stringBuilder.toString();
    }

    private String builderMethod(String fieldType, String fieldName) {
        String builderMethod =
                "        public Builder ##fieldName##(##fieldType## ##fieldName##) {\n" +
                        "            jsonObjectBuilder.put(\"##fieldName##\", ##fieldName##);\n" +
                        "            return this;\n" +
                        "        }\n";

        return builderMethod.replace("##fieldType##", fieldType).replace("##fieldName##", fieldName).replace("##FieldName##", fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1, fieldName.length()));
    }
}
