/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json.tool;

import com.zuunr.json.*;

import java.util.Map;
import java.util.function.Consumer;

/**
 * @author Niklas Eldberger
 */
public class JsonUtil {

    private static final JsonObjectFactory jsonObjectFactory = new JsonObjectFactory();

    public static JsonObject create(String string) {
        return create(JsonObject.class, string);
    }

    public static <T> T create(Class<T> clazz, String string) {

        JsonObject jsonObject = jsonObjectFactory.createJsonObject(string.replace('\'', '\"'));
        if (clazz == JsonObject.class) {
            return (T) jsonObject;
        } else {
            return jsonObject.as(clazz);
        }
    }

    public static String asJavaSourceCode(JsonValue jsonValue) {
        StringBuilder sourceCode = new StringBuilder();
        if (jsonValue.is(JsonObject.class)) {
            sourceCode.append("JsonObject.EMPTY");
            jsonValue.getValue(JsonObject.class).asMap().entrySet().stream().forEachOrdered(new Consumer<Map.Entry<String, JsonValue>>() {
                private final StringBuilder outerBuilder = sourceCode;

                @Override
                public void accept(Map.Entry<String, JsonValue> stringJsonValueEntry) {
                    outerBuilder
                            .append(".put(")
                            .append(escapeString(stringJsonValueEntry.getKey()))
                            .append(", ")
                            .append(asJavaSourceCode(stringJsonValueEntry.getValue()))
                            .append(")")
                    ;
                }
            });

        } else if (jsonValue.is(JsonArray.class)) {
            sourceCode.append("JsonArray.EMPTY");
            jsonValue.getValue(JsonArray.class).stream().forEachOrdered(new Consumer<JsonValue>() {
                private final StringBuilder outerBuilder = sourceCode;

                @Override
                public void accept(JsonValue jsonValue) {
                    outerBuilder
                            .append(".add(")
                            .append(asJavaSourceCode(jsonValue))
                            .append(")");
                }
            });


        } else if (jsonValue.is(String.class)) {
            sourceCode.append("\"").append(jsonValue.getValue()).append("\"");
        } else if (jsonValue.isNull()) {
            sourceCode.append("JsonValue.NULL");
        } else {
            sourceCode.append(jsonValue.getValue());

        }
        return sourceCode.toString();
    }

    private static String escapeString(String toBeEscaped) {
        String serialized = JsonSerializer.serialize(toBeEscaped);
        return "\"" + serialized + "\"";
    }
}
