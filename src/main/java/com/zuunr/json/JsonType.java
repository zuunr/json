package com.zuunr.json;

/**
 * @author Niklas Eldberger
 */
public enum JsonType {
    String, Object, Array, Boolean, Null, Number
}
