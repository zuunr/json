/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

import java.util.concurrent.TimeUnit;

/**
 * @author Niklas Eldberger
 */
public class JsonValueCache {

    private boolean enabled = false;

    Cache<Object, JsonValue> cache = Caffeine.newBuilder()
            .expireAfterAccess(1000000, TimeUnit.DAYS)
            //.recordStats()
            .softValues()
            .build();

    public JsonValue get(Object valueOfJsonValue) {

        JsonValue result;

        if (valueOfJsonValue == null) {
            result = JsonValue.NULL;
        } else if (enabled) {
            result = cache.getIfPresent(valueOfJsonValue);
        } else {
            result = null;
        }
        return result;
    }

    public void put(JsonValue jsonValue) {

        if (enabled && jsonValue.getValue() != null) {
            cache.put(jsonValue.getValue(), jsonValue);
        }
    }

    public void enabled(boolean enabled) {
        this.enabled = enabled;
    }
}
