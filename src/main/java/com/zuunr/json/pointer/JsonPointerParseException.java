package com.zuunr.json.pointer;

public class JsonPointerParseException extends RuntimeException {

    public JsonPointerParseException(String jsonPointer) {
        super(jsonPointer);
    }
}
