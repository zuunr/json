package com.zuunr.json.pointer;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArrayBuilder;
import com.zuunr.json.JsonValue;
import com.zuunr.json.UnsupportedTypeException;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * @author Niklas Eldberger
 */
public class JsonPointer {

    private static final JsonArray ARRAY_WITH_ONE_EMPTY_STRING = JsonArray.of("");

    private final JsonArray asArray;
    private JsonValue jsonPointerString;
    private JsonValue uriFragmentJsonPointerString;

    public static JsonPointer of(String jsonPointerString) {
        return JsonValue.of(jsonPointerString).as(JsonPointer.class);
    }

    public static JsonPointer of(JsonArray jsonArray) {
        JsonArrayBuilder builder = JsonArray.EMPTY.builder();
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonValue elem = jsonArray.get(i);
            if (elem.isString()) {
                builder.add(elem);
            } else if (elem.isJsonNumber() && elem.getJsonNumber().isJsonInteger()) {
                builder.add(elem.getJsonNumber().toIntString());
            } else {
                throw new RuntimeException("Cannot convert array to JSON pointer. Element must be either string or integer");
            }
        }
        return builder.build().as(JsonPointer.class);
    }

    private JsonPointer(JsonValue jsonValue) {

        if (jsonValue.isString()) {
            String s = jsonValue.getString();
            asArray = parseJsonPointer(s, true);
            if (s.length() > 0 && s.charAt(0) == '#') {
                uriFragmentJsonPointerString = jsonValue;
            } else {
                jsonPointerString = jsonValue;
            }
        } else if (jsonValue.isJsonArray()) {
            JsonArrayBuilder builder = JsonArray.EMPTY.builder();
            asArray = jsonValue.getJsonArray();
            for (JsonValue value : jsonValue.getJsonArray()) {
                if (value.isString()) {
                    builder.add(value);
                } else if (value.isJsonNumber() && value.getJsonNumber().isJsonInteger()) {
                    builder.add(value.getJsonNumber().toIntString());
                } else {
                    throw new RuntimeException("Element must be of string or integer type");
                }
            }
        } else {
            throw new UnsupportedTypeException("Only string and array is supported");
        }
    }

    public JsonPointer(JsonArray path) {
        asArray = path;
    }

    public JsonArray asArray() {
        return asArray;
    }

    public JsonValue getJsonPointerString() {
        if (jsonPointerString == null) {
            jsonPointerString = JsonValue.of(jsonPointerStringOf(asArray(), false));
        }
        return jsonPointerString;
    }

    public JsonValue getUriFragmentJsonPointerString() {
        if (uriFragmentJsonPointerString == null) {
            uriFragmentJsonPointerString = JsonValue.of(jsonPointerStringOf(asArray(), true));
        }
        return uriFragmentJsonPointerString;
    }

    static String jsonPointerStringOf(JsonArray path) {
        return jsonPointerStringOf(path, false);
    }

    static String uriFragmentJsonPointerStringOf(JsonArray path) {
        return jsonPointerStringOf(path, true);
    }

    private static String jsonPointerStringOf(JsonArray path, boolean fragmentStyle) {
        StringBuilder builder = new StringBuilder(fragmentStyle ? "#" : "");
        for (int i = 0; i < path.size(); i++) {
            JsonValue element = path.get(i);
            if (element.isString()) {
                String escaped = element.getString().replace("~", "~0").replace("/", "~1");
                escaped = fragmentStyle
                        ? URLEncoder.encode(escaped, StandardCharsets.UTF_8).replace("%7E", "~").replace("+", "%20")
                        : escaped;
                builder.append("/").append(escaped);
            } else if (element.isJsonNumber() && element.getJsonNumber().isJsonInteger()) {
                builder.append("/").append(element.getJsonNumber().intValue());
            } else {
                throw new UnsupportedTypeException("Element must be of either string or integer type");
            }
        }
        return builder.toString();
    }

    static JsonArray parseJsonPointer(String jsonPointer) {
        return parseJsonPointer(jsonPointer, true);
    }


    static JsonArray parseJsonPointer(String jsonPointer, boolean indexesAsStrings) {

        JsonArrayBuilder jsonArrayBuilder = JsonArray.EMPTY.builder();

        if ("".equals(jsonPointer) || "#".equals(jsonPointer)) {
            return JsonArray.EMPTY;
        }

        boolean isFragmentStyle = '#' == jsonPointer.charAt(0);
        int pointerStart = isFragmentStyle ? 1 : 0;

        if (jsonPointer.charAt(pointerStart) != '/') {
            throw new JsonPointerParseException(jsonPointer);
        }

        boolean isBeforeFirstDelimiter = true;
        StringBuilder elementBuilder = null;
        int pointerLength = jsonPointer.length();
        for (int i = 0; i < pointerLength; i++) {
            char currentChar = jsonPointer.charAt(i);
            if ('/' == currentChar) {
                if (!isBeforeFirstDelimiter) {
                    jsonArrayBuilder.add(elementBuilder == null ? JsonValue.EMPTY_STRING : createElement(isFragmentStyle, elementBuilder.toString(), true));
                }
                elementBuilder = null;
                isBeforeFirstDelimiter = false;
            } else {
                if (elementBuilder == null) {
                    elementBuilder = new StringBuilder();
                }
                if (!isBeforeFirstDelimiter) {
                    elementBuilder.append(currentChar);
                }
            }
        }
        if (jsonPointer.charAt(pointerLength - 1) == '/') {
            jsonArrayBuilder.add(JsonValue.EMPTY_STRING);
        }

        if (isBeforeFirstDelimiter) {
            return JsonArray.EMPTY;
        }
        return elementBuilder == null
                ? jsonArrayBuilder.build()
                : jsonArrayBuilder.add(createElement(isFragmentStyle, elementBuilder.toString(), indexesAsStrings)).build();
    }

    private static JsonValue createElement(boolean fragmentStyle, String string, boolean indexesAsString) {
        JsonValue toBeAppended = createIntegerOrString(fragmentStyle, string, indexesAsString);
        if (fragmentStyle && toBeAppended.isString()) {
            return JsonValue.of(URLDecoder.decode(toBeAppended.getString().replace("~1", "/").replace("~0", "~"), StandardCharsets.UTF_8));
        } else {
            return toBeAppended;
        }
    }

    private static JsonValue createIntegerOrString(boolean fragmentStyle, String pathElement) {
        return createIntegerOrString(fragmentStyle, pathElement, true);
    }

    private static JsonValue createIntegerOrString(boolean fragmentStyle, String pathElement, boolean onlyStringElements) {

        if (pathElement.isEmpty()) {
            return JsonValue.EMPTY_STRING;
        }

        StringBuilder stringBuilder = new StringBuilder();
        boolean containsNonDigits = onlyStringElements;

        for (int i = 0; i < pathElement.length(); ) {
            int nexti = i + 1;
            char currentChar = pathElement.charAt(i);
            if (currentChar < '0' || currentChar > '9') {
                containsNonDigits = true;
            }
            if (!fragmentStyle) {
                char nextChar = pathElement.length() > nexti ? pathElement.charAt(nexti) : 'X';
                if (currentChar == '~') {
                    if (nextChar == '1') {
                        stringBuilder.append('/');
                        i = nexti + 1;
                        continue;
                    } else if (nextChar == '0') {
                        stringBuilder.append('~');
                        i = nexti + 1;
                        continue;
                    } else {
                        stringBuilder.append(currentChar);
                    }
                } else {
                    stringBuilder.append(currentChar);
                }
            } else {
                stringBuilder.append(currentChar);
            }
            i = nexti;
        }
        return containsNonDigits ? JsonValue.of(stringBuilder.toString()) : JsonValue.of(Integer.valueOf(pathElement));
    }

    @Override
    public String toString() {
        return "JSON Pointer: " + getJsonPointerString().getString();
    }

    @Override
    public boolean equals(Object other) {
        return (other == this || other.getClass() == this.getClass() && this.asArray.equals(((JsonPointer) other).asArray));
    }
}
