/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

public abstract class TypedObject<T extends JsonObjectSupport> implements JsonObjectSupport {

    private static final JsonObjectPatcher patcher = new JsonObjectPatcher();
    protected final JsonObject jsonObject;

    protected TypedObject(JsonObject jsonObject){
        this.jsonObject = jsonObject;
    }

    protected TypedObject(JsonValue jsonValue) {
        this.jsonObject = jsonValue.getValue(JsonObject.class);
    }

    protected abstract T createNew(JsonObject jsonObject);

    public final T updateWith(T other) {


        JsonObject otherAsJsonObject = other.asJsonObject();

        JsonObject patched = patcher.patch(this.jsonObject, otherAsJsonObject, JsonObjectPatcher.Strategy.REPLACE_ARRAY);
        return createNew(patched);
    }

    @Override
    public final JsonObject asJsonObject() {
        return jsonObject;
    }

    @Override
    public final JsonValue asJsonValue() {
        return jsonObject.jsonValue();
    }

    @Override
    public final boolean equals(Object obj) {
        return
                obj != null &&
                getClass().equals(obj.getClass()) &&
                ((T)obj).asJsonObject().equals(jsonObject);
    }

    @Override
    public String toString() {
        return jsonObject.asJsonSorted();
    }

}
