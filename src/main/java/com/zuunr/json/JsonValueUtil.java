/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import com.zuunr.json.schema.validation.node.string.Pattern;

import java.util.Arrays;
import java.util.Iterator;

/**
 * @author Niklas Eldberger
 */
public class JsonValueUtil {

    /**
     * <p>The value must be encoded as in dotSeparatedString()</p>
     *
     * @param value
     * @return
     */
    public JsonArray createJsonArrayFromDotSeparated(String value) {
        String regex = "[.]";
        JsonValue result = createJsonArrayPathFromStringPath(value, regex).stream().sequential()
                .map(jsonValueStringOrInteger -> jsonValueStringOrInteger.isInteger() ? jsonValueStringOrInteger : JsonValue.of(jsonValueStringOrInteger.getValue(String.class).replace("\\\\\\\\", "\\\\")))
                .map(jsonValue -> jsonValue.is(String.class) ? JsonValue.of(decodeUnicoded(jsonValue.getValue(String.class).toCharArray()).toString()) : jsonValue)
                .reduce((jsonValueArrayOrString, jsonValueString) ->
                        jsonValueArrayOrString.is(JsonArray.class) ?
                                jsonValueArrayOrString.getValue(JsonArray.class).add(jsonValueString).jsonValue() :
                                JsonArray.EMPTY.add(jsonValueArrayOrString).add(jsonValueString).jsonValue())
                .orElse(JsonArray.EMPTY.jsonValue());
        return result.is(JsonArray.class) ? result.getValue(JsonArray.class) : JsonArray.EMPTY.add(result);
    }

    public JsonArray createJsonArrayPathFromStringPath(String value, String regex) {
        JsonArray result = JsonArray.EMPTY;
        String[] keys = null;
        if (value != null) {
            keys = value.split(regex);

            for (int i = 0; i < keys.length; i++) {
                if (keys[i].matches("[0-9]+")) {
                    result = result.add(Integer.valueOf(keys[i]));
                } else {
                    result = result.add(keys[i]);
                }
            }
        }
        return result;

    }

    /*
     * ["a",7,"b"] - "a.7.b"
     * ["a","b.c","d"] - "a.b..c.b"
     * ["a",".","b"] - "a....b"
     * ["a.","b"] - "a...
     *
     * @param jsonArrayOfOnlyStringOrIntegers
     * @return
     */
    public String dotSeparatedString(JsonArray jsonArrayOfOnlyStringOrIntegers) {
        StringBuilder stringBuilder = new StringBuilder();
        String separator = "";
        for (Iterator<JsonValue> iter = jsonArrayOfOnlyStringOrIntegers.iterator(); iter.hasNext(); ) {
            JsonValue jsonValue = iter.next();
            if (jsonValue.is(String.class)) {
                String value = jsonValue.getValue(String.class);
                stringBuilder.append(separator).append(escapeDotsWithUnicode(value));
            } else if (jsonValue.isInteger()) {
                stringBuilder.append(separator).append(jsonValue.toString());
            } else {
                throw new RuntimeException("jsonArrayOfOnlyStringOrIntegers must only contain Strings or Integers");
            }
            separator = ".";
        }
        return stringBuilder.toString();
    }

    /*
     * Escapes all backslashes with double backslashes and then adds dots '.'
     *
     * @param toBeAppended
     * @return
     */
    protected String escapeDotsWithUnicode(String toBeAppended) {
        // 1. Escape backslashes
        String allUnicodesEscaped = toBeAppended.replace("\\\\", "\\\\");
        // 2. Escape dots ('.')
        return Arrays.stream(allUnicodesEscaped.split(""))
                .sequential()
                .map(toBeEscaped -> ".".equals(toBeEscaped) ? "\\u" + Integer.toHexString(toBeEscaped.charAt(0) | 0x10000).substring(1) : toBeEscaped)
                .reduce((a, b) -> a + b).get();
    }

    public String decodeUnicoded(String unicoded) {
        return decodeUnicoded(unicoded.toCharArray()).toString();
    }

    protected StringBuilder decodeUnicoded(char[] unicoded) {

        int numberOfPreceedingBackslashes = 0;
        StringBuilder result = new StringBuilder();
        for (int index = 0; index < unicoded.length; index++) {

            if (numberOfPreceedingBackslashes % 2 == 0 &&
                    index + 5 < unicoded.length &&
                    unicoded[0 + index] == '\\' &&
                    unicoded[1 + index] == 'u' &&
                    (unicoded[2 + index] >= '0' && unicoded[2 + index] <= 'f') &&
                    (unicoded[3 + index] >= '0' && unicoded[3 + index] <= 'f') &&
                    (unicoded[4 + index] >= '0' && unicoded[4 + index] <= 'f') &&
                    (unicoded[5 + index] >= '0' && unicoded[5 + index] <= 'f')) {
                char decodedChar = (char) Integer.parseInt(new String(Arrays.copyOfRange(unicoded, index + 2, index + 6)), 16);
                result = result.append(decodedChar);
                index = index + 5; // +1 will be added by the for loop
            } else {
                result = result.append(unicoded[index]);
            }
            if (unicoded[index] == '\\') {
                numberOfPreceedingBackslashes++;
            } else {
                numberOfPreceedingBackslashes = 0;
            }

        }
        return result;
    }

    public JsonArray pathsStartingWith(JsonValue jsonValue, JsonArray path) {
        return pathsStartingWith(jsonValue, path, JsonArray.EMPTY);
    }

    private JsonArray pathsStartingWith(JsonValue jsonValue, JsonArray path, JsonArray pathSoFar) {

        JsonArray result = JsonArray.EMPTY;

        if (jsonValue == null) {
            result = null;
        } else if (path.isEmpty()) {
            result = result.add(pathSoFar);
        } else if (path.head().is(String.class) && jsonValue.is(JsonObject.class)) {
            String headValue = path.head().getValue(String.class);
            JsonArray paths = pathsStartingWith(jsonValue.get(headValue), path.tail(), pathSoFar.add(headValue));
            if (paths != null) {
                result = result.addAll(paths);
            }
        } else if (path.head().isInteger() && jsonValue.is(JsonArray.class)) {
            Integer headValue = path.head().asInteger();
            if (headValue == -1) {
                JsonArray array = jsonValue.getValue(JsonArray.class);
                for (int i = 0; i < array.size(); i++) {
                    JsonArray paths = pathsStartingWith(array.get(i), path.tail(), pathSoFar.add(i));
                    if (paths != null) {
                        result = result.addAll(paths);
                    }
                }
            } else {
                JsonArray paths = pathsStartingWith(jsonValue.get(headValue), path.tail(), pathSoFar.add(headValue));
                if (paths != null) {
                    result = result.addAll(paths);
                }
            }
        }
        return result;
    }

    public JsonArray pathsEndingWith(JsonValue jsonValue, JsonArray endingPath, boolean isRegex) {
        JsonArray paths = JsonArray.EMPTY;
        if (jsonValue.is(JsonObject.class)) {
            paths = pathsEndingWith(jsonValue.getValue(JsonObject.class), endingPath, isRegex);
        } else if (jsonValue.is(JsonArray.class)) {
            paths = pathsEndingWith(jsonValue.getValue(JsonArray.class), endingPath, isRegex);
        }
        return paths;
    }

    private JsonArray pathsEndingWith(JsonArray pairsOfJsonObject, JsonArray ending, boolean isRegex) {

        JsonArray paths = JsonArray.EMPTY;

        if (!ending.isEmpty()) {
            if (!pairsOfJsonObject.isEmpty()) {

                for (int i = 0; i < pairsOfJsonObject.size(); i++) {
                    JsonValue pair = pairsOfJsonObject.get(i);
                    if (!pair.is(JsonObject.class) && !pair.is(JsonArray.class)) {
                        if (ending.last().isInteger()) {
                            if (!isRegex &&
                                    ending.last().asInteger().equals(i) ||
                                    isRegex && (-1 == ending.last().asInteger() ||
                                            i == (ending.last().asInteger()))) {
                                paths = paths.add(JsonArray.EMPTY.add(i).jsonValue());
                            }
                        } else {
                            // paths = paths; :-) Do nothing!
                        }

                    } else {

                        JsonArray deeperPaths = pathsEndingWith(pair, ending, isRegex);

                        for (JsonValue deeperPathsJsonValue : deeperPaths.asList()) {
                            if (deeperPathsJsonValue.getValue(JsonArray.class).size() >= ending.size()) {
                                paths = paths.add(deeperPathsJsonValue.getValue(JsonArray.class).addFirst(i));
                            }
                        }

                        if (ending.head().isInteger()) {
                            if (isRegex && (-1 == ending.head().asInteger() || i == (ending.head().asInteger())) ||
                                    !isRegex && i == ending.head().asInteger()) {
                                JsonArray tailingPaths = pathsEndingWith(pair, ending.tail(), isRegex);
                                if (!tailingPaths.isEmpty()) {
                                    for (JsonValue tailingPath : tailingPaths.asList()) {
                                        if (tailingPath.getValue(JsonArray.class).size() == ending.tail().size()) {
                                            paths = paths.add(tailingPath.getValue(JsonArray.class).addFirst(JsonValue.of(i)).jsonValue());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return paths;
    }

    private JsonArray pathsEndingWith(JsonObject jsonObject, JsonArray ending, boolean isRegex) {

        JsonArray paths = JsonArray.EMPTY;

        if (!ending.isEmpty()) {
            JsonObject.Pairs pairsOfJsonObject = jsonObject.getPairs();
            if (!pairsOfJsonObject.isEmpty()) {

                JsonObject.Pairs pairs = pairsOfJsonObject;

                for (; !pairs.isEmpty(); pairs = pairs.tail()) {
                    JsonObject.Pair pair = pairs.head();
                    if (!pair.getValue().is(JsonObject.class) && !pair.getValue().is(JsonArray.class)) {
                        if (ending.last().is(String.class)) {
                            if (!isRegex &&
                                    ending.last().getValue(String.class).equals(pair.getKey()) ||
                                    isRegex &&
                                            pair.getKey().matches(ending.last().getValue(String.class))) {
                                paths = paths.add(JsonArray.EMPTY.add(pair.getKey()).jsonValue());
                            }
                        } else {
                            // paths = paths; // :-) Do nothing!
                        }
                    } else {

                        JsonArray deeperPaths = pathsEndingWith(pair.getValue(), ending, isRegex);

                        for (JsonValue deeperPathsJsonValue : deeperPaths.asList()) {
                            if (deeperPathsJsonValue.getValue(JsonArray.class).size() >= ending.size()) {
                                paths = paths.add(deeperPathsJsonValue.getValue(JsonArray.class).addFirst(JsonValue.of(pair.getKey())));
                            }
                        }
                        if (ending.head().is(String.class)) {
                            if (isRegex && pair.getKey().matches(ending.head().getValue(String.class)) || !isRegex && pair.getKey().equals(ending.head().getValue(String.class))) {
                                JsonArray tailingPaths = pathsEndingWith(pair.getValue(), ending.tail(), isRegex);
                                if (!tailingPaths.isEmpty()) {
                                    for (JsonValue tailingPath : tailingPaths.asList()) {
                                        if (tailingPath.getValue(JsonArray.class).size() == ending.tail().size()) {
                                            paths = paths.add(tailingPath.getValue(JsonArray.class).addFirst(JsonValue.of(pair.getKey())).jsonValue());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return paths;
    }

    public JsonValue putRegexPath(JsonValue target, JsonArray regexPath, JsonValue value) {
        JsonValue result = null;
        JsonValue head = regexPath.head();
        JsonArray tail = regexPath.tail();

        if (head.is(String.class)) {
            String regex = head.getValue(String.class);
            if (target.is(JsonObject.class)) {
                JsonObject jsonObject = target.getValue(JsonObject.class);
                for (JsonValue jsonValueKey : jsonObject.keys().asList()) {
                    String key = jsonValueKey.getValue(String.class);
                    if (key.matches(regex)) {
                        if (tail.isEmpty()) {
                            jsonObject = jsonObject.put(key, value);
                        } else {
                            jsonObject = jsonObject.put(key, putRegexPath(jsonObject.get(key), tail, value));
                        }
                    }
                }
                result = jsonObject.jsonValue();
            }
        } else {
            throw new RuntimeException("Only JsonObject support for now. Extend implementation!");
        }
        return result;

    }

    public JsonValue removeRegexPath(JsonValue target, JsonArray regexPath) {
        JsonValue result = null;
        JsonValue head = regexPath.head();
        JsonArray tail = regexPath.tail();

        if (head.is(String.class)) {
            String regex = head.getValue(String.class);
            if (target.is(JsonObject.class)) {
                JsonObject jsonObject = target.getValue(JsonObject.class);
                for (JsonValue jsonValueKey : jsonObject.keys().asList()) {
                    String key = jsonValueKey.getValue(String.class);
                    if (key.matches(regex)) {
                        if (tail.isEmpty()) {
                            jsonObject = jsonObject.remove(key);
                        } else {
                            jsonObject = jsonObject.put(key, removeRegexPath(jsonObject.get(key), tail));
                        }
                    }
                }
                result = jsonObject.jsonValue();
            }
        } else if (head.isInteger()) {
            Integer headInteger = head.asInteger();
            if (target.is(JsonArray.class)) {
                if (headInteger == -1) {

                    if (target.getValue(JsonArray.class).isEmpty()) {
                        result = JsonArray.EMPTY.jsonValue();
                    } else {
                        JsonArray newTail = removeRegexPath(target.getValue(JsonArray.class).tail().jsonValue(), regexPath).getValue(JsonArray.class);
                        JsonValue newHead = removeRegexPath(target.getValue(JsonArray.class).head(), tail);
                        result = newTail.addFirst(newHead).jsonValue();
                    }
                } else {
                    result = target.getValue(JsonArray.class).put(headInteger, removeRegexPath(target.getValue(JsonArray.class).get(headInteger), tail)).jsonValue();
                }
            }

        } else {
            throw new RuntimeException("Only JsonObject support for now. Extend implementation!");
        }
        return result;

    }

    public JsonArray getPathsByRegex(JsonValue target, JsonArray regexPath, boolean includeValueLast) {
        return getPathsByRegex(target, regexPath, 0, includeValueLast);
    }

    public JsonArray getPathsByRegex(JsonValue target, JsonArray regexPath, int regexPathIndex, boolean includeValueLast) {

        if (regexPathIndex == regexPath.size()) {
            if (includeValueLast) {
                return JsonArray.of(JsonArray.of(target));
            } else {
                return JsonArray.of(JsonArray.EMPTY);
            }
        }

        Pattern pattern = regexPath.get(regexPathIndex).as(Pattern.class);

        JsonArrayBuilder builder = JsonArray.EMPTY.builder();
        for (int i = 0; i < target.getJsonObject().size(); i++) {

            JsonValue keyJsonValue = target.getJsonObject().keys().get(i);
            String key = keyJsonValue.getString();
            if (pattern.compiled().matcher(key).matches()){
                JsonArray subPaths = getPathsByRegex(target.getJsonObject().values().get(i), regexPath, regexPathIndex+1, includeValueLast);
                if (subPaths != null) {
                    builder.addAll(subPaths.addFirstToEach(keyJsonValue));
                }
            }
        }
        return builder.build();
    }

    public JsonArray getPathsByRegex(JsonValue target, JsonArray regexPath) {
        return getPathsByRegex(target, regexPath, JsonValue.NULL);
    }

    // TODO: This is ineffective - make it better!!! getPaths is inefficient and that method iterates one more time...
    public JsonArray getPathsByRegex(JsonValue target, JsonArray regexPath, JsonValue paddingIfPathIsShorter) {
        JsonObject filteredPaths = JsonObject.EMPTY;
        //JsonArray filteredPath = JsonArray.EMPTY;
        JsonArray allPaths = target.getPaths();
        for (JsonValue jsonValue : allPaths.asList()) {
            boolean pathMatches = true;
            JsonArray onePath = jsonValue.getValue(JsonArray.class);
            for (int regExIndex = 0; regExIndex < regexPath.size(); regExIndex++) {
                JsonValue regExOfIndexJsnVal = regexPath.get(regExIndex);
                if (regExOfIndexJsnVal.is(String.class)) {
                    String regexOfIndex = regexPath.get(regExIndex).getValue(String.class);
                    String onePathOfIndex = null;
                    if (regExIndex < onePath.size()) {
                        if (onePath.get(regExIndex).is(String.class)) {
                            onePathOfIndex = onePath.get(regExIndex).getValue(String.class);
                            if (!onePathOfIndex.matches(regexOfIndex)) {
                                pathMatches = false;

                            }
                        }
                    } else {
                        pathMatches = false;
                    }
                } else if (regExOfIndexJsnVal.isInteger()) {
                    Integer regexOfIndex = regexPath.get(regExIndex).asInteger();
                    Integer onePathOfIndex = null;
                    if (regExIndex < onePath.size()) {
                        if (onePath.get(regExIndex).isInteger()) {
                            onePathOfIndex = onePath.get(regExIndex).asInteger();
                            if (!onePathOfIndex.equals(regexOfIndex) && regexOfIndex != -1) {
                                pathMatches = false;
                            }
                        }
                    } else {
                        pathMatches = false;
                    }

                } else {

                }
            }
            if (pathMatches) {
                JsonArray toBeInserted = onePath.subArray(0, regexPath.size());
                String key = dotSeparatedString(toBeInserted);
                filteredPaths = filteredPaths.put(key, toBeInserted);
            }
        }
        return filteredPaths.values();
    }

    public JsonValue replace(JsonValue toBeTranslated, JsonArray substitutions) {

        JsonValue result = toBeTranslated;

        if (!substitutions.isEmpty()) {
            JsonValue substitutionHead = substitutions.head();
            JsonValue toBeReplaced = substitutionHead.get("toBeReplaced");
            JsonValue replacement = substitutionHead.get("replacement");

            if (toBeTranslated.is(JsonObject.class)) {
                JsonObject resultJsonObject = toBeTranslated.getValue(JsonObject.class);
                if (!resultJsonObject.isEmpty()) {
                    String headKey = resultJsonObject.keys().head().getValue(String.class);
                    JsonValue headKeyValue = resultJsonObject.get(headKey);

                    if (toBeReplaced.equals(headKeyValue)) {
                        resultJsonObject = resultJsonObject.put(headKey, replacement);
                    }
                    resultJsonObject = replace(resultJsonObject.remove(headKey).jsonValue(), substitutions)
                            .getValue(JsonObject.class).put(headKey, replace(resultJsonObject.get(headKey), substitutions));
                }
                result = resultJsonObject.jsonValue();

            } else if (toBeTranslated.is(JsonArray.class)) {
                JsonArray resultJsonArray = toBeTranslated.getValue(JsonArray.class);
                if (!resultJsonArray.isEmpty()) {
                    if (toBeReplaced.equals(resultJsonArray.head())) {
                        resultJsonArray = resultJsonArray.add(0, replacement);
                    }
                    resultJsonArray = replace(resultJsonArray.tail().jsonValue(), substitutions).getValue(JsonArray.class).addFirst(resultJsonArray.head());
                }
                result = resultJsonArray.jsonValue();
            }
            result = replace(result, substitutions.tail());
        }

        return result;
    }

    public JsonValue maxOf(JsonValue jsonValue1, JsonValue jsonValue2) {
        return minOf(jsonValue1, jsonValue2) == jsonValue1 ? jsonValue2 : jsonValue1;
    }

    public JsonValue minOf(JsonValue jsonValue1, JsonValue jsonValue2) {
        JsonValue result = minOfInteger(jsonValue1, jsonValue2);
        if (result != null) {
            return result;
        }

        result = minOfString(jsonValue1, jsonValue2);
        return result;
    }

    private JsonValue minOfInteger(JsonValue min1, JsonValue min2) {
        try {
            long asLong1 = min1.getJsonNumber().longValue();
            long asLong2 = min2.getJsonNumber().longValue();

            if (asLong1 < asLong2) {
                return min1;
            } else {
                return min2;
            }
        } catch (ClassCastException e) {
            return null;
        }
    }

    private JsonValue minOfString(JsonValue min1, JsonValue min2) {
        try {
            String string1 = (String) min1.getValue();
            String string2 = (String) min2.getValue();

            if (string1.compareTo(string2) < 0) {
                return min1;
            } else {
                return min2;
            }
        } catch (ClassCastException e) {
            return null;
        }
    }
}
