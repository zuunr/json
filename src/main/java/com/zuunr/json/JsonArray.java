/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import clojure.lang.PersistentVector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

public final class JsonArray implements Comparable<Object>, Iterable<JsonValue> {

    public static final JsonArray EMPTY = new JsonArray(PersistentVector.EMPTY);
    public static final JsonArray EMPTY_IN_EMPTY = JsonArray.of(JsonArray.EMPTY);

    private static final JsonValueUtil jsonValueUtil = new JsonValueUtil();
    private static final JsonSerializer jsonSerializer = new JsonSerializer();

    private static final Logger logger = LoggerFactory.getLogger(JsonArray.class);

    final PersistentVector persistentVector;
    private final JsonValue jsonValue;

    private JsonArray sorted;

    public static com.zuunr.json.JsonArrayBuilder emptyBuilder() {
        return new JsonArrayBuilder(JsonArray.EMPTY);
    }

    public com.zuunr.json.JsonArrayBuilder builder() {
        return new com.zuunr.json.JsonArrayBuilder(this);
    }

    public <T> T as(Class<T> clazz) {
        return jsonValue.as(clazz);
    }

    @Override
    public boolean equals(Object comparedObject) {

        if (this == comparedObject) {
            return true;
        } else if (comparedObject == null || getClass() != comparedObject.getClass()) {
            return false;
        } else {
            JsonArray array = (JsonArray) comparedObject;
            return persistentVector.equals(array.persistentVector);
        }
    }

    @Override
    public int hashCode() {
        return persistentVector.hashCode();
    }

    public static JsonArray ofDotSeparated(String dotSeparated) {
        com.zuunr.json.JsonArrayBuilder result = JsonArray.EMPTY.builder();
        if (!dotSeparated.isEmpty()) {
            String[] array = dotSeparated.split("[.]");
            for (String s : array) {
                result = result.add(JsonValue.of(s));
            }
        }
        return result.build();
    }

    public static JsonArray ofList(List<JsonValue> jsonValues) {
        return of(PersistentVector.create(jsonValues));
    }

    // Default modifier because JsonArrayBuilder needs access
    static JsonArray of(PersistentVector persistentVector) {

        if (persistentVector == null) {
            throw new NullPointerException("persistentVector must not be null!");
        }

        JsonArray result = null;
        JsonValue fromCache = JsonValue.cache.get(persistentVector);

        if (fromCache != null) {
            result = fromCache.getValue(JsonArray.class);
        }
        if (result == null) {
            result = new JsonArray(persistentVector);
            JsonValue.cache.put(result.jsonValue);
        }
        return result;
    }

    // default modifier needed for JsonArrayBuilder
    JsonArray(PersistentVector persistentVector) {
        this.persistentVector = persistentVector;
        jsonValue = new JsonValue(this);
    }

    public JsonValue get(int index) {
        return (JsonValue) persistentVector.get(index);
    }

    public JsonValue get(int index, String defaultIfNull) {
        return get(index, JsonValue.of(defaultIfNull));
    }

    public JsonValue get(int index, JsonValue defaultIfNull) {

        JsonValue result = null;
        if (index < size()) {
            result = (JsonValue) persistentVector.get(index);
        } else {
            result = defaultIfNull;
        }

        if (result == null) {
            result = defaultIfNull;
        }
        return result;
    }

    public JsonValue get(JsonArray path, JsonValue defaultIfNull) {
        return jsonValue().get(path, defaultIfNull);
    }

    public JsonValue get(JsonArray path, JsonArray defaultIfNull) {
        return jsonValue().get(path, defaultIfNull.jsonValue());
    }

    public JsonValue get(int index, JsonObject defaultIfNull) {
        return jsonValue().get(index, defaultIfNull.jsonValue());
    }

    public JsonValue get(JsonArray path) {
        return jsonValue().get(path);
    }

    public int size() {
        return persistentVector.size();
    }

    private static void validateNull(JsonValue jsonValue) {
        if (jsonValue == null) {
            throw new NullPointerException("Not allowed to add null to JsonArray");
        }
    }

    public JsonArray add(JsonValue jsonValue) {
        validateNull(jsonValue);
        return add(persistentVector.size(), jsonValue);
    }

    public JsonArray add(int index, JsonValue jsonValue) {

        JsonArray result;
        validateNull(jsonValue);
        if (size() == index) {
            result = of(persistentVector.assocN(size(), jsonValue));
        } else {
            JsonArray start = subArray(0, index);
            com.zuunr.json.JsonArrayBuilder builder = start.builder().add(jsonValue);

            for (int i = index; i < size(); i++) {
                builder.add(get(i));
            }
            result = builder.build();
        }
        return result;
    }

    public JsonArray add(int index, JsonArray jsonArray) {
        JsonValue added = jsonArray == null ? JsonValue.NULL : jsonArray.jsonValue();
        return add(index, added);
    }

    public JsonArray addAll(JsonArray jsonArray) {

        if (persistentVector.isEmpty()) {
            return jsonArray;
        }

        JsonArrayBuilder builder = this.builder();
        for (int i = 0; i < jsonArray.size(); i++){
            builder.add(jsonArray.get(i));
        }
        return builder.build();
    }

    public JsonArray add(JsonObject jsonObject) {
        return add(jsonObject.jsonValue());
    }

    public JsonArray set(int index, JsonValue jsonValue) {
        validateNull(jsonValue);
        return of(persistentVector.assocN(index, jsonValue));
    }

    public JsonArray set(int index, JsonObject jsonObject) {
        JsonArray result = null;
        if (jsonObject == null) {
            result = set(index, JsonValue.NULL);
        } else {
            result = set(index, jsonObject.jsonValue());
        }
        return result;
    }

    public JsonArray set(int index, JsonArray jsonArray) {
        JsonArray result = null;
        if (jsonArray == null) {
            result = set(index, JsonValue.NULL);
        } else {
            result = set(index, jsonArray.jsonValue());
        }
        return result;
    }

    public JsonArray add(String stringValue) {
        return add(JsonValue.of(stringValue));
    }

    public JsonArray add(String... params) {
        JsonArray result = JsonArray.EMPTY;
        for (String param : params) {
            result = result.add(param);
        }
        return result;
    }

    public JsonArray add(Integer intValue) {
        return add(JsonValue.of(intValue));
    }

    public JsonArray add(int index, Integer intValue) {
        return add(index, JsonValue.of(intValue));
    }

    public JsonArray add(JsonArray jsonArray) {
        return add(jsonArray.jsonValue());
    }

    public JsonArray add(boolean b) {
        return add(JsonValue.of(b));
    }

    public JsonValue head() {
        JsonValue result = null;
        if (size() == 0) {
            result = null;

        } else {
            result = (JsonValue) persistentVector.get(0);
        }
        return result;
    }

    public JsonArray tail() {
        if (size() < 2) {
            return EMPTY;
        }
        return subArray(1, size());
    }

    @Override
    public String toString() {
        return jsonSerializer.serialize(persistentVector);
    }

    public JsonArray subArray(int start, int end) {
        return of(PersistentVector.create(persistentVector.subList(start, end)));
    }

    public JsonArray subArray(int start) {
        return subArray(start, persistentVector.size());
    }

    public boolean isEmpty() {
        return persistentVector.isEmpty();
    }

    public JsonValue jsonValue() {
        return jsonValue;
    }

    public JsonArray addFirst(JsonValue jsonValue) {
        JsonArray result = null;
        if (size() == 0) {
            result = add(jsonValue);
        } else {
            result = add(0, jsonValue);
        }
        return result;
    }

    public JsonArray addFirst(String string) {
        return addFirst(JsonValue.of(string));
    }

    public JsonArray addFirst(JsonArray jsonArray) {
        JsonArray result = null;
        if (size() == 0) {
            result = add(jsonArray);
        } else {
            result = add(0, jsonArray);
        }
        return result;
    }

    public JsonArray addFirst(int value) {
        return addFirst(JsonValue.of(value));
    }

    public JsonArray addFirst(JsonObject value) {
        return addFirst(JsonValue.of(value));
    }

    public JsonArray addToEachAt(int index, JsonValue jsonValue) {
        JsonArray result = null;
        if (isEmpty()) {
            result = EMPTY;
        } else {
            result = this.tail().addFirstToEach(jsonValue).add(index, head().getValue(JsonArray.class).add(index, jsonValue));
        }
        return result;
    }

    public JsonArray addToEach(JsonValue jsonValue) {
        JsonArray result = null;
        if (isEmpty()) {
            result = EMPTY;
        } else {
            result = this.tail().addFirstToEach(jsonValue).add(head().getValue(JsonArray.class).add(jsonValue));
        }
        return result;
    }

    public JsonArray addFirstToEach(JsonValue jsonValue) {

        JsonArrayBuilder builder = JsonArray.EMPTY.builder();

        for (JsonValue arrayjsonValue: this) {
           builder.add(arrayjsonValue.getJsonArray().addFirst(jsonValue));
        }
        return builder.build();
    }

    public JsonArray addFirstToEach(int value) {
        return addFirstToEach(JsonValue.of(value));
    }

    public List<Object> asMapsAndLists() {
        return asMapsAndLists(false);
    }

    public List<Object> asMapsAndLists(boolean sorted) {
        List<Object> result = new ArrayList<>();
        
        for (JsonValue entry : (Iterable<JsonValue>) persistentVector) {
            if (entry.is(JsonObject.class)) {
                result.add(entry.getValue(JsonObject.class).asMapsAndLists(sorted));
            } else if (entry.is(JsonArray.class)) {
                result.add(entry.getValue(JsonArray.class).asMapsAndLists(sorted));
            } else {
                result.add(entry.getValue());
            }
        }
        return result;
    }

    public Stream<JsonValue> stream() {
        return persistentVector.stream();
    }

    public JsonArray remove(int index) {
        ArrayList<JsonValue> newMutableList = new ArrayList<>(persistentVector);
        newMutableList.remove(index);
        return of(PersistentVector.create(newMutableList));
    }

    public JsonArray put(Integer index, JsonValue jsonValue) {

        JsonArray result = this;
        if (index >= result.size()) {
            while (index > result.size()) {
                result = result.add(JsonValue.NULL);
            }
            result = result.add(jsonValue);
        } else {
            result = result.remove(index).add(index, jsonValue);
        }
        return result;
    }

    public JsonArray put(JsonArray path, JsonObject jsonObject) {
        return put(path, jsonObject.jsonValue());
    }

    public JsonArray put(JsonArray path, JsonArray jsonArray) {
        return put(path, jsonArray.jsonValue());
    }

    public JsonArray put(JsonArray path, JsonValue jsonValue) {
        JsonArray result;

        if (path.head().isInteger()) {
            Integer head = path.head().asInteger();
            JsonArray tail = path.tail();
            if (tail.isEmpty()) {
                if (head == -1) {
                    result = add(jsonValue);
                } else {
                    result = put(head, jsonValue);
                }
            } else {
                if (head == -1) {
                    if (tail.head().isInteger()) {
                        result = add(JsonArray.EMPTY.put(tail, jsonValue));
                    } else if (tail.head().is(String.class)) {
                        result = add(JsonObject.EMPTY.put(tail, jsonValue));
                    } else {
                        String msg = "Path values must be either String or Integer";
                        logger.error(msg);
                        throw new UnsupportedTypeException(msg);
                    }

                } else {
                    JsonValue empty;
                    if (tail.head().isInteger()) {
                        empty = JsonArray.EMPTY.jsonValue();
                    } else {
                        empty = JsonObject.EMPTY.jsonValue();
                    }

                    result = put(head, get(head, empty).put(tail, jsonValue));
                }
            }
        } else {
            throw new UnsupportedTypeException("Only Integer is supported");
        }
        return result;
    }

    public List<JsonValue> asList() {
        return persistentVector;
    }

    public <T> List<T> asList(Class<T> clazz) {
        return jsonValue.asList(clazz);
    }

    public Iterator<JsonValue> iterator() {
        return asList().iterator();
    }

    public JsonArray allButLast() {
        JsonArray result = this;
        if (!isEmpty()) {
            result = subArray(0, size() - 1);
        }
        return result;
    }

    public JsonValue last() {
        JsonValue result = null;
        if (!isEmpty()) {
            result = get(size() - 1);
        }
        return result;
    }

    /**
     * <p>Returns new JsonArray with all old values equal to old replaced with newJsonValue.</p>
     * 
     * @param old
     * @param newInteger
     * @return
     */
    public JsonArray replace(String old, Integer newInteger) {
        return replace(JsonValue.of(old), JsonValue.of(newInteger));
    }

    public JsonArray replace(JsonValue old, JsonValue newJsonValue) {
        JsonArray jsonArray = JsonArray.EMPTY;
        for (JsonValue currentValue : this) {
            if (old.equals(currentValue)) {
                jsonArray = jsonArray.add(newJsonValue);
            } else {
                jsonArray = jsonArray.add(currentValue);
            }
        }
        return jsonArray;
    }

    public JsonArray replaceIntegers(JsonValue newJsonValue) {
        JsonArray jsonArray = JsonArray.EMPTY;
        for (JsonValue currentValue : this) {
            if (currentValue.isInteger()) {
                jsonArray = jsonArray.add(newJsonValue);
            } else {
                jsonArray = jsonArray.add(currentValue);
            }
        }
        return jsonArray;
    }

    /**
     * <p>Filters this {@link JsonArray}, removing entries that does not match the provided
     * regular expression pattern for the given parameter.</p>
     * 
     * @param pathToParamInItem is the path to the parameter to match against the regular expression pattern
     * @param regexPattern is the pattern to match to the provided parameter path
     * @return a {@link JsonArray} with matching entries
     */
    public JsonArray filter(JsonArray pathToParamInItem, String regexPattern) {
        JsonArray result = JsonArray.EMPTY;
        for (int i = 0; i < this.size(); i++) {
            JsonValue item = get(i);
            if (item.is(JsonObject.class)) {
                JsonValue value = item.getValue(JsonObject.class).jsonValue().get(pathToParamInItem, null);
                if (value != null && value.is(String.class)) {
                    if (value.getValue(String.class).matches(regexPattern)) {
                        result = result.add(item);
                    }
                } else if (value != null && value.isJsonNumber()) {
                    if (value.getJsonNumber().toString().matches(regexPattern)) {
                        result = result.add(item);
                    }
                } else {
                    String msg = "filter only implemented for String, Integer and Long so far...";
                    logger.error(msg);
                    throw new UnsupportedTypeException(msg);
                }
            } else {
                String msg = "filter only implemented for items of type JsonObject so far...";
                logger.error(msg);
                throw new UnsupportedTypeException(msg);
            }
        }
        return result;
    }

    public boolean contains(String value) {
        return contains(JsonValue.of(value));
    }

    public boolean contains(JsonValue jsonValue) {
        return persistentVector.contains(jsonValue);
    }

    public String asDotSeparatedString() {
        return jsonValueUtil.dotSeparatedString(this);
    }

    public JsonArray sort() {
        if (sorted == null) {
            List<JsonValue> list = new ArrayList<>(asList());
            Collections.sort(list, (o1, o2) -> o1.compareTo(o2));
            sorted = of(PersistentVector.create(list));
        }
        return  sorted;
    }

    public JsonArray sort(Comparator<JsonValue> comparator) {
        List<JsonValue> list = new ArrayList<>(asList());
        Collections.sort(list, comparator);
        return of(PersistentVector.create(list));
    }

    public String asJsonSorted() {
        StringBuilder result = new StringBuilder("[");
        String separator = "";
        for (JsonValue jsonValueElem : asList()) {
            result.append(separator);
            if (jsonValueElem.is(JsonObject.class)) {
                result.append(jsonValueElem.getValue(JsonObject.class).asJsonSorted());
            } else if (jsonValueElem.is(JsonArray.class)) {
                result.append(jsonValueElem.getValue(JsonArray.class).asJsonSorted());
            } else {
                result.append(jsonValueElem.toString());
            }
            separator = ",";
        }
        result.append("]");
        return result.toString();
    }

    public String asJson() {
        return toString();
    }

    /*
     * TODO: This one may better sort by the first element instead of length. Length seems less intuitive!
     *
     * @param o
     * @return
     */
    @Override
    public int compareTo(Object o) {
        Integer result = null;
        if (o.getClass().equals(getClass())) {
            JsonArray other = (JsonArray) o;
            if (size() == other.size()) {
                result = 0; //  Will be overwritten if both are not empty
                for (int i = 0; i < size(); i++) {
                    result = get(i).compareTo(((JsonArray) o).get(i));
                }
            } else {
                result = size() > other.size() ? 1 : -1;
            }
        } else {
            result = jsonValue().compareTo((JsonValue) o);
        }
        return result;
    }

    public int getKeyValueIndex(String key, JsonValue value) {
        int index = -1;
        for (int i = 0; i < this.size(); i++) {
            if (value.equals(this.get(i).get(key))) {
                index = i;
                break;
            }
        }
        return index;
    }

    public static JsonArray of(Object... elements) {
        PersistentVector pv = PersistentVector.EMPTY;
        for (Object element : elements) {
            if (element == null) {
                throw new UnsupportedTypeException("JsonArray cannot contain null value - use JsonValue.NULL instead!");
            }

            JsonValue jsonValue;
            if (element.getClass() == Integer.class) {
                jsonValue = JsonValue.of((Integer) element);
            } else if (element.getClass() == JsonObject.class) {
                jsonValue = ((JsonObject) element).jsonValue();
            } else if (element.getClass() == JsonArray.class) {
                jsonValue = ((JsonArray) element).jsonValue();
            } else {
                jsonValue = JsonValue.of(element);
            }
            pv = pv.assocN(pv.size(), jsonValue);
        }
        return of(pv);
    }

    public JsonArray reverse() {
        List<JsonValue> newList = new ArrayList<>(persistentVector);
        Collections.reverse(newList);
        return of(PersistentVector.create(newList));
    }

    public int getIndexOfFirstMatch(Predicate<JsonValue> matcher) {
        int index = -1;
        for (int i = 0; i < size(); i++) {
            if (matcher.test(get(i))) {
                index = i;
                break;
            }
        }
        return index;
    }

    @Override
    public void forEach(Consumer<? super JsonValue> action) {
        persistentVector.forEach(action);
    }

    @Override
    public Spliterator<JsonValue> spliterator() {
        return persistentVector.spliterator();
    }

    public JsonValue[] asArray() {

        JsonValue[] array = new JsonValue[persistentVector.size()];
        int i = 0;
        for (Iterator<JsonValue> iter = persistentVector.iterator(); iter.hasNext(); i++) {
            JsonValue elem = iter.next();
            array[i] = elem;
        }
        return array;
    }

    public JsonObject asJsonObject(JsonArray keyPath) {
        JsonObjectBuilder builder = JsonObject.EMPTY.builder();

        for (JsonValue jsonValueElem : this.asList()) {
            JsonValue keyJsonValue = jsonValueElem.get(keyPath);

            String key;
            if (keyJsonValue == null) {
                key = null;
            } else if (keyJsonValue.is(String.class)) {
                key = jsonValueElem.get(keyPath, JsonValue.NULL).as(String.class);
            } else if (keyJsonValue.isBoolean() || keyJsonValue.isInteger()) {
                key = keyJsonValue.getValue().toString();
            } else {
                throw new UnsupportedTypeException("Only String, Integer and Boolean are possible as keys");
            }
            if (key != null) {
                builder.put(key, jsonValueElem);
            }
        }
        return builder.build();
    }

    public JsonArray asSet() {
        HashSet<JsonValue> hashSet = new HashSet<JsonValue>();
        for (Iterator<JsonValue> jsonValueIterator = this.iterator(); jsonValueIterator.hasNext();){
            hashSet.add(jsonValueIterator.next());
        }
        return JsonArray.of(hashSet.toArray());
    }

}
