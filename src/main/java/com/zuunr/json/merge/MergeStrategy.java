/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json.merge;

/**
 * @author Niklas Eldberger
 */
public class MergeStrategy {

    public static final MergeStrategy NULL_AS_VALUE_AND_ARRAY_AS_ATOM = new MergeStrategy(NullMergeStrategy.NULL_AS_VALUE, ArrayMergeStrategy.ARRAY);
    public static final MergeStrategy NULL_AS_DELETE_AND_ARRAY_AS_ATOM = new MergeStrategy(NullMergeStrategy.NULL_AS_DELETE, ArrayMergeStrategy.ARRAY);
    public static final MergeStrategy NULL_AS_VALUE_AND_ARRAY_BY_ELEMENT = new MergeStrategy(NullMergeStrategy.NULL_AS_VALUE, ArrayMergeStrategy.ELEMENT);

    public final NullMergeStrategy nullMergeStrategy;
    public final ArrayMergeStrategy arrayMergeStrategy;

    public MergeStrategy(NullMergeStrategy nullMergeStrategy, ArrayMergeStrategy arrayMergeStrategy) {
        this.nullMergeStrategy = nullMergeStrategy;
        this.arrayMergeStrategy = arrayMergeStrategy;
    }
}
