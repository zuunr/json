/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import clojure.lang.APersistentVector;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class WrapperList<T> implements List<T> {

    private Class clazz;
    private APersistentVector persistentVector;
    private JsonArray jsonArray;
    private boolean nativeValue;

    public WrapperList(Class<T> clazz, JsonArray jsonArray) {
        this(clazz, jsonArray.jsonValue());
    }

    public WrapperList(Class<T> clazz, JsonValue jsonValue) {
        nativeValue = clazz == JsonArray.class || clazz == JsonObject.class || clazz == String.class || clazz == Integer.class || clazz == Long.class || clazz == Boolean.class || clazz == Object.class;
        this.clazz = clazz;
        this.jsonArray = jsonValue.getValue(JsonArray.class);
        this.persistentVector = jsonArray.persistentVector;
    }

    @Override
    public void forEach(Consumer action) {
        persistentVector.forEach(action);
    }

    @Override
    public Spliterator spliterator() {
        throw new RuntimeException("Not implemented!");
    }

    @Override
    public void replaceAll(UnaryOperator operator) {
        persistentVector.replaceAll(operator);
    }

    @Override
    public void sort(Comparator c) {
        persistentVector.sort(c);
    }

    @Override
    public boolean removeIf(Predicate filter) {
        return persistentVector.removeIf(filter);
    }

    @Override
    public Stream<T> stream() {
        return persistentVector.stream().map(jsonValue -> ((JsonValue) jsonValue).as(clazz));
    }

    @Override
    public Stream parallelStream() {
        return persistentVector.parallelStream().map(jsonValue -> ((JsonValue) jsonValue).as(clazz));
    }

    @Override
    public int size() {
        return persistentVector.size();
    }

    @Override
    public boolean isEmpty() {
        return persistentVector.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        throw new RuntimeException("Not implemented!");
    }

    @Override
    public Iterator<T> iterator() {
        return new WListIterator<>(clazz, persistentVector, 0, nativeValue);
    }

    @Override
    public Object[] toArray() {
        throw new RuntimeException("Not implemented!");
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        throw new RuntimeException("Not implemented!");
    }

    @Override
    public boolean add(T t) {
        throw new RuntimeException("Not implemented!");
    }

    @Override
    public boolean remove(Object o) {
        throw new RuntimeException("Not implemented!");
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new RuntimeException("Not implemented!");
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        throw new RuntimeException("Not implemented!");
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        throw new RuntimeException("Not implemented!");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new RuntimeException("Not implemented!");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new RuntimeException("Not implemented!");
    }

    @Override
    public void clear() {
        throw new RuntimeException("Not implemented!");
    }

    @Override
    public T get(int index) {
        return (T) ((JsonValue) persistentVector.get(index)).as(clazz);
    }

    @Override
    public T set(int index, T element) {
        throw new RuntimeException("Not implemented!");
    }

    @Override
    public void add(int index, T element) {
        throw new RuntimeException("Not implemented!");
    }

    @Override
    public T remove(int index) {
        throw new RuntimeException("Not implemented!");
    }

    @Override
    public int indexOf(Object o) {
        throw new RuntimeException("Not implemented!");
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new RuntimeException("Not implemented!");
    }

    @Override
    public ListIterator<T> listIterator() {
        return new WListIterator<>(clazz, persistentVector, 0, nativeValue);

    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return new WListIterator<>(clazz, persistentVector, 0, nativeValue);
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return jsonArray.subArray(fromIndex, toIndex).asList(clazz);
    }

    @Override
    public String toString() {
        try {
            return jsonArray.toString();
        } catch (Exception e) {
            return "WrapperList.toString failed " + UUID.randomUUID().toString();
        }
    }

    public static class WListIterator<T> implements ListIterator<T> {

        private Class<T> clazz;
        private ListIterator<T> wrappedIterator;
        private boolean nativeValue;

        protected WListIterator(Class<T> clazz, APersistentVector persistentVector, int index, boolean nativeValue) {
            this.clazz = clazz;
            this.wrappedIterator = persistentVector.listIterator(index);
            this.nativeValue = nativeValue;
        }

        @Override
        public boolean hasNext() {
            return wrappedIterator.hasNext();
        }

        @Override
        public T next() {
            return nativeValue ? ((JsonValue) wrappedIterator.next()).getValue(clazz) : ((JsonValue) wrappedIterator.next()).as(clazz);
        }

        @Override
        public boolean hasPrevious() {
            return wrappedIterator.hasPrevious();
        }

        @Override
        public T previous() {
            return nativeValue ? ((JsonValue) wrappedIterator.previous()).getValue(clazz) : ((JsonValue) wrappedIterator.previous()).as(clazz);
        }

        @Override
        public int nextIndex() {
            return wrappedIterator.nextIndex();
        }

        @Override
        public int previousIndex() {
            return wrappedIterator.previousIndex();
        }

        @Override
        public void remove() {
            wrappedIterator.remove();
        }

        @Override
        public void set(T t) {
            wrappedIterator.set(t);
        }

        @Override
        public void add(T t) {
            wrappedIterator.add(t);
        }
    }
}
