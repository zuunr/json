/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import clojure.lang.MapEntry;
import clojure.lang.PersistentHashMap;
import com.zuunr.json.pointer.JsonPointer;

import java.lang.ref.SoftReference;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author Niklas Eldberger
 */
public final class JsonObject {

    private static final JsonSerializer jsonSerializer = new JsonSerializer();
    private static final JsonValueUtil jsonValueUtil = new JsonValueUtil();

    public static final JsonObject EMPTY = new JsonObject();

    final PersistentHashMap persistentMap;
    private final JsonValue meAsjsonValue;

    private boolean optimizedForRead = false;

    private SoftReference<JsonObjectInternalCache> cache = null;

    // default modifier to be accessible from JsonObjectBuilder
    static JsonObject of(PersistentHashMap persistentHashMap) {

        if (persistentHashMap == null) {
            throw new NullPointerException("persistentHashMap must not be null!");
        }

        JsonObject theJsonObject = null;
        JsonValue tempJsonValue = JsonValue.cache.get(persistentHashMap);
        if (tempJsonValue != null) {
            theJsonObject = tempJsonValue.getValue(JsonObject.class);
        }
        if (theJsonObject == null) {
            theJsonObject = new JsonObject(persistentHashMap);
            JsonValue.cache.put(theJsonObject.jsonValue());
        }
        return theJsonObject;
    }

    public com.zuunr.json.JsonObjectBuilder builder() {
        return new com.zuunr.json.JsonObjectBuilder(this);
    }

    private JsonObject(PersistentHashMap persistentMap) {
        this.persistentMap = persistentMap;
        meAsjsonValue = new JsonValue(this);
    }

    protected JsonObject() {
        meAsjsonValue = new JsonValue(this);
        persistentMap = PersistentHashMap.EMPTY;
    }

    public JsonValue get(String key) {
        JsonValue result;

        if (!optimizedForRead) {
            result = (JsonValue) persistentMap.valAt(key, null);
        } else {
            JsonObjectInternalCache internalCache = getInternalCache();
            if (internalCache == null || internalCache.cachedValues == null) {
                optimizeGet(false);
                result = (JsonValue) persistentMap.valAt(key, null);
            } else {
                result = internalCache.cachedValues.get(key);
            }
        }

        return result;
    }

    public JsonObject optimizeGet(boolean recursive) {
        optimizedForRead = true;
        JsonObjectInternalCache internalCache = getInternalCache();

        if (internalCache == null) {
            internalCache = new JsonObjectInternalCache();
            setInternalCache(internalCache);
        }

        if (internalCache.cachedValues == null || recursive && !internalCache.isRecursiveCache) {
            int size = persistentMap.size();
            HashMap<String, JsonValue> tempCachedValues = new HashMap<>(size);
            tempCachedValues.putAll(persistentMap);

            if (recursive) {
                for (JsonValue value : values()) {
                    if (value.isJsonObject()) {
                        value.getJsonObject().optimizeGet(true);
                    }
                }
                internalCache.isRecursiveCache = true;
            }
            internalCache.cachedValues = tempCachedValues;
        }
        return this;
    }

    public JsonValue get(String key, String defaultIfNull) {
        return get(key, (Object) defaultIfNull);
    }

    public JsonValue get(String key, JsonValueSupport defaultIfNull) {
        return get(key, defaultIfNull.asJsonValue());
    }

    public JsonValue get(String key, JsonObject defaultIfNull) {

        return get(key, defaultIfNull.jsonValue());
    }

    public JsonValue get(String key, JsonArray defaultIfNull) {
        return get(key, defaultIfNull.jsonValue());
    }

    public JsonValue get(JsonArray path, Boolean defaultIfNull) {
        return jsonValue().get(path, (Object) defaultIfNull);
    }

    public JsonValue get(String key, JsonValue defaultIfNull) {
        JsonValue result = get(key);
        if (result == null) {
            result = defaultIfNull;
        }
        return result;
    }

    private JsonValue get(String key, Object defaultIfNull) {
        JsonValue result = get(key);
        if (result == null) {
            result = JsonValue.of(defaultIfNull);
        }
        return result;
    }

    public boolean containsKey(String key) {
        return persistentMap.containsKey(key);
    }

    public JsonObject put(String key, boolean value) {
        JsonObject result = null;
        if (value) {
            result = put(key, JsonValue.TRUE);
        } else {
            result = put(key, JsonValue.FALSE);
        }
        return result;
    }

    public JsonObject put(String key, long value) {
        return put(key, JsonValue.of(value));
    }

    public JsonObject put(String key, BigDecimal value) {
        return put(key, JsonValue.of(value));
    }

    public JsonObject put(String key, int value) {
        return put(key, JsonValue.of(value));
    }

    public JsonObject put(String key, JsonNumber value) {
        return put(key, value.jsonValue());
    }

    public JsonObject put(String key, JsonArray jsonArray) {
        JsonObject result = null;
        if (jsonArray == null) {
            throw new NullPointerException("jsonArray must not be null");
        } else {
            result = put(key, jsonArray.jsonValue());
        }
        return result;
    }

    public JsonObject put(String key, JsonObject jsonObject) {
        JsonObject result = null;
        if (jsonObject == null) {
            throw new NullPointerException("jsonObject must not be null");
        } else {
            result = put(key, jsonObject.jsonValue());
        }
        return result;
    }

    private void validateNull(String key, JsonValue jsonValue) {
        if (key == null) {
            throw new NullPointerException("key must not be null in JsonObject");
        }
        if (jsonValue == null) {
            throw new NullPointerException("jsonValue must not be null in JsonObject");
        }
    }

    public JsonObject put(String key, JsonValue value) {
        validateNull(key, value);
        return JsonObject.of((PersistentHashMap) persistentMap.assoc(key, value));
    }

    public JsonObject put(String key, JsonValueSupport value) {
        return put(key, value.asJsonValue());
    }

    public JsonObject put(String key, Integer value) {
        return put(key, JsonValue.of(value));
    }

    public JsonObject put(String key, String value) {
        return put(key, JsonValue.of(value));
    }

    public JsonObject putAll(JsonObject jsonObject) {

        com.zuunr.json.JsonObjectBuilder result = this.builder();

        if (jsonObject == null) {
            throw new NullPointerException("Cannot put null into JsonObject");
        } else {

            Iterator<JsonValue> valuesIterator = jsonObject.values().iterator();

            for (Iterator<JsonValue> keysIterator = jsonObject.keys().iterator(); keysIterator.hasNext(); ) {
                result = result.put(keysIterator.next().getValue(String.class), valuesIterator.next());
            }
        }
        return result.build();
    }

    @Override
    public String toString() {
        return jsonSerializer.serialize(asMap());
    }

    public JsonArray keys() {

        JsonObjectInternalCache internalCache = getInternalCache();

        if (internalCache == null) {
            internalCache = new JsonObjectInternalCache();
            setInternalCache(internalCache);
        }

        if (internalCache.keys == null) {
            internalCache = createKeysAndValues(internalCache);
        }
        return internalCache.keys;
    }

    public JsonArray values() {

        JsonObjectInternalCache internalCache = getInternalCache();

        if (internalCache == null) {
            internalCache = new JsonObjectInternalCache();
            setInternalCache(internalCache);
        }

        if (internalCache.values == null) {
            internalCache = createKeysAndValues(internalCache);
        }
        return internalCache.values;
    }


    /**
     * @return A JsonObjectInternalCache object where both keys and values are set
     */
    private JsonObjectInternalCache createKeysAndValues(JsonObjectInternalCache internalCache) {

        JsonArrayBuilder keyArray = JsonArray.EMPTY.builder();
        JsonArrayBuilder valueArray = JsonArray.EMPTY.builder();

        for (Iterator<MapEntry> i = persistentMap.iterator(); i.hasNext(); ) {
            MapEntry mapEntry = i.next();
            keyArray.add((String) mapEntry.getKey());
            valueArray.add((JsonValue) mapEntry.getValue());
        }

        internalCache.values = valueArray.build();
        internalCache.keys = keyArray.build();

        return internalCache;
    }

    /**
     * <p>Creates a new array each time method is called.</p>
     *
     * @return
     */
    public JsonValue[] valuesAsArray() {

        JsonValue[] array = new JsonValue[(persistentMap).size()];

        int index = 0;
        for (Iterator<MapEntry> i = persistentMap.iterator(); i.hasNext(); index++) {
            MapEntry mapEntry = i.next();
            array[index] = (JsonValue) mapEntry.getValue();
        }
        return array;
    }

    public int size() {
        return persistentMap.count();
    }

    public boolean isEmpty() {
        return persistentMap.equals(PersistentHashMap.EMPTY);
    }

    public JsonObject remove(String key) {
        return JsonObject.of((PersistentHashMap) persistentMap.without(key));
    }


    public JsonArray keys(JsonValue filter) {
        return keys(filter, false);
    }

    public JsonArray keysByRegex(JsonValue filter) {
        return keys(filter, false);
    }

    protected JsonArray keys(JsonValue filter, boolean filterIsRegEx) {

        JsonArrayBuilder arrayBuilder = JsonArray.EMPTY.builder();

        for (Iterator<MapEntry> i = persistentMap.iterator(); i.hasNext(); ) {
            MapEntry mapEntry = i.next();

            boolean doAdd = JsonValue.ANY_ELEMENT.equals(filter);
            if (!doAdd) {
                if (filterIsRegEx) {
                    doAdd = ((String) mapEntry.getKey()).matches(filter.getValue(String.class));
                } else {
                    doAdd = (mapEntry.getKey()).equals(filter.getValue(String.class));
                }
            }

            if (doAdd) {
                arrayBuilder = arrayBuilder.add((String) mapEntry.getKey());
            }
        }
        return arrayBuilder.build();
    }

    public <T> T as(Class<T> clazz) {
        return jsonValue().as(clazz);
    }

    public JsonValue jsonValue() {
        return meAsjsonValue;
    }

    public Map<String, Object> asMapsAndLists() {
        return asMapsAndLists(false);
    }

    public Map<String, Object> asMapsAndLists(boolean sorted) {
        Map<String, Object> result = sorted ? new TreeMap<>() : new HashMap<>();

        for (int i = 0; i < keys().size(); i++) {

            JsonValue valueOfKey = values().get(i);
            if (valueOfKey.is(JsonObject.class)) {
                result.put(keys().get(i).getValue(String.class), valueOfKey.getValue(JsonObject.class).asMapsAndLists(sorted));
            } else if (valueOfKey.is(JsonArray.class)) {
                result.put(keys().get(i).getValue(String.class), valueOfKey.getValue(JsonArray.class).asMapsAndLists(sorted));
            } else if (valueOfKey.isJsonNumber()) {
                result.put(keys().get(i).getValue(String.class), valueOfKey.getJsonNumber().getWrappedNumber());
            } else {
                result.put(keys().get(i).getValue(String.class), valueOfKey.getValue());
            }
        }
        return result;
    }

    public Map<String, JsonValue> asMap() {
        return persistentMap;
    }

    @Override
    public int hashCode() {
        return persistentMap.hashCode();
    }

    @Override
    public boolean equals(Object obj) {

        return
                this == obj ||
                        obj != null &&
                                obj.getClass() == getClass() &&
                                persistentMap.equals(((JsonObject) obj).persistentMap);

    }

    public JsonObject put(JsonArray path, Integer value) {
        return put(path, JsonValue.of(value));
    }

    public JsonObject put(JsonArray path, Boolean value) {
        return put(path, JsonValue.of(value));
    }

    public JsonObject put(JsonArray path, String value) {
        return put(path, JsonValue.of(value));
    }

    public JsonObject put(JsonArray path, JsonArray jsonArray) {
        return put(path, jsonArray.jsonValue());
    }

    public JsonObject put(JsonArray path, JsonObject jsonObject) {

        return put(path, jsonObject.jsonValue());
    }

    // TODO: Make use of JsonObjectBuilder to avoid extra GC
    public JsonObject put(JsonArray path, JsonValue jsonValue) {
        JsonObject result;

        JsonValue headJsonValue = path.head();
        if (headJsonValue.isString()) {
            String head = path.head().getString();

            JsonArray tail = path.tail();
            if (tail.isEmpty()) {
                result = put(head, jsonValue);
            } else {

                JsonValue empty = JsonObject.EMPTY.jsonValue();
                if (tail.head().isInteger()) {
                    empty = JsonArray.EMPTY.jsonValue();
                }
                result = put(head, get(head, empty).put(tail, jsonValue));
            }
        } else {
            throw new UnsupportedTypeException("Only String is supported");
        }
        return result;
    }

    public JsonObject put(JsonPointer jsonPointer, JsonValue jsonValue) {
        return meAsjsonValue.put(jsonPointer, jsonValue).getJsonObject();
    }

    public JsonObject put(JsonPointer jsonPointer, JsonObject jsonObject) {
        return meAsjsonValue.put(jsonPointer, jsonObject.jsonValue()).getJsonObject();
    }

    public JsonObject put(JsonPointer jsonPointer, JsonArray jsonArray) {
        return meAsjsonValue.put(jsonPointer, jsonArray.jsonValue()).getJsonObject();
    }

    public JsonObject put(JsonPointer jsonPointer, String string) {
        return meAsjsonValue.put(jsonPointer, JsonValue.of(string)).getJsonObject();
    }

    public JsonObject put(JsonPointer jsonPointer, boolean bool) {
        return meAsjsonValue.put(jsonPointer, JsonValue.of(bool)).getJsonObject();
    }

    public JsonObject put(JsonPointer jsonPointer, JsonNumber jsonNumber) {
        return meAsjsonValue.put(jsonPointer, jsonNumber.jsonValue()).getJsonObject();
    }

    public JsonValue get(JsonPointer jsonPointer, JsonValue defaultIfNull) {
        return meAsjsonValue.get(jsonPointer, defaultIfNull);
    }

    public JsonValue get(JsonPointer jsonPointer) {
        return meAsjsonValue.get(jsonPointer, null);
    }

    public JsonObject remove(JsonPointer jsonPointer) {
        return meAsjsonValue.removeByPointer(jsonPointer).getJsonObject();
    }

    public JsonObject remove(JsonArray path) {
        JsonObject result;
        if (path == null || path.size() == 0) {
            result = this;
        } else if (path.size() == 1) {
            result = remove(path.head().getValue(String.class));
        } else {
            String headKey = path.head().getValue(String.class);
            if (get(headKey) != null) {
                result = put(headKey, get(headKey).getValue(JsonObject.class).remove(path.tail()));
            } else {
                result = this;
            }
        }
        return result;
    }

    public JsonValue get(JsonArray path) {
        return jsonValue().get(path);
    }

    public JsonValue get(JsonArray path, JsonArray defaultIfNull) {
        return get(path, defaultIfNull.jsonValue());
    }

    public JsonValue get(JsonArray path, String defaultIfNull) {
        return jsonValue().get(path, (Object) defaultIfNull);
    }

    public JsonValue get(JsonArray path, Integer defaultIfNull) {
        return jsonValue().get(path, (Object) defaultIfNull);
    }

    public JsonValue get(String key, Integer defaultIfNull) {
        return get(key, (Object) defaultIfNull);
    }

    public JsonValue get(String key, Boolean defaultIfNull) {
        return get(key, (Object) defaultIfNull);
    }

    public JsonValue get(JsonArray path, JsonValue defaultIfNull) {
        return jsonValue().get(path, defaultIfNull);
    }

    public JsonValue get(JsonArray path, JsonObject defaultIfNull) {
        return get(path, defaultIfNull.jsonValue());
    }

    public String asJson() {
        return toString();
    }

    public Set<Map.Entry<String, JsonValue>> entrySet() {
        return persistentMap.entrySet();
    }

    public JsonArray asKeyValueTuples() {

        JsonArrayBuilder result = JsonArray.EMPTY.builder();

        for (Map.Entry<String, JsonValue> entryObject : entrySet()) {
            result.add(JsonArray.of(entryObject.getKey(), entryObject.getValue()));
        }

        return result.build();
    }

    public String asJsonSorted() {
        StringBuilder builder = new StringBuilder("{");

        String leadingCommaSeparator = "";

        // sort by key names
        JsonArray toBeSorted = asKeyValueTuples();
        toBeSorted.sort((o1, o2) -> {
            String key1 = o1.get(0).getValue(String.class);
            String key2 = o2.get(0).getValue(String.class);
            return key1.compareTo(key2);
        });

        for (int i = 0; i < toBeSorted.size(); i++) {
            builder.append(leadingCommaSeparator);

            JsonArray entry = toBeSorted.get(i).getValue(JsonArray.class);

            JsonValue key = entry.get(0);
            JsonValue value = entry.get(1);
            builder.append("\"").append(key.getValue(String.class)).append("\":").append(value.asJsonSorted());
            leadingCommaSeparator = ",";
        }
        builder.append("}");
        return builder.toString();
    }


    public JsonObject replaceInKeys(String target, String replacement, boolean recursive) {
        return jsonValue().replaceInKeys(target, replacement, recursive).getValue(JsonObject.class);
    }

    public JsonObject removeRegexPath(JsonArray regexPath) {
        return jsonValueUtil.removeRegexPath(jsonValue(), regexPath).getValue(JsonObject.class);
    }


    public JsonObject removePaths(JsonArray allPaths) {
        JsonObject result = this;
        for (JsonValue pathJsonValue : allPaths.asList()) {
            result = result.remove(pathJsonValue.getValue(JsonArray.class));
        }
        return result;
    }

    public JsonObject removeKeys(JsonArray keys) {
        JsonObject result = this;
        for (JsonValue key : keys.asList()) {
            result = result.remove(key.getValue(String.class));
        }
        return result;
    }

    public JsonObject putRegexPath(JsonArray regexPath, JsonValue jsonValue) {
        JsonObject result = this;
        JsonArray paths = jsonValueUtil.getPathsByRegex(jsonValue, regexPath);
        for (JsonValue pathJsonValue : paths.asList()) {
            JsonArray path = pathJsonValue.getValue(JsonArray.class);
            result = result.put(path, jsonValue);
        }
        return result;
    }


    public String asPrettyJson() {
        return jsonSerializer.asPrettyJson(this);
    }

    public JsonObject diff(JsonObject comparedTo) {
        return diff(this.jsonValue(), comparedTo.jsonValue());
    }

    private JsonObject diff(JsonValue thisOne, JsonValue comparedTo) {
        JsonValue expansion = expansionComparedTo(thisOne, comparedTo);
        JsonValue reduction = expansionComparedTo(comparedTo, thisOne);

        return JsonObject.EMPTY.put("expansion", expansion == null ? JsonValue.NULL : expansion).put("reduction", reduction == null ? JsonValue.NULL : reduction);
    }

    JsonValue expansionComparedTo(JsonValue thisOne, JsonValue newOne) {
        JsonValue result;

        if (thisOne.equals(newOne)) {
            result = null;
        } else if (newOne != null &&
                newOne.getValue() != null &&
                thisOne.getValue() != null &&
                thisOne.getValue().getClass().equals(newOne.getValue().getClass())) {
            if (thisOne.is(JsonObject.class)) {
                JsonObject diff = expansionComparedTo(thisOne.getValue(JsonObject.class), newOne.getValue(JsonObject.class));
                result = diff == null ? null : diff.jsonValue();
            } else {
                // JsonArray, String, Integer etc is handled the same way
                result = thisOne;
            }
        } else {
            result = thisOne;
        }
        return result;
    }

    private JsonObject expansionComparedTo(JsonObject thisOne, JsonObject newOne) {
        return expansionComparedTo(thisOne.getPairs(), newOne);
    }

    private JsonObject expansionComparedTo(Pairs pairs, JsonObject newOne) {
        JsonObject result = null;
        if (!pairs.isEmpty()) {
            JsonValue valueInNew = newOne.get(pairs.head().getKey());
            JsonValue deepDiff = expansionComparedTo(pairs.head().getValue(), valueInNew);
            newOne = deepDiff == null ? newOne.remove(pairs.head().getKey()) : newOne;
            result = expansionComparedTo(pairs.tail(), newOne);
            if (deepDiff != null) {
                result = result == null ? JsonObject.EMPTY.put(pairs.head().getKey(), deepDiff) : result.put(pairs.head().getKey(), deepDiff);
            }
        }
        return result;
    }

    public Set<String> keySet() {
        return persistentMap.keySet();
    }

    public Pairs getPairs() {
        return new Pairs(this);
    }

    public Pairs getPairs(String keyPattern) {
        return new Pairs(this, keyPattern);
    }

    public JsonObject remove(Pairs pairs) {
        JsonObject result = this;
        if (!pairs.isEmpty()) {
            result = remove(pairs.head().key);
            result = result.remove(pairs.tail());
        }
        return result;
    }

    /**
     * @deprecated use keys() and values() instead
     */
    @Deprecated
    public class Pairs {
        private JsonArray keys;
        private JsonArray values;

        private Pairs(JsonArray keys, JsonArray values) {
            this.keys = keys;
            this.values = values;
        }

        private Pairs(JsonObject jsonObject) {
            this(jsonObject, null);
        }

        private Pairs(JsonObject jsonObject, String keyPattern) {
            values = keys = JsonArray.EMPTY;

            for (Map.Entry<String, JsonValue> entry : jsonObject.asMap().entrySet()) {
                if (keyPattern == null || entry.getKey().matches(keyPattern)) {
                    keys = keys.add(entry.getKey());
                    values = values.add(entry.getValue());
                }
            }
        }

        public Pairs tail() {
            return new Pairs(keys.tail(), values.tail());
        }

        public Pair head() {
            return keys.isEmpty() ? null : new Pair(keys.head().getValue(String.class), values.head());
        }

        public boolean isEmpty() {
            return keys.isEmpty();
        }

        public String toString() {
            return asJsonArray().toString();
        }

        public JsonArray asJsonArray() {
            JsonArray result = JsonArray.EMPTY;
            if (!isEmpty()) {
                result = tail().asJsonArray().addFirst(head().asJsonObject());
            }
            return result;
        }

        public JsonArray values() {
            return values;
        }

        public JsonArray keys() {
            return keys;
        }

        public int size() {
            return keys.size();
        }

        public String getKey(int index) {
            return keys.get(index).getValue(String.class);
        }

        public JsonValue getValue(int index) {
            return values.get(index);
        }
    }

    public int compareTo(Object o) {

        int result;
        if (o.getClass() == getClass()) {

            if (size() == ((JsonObject) o).size()) {

                result = asJsonSorted().compareTo(((JsonObject) o).asJsonSorted());
            } else {
                return size() - ((JsonObject) o).size();
            }
        } else {
            result = getClass().getName().compareTo(o.getClass().getName());
        }
        return result;
    }

    public final class Pair {
        private final String key;
        private final JsonValue value;

        private Pair(String key, JsonValue value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public JsonValue getValue() {
            return value;
        }

        public String toString() {
            return asJsonObject().toString();
        }

        public JsonObject asJsonObject() {
            return JsonObject.EMPTY.put(key, value);
        }
    }

    /**
     * @return
     * @deprecated Use jsonValue() instead
     */
    @Deprecated
    public JsonValue asJsonValue() {
        return jsonValue();
    }

    /**
     * @return
     * @deprecated Use this JsonObject instead
     */
    @Deprecated
    public JsonObject asJsonObject() {
        return this;
    }

    private JsonObjectInternalCache getInternalCache() {
        if (cache == null) {
            return null;
        } else {
            return cache.get();
        }
    }

    private void setInternalCache(JsonObjectInternalCache internalCache) {
        cache = new SoftReference<>(internalCache);
    }

    public KeyValuePairs getKeyValuePairs() {
        JsonObjectInternalCache internalCache = getInternalCache();
        if (internalCache == null) {
            internalCache = new JsonObjectInternalCache();
            setInternalCache(internalCache);
        }
        if (internalCache.values == null) {
            createKeysAndValues(internalCache);
        }
        return internalCache;
    }
}
