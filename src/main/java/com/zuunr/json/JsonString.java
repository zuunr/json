package com.zuunr.json;

import java.math.BigDecimal;

/**
 * @author Niklas Eldberger
 */
public final class JsonString implements Comparable {

    private final String asString;
    private final JsonValue asJsonValue;

    static JsonString of(String value) {

        return new JsonString(value);
    }


    private JsonString(String string) {
        this.asString = string;
        this.asJsonValue = new JsonValue(this);
    }


    @Override
    public String toString() {
        return asString;
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj.getClass() == JsonString.class &&
                asString.equals(obj.toString());
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }


    @Override
    public int compareTo(Object o) {

        if (o == null || o.getClass() != JsonString.class) {
            throw new UnsupportedTypeException("Cannot compare!");
        }

        return asString.compareTo(((JsonString) o).asString);
    }

    public JsonValue jsonValue() {
        return asJsonValue;
    }
}
