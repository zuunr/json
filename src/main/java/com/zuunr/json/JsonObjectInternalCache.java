package com.zuunr.json;

import java.util.HashMap;

public class JsonObjectInternalCache implements KeyValuePairs {

    HashMap<String, JsonValue> cachedValues;
    boolean isRecursiveCache = false;

    JsonArray keys;
    JsonArray values;

    @Override
    public JsonValue getKey(int index) {
        return keys.get(index);
    }

    @Override
    public JsonValue getValue(int index) {
        return values.get(index);
    }

    @Override
    public int getSize() {
        return keys.size();
    }

}
