package com.zuunr.json;

import com.zuunr.json.pointer.JsonPointer;

public class PointerMismatchJsonValueException extends RuntimeException {

    private final JsonPointer pointer;
    private final JsonType type;

    public PointerMismatchJsonValueException(JsonPointer pointer, JsonType type) {
        super("Value of "+ pointer.getJsonPointerString().getString()+ " is of type " + type.name());
        this.pointer = pointer;
        this.type = type;
    }

    public JsonPointer getPointer() {
        return pointer;
    }

    public JsonType getType() {
        return type;
    }
}
