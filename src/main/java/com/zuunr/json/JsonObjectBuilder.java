/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import clojure.lang.ITransientMap;
import clojure.lang.PersistentHashMap;

import java.math.BigDecimal;
import java.util.Iterator;

/**
 * @author Niklas Eldberger
 */
public class JsonObjectBuilder {

    private ITransientMap transientMap;

    public JsonObjectBuilder(JsonObject startingObject) {
        transientMap = startingObject.persistentMap.asTransient();
    }

    public JsonObjectBuilder put(String key, JsonValue jsonValue) {
        transientMap = transientMap.assoc(key, jsonValue);
        return this;
    }

    public JsonObjectBuilder put(String key, JsonValueSupport jsonValueSupport) {
        return put(key, jsonValueSupport.asJsonValue());
    }

    public JsonObjectBuilder put(String key, boolean value) {
        return put(key, JsonValue.of(value));
    }

    public JsonObjectBuilder put(String key, JsonArray value) {
        return put(key, value.jsonValue());
    }

    public JsonObjectBuilder put(String key, JsonObject value) {
        return put(key, value.jsonValue());
    }

    public JsonObjectBuilder put(String key, JsonObjectBuilder builder) {
        return put(key, builder.build().jsonValue());
    }

    public JsonObjectBuilder put(String key, String value) {
        return put(key, JsonValue.of(value));
    }

    public JsonObjectBuilder put(String key, JsonNumber value) {
        return put(key, value.jsonValue());
    }

    public JsonObjectBuilder put(String key, Integer value) {
        return put(key, JsonValue.of(value));
    }

    public JsonObjectBuilder put(String key, Long value) {
        return put(key, JsonValue.of(value));
    }

    public JsonObjectBuilder put(String key, BigDecimal value) {
        return put(key, JsonValue.of(value));
    }

    public JsonObjectBuilder putAll(JsonObject jsonObject) {

        Iterator<JsonValue> values = jsonObject.values().iterator();
        for (Iterator<JsonValue> keys = jsonObject.keys().iterator(); keys.hasNext();) {
            put(keys.next().as(String.class), values.next());
        }
        return this;
    }

    public JsonObjectBuilder remove(String key) {
        transientMap = transientMap.without(key);
        return this;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public int size() {
        return transientMap.count();
    }

    public JsonObject build() {
        return JsonObject.of((PersistentHashMap) transientMap.persistent());
    }
}
