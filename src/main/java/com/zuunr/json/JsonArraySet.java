/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import java.util.HashSet;

public class JsonArraySet {

    private HashSet<JsonValue> mutableSet;
    private JsonArray jsonArray;

    public static final JsonArraySet EMPTY = new JsonArraySet(new HashSet<>(), JsonArray.EMPTY);

    private JsonArraySet(HashSet<JsonValue> mutableSet, JsonArray jsonArray) {
        this.mutableSet = mutableSet;
        this.jsonArray = jsonArray;

    }

    public JsonArraySet add(JsonValue jsonValue) {

        JsonArraySet result = this;

        HashSet<JsonValue> newHashSet = new HashSet<>();
        int size = mutableSet.size();
        newHashSet.addAll(mutableSet);
        newHashSet.add(jsonValue);
        if (newHashSet.size() != size) {
            result = new JsonArraySet(newHashSet, jsonArray.add(jsonValue));
        }
        return result;
    }

    public JsonArray jsonArray() {
        return jsonArray;
    }


    public JsonArraySet addAll(JsonArray jsonArray) {
        JsonArraySet set = this;
        for (JsonValue value : jsonArray.asList()) {
            set = set.add(value);
        }
        return set;
    }

    public boolean contains(JsonValue jsonValue) {
        return mutableSet.contains(jsonValue);
    }


    public static boolean isSet(JsonArray jsonArray) {
        return JsonArraySet.EMPTY.addAll(jsonArray).jsonArray().size() == jsonArray.size();
    }

    public String toString() {
        return jsonArray().toString();
    }

    /*
     * Compared to jsonArraySet without looking at the order of the JsonValues in the JsonArraySet
     * @param jsonArraySet
     * @return
     */
    public boolean equalsAsSet(JsonArraySet jsonArraySet) {
        return mutableSet.equals(jsonArraySet.mutableSet);
    }

}
