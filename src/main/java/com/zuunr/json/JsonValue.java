/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.json;

import com.zuunr.json.pointer.JsonPointer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Niklas Eldberger
 */
public final class JsonValue implements Comparable<JsonValue> {
    private static final Logger logger = LoggerFactory.getLogger(JsonValue.class);

    // Default access needed from JsonArray and JsonObject
    static JsonValueCache cache = new JsonValueCache();

    public static final JsonValue NULL = new JsonValue();
    public static final JsonValue TRUE = of(true);
    public static final JsonValue FALSE = of(false);
    public static final JsonValue EMPTY_STRING = of("");
    public static final JsonValue MINUS_ONE = of(-1);
    public static final JsonValue ONE = of(1);
    public static final JsonValue ANY_ELEMENT = MINUS_ONE;
    public static final JsonValue ZERO = of(0);
    public static final JsonValue INTEGER_MAX = of(Integer.MAX_VALUE);
    public static final JsonValue INTEGER_MIN = of(Integer.MIN_VALUE);

    private static long instances = 0;
    private Object value;

    private static final ConcurrentHashMap<Class, Constructor> cachedConstructors = new ConcurrentHashMap<>();
    private transient JsonValueSupportedPojoCache cachedAggregates;
    private transient JsonValueSupportedPojoCache cachedListAggregates;

    // Cached types of value
    private String asJsonSorted;

    public static void cacheEnabled(boolean enabled) {
        cache.enabled(enabled);
    }

    private JsonValue() {
        instances = instances + 1;
    }

    JsonValue(JsonArray value) {
        this((Object) value);
    }

    JsonValue(JsonObject value) {
        this((Object) value);
    }

    JsonValue(JsonNumber value) {
        this((Object) value);
    }

    JsonValue(String value) {
        this((Object) value);
    }

    JsonValue(JsonString value) {
        this((Object) value);
    }

    private JsonValue(Object value) {
        this();
        if (value == null) {
            throw new NullPointerException("value must not be null. Use JsonValue.NULL instead.");
        }

        this.value = value;
    }

    private JsonValue(Boolean value) {
        this((Object) value);
    }

    public boolean is(Class clazz) {
        boolean result = false;
        if (value == null) {
            if (clazz == null) {
                result = true;
            }
        } else if (value.getClass() == clazz || clazz == Object.class) {
            result = true;
        }
        return result;
    }

    public <T> T getValue(Class<T> clazz) {

        T result;

        if (clazz == null) {
            throw new NullPointerException("Class must not be null");
        }

        if (value == null) {
            result = null;
        } else if (is(clazz)) {
            result = (T) value;
        } else {
            throw new ClassCastException("value is not of class " + clazz);
        }
        return result;
    }

    private JsonValueSupportedPojoCache getCachedAggregates() {
        if (cachedAggregates == null) {
            cachedAggregates = new JsonValueSupportedPojoCache();
        }
        return cachedAggregates;
    }

    public JsonValueSupportedPojoCache getCachedListAggregates() {
        if (cachedListAggregates == null) {
            cachedListAggregates = new JsonValueSupportedPojoCache();
        }
        return cachedListAggregates;
    }

    public <T> T as(Class<T> clazz) {

        if (is(clazz)) {
            return getValue(clazz);
        }
        Object aggregate = getCachedAggregates().getPojo(clazz);
        if (aggregate == null) {
            try {
                if (value == null) {
                    // Possible to cache the result of null value globally for every clazz
                } else {
                    Constructor constructor = getConstructor(clazz);
                    aggregate = constructor.newInstance(this);
                    getCachedAggregates().putPojo(clazz, aggregate);
                }
            } catch (NoSuchMethodException e) {
                String msg = clazz.getName() + " has no constructor with argument type com.zuunr.JsonValue. This is needed to support JsonValue.as(" + clazz.getName() + ").";
                logger.error(msg);
                throw new RuntimeException(msg, e);

            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException | InvocationTargetException e) {
                String msg = clazz.getName() + " could not be created. JsonValue is: "+getValue();
                logger.error(msg);
                throw new RuntimeException(e);
            }
        }
        return (T) aggregate;
    }

    public <T> List<T> asList(Class<T> clazz) {
        List<T> list = (List<T>) getCachedListAggregates().getPojo(clazz);
        if (list == null) {

            if (value != null) {
                list = new WrapperList<>(clazz, this);
                getCachedListAggregates().putPojo(clazz, list);
            }
        }
        return list;
    }

    public Integer asInteger() {
        return value == null ? null : getJsonNumber().intValue();
    }


    /**
     * @deprecated use getJsonNumber().isJsonInteger()
     */
    @Deprecated
    public boolean isJsonInteger() {
        return getJsonNumber().isJsonInteger();
    }

    private Constructor getConstructor(Class clazz) throws ClassNotFoundException, NoSuchMethodException {
        Constructor constructor = cachedConstructors.get(clazz);
        if (constructor == null) {
            constructor = Class.forName(clazz.getName()).getDeclaredConstructor(JsonValue.class);
            constructor.setAccessible(true);
            cachedConstructors.put(clazz, constructor);
        }
        return constructor;
    }

    public JsonValue get(String attribute) {
        JsonValue result = null;
        if (isJsonObject()) {
            JsonObject jsonObject = getJsonObject();
            if (jsonObject != null) {
                result = jsonObject.get(attribute, (JsonValue) null);
            }
        }
        return result;
    }


    public JsonValue get(String attribute, Integer defaultIfNull) {
        return get(attribute, of(defaultIfNull));
    }

    public JsonValue get(String attribute, Long defaultIfNull) {
        return get(attribute, of(defaultIfNull));
    }

    public JsonValue get(String attribute, JsonObject defaultIfNull) {
        return get(attribute, defaultIfNull.jsonValue());
    }

    public JsonValue get(String attribute, JsonArray defaultIfNull) {
        return get(attribute, defaultIfNull.jsonValue());
    }

    public JsonValue get(String attribute, String defaultIfNull) {
        return get(attribute, of(defaultIfNull));
    }

    public JsonValue get(String attribute, Boolean defaultIfNull) {
        return get(attribute, of(defaultIfNull));
    }

    public JsonValue get(String attribute, JsonValue defaultIfNull) {
        JsonValue result = NULL;

        if (is(JsonObject.class)) {
            JsonObject jsonObject = getValue(JsonObject.class);
            if (jsonObject != null) {
                result = jsonObject.get(attribute, defaultIfNull);
            }
        } else {
            result = defaultIfNull;
        }
        return result;
    }

    public JsonValue get(Integer index) {
        return get(index, null);
    }

    public JsonValue get(Integer index, JsonValue defaultIfOutOfRange) {
        JsonValue result = defaultIfOutOfRange;

        try {
            JsonArray jsonArray = getValue(JsonArray.class);
            if (jsonArray != null) {
                result = jsonArray.get(index);
            }
        } catch (IndexOutOfBoundsException e) {
            // Do nothing - result = defaultIfOutOfRange;
        }

        return result;
    }

    public JsonValue get(JsonValue jsonValue) {
        JsonValue result = null;
        if (isJsonObject()) {
            result = get(jsonValue.getString());
        } else if (isJsonArray()) {
            result = get(jsonValue.getJsonNumber().intValue());
        }
        return result;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public String toString() {
        if (value == null) {
            return "null";
        } else if (is(String.class)) {
            return escapeString();
        } else {
            return getValue().toString();
        }
    }

    private String escapeString() {
        return escapeString(getValue(String.class));
    }

    private String escapeString(String toBeEscaped) {
        String serialized = JsonSerializer.serialize(toBeEscaped);
        return "\"" + serialized + "\"";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        JsonValue jsonValue = (JsonValue) o;

        if (value != null ? !value.equals(jsonValue.value) : jsonValue.value != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }


    public JsonValue put(JsonArray path, String value) {
        return put(path, of(value));
    }

    public JsonValue put(JsonArray path, JsonObject jsonObject) {
        return put(path, jsonObject.jsonValue());
    }


    public JsonValue remove(JsonPointer pointer) {
        return remove(pointer.asArray()); // TODO: This must be replaced by removeByPointer (now it fails in arrays!)
    }

    public JsonValue put(JsonPointer pointer, JsonValue jsonValue) {
        return putByPointer(pointer.asArray(), 0, jsonValue);
    }

    private JsonValue putByPointer(JsonArray pointer, int pointerIndex, JsonValue jsonValue) throws PointerMismatchJsonValueException {


        if (pointer.size() == pointerIndex) {
            return jsonValue;
        }

        JsonValue head = pointer.get(pointerIndex);
        if (isJsonArray()) {

            if (head.isString()) {
                String headString = head.getString();

                if ("-".equals(headString)) {
                    return getJsonArray().add(putByPointer(pointer, pointerIndex + 1, jsonValue)).jsonValue();
                } else if (!headString.startsWith("0") || "0".equals(headString)) { // leading zero is not allowed by JSON pointer spec RFC6901
                    int index;
                    try {
                        index = Integer.parseInt(headString);
                    } catch (NumberFormatException e) {
                        throw new PointerMismatchJsonValueException(JsonPointer.of(pointer.subArray(0, pointerIndex)), this.type());
                    }
                    return getJsonArray().set(index, getJsonArray().get(index).putByPointer(pointer, pointerIndex + 1, jsonValue)).jsonValue();
                }
            } else {
                int index = head.getJsonNumber().intValue();
                getJsonArray().set(index, getJsonArray().get(index).putByPointer(pointer, pointerIndex + 1, jsonValue)).jsonValue();
            }
            throw new PointerMismatchJsonValueException(JsonPointer.of(pointer.subArray(0, pointerIndex)), this.type());
        } else if (isJsonObject()) {
            String headString = head.getString();
            JsonValue currentValueOfHead = get(headString, JsonObject.EMPTY);
            return getJsonObject()
                    .put(headString, currentValueOfHead.putByPointer(pointer, pointerIndex + 1, jsonValue))
                    .jsonValue();
        } else {
            throw new PointerMismatchJsonValueException(JsonPointer.of(pointer.subArray(0, pointerIndex)), this.type());
        }
    }


    JsonValue removeByPointer(JsonPointer pointer){
        JsonArray jsonArray = pointer.asArray();
        if (jsonArray.size() == 0){
            return this;
        } else {
            return removeByPointer(jsonArray, 0);
        }
    }

    private JsonValue removeByPointer(JsonArray pointer, int pointerIndex) throws PointerMismatchJsonValueException {

        String head = pointer.get(pointerIndex).getString();
        if (pointerIndex + 1 == pointer.size()){
            if (isJsonObject()) {
                return getJsonObject().remove(head).jsonValue();
            }else {
                throw new PointerMismatchJsonValueException(JsonPointer.of(pointer.subArray(0, pointerIndex)), type());
            }
        }

        if (isJsonArray()) {
            if (!head.startsWith("0") || head.length() == 1) { // leading zero is not allowed by JSON pointer spec RFC6901
                int index;
                try {
                    index = Integer.parseInt(head);
                } catch (NumberFormatException e) {
                    throw new PointerMismatchJsonValueException(JsonPointer.of(pointer.subArray(0, pointerIndex)), this.type());
                }
                return getJsonArray().set(index, getJsonArray().get(index).removeByPointer(pointer, pointerIndex + 1)).jsonValue();
            }
            throw new PointerMismatchJsonValueException(JsonPointer.of(pointer.subArray(0, pointerIndex)), this.type());
        } else if (isJsonObject()) {
            JsonValue currentValueOfHead = get(head, JsonObject.EMPTY);
            return getJsonObject()
                    .put(head, currentValueOfHead.removeByPointer(pointer, pointerIndex + 1))
                    .jsonValue();
        } else {
            throw new PointerMismatchJsonValueException(JsonPointer.of(pointer.subArray(0, pointerIndex)), this.type());
        }
    }


    public JsonValue put(JsonArray path, JsonValue jsonValue) {

        JsonValue result;
        if (!path.isEmpty()) {
            if (path.head().is(String.class)) {
                JsonObject valueOfHead = isJsonObject() ? getJsonObject() : JsonObject.EMPTY;
                result = valueOfHead
                        .put(path, jsonValue).jsonValue();
            } else if (path.head().isInteger()) {
                JsonArray valueOfHead = isJsonArray() ? getJsonArray() : JsonArray.EMPTY;
                result = valueOfHead.put(path, jsonValue).jsonValue();
            } else {
                throw new RuntimeException("Only String and Integer is supported");
            }
        } else {
            result = jsonValue;
        }
        return result;
    }

    public JsonValue get(JsonArray path) {
        return get(path, null);
    }

    public JsonValue get(JsonPointer jsonPointer) throws PointerMismatchJsonValueException {
        return get(jsonPointer, null);
    }

    public JsonValue get(JsonPointer jsonPointer, JsonValue defaultIfNull) throws PointerMismatchJsonValueException {

        JsonArray pointer = jsonPointer.asArray();

        JsonValue current = this;

        for (int i = 0; i < pointer.size(); i++) {
            String head = pointer.get(i).getString();
            if (current.isJsonObject()) {
                current = current.get(head);
            } else if (current.isJsonArray() && (head.length() == 1 || !head.startsWith("0"))) {
                int index;
                try {
                    index = Integer.parseInt(head);
                    current = current.get(index);
                } catch (NumberFormatException e) {
                    throw new PointerMismatchJsonValueException(pointer.subArray(0, i).as(JsonPointer.class), current.type());
                }
            } else {
                throw new PointerMismatchJsonValueException(pointer.subArray(0, i).as(JsonPointer.class), current.type());
            }

            if (current == null) {
                return defaultIfNull;
            }
        }


        return current;
    }


    public JsonValue get(JsonArray path, JsonValue defaultValue) {
        JsonValue result = this;

        for (JsonValue fieldName : path.asList()) {

            if (fieldName.is(String.class)) {
                result = result.get(fieldName.getString());
                if (result == null) {
                    result = defaultValue;
                    break;
                }
            } else if (fieldName.isInteger()) {
                result = result.isJsonArray() ? result.get(fieldName.getJsonNumber().intValue()) : result.get(fieldName.getJsonNumber().toIntString());
                if (result == null) {
                    result = defaultValue;
                    break;
                }
            } else {
                throw new RuntimeException("Only String and Integer is supported in path but value of index is: " + fieldName.asJson());
            }
        }
        return result;
    }


    JsonValue get(JsonArray path, Object defaultValue) {

        return get(path, JsonValue.of(defaultValue));
    }

    public JsonArray getPaths() {
        return getPathsAndValue(false);
    }

    public JsonArray getPaths(boolean includeValue) {
        if (isLeaf()) {
            return includeValue
                    ? JsonArray.of(JsonArray.of(this))
                    : JsonArray.EMPTY_IN_EMPTY;
        }
        JsonArrayBuilder builder = JsonArray.EMPTY.builder();

        if (isJsonObject()) {
            JsonObject jsonObject = getJsonObject();
            Iterator<JsonValue> jsonValueIterator = jsonObject.values().iterator();
            for (JsonValue key : jsonObject.keys()) {
                builder.addAll(jsonValueIterator.next().getPaths(includeValue).addFirstToEach(key));
            }
        } else {
            if (isJsonArray()) {
                int i = 0;
                for (JsonValue value : getJsonArray()) {
                    builder.addAll(value.getPaths(includeValue).addFirstToEach(i++));
                }
            }
        }
        return builder.build();
    }

    @Deprecated
    public JsonArray getPathsAndValue(boolean includeValueLastInPath) { // [["person", "parents", 0, "name"],["person", "parents", 1, "name"]]
        JsonArray paths = JsonArray.EMPTY;
        if (is(JsonObject.class) && !getValue(JsonObject.class).isEmpty()) {

            JsonArray keys = getValue(JsonObject.class).keys();
            for (int i = 0; i < keys.size(); i++) {
                paths = paths.add(JsonArray.EMPTY.add(keys.get(i)));
            }

            JsonArray values = getValue(JsonObject.class).values();
            for (int i = 0; i < values.size(); i++) {
                JsonValue indexedValue = values.get(i);
                if (indexedValue == null) {
                    continue;
                } else if (JsonValue.NULL.equals(indexedValue)) {
                    if (includeValueLastInPath) {
                        paths = paths.set(i, paths.get(i).getValue(JsonArray.class).add(JsonValue.NULL));
                    }
                } else {
                    JsonArray pathsOfValue = values.get(i).getPathsAndValue(includeValueLastInPath);

                    if (pathsOfValue != JsonArray.EMPTY) {
                        paths = paths.set(i, JsonValue.NULL);
                    }

                    for (int j = 0; j < pathsOfValue.size(); j++) {
                        paths = paths.add(pathsOfValue.get(j).getValue(JsonArray.class).add(0, keys.get(i)));//.add(0, keys.get(i));
                    }
                }
            }
        } else if (is(JsonArray.class) && !getValue(JsonArray.class).isEmpty()) {

            JsonArray array = getValue(JsonArray.class);
            for (int i = 0; i < array.size(); i++) {
                paths = paths.add(JsonArray.EMPTY.add(i));
            }

            for (int index = 0; index < array.size(); index++) {
                JsonArray pathsOfIndex = array.get(index).getPathsAndValue(includeValueLastInPath);

                if (pathsOfIndex != JsonArray.EMPTY) {
                    paths = paths.set(index, JsonValue.NULL); // This path will be part of longer paths - mark it for deletion
                }

                for (int j = 0; j < pathsOfIndex.size(); j++) {
                    paths = paths.add(pathsOfIndex.get(j).getValue(JsonArray.class).add(0, index));//.add(0, keys.get(i));
                }
            }
        } else {
            if (includeValueLastInPath) {
                paths = paths.add(JsonArray.of(this));
            }
        }

        JsonArray restOfPaths = paths;
        paths = JsonArray.EMPTY;
        while (!restOfPaths.isEmpty()) {
            if (!JsonValue.NULL.equals(restOfPaths.head())) {
                paths = paths.add(restOfPaths.head());
            }
            restOfPaths = restOfPaths.tail();
        }
        return paths;
    }

    JsonValue replaceInKeys(String target, String replacement, boolean recursive) {
        JsonValue replaced = this;

        if (is(JsonObject.class)) {
            JsonObject jsonObject = getValue(JsonObject.class);
            if (!jsonObject.isEmpty()) {
                String key = jsonObject.keys().head().getValue(String.class);
                JsonValue value = jsonObject.values().head();
                if (recursive) {
                    value = value.replaceInKeys(target, replacement, recursive);
                }

                String translatedKey = key.replace(target, replacement);
                replaced = jsonObject
                        .remove(key).jsonValue()
                        .replaceInKeys(target, replacement, recursive).getValue(JsonObject.class)
                        .put(translatedKey, value).jsonValue();
            }
        } else if (recursive) {
            if (is(JsonArray.class)) {
                JsonArray jsonArray = getValue(JsonArray.class);
                if (!jsonArray.isEmpty()) {
                    JsonValue translatedHeadValue = jsonArray.head().replaceInKeys(target, replacement, recursive);
                    replaced = jsonArray.tail().jsonValue().replaceInKeys(target, replacement, recursive).getValue(JsonArray.class).addFirst(translatedHeadValue).jsonValue();
                }
            }
        }
        return replaced;
    }

    @Override
    public int compareTo(JsonValue jsonValue) {

        int result;

        if (is(JsonObject.class)) {
            result = getJsonObject().compareTo(jsonValue.getValue());
        } else {
            try {
                Class classOfMe = getValue().getClass();
                if (classOfMe != jsonValue.getValue().getClass()) {
                    result = classOfMe.getName().compareTo(jsonValue.getValue().getClass().getName());
                } else {
                    result = ((Comparable<Object>) getValue()).compareTo(jsonValue.getValue());
                }
            } catch (ClassCastException e) {
                logger.error(e.getMessage(), e);
                throw new RuntimeException(e);
            }
        }
        return result;
    }

    public String asJsonSorted() {

        if (asJsonSorted == null) {
            if (is(String.class)) {
                asJsonSorted = escapeString();
            } else if (is(JsonObject.class)) {
                asJsonSorted = getValue(JsonObject.class).asJsonSorted();
            } else if (is(JsonArray.class)) {
                asJsonSorted = getValue(JsonArray.class).asJsonSorted();
            } else {
                asJsonSorted = getValue() == null ? "null" : getValue().toString();
            }
        }
        return asJsonSorted;
    }

    public JsonValue remove(JsonArray path) {
        JsonValue result;
        if (is(JsonObject.class)) {
            result = getValue(JsonObject.class).remove(path).jsonValue();
        } else if (is(JsonArray.class)) {
            JsonArray array = getJsonArray();
            int index = path.get(0).getJsonNumber().intValue();
            JsonValue valAtIndex = array.get(index);
            result = array.remove(index).put(index, valAtIndex.remove(path.tail())).jsonValue();
        } else {
            throw new RuntimeException("Only possible to remove path from JsonArray or JsonObject");
        }
        return result;
    }

    public JsonValue replaceAll(String regex, String replacement) {
        JsonValue result = this;
        if (is(String.class)) {
            result = of(getValue(String.class).replaceAll(regex, replacement));
        } else if (is(JsonObject.class)) {
            JsonObject thisJsonObject = getValue(JsonObject.class);
            if (!thisJsonObject.isEmpty()) {
                String key = thisJsonObject.keys().head().getValue(String.class);
                result = thisJsonObject.remove(key).jsonValue().replaceAll(regex, replacement).getValue(JsonObject.class).put(key.replaceAll(regex, replacement), thisJsonObject.get(key).replaceAll(regex, replacement)).jsonValue();
            }
        } else if (is(JsonArray.class)) {
            JsonArray thisJsonArray = getValue(JsonArray.class);

            if (!thisJsonArray.isEmpty()) {
                result = thisJsonArray.tail().jsonValue().replaceAll(regex, replacement).getValue(JsonArray.class).addFirst(thisJsonArray.head().replaceAll(regex, replacement)).jsonValue();
            }
        }
        return result;
    }

    public boolean isNull() {
        return value == null;
    }

    public static JsonValue of(Object object) {
        JsonValue result;

        result = cache.get(object);

        if (result == null) {
            if (object == null) {
                throw new NullPointerException("object must not be null. Use JsonValue.NULL instead.");
            } else if (object.getClass() == JsonValue.class) {
                result = (JsonValue) object;
            } else if (object.getClass() == String.class) {
                result = new JsonValue(object);
            } else if (object.getClass() == Boolean.class) {
                result = new JsonValue(object);
            } else if (object instanceof Number) {
                result = JsonNumber.of((Number) object).jsonValue();
            } else if (object instanceof JsonValueSupport) {
                throw new RuntimeException("JsonValueSupport is not supported and should not be supported");
            } else {
                throw new RuntimeException("Not supported");
            }
            cache.put(result);
        }
        return result;
    }

    public static JsonValue of(String value) {
        return of((Object) value);
    }

    public static JsonValue of(Boolean b) {
        return of((Object) b);
    }

    public static JsonValue of(JsonValue j) {
        return j;
    }

    public static JsonValue of(JsonObject jsonObject) {
        if (jsonObject == null) {
            return JsonValue.NULL;
        } else {
            return jsonObject.jsonValue();
        }
    }

    public static JsonValue of(JsonArray jsonArray) {
        if (jsonArray == null) {
            return JsonValue.NULL;
        } else {
            return jsonArray.jsonValue();
        }
    }

    public static JsonValue of(Long value) {
        return of((Object) value);
    }

    public static JsonValue of(BigDecimal value) {
        return of((Object) value);
    }

    public static JsonValue of(Integer value) {
        return of((Object) value);
    }

    public boolean isInteger() {
        if (isJsonNumber()) {
            Number wrapped = getJsonNumber().getWrappedNumber();
            if (wrapped.getClass() == Integer.class) {
                return true;
            } else if (wrapped.getClass() == Long.class) {
                return wrapped.longValue() == (long) wrapped.intValue();
            }
        }
        return false;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public String asString() {
        return (String) getValue();
    }

    public String getString() {
        return (String) getValue();
    }

    public JsonObject getJsonObject() {
        return (JsonObject) getValue();
    }

    public boolean isJsonObject() {
        return value instanceof JsonObject;
    }

    public JsonArray getJsonArray() {
        return (JsonArray) getValue();
    }

    public boolean isJsonArray() {
        return value instanceof JsonArray;
    }

    public Long getLong() {
        return value == null ? null : getJsonNumber().longValue();
    }

    public Integer getInteger() {
        return value == null ? null : getJsonNumber().intValue();
    }

    @Deprecated
    public Long asLong() {
        return getLong();
    }

    public String asJson() {
        return toString();
    }

    static long getInstances() {
        return instances;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public BigDecimal asBigDecimal() {
        return getBigDecimal();
    }

    public BigDecimal getBigDecimal() {
        return value == null ? null : getJsonNumber().getBigDecimal();
    }

    public boolean isBoolean() {
        return is(Boolean.class);
    }

    public Boolean getBoolean() {
        return getValue(Boolean.class);
    }


    public boolean isString() {
        return is(String.class);
    }

    @Deprecated
    public boolean isBigDecimal() {
        try {
            getJsonNumber().getBigDecimal();
            return true;
        } catch (ClassCastException classCastException) {
            return false;
        }
    }

    public boolean isLeaf() {
        return !(isJsonObject() && !getJsonObject().isEmpty()) && !(is(JsonArray.class) && !getJsonArray().isEmpty());
    }

    public JsonType type() {
        return isString() ? JsonType.String :
                isJsonObject() ? JsonType.Object :
                        isJsonArray() ? JsonType.Array :
                                isBoolean() ? JsonType.Boolean :
                                        isNull() ? JsonType.Null :
                                                JsonType.Number;
    }

    public boolean isJsonNumber() {
        return value != null && JsonNumber.class == value.getClass();
    }

    public JsonNumber getJsonNumber() {
        return (JsonNumber) value;
    }

}
