package com.zuunr.json.impl;

/**
 * @author Niklas Eldberger
 */
public class NullableCachedValue<T> {
    public final T value;

    public NullableCachedValue(T value) {
        this.value = value;
    }
}