# Immutable JSON representation and JSON Schema support in Java #

![Apache License](https://img.shields.io/static/v1.svg?label=License&message=Apache-2.0&color=%230af)
[![Maven Central](https://img.shields.io/maven-central/v/com.zuunr/json.svg?label=Maven%20Central&color=%230af)](https://search.maven.org/search?q=com.zuunr json)
[![Snyk](https://snyk-widget.herokuapp.com/badge/mvn/com.zuunr/json/badge.svg)](https://snyk.io/vuln/maven:com.zuunr%3Ajson)

JSON

    {
        "name": "Peter Andersson",
        "children": [
            {"name": "Laura Andersson"},
            {"name": "Phil Andersson"}
        ],
        "contact": {
            "email": "peter@gmail.com",
            "twitter": "@peterandersson"
        }
    }

Java (programmatically)

    JsonObject.EMPTY
        .put("name", "Peter Andersson")
        .put("children", JsonArray.EMPTY
            .add(JsonObject.EMPTY
                .put("name", "Laura Andersson"))
            .add(JsonObject.EMPTY
                .put("name", "Phil Andersson")))
        .put("contact", JsonObject.EMPTY
                .put("email", "peter@gmail.com")
                .put("twitter", "@peterandersson"));



JSON to Java

Use the <code>JsonValueFactory</code> with support for <code>InputStream</code> or <code>String</code>

_Example with String:_

    JsonObject jsonObject = JsonValueFactory
            .create("{\"name\": \"Peter\"}")  // creates JsonValue
            .getJsonObject();                 // gets the JsonObject which is wrapped by the JsonValue  

...is the same as

    JsonObject jsonObject = JsonObject.EMPTY.put("name", "Peter");

## Core classes

The main classes are representing the structures specified in ECMA-404 described at [https://json.org]

### JsonValue

JsonValue is the base type. All other types are wrapped by a JsonValue. The wrapped object can be reached by calling getValue

A JsonValue may be created as

    JsonValue jsonValue = JsonValue.of("Peter");    // "Peter"

and the String "Peter" wrapped by jsonValue can be fetched by calling the getString() method

    String name = jsonValue.getString();            // Peter

### JsonObject

    JsonObject.EMPTY.put("name", "Peter");          // {"name": "Peter"}

### JsonArray

    JsonArray.EMPTY.add("Laura").add("Phil");           // ["Laura", "Phil"]

### Together

    JsonObject.EMPTY.put("name", "Peter").put("friends", JsonArray.EMPTY.add("Laura").add("Phil"));    // {"name": "Peter", "friends": ["Laura", "Phil"]}


### String, Number, true, false and null
    
The immutable Java types String, Boolean, Integer, Long, Short and BigDecimal are supported

    JsonArray.EMPTY.add("abc")            // ["abc"]
    JsonArray.EMPTY.add(-1L),             // [-1] 
    JsonArray.EMPTY.add(30),              // [30]
    JsonArray.EMPTY.add(true),            // [false]    
    JsonArray.EMPTY.add(false),           // [true]
    JsonArray.EMPTY.add(JsonValue.NULL)   // [null]
    JsonArray.EMPTY.add(JsonObject.EMPTY) // [{}]

# JSON Schema validation and filtering

Example of successful validation:

    JsonSchemaValidator jsonSchemaValidator = new JsonSchemaValidator();

    JsonValue jsonSchema = JsonValue.of(
            JsonObject.EMPTY
                    .put("properties", JsonObject.EMPTY
                            .put("firstName", true))
                    .put("additionalProperties", false)
    );

    JsonValue validInstance = JsonObject.EMPTY
            .put("firstName", "Peter")
            .jsonValue();

    JsonObject validationResult = jsonSchemaValidator
            .validate(
                    validInstance,            // data to be validated
                    jsonSchema,               // schema use when validating
                    OutputStructure.DETAILED  // how to present result in case of errors
            );

    System.out.println(validationResult.asJson());

...results in:

    {
      "valid": true
    }

Example with error:

    JsonSchemaValidator jsonSchemaValidator = new JsonSchemaValidator();

    JsonValue jsonSchema = 
            JsonObject.EMPTY
                    .put("properties", JsonObject.EMPTY
                            .put("firstName", true))
                    .put("additionalProperties", false))
                    .jsonValue();             // gets the JsonValue instance that wraps the JsonObject

    JsonValue invalidInstance = JsonObject.EMPTY
            .put("firstName", "Peter")
            .put("age", 35)
            .jsonValue();


    JsonObject validationResult = jsonSchemaValidator
            .validate(
                    invalidInstance,          // data to be validated
                    jsonSchema,               // schema use when validating
                    OutputStructure.DETAILED  // how to present result in case of errors
            );

    System.out.println(validationResult.asJson());    

...results in:

    {
      "instanceLocation": "",                 // JSON Pointer of empty string means root instance
      "errors": [
        {
          "instanceLocation": "/age",                  // the field named "age" in the root instance
          "keywordLocation": "/additionalProperties",  // keyword in the root schema which made validation fail
          "valid": false,                     
          "keywordValue": false                        // any additionalProperty will fail  
        }
      ],
      "keywordLocation": "",                  // JSON Pointer to the root JSON Schema
      "valid": false
    }

Example of filtering:

    JsonSchemaValidator jsonSchemaValidator = new JsonSchemaValidator();

    JsonValue jsonSchema = JsonValue.of(
            JsonObject.EMPTY
                    .put("properties", JsonObject.EMPTY
                            .put("firstName", true))
                    .put("additionalProperties", false)
    );

    JsonValue invalidInstance = JsonObject.EMPTY
            .put("firstName", "Peter")
            .put("age", 35)
            .jsonValue();


    JsonValue filtered = jsonSchemaValidator
            .filter(
                    invalidInstance,         // data to be filtered
                    jsonSchema               // schema use when validating
            );

    System.out.println(filtered.asJson());

...results in:

    {
      "firstName": "Peter"  // field "age" is removed as it was not allowed according to the JSON Schema
    }

## Dynamic validation rules

The implementation supports dynamic validation (AJV style) and filtering by using the $data property. 

The $data property is a special feature that allows schema developers to create more dynamic and flexible validation rules. It enables the referencing of data from the instance being validated within the schema itself. This can be particularly useful for scenarios where the schema's validation logic depends on other values within the same data object.

Example with $data:

    JsonValue jsonSchema = JsonValueFactory.create("""
            {
                "properties": {
                    "value": {
                        "enum": {"$data": "/dynamicEnum"}
                    }
                }
            }
            """);

    JsonValue validModel = JsonValueFactory.create("""
            {
                "dynamicEnum": ["A","B"],
                "value": "A"
            }
            """);

    JsonValue invalidModel = JsonValueFactory.create("""
            {
                "dynamicEnum": ["B", "C"],
                "value": "A"
            }
            """);

    JsonObject validResult = validator.validate(validModel, jsonSchema, OutputStructure.BASIC);      // {"valid": true}
        
    JsonObject invalidResult = validator.validate(invalidModel, jsonSchema, OutputStructure.BASIC);  // {"valid": false}

